﻿namespace tmkhogares.Constantes
{
    public static class Configuraciones
    {
        public const string MENSAJE_ERROR_FORMATO_ASIGNACION = "Error Formato, Por favor verique el formato de cargue, se ha generado un archivo excel indicando los errores en el formato.";
        public const string MENSAJE_ERROR_COLUMNAS           = "Por favor verifiqué el nombre de las columnas del archivo excel.";
        public const string MENSAJE_ERROR_FORMATO_INCORRECTO = "Error Formato, Por favor verique que el formato sea el adecuado.";
    }

    public static class Campanas
    {
        public const int CLARO_COLOMBIA_TMK = 1;
        public const int CLARO_TYT = 3;
        public const int CLARO_COLOMBIA_MIGRACIONES = 5;
        public const int CLARO_COLOMBIA_PORTA_POS = 6;
        public const int CLARO_COLOMBIA_PERSONAS_INBOUND = 7;
        public const int CLARO_COLOMBIA_PERSONAS_OUTBOUND = 8;
        public const int CLARO_COLOMBIA_HOGARES_DIGITAL = 9;
        public const int CLARO_COLOMBIA_HOGARES_INBOUND = 10;
        public const int CLARO_COLOMBIA_HOGARES_OUTBOUND = 11;
        public const int CLARO_CGV = 12;
        public const int CLARO_DIGITAL_PERSONAS = 14;
        public const int CELULA_DE_VENTAS = 28;
        public const int SEGUNDO_ANILLO = 29;
        public const int TERCER_ANILLO_MOVIL = 30;
        public const int TERCER_ANILLO_HOGAR = 35;
        public const int LEALTAD = 36;
        public const int TRAFICO_INBOUND = 37;

    }

    public static class Rol
    {
        public const string ASESOR        = "4450491C-32E6-41AE-B301-CB22DFC8BBD9";
        public const string ADMINISTRADOR = "13FA2EA3-C034-4B0A-8F66-F2AB10DA8944";
        public const string TEAM_LEADER   = "B0A06B9B-FB6E-4195-BC1C-AD84AD9A194D";
        public const string CALIDAD       = "C2FA35E2-1FAF-4F68-945D-9F5262484ED1";
        public const string REPORTING     = "EC52E1C8-D717-4762-B40E-7BEC049EDC45";
        public const string GERENTE       = "0A2155D6-3E87-485E-8233-2DB2EA0DEDBE";
    }

    public static class TipoAtributo
    {      
        public const string CRITICO      = "FD0EE06F-2F62-44A1-B82B-FA5F44A6C861";
        public const string GENERICO     = "0FC2F642-6FCE-4AA5-A79B-6D16209D71A0";
        public const string PROCESO      = "41A93AD2-F3F5-4261-AF65-B250A4A89973";
        public const string GENERAL      = "B2850A70-0D93-4C98-88C9-DF7DFB176FDD";
       
    }

    public static class Letra
    {
        public const int LETRA_G  = 1;
        public const int LETRA_A1 = 2;
        public const int LETRA_N  = 3;
        public const int LETRA_A2 = 4;

    }

    public static class TipoAuditoria
    {
        public const string FIDELIZACION = "01A608E9-63A3-4EEA-91BB-62A3053E875E";
        public const string TMK_TYT      = "98D0356A-52FE-4FAB-B8A8-C285A534532B";     

    }


    public static class Calificacion
    {
        public const string Cumple   = "e05f0ec2-bf14-4866-b0d2-62551fdbe634";
        public const string NoCumple = "0302d06b-63b4-4335-b215-9a581488bb5a";
        public const string NoAplica = "dc803b8a-fd8d-43be-afc5-5ac30d5aa1e0";     
        public const string Cinco    = "F834FE77-633B-4BF4-950E-BB12EA12BF77";     
        public const string Cuatro   = "F00A5CAF-DC96-4CF4-A7B2-ADC232CC89A4";     
        public const string Tres     = "11698050-D6CB-4328-A2ED-52FA2A17EF26";     
        public const string Dos      = "462ACFB9-9FAD-4B2E-B82B-C7FCFD532C52";     
        public const string Uno      = "AB5221B2-826C-4069-8283-599F07E88491";     

    }

    public static class FormulariosApp
    {
        public const string CLARO_COLOMBIA_TMK = "0A82B341-D809-444A-8CBA-A87DEC267C4E";
        public const string CLARO_TYT          = "3403BC59-1336-4F0C-B977-770F9A8CB27A";
        public const string CLARO_GUATEMALA    = "CBC897BD-2ED5-4469-86E3-4C0025E5F822";
        //CGV
        public const string ESPECIALISTAS      = "86D3A9E5-B7D6-4D20-8F60-06BDAEAE50F8";
        public const string DTH                = "24869366-6468-4223-915A-0A9C49735B20";
        public const string SOPORTE            = "6B0FF304-856C-4611-9F88-7EAB755794DC";
        public const string DIRECTOS           = "0D8B4298-5482-4FC3-A324-E086401F1764";
        public const string EVIDENTES          = "BFE47E91-4AEC-4028-B452-FCA8EA11808F";
    }
}