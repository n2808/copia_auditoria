﻿//creacion cliente crm
$("body").on("click", "#guardarCliente", function () {

    var data = {
        //datos representante
        REPRESENTANTE_NOMBRE: $("#REPRESENTANTE_NOMBRE").val(),
        REPRESENTANTE_TIPO_IDENT: $('#REPRESENTANTE_TIPO_IDENT option:selected').val(),
        REPRESENTANTE_NUMERO_IDENTIFICACION: $("#REPRESENTANTE_NUMERO_IDENTIFICACION").val(),
        REPRESENTANTE_DIRECCION: $("#REPRESENTANTE_DIRECCION").val(),
        REPRESENTANTE_CIUDAD: $("#REPRESENTANTE_CIUDAD").val(),
        REPRESENTANTE_ESTRATO: $("#REPRESENTANTE_ESTRATO option:selected").val(),
        REPRESENTANTE_GENERO: $('#REPRESENTANTE_GENERO option:selected').val(),
        REPRESENTANTE_TELEFONO_1: $("#REPRESENTANTE_TELEFONO_1").val(),
        REPRESENTANTE_TELEFONO_2: $("#REPRESENTANTE_TELEFONO_2").val(),
        REPRESENTANTE_TELEFONO_3: $("#REPRESENTANTE_TELEFONO_3").val(),
        REPRESENTANTE_FECHA_NACIMIENTO: $("#REPRESENTANTE_FECHA_NACIMIENTO").val(),

        //datos empresa
        IdCity: $("#EMPRESA_CIUDAD option:selected").val(),

        EMPRESA_CODIGO_CLIENTE: $("#EMPRESA_CODIGO_CLIENTE").val(),
        EMPRESA_NOMBRE_CLIENTE: $("#EMPRESA_NOMBRE_CLIENTE").val(),
        EMPRESA_TIPO_IDENT: $('#EMPRESA_TIPO_IDENT option:selected').val(),
        EMPRESA_NUMERO_IDENTIFICACION: $('#EMPRESA_NUMERO_IDENTIFICACION').val(),
        EMPRESA_DIRECCION: $("#EMPRESA_DIRECCION").val(),
        EMPRESA_CIUDAD: $('#EMPRESA_CIUDAD option:selected').text(),
        EMPRESA_ESTRATO: $("#EMPRESA_ESTRATO option:selected").val(),
        FACTURACION_ACTUAL: $("#FACTURACION_ACTUAL").val(),
        TELEFONO_1: $("#TELEFONO_1").val(),
        TELEFONO_2: $("#TELEFONO_2").val(),
        TELEFONO_3: $("#TELEFONO_3").val(),
        TELEFONO_4: $("#TELEFONO_4").val(),
        TELEFONO_5: $("#TELEFONO_5").val(),
        EMAIL: $("#EMAIL").val()
    }
    $.post("/Clientes/Create", data)
        .done((Result) => {
            if (Result.status === 201) {

                swal({
                    position: 'top-right',
                    type: 'success',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                $("#formularioCreacionCliente").trigger("reset");

                window.location.href = "/Clientes/Edit/" + Result.clienteId;
            } else {
                swal({
                    position: 'top-right',
                    type: 'error',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1800
                });
            }

        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })
})

//actualizacion cliente crm
$("body").on("click", "#actualizarCliente", function () {

    var data = {

        //id

        Id: $("#IdCliente").val(),
        IdCity: $("#EMPRESA_CIUDAD option:selected").val(),
        //datos representante
        REPRESENTANTE_NOMBRE: $("#Cliente_REPRESENTANTE_NOMBRE").val(),
        REPRESENTANTE_TIPO_IDENT: $('#REPRESENTANTE_TIPO_IDENT option:selected').val(),
        REPRESENTANTE_NUMERO_IDENTIFICACION: $("#Cliente_REPRESENTANTE_NUMERO_IDENTIFICACION").val(),
        REPRESENTANTE_DIRECCION: $("#Cliente_REPRESENTANTE_DIRECCION").val(),
        REPRESENTANTE_CIUDAD: $("#Cliente_REPRESENTANTE_CIUDAD").val(),
        REPRESENTANTE_ESTRATO: $("#REPRESENTANTE_ESTRATO option:selected").val(),
        REPRESENTANTE_GENERO: $('#REPRESENTANTE_GENERO option:selected').val(),
        REPRESENTANTE_TELEFONO_1: $("#Cliente_REPRESENTANTE_TELEFONO_1").val(),
        REPRESENTANTE_TELEFONO_2: $("#Cliente_REPRESENTANTE_TELEFONO_2").val(),
        REPRESENTANTE_TELEFONO_3: $("#Cliente_REPRESENTANTE_TELEFONO_3").val(),
        REPRESENTANTE_FECHA_NACIMIENTO: $("#Cliente_REPRESENTANTE_FECHA_NACIMIENTO").val(),

        //datos empresa
        EMPRESA_CODIGO_CLIENTE: $("#Cliente_EMPRESA_CODIGO_CLIENTE").val(),
        EMPRESA_NOMBRE_CLIENTE: $("#Cliente_EMPRESA_NOMBRE_CLIENTE").val(),
        EMPRESA_TIPO_IDENT: $('#EMPRESA_TIPO_IDENT option:selected').val(),
        EMPRESA_NUMERO_IDENTIFICACION: $('#Cliente_EMPRESA_NUMERO_IDENTIFICACION').val(),
        EMPRESA_DIRECCION: $("#Cliente_EMPRESA_DIRECCION").val(),
        EMPRESA_CIUDAD: $("#Cliente_EMPRESA_CIUDAD option:selected").text(), 
        EMPRESA_ESTRATO: $("#EMPRESA_ESTRATO option:selected").val(),
        FACTURACION_ACTUAL: $("#Cliente_FACTURACION_ACTUAL").val(),
        TELEFONO_1: $("#Cliente_TELEFONO_1").val(),
        TELEFONO_2: $("#Cliente_TELEFONO_2").val(),
        TELEFONO_3: $("#Cliente_TELEFONO_3").val(),
        TELEFONO_4: $("#Cliente_TELEFONO_4").val(),
        TELEFONO_5: $("#Cliente_TELEFONO_5").val(),
        EMAIL: $("#Cliente_EMAIL").val()
    }
    $.post("/Clientes/Edit", data)
        .done((Result) => {

            if (Result.status === 201) {

                swal({
                    position: 'top-right',
                    type: 'success',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                window.location.href = "/Clientes/Edit/" + Result.clienteId;
            } else {
                swal({
                    position: 'top-right',
                    type: 'error',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1800
                });
            }


        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })
})

