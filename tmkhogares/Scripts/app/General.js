﻿DEV_MODE = true;
SHOW_ALERT = false;
let consecutivo_tip = 1;


function _console(msg, title) {
    if (DEV_MODE == false) { return };
    SHOW_ALERT ? alert(msg) :
        console.log('%c|...............................................', 'color: #00a7d0') +
        console.log(msg) +
        console.log('%c|...............................................', 'color: #00a7d0');
}
function _error(msg) {
    if (DEV_MODE == false) { return };
    SHOW_ALERT ? alert(msg) :
        console.log('%c|...............................................', 'color: #df604a') +
        console.log(msg) +
        console.log('%c|...............................................', 'color: #df604a');
}

function normalizeTrueFalse(value) {
    return value == true ? 1 : 0;
}

/*-----------------------------------------------------------------------------------*/
/*---------| CLASE DE EVENTOS GENERALES
/*-----------------------------------------------------------------------------------*/


class General {
    constructor() {
        this.url = {};
        this.url.Gestion = BaseUrl + "Gestions/";
        this.url.Cuentas = BaseUrl + "Back/Cuentas/";
        this.url.back = BaseUrl + "GestionActivacion/";
        this.url.AyudaVenta = BaseUrl + "Ayudas/";
        this.url.Grabaciones = BaseUrl + "Grabaciones/";
        this.data = {};

    }

    toBottom() {
        window.scrollTo(0, document.body.scrollHeight);
    }

    PopulateDropdown(Result, obj) {
        $(obj).append('<option value="0">Sin Seleccionar</option>');
        $.each(Result, function (index, Value) {
            $(obj).append('<option value="'
                + Value.Value + '">' +
                Value.Text + '</option>');
        });
    }

    PopulateDropdownIdName(Result, obj) {
        $(obj).append('<option value="0">Sin Seleccionar</option>');
        $.each(Result, function (index, Value) {
            $(obj).append('<option value="'
                + Value.Id + '">' +
                Value.Name + '</option>');
        });
    }

    JsonDecodeErrors(ErrorObject) {
        if (ErrorObject.Type == 0) {
            return string_mensaje(ErrorObject.Message, "negative");
        }
        else
            return string_mensaje(ErrorObject.Message + "[" + ErrorObject.Name + "]", "negative");
    }

}



/*===========================================================*/
/*==|  CONVIERTE UN NUMERO EN FORMATO DE MONEDA             |*/
/*===========================================================*/


const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0
})

$(document).ready(function () {
    $(".datepicker").datepicker();
    $(".select2").select2();
})



/*===========================================================*/
/*==|  ACTUALIZA EL MODAL CON LA VISTA INDICADA             |*/
/*===========================================================*/

$(document).on('click', '.loadModaldata', function () {
    _console("precionando un botton")
    var route = $(this).data("target");
    _error(route)
    $('#loadmodalData #data').load(route);
    $('#loadmodalData')
        .modal('show');
})

$(document)
    .ready(function () {
        // fix menu when passed
        $('.masthead')
            .visibility({
                once: false,
                onBottomPassed: function () {
                    $('.fixed.menu').transition('fade in');
                },
                onBottomPassedReverse: function () {
                    $('.fixed.menu').transition('fade out');
                }
            });
        $(".ui.sidebar").sidebar()
        $('.ui.dropdown').dropdown();
        $('.tabular.menu .item').tab();


    });