﻿/*-----------------------------------------------------------------------------------*/
/*---------| DEFINICION DE EVENTOS DEL MODULO AGENTE PARA EL AYUDAVENTA.
/*-----------------------------------------------------------------------------------*/


let G_ProductoId = 2;
let G_AdicionalValor = 0;
let G_AdicionalTotal = 0;
let G_ValorTotalCarrito = 0;
/*
 * AÑADE UN SERVICIO ADICIONAL A LA VENTA
 */

$("body").on('click', '.AddAdicional', function () {
    _AyudaVenta.AddToCarAdicional();
})


/*
 * LLAMA AL EVENTO PARA VENDER DTH
 */

$("body").on('click', '.productoDTH', function () {
    _AyudaVenta.VenderDTH();
})

/*
 * LLAMA AL EVENTO PARA VENDER HFC
 */

$("body").on('click', '.productoHFC', function () {
    _AyudaVenta.VenderHFC();
})

/*
 * LLAMA AL EVENTO PARA VENDER FO
 */

$("body").on('click', '.productoFO', function () {
    _AyudaVenta.VenderFO();
})

/*
 * LLAMA AL EVENTO PARA VENDER HFC
 */

$("body").on('click', '.productoCoorp', function () {
    _AyudaVenta.VenderCoorp();
})









/*
 *AÑADIMOS UN SERVICIO PRINCIPAL A LA VENTA.
 */

$("body").on('click', '.AddPrincipal', function () {
    console.log("datos principales")
    _AyudaVenta.AddtoCarPrincipal();
})


$("body").on('click', '.eliminarProducto', function () {
    var p = $(this);
    if (!confirm("¿Esta seguro de remover este producto?")) {
        return false;
    }
    _AyudaVenta.RemoverProducto(p);

})


/*
 * ACTUALIZA LA CANTIDAD A 1
 */

$("body").on('input', '#PRODUCTO_Adicional', function () {

    $("#PRODUCTO_Cantidad").val(1);
    const cantidad = $("#PRODUCTO_Cantidad").val();
    _AyudaVenta.AdicionalesCalcularValor(parseInt(cantidad));

})


/*
 * AÑADE UN SERVICIO ADICIONAL A LA VENTA
 */

$("body").on('input', '#PRODUCTO_Cantidad', function () {

    const cantidad = $(this).val();
    _AyudaVenta.AdicionalesCalcularValor(parseInt(cantidad));
})




/*
 * Actualiza el precio de tarifa plena y promocion.
 */

$("body").on('input', '.ProductoAyudaVenta', function () {
    _AyudaVenta.removePetar();
    _AyudaVenta.updateFiltros();
    _AyudaVenta.campana_verificarAdicionales();
    _AyudaVenta.updatePlanes();
    _AyudaVenta.updateCostos();


    //if (Carro.ValidarVenta()) {
    //    _error("ingresando a vender");
    //    Carro.GuardarVenta();
    //}

})

$("body").on('input', '.ayuda_valorAgregado select', function () {
    _console("evento de items valorAgregado")
    _AyudaVenta.updateCostos();

    //if (Carro.ValidarVenta()) {
    //    _error("ingresando a vender");
    //    Carro.GuardarVenta();
    //}
})



$("body").on('change', '.plan_seleccionado', function (elm) {

    const slm = $(this);


    _AyudaVenta.CamapanaNombrePromocion = slm.parent().find("span.bg-green").text();
    console.log("este es el____________________________", _AyudaVenta.CamapanaNombrePromocion)

    //_error($(slm).parents(".car_item"));
    $("#tv_ayudaventa").val(slm.data("tv"));
    $("#linea_ayudaventa").val(slm.data("linea"));
    $("#ba_ayudaventa").val(slm.data("ba"));
    _AyudaVenta.setValorPromocion(slm.data("oferta"), slm.data("petar"));
    _AyudaVenta.updateFiltros();
    _AyudaVenta.campana_verificarAdicionales();
    _AyudaVenta.getPlanActual();
    _AyudaVenta.updateCostos();


    //if (Carro.ValidarVenta()) {
    //    _error("ingresando a vender");
    //    Carro.GuardarVenta();
    //}


})


/*
 *  SE ACTUALIZA UN NUEVO FILTRO EN EL SISTEMA
 */
$("body").on('input', '#slct_ayuda_estrato,#chk_cliente_movil,#chk_cliente_fijo,#chk_cliente_nuevo,#ayuda_ciudad', function () {
    _AyudaVenta.updateFiltros();
    _AyudaVenta.campana_verificarAdicionales();
    _AyudaVenta.updatePlanes();
})

$("body").on('change', '#ayuda_ciudad', function () {
    _AyudaVenta.updateFiltros();
    _AyudaVenta.updatePlanes();

})

$("body").on('change', '.filtrotvBa', function () {
    _console("filtrando tv y ba");
    _AyudaVenta.filterTvBa($("#ayuda_cam_filtrotv").val(), $("#ayuda_cam_filtroBa").val());

})



/*-----------------------------------------------------------------------------------*/
/*---------| CLASE DE AYUDA VENTAS
/*-----------------------------------------------------------------------------------*/


var _detalleventa = {

    ptar: [],
    productos: [],
    ValorAgregado: [],
    valorTotal: "",
    ValorPromocion: "",
    tv: "",
    ba: "",
    linea: "",
    decosAdicionales: "",
    Multiplay: "",
    ventaNumeroActivar: "",
    Politica: "",
    ServiciosVendidos: "",
    ActualiarTarifa: "",
    RentaMensual: "",
    tipoSolicitud: "",
    ServiciosVendidos: "",
    CantidadServiciios: "",
    Estrato: "",
    DepartamentoId: "",
    CiudadId: ""

};



class AyudaVenta extends General {
    constructor() {
        super();

        /*----------------------------------------------------------------------- 
         * VARIABLES DE SERVICIOS ADICIONALES.
         -----------------------------------------------------------------------*/

        this.TarifaAdicionales = [];
        


        /*----------------------------------------------------------------------- 
         * VARIABLES DE SERVICIOS PRINCIPALES.
         -----------------------------------------------------------------------*/

        // configuraciones seleccionadas,
        this.PlanActual = null;
        this.TarifaPlena;
        this.Ba = null;
        this.tv = null;
        this.linea = null;
        /*----------------------------------------------------------------------- 
         * VARIABLES DE CAMPAÑAS.
         -----------------------------------------------------------------------*/
        this.PtarSeleccionada = null;
        this.DescuentosSecuenciales = null;
        this.campanasEliminadas = [];
        // Lista de campañas y promociones.
        this.Campanas = null;
        this.CiudadesCampanas = null;
        this.PaquetesEspecificos = null;
        this.PlanesEspecificos = null;
        this.CampanasAprovadas = [];

        /*----------------------------------------------------------------------- 
         * VARIABLES GENERALES.
         -----------------------------------------------------------------------*/
        this.ValorTarifaPlena = 0;
        this.ValorPromocion = 0;
        this.VAlorPromocionfinal = 0;
        this.CampanaPague1 = "";
        this.CamapanaNombrePromocion = "";

        //Lista del configurador
        this.ClienteMovil = false;
        this.ClienteFijo = false;
        this.ClienteNuevo = false;
        this.TipoBase = TipoBase;
        this.estrato = 0;
        this.Ciudad = 0;
        // Lista de DTH.

        this.CiudadesDTH = null;
        this.Configuracion_TipoOferta = {
            valorFijoProducto: "60ba76ee-16c3-4a9c-9e14-c36ad244c6d3",
            DescuentoPaquetePorcentajeValor: 'f62793a3-6a03-4429-ba7d-ea92636ccb07',
            MegasFijasEnProducto: 'aafdf47c-87e5-474e-b907-51dfe496126d',
            DesVarMesesCostoPorcentaje: 'a4443c08-b898-4563-a2ab-4750974c0063',
            MesGratiParteMes: '47afc824-2045-4af4-a941-c2eb4624d134',

        }
        this.Configuracion_TipoProducto = {
            paquete: 'cc42314b-3fd2-41d6-ad96-565020826439',
            plan: "e9d87dc0-70aa-46c2-bae1-c449089dd8ae"
        }
        this.Configuracion_TipoCiudades = {
            nacional: '6bab1ee4-d3ca-4119-ba45-92b4ae91fd49',
            incluyente: 'e6b65017-a7b9-45b4-a991-19d1c6b1d27e',
            excluyente: 'c9e0c667-2434-4f3c-9dfb-a2f1c35b5f2f'
        }
        this.Configuracion_TipoPetarInclExcl = {
            incluyente: 'da539a47-607a-472e-8022-97d6f712d466',
            excluyente: '9e9cb70c-a50e-450a-a3aa-2d50b810db46'
        }

        // Obtener los datos para trabjar
        $.post(this.url.AyudaVenta + 'Getplanes', { Tipo: this.TipoBase })
            .done(Result => {

                this.TarifaPlena = Result.TarifasPlenas;
                this.CiudadesCampanas = Result.CiudadesCampanas;
                this.PaquetesEspecificos = Result.PaquetesEspecificos;
                this.PlanesEspecificos = Result.PlanesEspecificos;
                this.Campanas = Result.Campanas;
                this.DescuentosSecuenciales = Result.DescuentoSecuencial;
                this.CiudadesDTH = Result.ciudadesDTH;
                this.TarifaAdicionales = Result.TarifaAdicionales;
            })
            .fail((data, status) => {
                _error("Error interno favor contactar al administrador");
            })

    }


    /*-----------------------------------------------------------------------------------*/
    /*---------|  
    /*---------| MODULO PARA LOS SERVICIOS ADICIONALES 
    /*---------|
    /*-----------------------------------------------------------------------------------*/


    /**
        * Nombre: VenderHFC
        * Descripcion: FUNCION QUE ACTUALIZA EL AYUDAVENTAS PARA VENDER HFC
        * Parametros:
     */


    VenderHFC() {
        G_ProductoId = 2;
        $.post(this.url.AyudaVenta + 'Getplanes', { Tipo: 2 })
            .done(Result => {

                this.TarifaPlena = Result.TarifasPlenas;
                this.CiudadesCampanas = Result.CiudadesCampanas;
                this.PaquetesEspecificos = Result.PaquetesEspecificos;
                this.PlanesEspecificos = Result.PlanesEspecificos;
                this.Campanas = Result.Campanas;
                this.DescuentosSecuenciales = Result.DescuentoSecuencial;
                this.CiudadesDTH = Result.ciudadesDTH;
                this.TarifaAdicionales = Result.TarifaAdicionales;
                this.Actualizar_dropdownserviciosprincipales(Result)
                $(".Estavendiendo").text("HFC");

            })
            .fail((data, status) => {
                _error("Error interno favor contactar al administrador");
            })
    }


    /**
        * Nombre: VenderFO
        * Descripcion: FUNCION QUE ACTUALIZA EL AYUDAVENTAS PARA VENDER FIBRA OPTICA
        * Parametros:
     */


    VenderFO() {
        G_ProductoId = 8;
        $.post(this.url.AyudaVenta + 'Getplanes', { Tipo: 8 })
            .done(Result => {

                this.TarifaPlena = Result.TarifasPlenas;
                this.CiudadesCampanas = Result.CiudadesCampanas;
                this.PaquetesEspecificos = Result.PaquetesEspecificos;
                this.PlanesEspecificos = Result.PlanesEspecificos;
                this.Campanas = Result.Campanas;
                this.DescuentosSecuenciales = Result.DescuentoSecuencial;
                this.CiudadesDTH = Result.ciudadesDTH;
                this.TarifaAdicionales = Result.TarifaAdicionales;
                this.Actualizar_dropdownserviciosprincipales(Result)
                $(".Estavendiendo").text("FO");

            })
            .fail((data, status) => {
                _error("Error interno favor contactar al administrador");
            })
    }

/**
* Nombre: VenderCoorp
* Descripcion: FUNCION QUE ACTUALIZA EL AYUDAVENTAS PARA VENDER PLANES COORPORATIVOS
* Parametros:
*/
    VenderCoorp() {
        G_ProductoId = 9;
        $.post(this.url.AyudaVenta + 'Getplanes', { Tipo: 9 })
            .done(Result => {

                this.TarifaPlena = Result.TarifasPlenas;
                this.CiudadesCampanas = Result.CiudadesCampanas;
                this.PaquetesEspecificos = Result.PaquetesEspecificos;
                this.PlanesEspecificos = Result.PlanesEspecificos;
                this.Campanas = Result.Campanas;
                this.DescuentosSecuenciales = Result.DescuentoSecuencial;
                this.CiudadesDTH = Result.ciudadesDTH;
                this.TarifaAdicionales = Result.TarifaAdicionales;
                this.Actualizar_dropdownserviciosprincipales(Result)
                $(".Estavendiendo").text("Corporativo");

            })
            .fail((data, status) => {
                _error("Error interno favor contactar al administrador");
            })
    }


    Actualizar_dropdownserviciosprincipales(Result) {
        //ACTUALIZAR TV
        $("#tv_ayudaventa").html("");
        this.PopulateDropdownIdName(Result.listaTv, "#tv_ayudaventa");

        //ACTUALIZAR BA
        $("#ba_ayudaventa").html("");
        this.PopulateDropdownIdName(Result.listaBa, "#ba_ayudaventa");

        //ACTUALIZAR LINEA
        $("#linea_ayudaventa").html("");
        this.PopulateDropdownIdName(Result.listaLinea, "#linea_ayudaventa");


        //ACTUALIZAR FILTRO TV
        $("#ayuda_cam_filtrotv").html("");
        this.PopulateDropdownIdName(Result.listaTv, "#ayuda_cam_filtrotv");


        //ACTUALIZAR FILTRO BA
        $("#ayuda_cam_filtroBa").html("");
        this.PopulateDropdownIdName(Result.listaBa, "#ayuda_cam_filtroBa");


        // REINICIAR TODOS LOS DATOS DEL AYUDAVENTA
        this.ReiniciarCostos();

    }



    /**
        * Nombre: VenderDTH
        * Descripcion: FUNCION QUE SE USA PARA ACTUALIZAR LA VENTA A DTH
        * Parametros:
     */





    VenderDTH() {
        G_ProductoId = 1;
        $.post(this.url.AyudaVenta + 'Getplanes', { Tipo: 1 })
            .done(Result => {

                this.TarifaPlena = Result.TarifasPlenas;
                this.CiudadesCampanas = Result.CiudadesCampanas;
                this.PaquetesEspecificos = Result.PaquetesEspecificos;
                this.PlanesEspecificos = Result.PlanesEspecificos;
                this.Campanas = Result.Campanas;
                this.DescuentosSecuenciales = Result.DescuentoSecuencial;
                this.CiudadesDTH = Result.ciudadesDTH;
                this.TarifaAdicionales = Result.TarifaAdicionales;
                this.Actualizar_dropdownserviciosprincipales(Result);
                $(".Estavendiendo").text("DTH");



            })
            .fail((data, status) => {
                _error("Error interno favor contactar al administrador");
            })
    }




    /**
        * Nombre: AdicionalesCalcularValor
        * Descripcion: calcula el valor de un servicio adicional y lo agrega  a la tabla de valor para ser mostrado a la paersona.
        * Parametros:
     */

    AdicionalesCalcularValor(cantidad) {
        var url = this.url;

        var productoSeleccionado = $("#PRODUCTO_Adicional option:selected");
        var valor = productoSeleccionado.data("valor");
        var valor1 = productoSeleccionado.data("valor1");
        var valor2 = productoSeleccionado.data("valor2");
        var valor3 = productoSeleccionado.data("valor3");

        

        let valorAdicional = (valor);
        let valortotal = 0;
        if (valor == "-1") {
            valorAdicional = formatter.format(valor1);
            if (valor2 != null) { valorAdicional = valorAdicional + "," + formatter.format(valor2) }
            if (valor3 != null) { valorAdicional = valorAdicional + "," + formatter.format(valor3) }
        }
        else {
            valortotal = valor * cantidad;
        }




        $(".txt_valor_adicional").html(formatter.format(valorAdicional))
        $(".txt_valor_adicional_total").html(formatter.format(valortotal))

        G_AdicionalValor = valorAdicional;
        G_AdicionalTotal = valortotal;

    }







    /*
    * AGREGA UN PRODUCTO ADICIONAL AL CARRITO DE VENTA.
 */
    AddToCarAdicional() {
        $(".alert-danger").remove();
        b_validacion = true;
        primerElementoG = 1;

        var productoSeleccionado = $("#PRODUCTO_Adicional option:selected");
        var valor = productoSeleccionado.data("valor");
        var valor1 = productoSeleccionado.data("valor1");
        var valor2 = productoSeleccionado.data("valor2");
        var valor3 = productoSeleccionado.data("valor3");
        var cantidadbase = productoSeleccionado.data("cantidadbase");
        var N_nuevbs = parseInt(cantidadbase) * parseInt($("#PRODUCTO_Cantidad").val())

        if (productoSeleccionado.val() == null || productoSeleccionado.val() == undefined || productoSeleccionado.val() == "0") {
            alert("no se ha selecciondao ningun adicional");
            return false;
        }

        
        var data = {
            NombreProducto: $("#PRODUCTO_Adicional option:selected").text(),
            CampanaPague1: null,
            CampanaPromocion: null,
            Descripcion: $("#PRODUCTO_Adicional option:selected").text(),
            Valor1: G_AdicionalValor,
            ValorPromocion: G_AdicionalValor,
            MegasPromocion: null,
            TVPromocion: null,
            cantidad: $("#PRODUCTO_Cantidad").val(),
            CantidadBase: cantidadbase,
            productoId: G_ProductoId,
            TipoProducto: 1,           
            OrdenId: $("#gtuuId").val(),
            CantidadUpgrade: 0,
            CantidadNuevos: N_nuevbs,
            CategoriaProducto: $("#PRODUCTO_tipoVenta option:selected").text(),
            SubCategoriaProducto: $("#PRODUCTO_paquete option:selected").text()


        }
        
        $.post(BaseUrl + "AyudasDetalleVenta/" + 'AdionarProducto', data)
            .done((Result) => {

       

                if (Result.status == 201) {
                    console.log("si es 201")
                    var _producto = Result.message;
                    $("#tbl_ventaProductos").html("");
                    G_ValorTotalCarrito = 0;
                    $.each(_producto, function (ind, val) {
                        _AyudaVenta.AgregarProductoATabla(val)

                    })
                    $("#resumen_datos").text(formatter.format(G_ValorTotalCarrito));
                }
                else {
                    console.log("no es 201")
                }
            })
            .fail((data, status) => {
                _console("error interno ");
                _console(data);
                _console(status);

            })
    }

    /*
        * AGREGA UN PRODUCTO ADICIONAL AL CARRITO DE VENTA.
    */

    AddtoCarPrincipal() {
        $(".alert-danger").remove();
        b_validacion = true;
        primerElementoG = 1;

        var _NombreProducto = "", _CantidadServicios = "", _Ba = "", _Tv = "", _Linea = "" , _DescPlan = "", _Ptar = "", _MBPromocion;



        if (this.PlanActual == null) {

            alert("No se ha selecionado ningun plan")
            return false;
        }
    /*
        * VERIFICAMOS QUE SOLO SE AGREGUE UN PRODUCTO PRINCIPAL.
        
    */
        if ($("#tbl_ventaProductos tr[data-tipoproducto=2]").length > 0) {
            alert("No se pueden vender dos productos principales")
            return false;
        }





        _NombreProducto = this.PlanActual.Nombre;
        _CantidadServicios = this.PlanActual.TipoPlan.NumeroServicios
        _DescPlan = this.PlanActual.TipoPlan.Nombre
        _Ptar = this.petar

        var serviciosUpgrade = 0;
        var ServiciosNuevos = _CantidadServicios;


        // * se verifica si tiene plan de ba
        if (this.PlanActual.Ba != null) {
            _Ba = this.PlanActual.Ba.Name
        }
        // * se verifica si tiene plan tv
        if (this.PlanActual.Tv != null) {
            _Tv = this.PlanActual.Tv.Name
        }

        // * se verifica si tiene plan linea
        if (this.PlanActual.Line != null) {
            _Linea = this.PlanActual.Line.Name
            console.log(_Linea);
        }

        if (this.CamapanaNombrePromocion.includes("mb") || this.CamapanaNombrePromocion.includes("MB")) {
            _MBPromocion = this.CamapanaNombrePromocion
            _Ba = _MBPromocion;
        }
        var D_NombreServici = [];
        var D_NumeroServicios = [];
        var D_TipoServicio = [];
        var D_Ba = [];
        var D_Tv = [];
        var D_Linea = [];


        // CUANDO ES UNA VENTA DE EMPAQUETAMIENTO SE REALIZA UNA VALIDACION DE ACTUALIZACION.
        if (this.ClienteFijo == true) {
            var serviciosUpgrade = $("#serviciosUpgrade").val();
            var ServiciosNuevos = $("#ServiciosNuevos").val();
            var _NombreServicioUpgrade = $("#serviciosUpgrade option:selected").text()
            var _NombreServicioNuevo = $("#ServiciosNuevos option:selected").text()


            var serviciosConfigurados = parseInt(serviciosUpgrade) + parseInt(ServiciosNuevos);
            if (serviciosUpgrade == 0 && ServiciosNuevos == 0) {
                alert("por favor seleccione los productos vendidos");
                return false;
            }
            if (_NombreServicioUpgrade == _NombreServicioNuevo) {
                alert("No se puede seleccionar el mismo producto para upgrade y nuevo ")
                return false;
            }

            

            if (serviciosConfigurados > _CantidadServicios) {
                alert("No se han seleccionado los productos del empaquetamiento de forma correcta");
                return false;
            }


            if (serviciosUpgrade != 0) {
                D_NombreServici.push(_NombreServicioUpgrade);
                D_NumeroServicios.push(serviciosUpgrade);
                D_TipoServicio.push(1);

                _NombreServicioUpgrade.includes("tv") == true || _NombreServicioUpgrade.includes("Tv") == true ? D_Tv.push(_Tv) : D_Tv.push("")
                _NombreServicioUpgrade.includes("@") == true ? D_Ba.push(_Ba) : D_Ba.push("")
                //pendiente upgrade de linea


            }
            if (ServiciosNuevos != 0) {
                D_NombreServici.push(_NombreServicioNuevo)
                D_NumeroServicios.push(ServiciosNuevos);
                D_TipoServicio.push(2);
                _NombreServicioNuevo.includes("tv") == true || _NombreServicioNuevo.includes("Tv") == true ? D_Tv.push(_Tv) : D_Tv.push("")
                _NombreServicioNuevo.includes("@") == true ? D_Ba.push(_Ba) : D_Ba.push("")
                //pendiente upgrade de linea

            }
        }
        else {
            D_NombreServici.push(_NombreProducto);
            D_NumeroServicios.push(ServiciosNuevos);
            D_TipoServicio.push(2);
            D_Tv.push(_Tv)
            D_Ba.push(_Ba)
            D_Linea.push(_Linea)
        }

        var data = {
            NombreProducto: _NombreProducto,
            CampanaPague1: this.CampanaPague1,
            CampanaPromocion: _Ptar,
            Descripcion: _DescPlan,
            Valor1: _AyudaVenta.ValorTarifaPlena,
            ValorPromocion: this.VAlorPromocionfinal,
            MegasPromocion: _MBPromocion,
            TVPromocion: null,
            cantidad: 1,
            CantidadBase: _CantidadServicios,
            productoId: G_ProductoId,
            TipoProducto: 2,
            OrdenId: $("#gtuuId").val(),
            CantidadUpgrade: serviciosUpgrade,
            CantidadNuevos: ServiciosNuevos,
            D_NombreServici: D_NombreServici,
            D_NumeroServicios: D_NumeroServicios,
            D_TipoServicio: D_TipoServicio,
            D_Ba: D_Ba,
            D_Tv: D_Tv,
            D_Linea: D_Linea
        }

        $.post(BaseUrl + "AyudasDetalleVenta/" + 'AdionarProducto', data)
            .done((Result) => {
                if (Result.status == 201) {
                    var _producto = Result.message;
                    $("#tbl_ventaProductos").html("");
                    G_ValorTotalCarrito = 0;
                    $.each(_producto, function (ind, val) {
                        _AyudaVenta.AgregarProductoATabla(val)

                    })
                    $("#resumen_datos").text(formatter.format(G_ValorTotalCarrito));
                }
                else {
                    console.log("no es 201")
                }
            })
            .fail((data, status) => {
                _console("error interno ");
                _console(data);
                _console(status);

            })
    }


    /*-----------------------------------------------------------------------------------*/
    /*---------| VENTA 
    /*-----------------------------------------------------------------------------------*/








    RemoverProducto(p_boton) {
        var url = this.url;

        var id = p_boton.data("id");
        console.log(p_boton);
        $.post(/AyudasDetalleVenta/ + 'RemoverProducto', { Id: id })
            .done((Result) => {
                if (Result.status == 201) {
                    var _producto = Result.message;
                    $("#tbl_ventaProductos").html("");
                    G_ValorTotalCarrito = 0;
                    $.each(_producto, function (ind, val) {
                        _AyudaVenta.AgregarProductoATabla(val)

                    })
                    $("#resumen_datos").text(formatter.format(G_ValorTotalCarrito));
                    p_boton.parents("tr").remove();
                }

            })
            .fail((data, status) => {

            })



    }





    //AdicionarProducto() {
    //    $(".alert-danger").remove();
    //    b_validacion = true;
    //    primerElementoG = 1;


    //    //$(this.CamposValidarAddProducto).each(void_validaCampo);
    //    //if (!b_validacion) { return false }


    //    var data = {
    //        ProductoId: $("#add_proProductoId").val(),
    //        GestionId: $("#gtuuId").val()
    //    }

    //    $.post(BaseUrl + "AyudasDetalleVenta/" + 'AdionarProducto', data)
    //        .done((Result) => {
    //            if (Result.status == 201) {
    //                console.log("si es 201")
    //                var _producto = Result.message;
    //                $("#tbl_ventaProductos").html("");
    //                G_ValorTotalCarrito = 0;
    //                $.each(_producto, function (ind, val) {
    //                    _AyudaVenta.AgregarProductoATabla(val)
                        
    //                })
    //                $("#resumen_datos").text(G_ValorTotalCarrito);
    //            }
    //            else {
    //                console.log("no es 201")
    //            }
    //        })
    //        .fail((data, status) => {
    //            _console("error interno ");
    //            _console(data);
    //            _console(status);
    //        })
    //}


    AgregarProductoATabla(Item) {

   
        var cantidadVendida = parseInt(Item.CantidadUpgrade + Item.CantidadNuevos);
        var total = parseInt(Item.ValorPromocion) * parseInt(Item.CantidadNuevos);
        if (Item.TipoProducto == 2) {
            total = Item.ValorPromocion;
        }

        var Pague1 = Item.CampanaPague1 == "null" || Item.CampanaPague1 == null ? "" : Item.CampanaPague1;
        var CampanaPromocion = Item.CampanaPromocion == "null" || Item.CampanaPromocion == null ? "" : Item.CampanaPromocion;
        var MegasPromocion = Item.MegasPromocion == "null" || Item.MegasPromocion == null ? "" : Item.MegasPromocion;


        const ElementItem = "<tr data-id='" + Item.Id + "' data-tipoproducto='" + Item.TipoProducto +"' > \
            <td>" + Item.Id + "</td><td>" + Item.NombreProducto + "</td><td>"
            + Item.Valor1 + "</td><td>"
            + Item.ValorPromocion + "</td><td>"
            + cantidadVendida + "</td><td>"
            + Item.ValorPromocion + "</td><td>"
            + Pague1 + "</td><td>"
            + CampanaPromocion + "</td><td>"
            + MegasPromocion + "</td><td>"
            + Item.CantidadUpgrade + "</td><td>"
            + Item.CantidadNuevos + "</td><td>"
            + total + "</td><td>"
            + "<button  data-id='" + Item.Id + "' class='ui icon black button eliminarProducto mini'><i class='icon trash'></i></button></td></tr>";
        $("#tbl_ventaProductos").append(ElementItem);
   

        G_ValorTotalCarrito = G_ValorTotalCarrito + total
    }





    //funciones para DTH
    /**/

    DTH_verificarCiudad() {

        const _Ciudad = this.Ciudad;
        const _CiudadesDTH = this.CiudadesDTH



        var Existeciudad = _CiudadesDTH.filter(Result => { return Result.CiudadId == _Ciudad });

        if (Existeciudad.length == 0) {

            alert("esta ciudad no aplica para venta de dth")
        }
        else {
            var _ciudad = Existeciudad[0];
            var _Internet = _ciudad.Internet;
            if (_Internet == null) {
                _Internet = "N/A";
            }
            $("#dth_tipoInternet p").html(_Internet)
        }

    }



    /*---------------------------------------------------------------- 
     * OBTIENE Y ESTABLECE EL JSON QUE CONTIENE LOS DATOS DE LA VENTA.
     ----------------------------------------------------------------*/

    //getJson_Venta() {


    //    console.log("ingregando plan Actual")
    //    console.log(this.PlanActual)

    //    if (this.PlanActual == null || this.PlanActual == undefined) {
    //        return true;
    //    }
    //    _detalleventa.CantidadServiciios = this.PlanActual.TipoPlan.NumeroServicios; // verificar
    //    _detalleventa.Estrato = this.estrato;
    //    _detalleventa.CiudadId = this.Ciudad;
    //    _detalleventa.tv = this.PlanActual.TvName;
    //    _detalleventa.ba = this.PlanActual.BaName;
    //    _detalleventa.linea = this.PlanActual.LineName;
    //    _detalleventa.valorTotal = this.ValorTarifaPlena;
    //    _detalleventa.ValorPromocion = this.ValorPromocion;
    //    _detalleventa.ServiciosVendidos = this.PlanActual.TvName + " + " + this.PlanActual.BaName + " + " + this.PlanActual.LineName // verificar para adicionar servicios adicionales.
    //    //ayudaventa.
    //    _detalleventa.ValorPromocion = this.ValorPromocion;



    //}

    updatePlanes() {
        this.ReiniciarCostos();
        this.updateCostos();
        if (this.estrato == 0) {
            _error("Seleccione un estrato");
            return;
        }

        this.updatePromociones();
        this.getPlanActual();
        //Update Tv.
        this.updateCostos();

    }

    getPlanActual() {

        const _tv = this.tv;
        const _Ba = this.Ba;
        const _linea = this.linea;


        /***
            * ACTUALIZANDO EL PLAN QUE SE LE OFRECE.
            * */
        var Plan = this.TarifaPlena.filter(Tarifa => {
            return (
                Tarifa.TvId == _tv &&
                Tarifa["VEstrato" + this.estrato] !== null &&
                Tarifa.BaId === _Ba && Tarifa.LineId === _linea
            )
        })

        if (Plan.length > 0) {
            const PlanActual = Plan[0];
            this.PlanActual = PlanActual;
            this.ValorTarifaPlena = parseInt(PlanActual["VEstrato" + this.estrato]);


        }
        else {
            _error("No se encontro ningun plan")
        }
    }


    /**
     * ACTUALIZA LOS VALORES DE TARIFA PLENA Y PROMOCION. APLICANDO EL DESCUENTO ACUMULADO
     * / */

    updateCostos() {
        $(".label_cam").removeClass("alert-success")
        $(".descuentoAplicado").remove();
        //this.updateDescuentoPromociones();
        //if (this.ValorTarifaPlena == this.ValorPromocion) {
        //    $("#btn_ayudaTarifaPromocion").text("N/A");

        //}
        //else {
        //    $("#btn_ayudaTarifaPromocion").text(formatter.format(this.ValorPromocion));

        //}

        let _valor = 0;
        if (this.ValorPromocion != null && this.ValorPromocion != 0) {
            _valor = this.ValorPromocion;
        }
        else {
            _valor = this.ValorTarifaPlena;

        }

        /*
         *VERIFICAR CAMPAÑAS QUE TIENEN DESCUENTO EN PAQUETE
         */


        const _TipoOfertas = this.Configuracion_TipoOferta;
        const _TipoProducto = this.Configuracion_TipoProducto;
        
        const _PaquetesEspecificos = this.PaquetesEspecificos;
        
        const _isMovil = this.ClienteMovil;
        const _isNuevo = this.ClienteNuevo;
        
        const _tvActual = this.tv;
        const _baActual = this.Ba;
        const _lineaActual = this.linea;


        /**-----------------------------------------------------------
         * APLICAR EL DESCUENTO DE PORCENTAJE Y VALOR
         *------------------------------------------------------------*/
        const CampanasAprovadasDescuentoPorcentajeValor = this.CampanasAprovadas.filter(Result => { return Result.TipoOfertaId == _TipoOfertas.DescuentoPaquetePorcentajeValor && Result.TipoProductoId == _TipoProducto.paquete });
        $.each(CampanasAprovadasDescuentoPorcentajeValor, function (ind, val) {


            const _paquete = _PaquetesEspecificos.filter(Paquete => { return (Paquete.CampanaId == val.Id) })
            if (_paquete.length == 0) {
                return true;
            }
            let _primerPaquete = _paquete[0];
            let _lineaCampana = _primerPaquete.Line;
            let _baCampana = _primerPaquete.ba;
            let _tvCampana = _primerPaquete.tv;




            //verificando el descuento a aplicar
            if (_tvCampana != null && _tvActual == null) { return true }
            if (_lineaCampana != null && _lineaActual == null) { return true }
            if (_baCampana != null && _baActual == null) { return true }

            // verficando tv.
            if (_tvCampana != null && !_tvCampana.includes(_tvActual)) {
                _error("No hay tv incluida");
                return true;
            }
            // verficando Ba.
            if (_baCampana != null && !_baCampana.includes(_baActual)) {
                _error("No hay ba incluida");
                return true;
            }

            // verficando linea.
            if (_lineaCampana != null && !_lineaCampana.includes(_lineaActual)) {
                _error("No hay linea incluida");
                return true;
            }





            if (val.percentValueClienteMovil != null && _isMovil == true) {

                _valor = _valor - (_valor * val.percentValueClienteMovil / 100)
                $("#label_cam_" + val.Id).append("<strong class='descuentoAplicado'>(aplicado " + val.percentValueClienteMovil + "%)<strong>")

            }
            else if (val.percentValue != null && _isNuevo == true) {
                _valor = _valor - (_valor * val.percentValue / 100)
                $("#label_cam_" + val.Id).append("<strong class='descuentoAplicado'>(aplicado " + val.percentValue + "%)<strong>")

            }


            $("#label_cam_" + val.Id).addClass("alert-success");

        })






        /**--------------------------------------------------
        * APLICAR EL MES GRATIS O PARTE DE MES GRATIS
        *-------------------------------------------------- */
        const _Configuracion_TipoPetarInclExcl = this.Configuracion_TipoPetarInclExcl
        const _petarActual = this.petar
        const CampanasAprovadasDescuentoMesGratis = this.CampanasAprovadas.filter(Result => { return Result.TipoOfertaId == _TipoOfertas.MesGratiParteMes && Result.TipoProductoId == _TipoProducto.paquete });
        $.each(CampanasAprovadasDescuentoMesGratis, function (ind, val) {

            const _paquete = _PaquetesEspecificos.filter(Paquete => { return (Paquete.CampanaId == val.Id) })
            if (_paquete.length == 0) {
                return true;
            }
            let _primerPaquete = _paquete[0];
            let _lineaCampana = _primerPaquete.Line;
            let _baCampana = _primerPaquete.ba;
            let _tvCampana = _primerPaquete.tv;


            //verificando el descuento a aplicar
            if (_tvCampana != null && _tvActual == null) { return true }
            if (_lineaCampana != null && _lineaActual == null) { return true }
            if (_baCampana != null && _baActual == null) { return true }

            // verficando tv.
            if (_tvCampana != null && !_tvCampana.includes(_tvActual)) {
                _error("No hay tv incluida");
                return true;
            }
            // verficando Ba.
            if (_baCampana != null && !_baCampana.includes(_baActual)) {
                _error("No hay ba incluida");
                return true;
            }

            // verficando linea.
            if (_lineaCampana != null && !_lineaCampana.includes(_lineaActual)) {
                _error("No hay linea incluida");
                return true;
            }


            if (val.TipoPtar == _Configuracion_TipoPetarInclExcl.excluyente && _petarActual != null && val.Ptars.includes(_petarActual)) {
                _error("no aplica para petar")
                return true;
            }

            $("#label_cam_" + val.Id).addClass("alert-success");

        })

        $.each($("cam_promociones .label_cam.alert-success"), function (ind , val) {
            console.log("encontre una pcam en verde");

        })




        /**--------------------------------------------------
        * ELIMINANDO LAS CAMPAÑAS DE MES GRATIS QUE APLICAN
        *-------------------------------------------------- */






        const valorAdicionar = this.AddCostoAdicionales();
        this.VAlorPromocionfinal = _valor;
        this.ImprimirValores(formatter.format(this.ValorTarifaPlena + valorAdicionar), formatter.format(_valor + valorAdicionar));

    }



    ImprimirValores(_tarifa , _promocion) {
        $("#btn_ayudaTarifaPlena").text(_tarifa);
        $("#btn_ayudaTarifaPromocion").text(_promocion);
    }

    // permite añadir los costos de los valores adicionales al valor total a pagar.
    AddCostoAdicionales() {
        //let _valoradiconal = 0;
        //const _listaProductos = $(".ayuda_valorAgregado");



        ////obtener los costos del servicio
        //$.each(_listaProductos, (ind, item) => {
        //    var s_elm = $(item);

        //    const _cantidad = parseInt(s_elm.find("select").val());
        //    const _valor = parseInt(s_elm.data("valor"));

        //    if (_cantidad == 0) { return true }


        //    for (var cantidad = 1; cantidad <= _cantidad; cantidad++) {
        //        if (s_elm.data("valor" + cantidad) != null) {
        //            _console(s_elm.data("valor" + cantidad))
        //            _valoradiconal = _valoradiconal + parseInt(s_elm.data("valor" + cantidad));

        //        }
        //        else {
        //            _valoradiconal = _valoradiconal + s_elm.data("valor");

        //        }
        //    }



        //});
        //return (_valoradiconal);

        return 0;



    }


    ReiniciarCostos() {
        $("#ayuda_listaPromociones,#cam_promociones").html("");
        this.ValorTarifaPlena = 0;
        this.ValorPromocion = 0;
        this.PlanActual = null;
        this.campanasEliminadas = [];
        this.CampanasAprovadas = this.Campanas;

    }


    /**
     * SE ENCARGA DE MOSTRAR EL VALOR DE DESCUENTO Y EL VALOR DE LA TARIFA PLENA. CON TODOS LOS PRODUCTOS ESCOGIDOS.
     * 
     * */
    updateDescuentoPromociones() {

        const _Estrato = this.estrato;
        const planActualId = this.PlanActual != null ? this.PlanActual.Id : null;


        let CampanaActual = null;
        let planActual = null;
        if (this.CampanasAprovadas == null || this.PlanActual == null) { return }


        /*
         * TRABAJANDO CON VALOR FIJO POR PAQUETE
         */

        const CampanasAprovadasValorFijo = this.CampanasAprovadas.filter(Result => { return Result.TipoOfertaId == "60ba76ee-16c3-4a9c-9e14-c36ad244c6d3" && Result.TipoProductoId == "e9d87dc0-70aa-46c2-bae1-c449089dd8ae" });
        if (CampanasAprovadasValorFijo.length > 0) {
            CampanaActual = CampanasAprovadasValorFijo[0];
            let CampanaId = parseInt(CampanaActual.Id);

            const PlanAplicar = this.PlanesEspecificos.filter(Result => {

                return (Result.CampanaId == CampanaId && Result.TarifaPlenaId == planActualId)
            })
            if (PlanAplicar.length > 0) {
                planActual = PlanAplicar[0];


            }
            else {


            }

        }



    }


    /**
     * ACTUALIZA LOS VALORES PARA LOS FILTROS DE LA PROMOCION
     */
    updateFiltros() {
        _console("1. => INGRESANDO A UPDATE FILTROS")
        //$(".ProductoAyudaVenta").val(0);

        this.ClienteMovil = $("#chk_cliente_movil").is(":checked");
        this.ClienteFijo = $("#chk_cliente_fijo").is(":checked");
        this.ClienteNuevo = $("#chk_cliente_nuevo").is(":checked");
        // verificar estrato.
        this.estrato = 1;
        this.Ciudad = $("#ayuda_ciudad").val();

        $(".filtrotvBa").val("0")
        this.Ba = $("#ba_ayudaventa").val() == 0 ? null : $("#ba_ayudaventa").val();
        this.tv = $("#tv_ayudaventa").val() == 0 ? null : $("#tv_ayudaventa").val();
        this.linea = $("#linea_ayudaventa").val() == 0 ? null : $("#linea_ayudaventa").val();

        if (this.TipoBase == 1) {
            this.DTH_verificarCiudad();
        }
    }




    /**
     * ACTUALIZA LAS PROMOSIONES QUE MUESTRAN EN EL SISTEMA
     * 
     */
    updatePromociones() {
        // FILTROS PARA LAS CAMPAÑAS
        this.Campana_VerificarCiudad();

        //this.Campana_VerificarEstrato();
        this.Campana_VerificarTipoCliente();
        //this.campana_verificarBandaAncha();


        // UPDATE PROMOCIONES PARA CLIENTE FIJOS.




        const _TipoOfertas = this.Configuracion_TipoOferta;
        const _TipoProducto = this.Configuracion_TipoProducto;
        const _PlanesEspecificos = this.PlanesEspecificos;
        const Estrato = this.estrato;
        let _CampanaData = {};
        let _PlanesData = [];




        /*
         *VERIFICAR BA PARA LAS CAMPAÑAS DE PLAN ESPECIFICO.
         */


        const CampanasAprovadasValorFijo = this.CampanasAprovadas.filter(Result => { return Result.TipoOfertaId == _TipoOfertas.valorFijoProducto && Result.TipoProductoId == _TipoProducto.plan });
        $.each(CampanasAprovadasValorFijo, function (ind, val) {
            _PlanesData = []
            _CampanaData = { Nombre: val.Nombre, Id: val.Id, Tipo: _TipoOfertas.valorFijoProducto, DescripcionCorta: val.DescripcionCorta }
            const id = val.Id;
            const PlanAplicar = _PlanesEspecificos.filter(Result => {

                return (Result.CampanaId == id)
            })
            _error("planes especificos")
            _console(_PlanesEspecificos)
            console.log(PlanAplicar)
            $.each(PlanAplicar, (ind_plan, val_plan) => {
                console.log(val_plan)
                const _PromocionAdd = {
                    TvId: val_plan.TarifaPlena.TvId,
                    BaId: val_plan.TarifaPlena.BaId,
                    LineaId: val_plan.TarifaPlena.LineId,
                    Plan: val_plan.TarifaPlena.Nombre,
                    Nombre: val_plan.Campana.Nombre,
                    TarifaPlena: val_plan.TarifaPlena["VEstrato" + Estrato],
                    Oferta: val_plan["VEstrato" + Estrato],
                    NombrePlan: val_plan.Nombre_plan

                }
                _PlanesData.push(_PromocionAdd)

            })

            if (_PlanesData.length > 0) {
                _AyudaVenta.updatePromociones_Productos(_CampanaData, _PlanesData);
            }
        })




        /*
         *VERIFICAR BA PARA LAS CAMPAÑAS DE MEGAS ESPECIFICAS.
         */


        const CampanasAprovadasMegasEspecificas = this.CampanasAprovadas.filter(Result => { return Result.TipoOfertaId == _TipoOfertas.MegasFijasEnProducto && Result.TipoProductoId == _TipoProducto.plan });

        $.each(CampanasAprovadasMegasEspecificas, function (ind, val) {
            _PlanesData = []
            _CampanaData = { Nombre: val.Nombre, Id: val.Id, Tipo: _TipoOfertas.MegasFijasEnProducto, DescripcionCorta: val.DescripcionCorta }
            const id = val.Id;
            const PlanAplicar = _PlanesEspecificos.filter(Result => {

                return (Result.CampanaId == id)
            })


            $.each(PlanAplicar, (ind_plan, val_plan) => {
                if (val_plan["CEstrato" + Estrato] == null) {
                    return true;
                }
                const _PromocionAdd = {
                    TvId: val_plan.TarifaPlena.TvId,
                    BaId: val_plan.TarifaPlena.BaId,
                    LineaId: val_plan.TarifaPlena.LineId,
                    Plan: val_plan.TarifaPlena.Nombre,
                    Nombre: val_plan.Campana.Nombre,
                    TarifaPlena: val_plan.TarifaPlena["VEstrato" + Estrato],
                    Oferta: val_plan["velocidad"] + " MB",
                    DescuentoAplicado: val_plan["VEstrato" + Estrato],
                    NombrePlan: val_plan.Nombre_plan

                }
                _PlanesData.push(_PromocionAdd)

            })

            if (_PlanesData.length > 0)
                _AyudaVenta.updatePromociones_Productos(_CampanaData, _PlanesData);
        })




        /*
         *VERIFICAR CAMPAÑAS LAS CUALES TIENEN UN DESCUENTO SECUENCIAL.
         */


        const CampanasAprovadasDescuentoSecuencial = this.CampanasAprovadas.filter(Result => { return Result.TipoOfertaId == _TipoOfertas.DesVarMesesCostoPorcentaje && Result.TipoProductoId == _TipoProducto.plan });

        $.each(CampanasAprovadasDescuentoSecuencial, function (ind, val) {
            _PlanesData = []
            _CampanaData = { Nombre: val.Nombre, Id: val.Id, Tipo: _TipoOfertas.DesVarMesesCostoPorcentaje, DescripcionCorta: val.DescripcionCorta }
            const id = val.Id;
            const PlanAplicar = _PlanesEspecificos.filter(Result => {

                return (Result.CampanaId == id)
            })

            $.each(PlanAplicar, (ind_plan, val_plan) => {

                if (val_plan["CEstrato" + Estrato] == null) {
                    return true;
                }

                let _valorAplicar = val_plan["VEstrato" + Estrato] != null ? val_plan["VEstrato" + Estrato] : val_plan.TarifaPlena["VEstrato" + Estrato]

                const _PromocionAdd = {
                    TvId: val_plan.TarifaPlena.TvId,
                    BaId: val_plan.TarifaPlena.BaId,
                    LineaId: val_plan.TarifaPlena.LineId,
                    Plan: val_plan.TarifaPlena.Nombre,
                    Nombre: val_plan.Campana.Nombre,
                    TarifaPlena: _valorAplicar,
                    Oferta: val_plan["velocidad"] + " MB",
                    NombrePlan: val_plan.Nombre_plan
                }
                _PlanesData.push(_PromocionAdd)

            })

            if (_PlanesData.length > 0)
                _AyudaVenta.updatePromociones_Productos(_CampanaData, _PlanesData);
        })







        /*
         *VERIFICAR CAMPAÑAS QUE DAN MES GRATIS
         */



        const CampanasAprovadasDescuentoMes = this.CampanasAprovadas.filter(Result => { return Result.TipoOfertaId == _TipoOfertas.MesGratiParteMes && Result.TipoProductoId == _TipoProducto.paquete });

        $.each(CampanasAprovadasDescuentoMes, function (ind, val) {


            _CampanaData = { Nombre: val.Nombre, Id: val.Id, Tipo: _TipoOfertas.DesVarMesesCostoPorcentaje, DescripcionCorta: val.DescripcionCorta, TipoPtar: val.TipoPtar, Ptars: val.Ptars }
            _AyudaVenta.updatePromociones_pcam(_CampanaData);
        })



        /*
         *VERIFICAR CAMPAÑAS QUE TIENEN DESCUENTO EN PAQUETE
         */



        const CampanasAprovadasDescuentoPorcentajeValor = this.CampanasAprovadas.filter(Result => { return Result.TipoOfertaId == _TipoOfertas.DescuentoPaquetePorcentajeValor && Result.TipoProductoId == _TipoProducto.paquete });

        $.each(CampanasAprovadasDescuentoPorcentajeValor, function (ind, val) {
            _CampanaData = { Nombre: val.Nombre, Id: val.Id, Tipo: _TipoOfertas.DesVarMesesCostoPorcentaje, DescripcionCorta: val.DescripcionCorta, TipoPtar: val.TipoPtar, Ptars: val.Ptars}

            _AyudaVenta.updatePromociones_pcam(_CampanaData);
        })







        /*
        * INSERTA LAS PROMOCIONES DE DESCUENTO EN PAQUETE.
        */

        //$.each(this.CampanasAprovadas, function (ind, val) {
        //    _console("..Agregando Campana", val)
        //    var promocion = _AyudaVenta.ConverPromociones(val);
        //    $("#ayuda_listaPromociones").append(promocion);

        //})


    }

    /**
     * VERIFICA QUE LOS SERVICIOS ADICIONALES SE PUEDAN OFRECER
     * */
    campana_verificarAdicionales() {

        //const _adicionales = this.TarifaAdicionales

        //$(".ayuda_valorAgregado").remove();

        //if (this.tv == null || this.tv == "0") {
        //    return;
        //}
        //$.each(_adicionales, (ind, item) => {

        //    // verificar que el producto sea el mismo que se trabaja.
        //    if (item.productoId != null && item.productoId != this.TipoBase) { return true; }

        //    const _Numero = item.cantidadMaxima
        //    let _option = "<option value='0'>No</option>";
        //    _error(_Numero)
        //    for (var i = 1; i <= _Numero; i++) {

        //        _option = _option + "<option value=" + i + ">" + i + "</option>";
        //    }
        //    let valorAdicional = formatter.format(item.valor);
        //    if (item.valor == "-1") {
        //        valorAdicional = formatter.format(item.valor1);
        //        if (item.valor2 != null) { valorAdicional = valorAdicional + "," + item.valor2 }
        //        if (item.valor3 != null) { valorAdicional = valorAdicional + "," + item.valor3 }
        //        if (item.valor4 != null) { valorAdicional = valorAdicional + "," + item.valor4 }
        //        if (item.valor5 != null) { valorAdicional = valorAdicional + "," + item.valor5 }
        //        if (item.valor6 != null) { valorAdicional = valorAdicional + "," + item.valor6 }

        //    }
        //    const _adicional = '<div class="ayuda_valorAgregado"  data-id="' + item.Id + '" data-valor="' + item.valor + '" data-valor1="' + item.valor1 + '"data-valor2="' + item.valor2 + '"data-valor3="' + item.valor3 + '"data-valor4="' + item.valor4 + '"data-valor5="' + item.valor5 + '"data-valor6="' + item.valor6 + '"    >\
        //                            <label>'+ item.Nombre + ' <span class="bg-red"> ' + valorAdicional + '</span></label>\
        //    <select class="form-control" style="width:100px;zoom:0.8;display:inline-block" class="chck_adicional" data-id="'+ item.Id + '">' + _option + '</select>\
        //                        </div>';
        //    $("#ayudaventa_itemsValorAgregado").append(_adicional);
        //});
    }

    /**
     * Permite adicionar y poner en verda
     * @param {any} Campana
     */
    updatePromociones_pcam(Campana) {

        var _exlcuyente = "";
        if (Campana.TipoPtar == this.Configuracion_TipoPetarInclExcl.excluyente) {
            _exlcuyente = Campana.Ptars;
        }

        let Items = "";
        // Adicion de planes a las promociones.
        const _TipoOfertas = this.Configuracion_TipoOferta;
        const fila = "<h6 class='label_cam' data-ptarexcluyente='" + _exlcuyente + "' id='label_cam_" + Campana.Id + "'><label>" + Campana.Nombre + "</label> </h6>";
        $("#cam_promociones").append(fila);

    }




    setValorPromocion(valor, petar) {
        if (valor != null && valor != "" && !isNaN(valor))
            this.ValorPromocion = valor;
        if (petar != undefined && petar != null)
            this.petar = petar;

    }

    removePetar() {
        this.petar = null;
    }


    //agrega los descuentos a la derecha de cada producto.
    updatePromociones_Productos(Campana, Planes) {

        let Items = "";
        // Adicion de planes a las promociones.
        const _TipoOfertas = this.Configuracion_TipoOferta;



        /*
         * TRABAJANDO CON TODOS LOS PLANES.
         */
        $.each(Planes, (indx, Plan) => {
            let _dataoferta = 0;
            _dataoferta = Plan.TarifaPlena;
            let _Oferta = "";
            let Item = "";
            if (Plan.Oferta == null || Plan.Oferta == 0 || Plan.Oferta == undefined || Plan.Oferta == "") {
                Plan.Oferta = Plan.TarifaPlena;
            }

            //TvId: val_plan.TarifaPlena.TvId,
            //    BaId: val_plan.TarifaPlena.BaId,
            //        LineaId: val_plan.TarifaPlena.LineId,
            //            Plan: val_plan.TarifaPlena.Nombre,
            //                Nombre: val_plan.Campana.Nombre,
            //                    TarifaPlena: val_plan.TarifaPlena["VEstrato" + Estrato],
            //    Oferta: val_plan["VEstrato" + Estrato]


            const Nombre = Plan.NombrePlan != null ? Plan.NombrePlan : Plan.Plan;
            const _valor = formatter.format(Plan.TarifaPlena);
            // OFERTA DE MULTIPLAY.
            if (Campana.Nombre.indexOf("MULTIPLAY") != -1) {

                _Oferta = Plan.Oferta;
                if (Plan.DescuentoAplicado != null && Plan.DescuentoAplicado != undefined) {
                    _Oferta = formatter.format(Plan.DescuentoAplicado);
                    _dataoferta = Plan.DescuentoAplicado; // precio del plan

                }
                let ofertaMb = "| <span class='bg-green'>" + Plan.Oferta + "</span>";
                if (Plan.Oferta.indexOf("null") != -1) {
                    ofertaMb = "";
                }

                Item = "<div class='cam_item' data-tv='" + Plan.TvId + "' data-ba='" + Plan.BaId + "' data-linea='" + Plan.LineaId + "' data-petar='" + Campana.DescripcionCorta + "'  >\
                        <label>\
                            <input data-tv='" + Plan.TvId + "' data-ba='" + Plan.BaId + "' data-linea='" + Plan.LineaId + "'  data-oferta='" + _dataoferta + "'   data-petar='" + Campana.DescripcionCorta + "'   type='radio' class='plan_seleccionado' name='plan_seleccionado' />\
                            "+ Nombre + " | <span class='bg-red'>" + _Oferta + "</span>" +
                    "</label>\
                </div>";
            }
            // OFERTA DE MEGAS DE VELOCIDAD
            else if (Campana.Tipo == _TipoOfertas.MegasFijasEnProducto) {
                _Oferta = Plan.Oferta;
                if (Plan.DescuentoAplicado != null && Plan.DescuentoAplicado != undefined) {
                    _Oferta = formatter.format(Plan.DescuentoAplicado);
                    _dataoferta = Plan.DescuentoAplicado;
                }
                Item = "<div class='cam_item' data-tv='" + Plan.TvId + "' data-ba='" + Plan.BaId + "' data-linea='" + Plan.LineaId + "' data-petar='" + Campana.DescripcionCorta + "'  >\
                        <label>\
                            <input data-tv='" + Plan.TvId + "' data-ba='" + Plan.BaId + "' data-linea='" + Plan.LineaId + "'  data-oferta='" + _dataoferta + "'   data-petar='" + Campana.DescripcionCorta + "'   type='radio' class='plan_seleccionado' name='plan_seleccionado' />\
                            "+ Nombre + " | <span class='bg-red'>" + _valor + "</span> | <span class='bg-green'>" + _Oferta + "</span>\
                    </label>\
                </div>";
            }
            // VALOR FIJO EN PRODUCTO
            else if (Campana.Tipo == _TipoOfertas.valorFijoProducto) {
                // verificar si hay un aumento en la velocidad aparte del valor fijo

                let bg_red = ""
                if (_valor > _Oferta) {
                    bg_red = "| <span class='bg-red'>" + _valor + "</span>";
                }
                _Oferta = formatter.format(Plan.Oferta);


                Item = "<div class='cam_item'  data-tv='" + Plan.TvId + "' data-ba='" + Plan.BaId + "' data-linea='" + Plan.LineaId + "'  >\
                        <label>\
                            <input data-tv='" + Plan.TvId + "' data-ba='" + Plan.BaId + "' data-linea='" + Plan.LineaId + "' data-oferta='" + Plan.Oferta + "'   data-petar='" + Campana.DescripcionCorta + "'   type='radio' class='plan_seleccionado' name='plan_seleccionado' />\
                            "+ Nombre + bg_red + " | <span class='bg-green'>" + _Oferta + "</span>\
                    </label>\
               </div>";


            }
            //DESCUENTO SECUENCIAL
            else if (Campana.Tipo == _TipoOfertas.DesVarMesesCostoPorcentaje) {
                Item = "<div class='cam_item'  data-tv='" + Plan.TvId + "' data-ba='" + Plan.BaId + "' data-linea='" + Plan.LineaId + "'  >\
                        <label>\
                            <input data-tv='" + Plan.TvId + "' data-ba='" + Plan.BaId + "' data-linea='" + Plan.LineaId + "'  data-oferta='" + Plan.TarifaPlena + "'  data-petar='" + Campana.DescripcionCorta + "'  type='radio' class='plan_seleccionado' name='plan_seleccionado' />\
                            "+ Nombre + " | <span class='bg-red'>" + _valor + "</span>\
                    </label>"+ _AyudaVenta.GetTableDescuentoSecuencial(Plan.TarifaPlena, Campana.Id) + "</div>";

            }



            Items = Items + Item;

        });

        const fila = "<div class='panel box box-primary'>\
            <div class='box-header with-border'>\
                <h4 class='box-title'>\
                    <a data-toggle='collapse' data-parent='#accordion' href='#acordeon_"+ Campana.Id + "' aria-expanded='false' class='collapsed'>" + Campana.Nombre + "</a>\
                </h4>\
            </div>\
            <div id='acordeon_"+ Campana.Id + "' class='panel-collapse collapse' aria-expanded='false' style='height: 0px;'>\
                <div class='box-body'>"+ Items + " </div>\
        </div>\
        </div>";

        $("#ayuda_listaPromociones").append(fila);


    }


    GetTableDescuentoSecuencial(Tarifa, Id) {

        let _ItemDescuento = "";
        const _DescuentosSecuenciales = this.DescuentosSecuenciales.filter(Result => { return (Result.CampanaId == Id) })

        $.each(_DescuentosSecuenciales, (indx, Descuento) => {





            const Item = "<tr><td>" + Descuento.mes + "</td><td>" + Descuento.porcentajeDescuento + "% </td><td>" + formatter.format((Tarifa * parseFloat(Descuento.porcentajeDescuento)) / 100) + "</td></tr>"
            _ItemDescuento = _ItemDescuento + Item
        });

        var Table = '<table class="table table-bordered table-striped" style="zoom:0.9" >\
                <thead> <tr>  <th>Mes</th>   <th>Descuento</th><th>Total</th></tr> </thead>\
                <tbody>'+ _ItemDescuento + '</tbody>\
           </table>';
        return Table;
    }





    /**
    * VERIFICA QUE EL TIPO DE CLIENTE  CORRESPONDA CON EL INDICADO
    * 
    */

    Campana_VerificarTipoCliente() {

        const ClienteMovil = this.ClienteMovil;
        const ClienteFijo = this.ClienteFijo;
        const ClienteNuevo = this.ClienteNuevo;


        $.each(this.CampanasAprovadas, function (ind, val) {
            const id = val.Id;
            let _remover = true;
            let _tipocliente = "";



            if (val.ClienteMovil == true && ClienteMovil != true) {
                _AyudaVenta.RemoverCampana(id, "clienteMovil");
                return true;
            }


            if (val.ClienteFijo != false && _remover === true) {

                if (ClienteFijo == true)
                    _remover = false;
                _tipocliente = "clienteFijo";
                //_AyudaVenta.RemoverCampana(id, "clienteFijo");


            }
            if (val.ClienteNuevo == true && _remover === true) {


                if (ClienteNuevo == true)
                    _remover = false;
                //_AyudaVenta.RemoverCampana(id, "clienteNuevo");
                _tipocliente = "clienteNuevo";


            }

            if (_remover === true && _tipocliente != "") {
                _AyudaVenta.RemoverCampana(id, _tipocliente);
            }

        })
    }


    /**
    * VERIFICA QUE EL ESTRATO  CORRESPONDA CON EL INDICADO ESTE FILTRO ESTA DESCTIVADO.
    * 
    */


    campana_verificarBandaAncha() {

        const _Ba = this.Ba;
        const _TipoOfertas = this.Configuracion_TipoOferta;
        const _TipoProducto = this.Configuracion_TipoProducto;
        const _PlanesEspecificos = this.PlanesEspecificos;
        const CampanasAprovadasValorFijo = this.CampanasAprovadas.filter(Result => { return Result.TipoOfertaId == _TipoOfertas.valorFijoProducto && Result.TipoProductoId == _TipoProducto.plan });



        /*
         *VERIFICAR BA PARA LAS CAMPAÑAS DE PLAN ESPECIFICO.
         */
        $.each(CampanasAprovadasValorFijo, function (ind, val) {
            const id = val.Id;
            const PlanAplicar = _PlanesEspecificos.filter(Result => {

                return (Result.CampanaId == id && Result.TarifaPlena.BaId == _Ba)
            })

            if (PlanAplicar.length == 0) {
                _AyudaVenta.RemoverCampana(id, "Ba");
            }

        })
    }

        /**
        * REMUEVE EL FILTRO DE LA CIUDAD
        *
        */
        Campana_VerificarEstrato() {
            const estrato = this.estrato;

            $.each(this.CampanasAprovadas, function (ind, val) {
                const id = val.Id;
                const Estratos = val.Estratos;
                if (Estratos.indexOf(estrato) == -1) {
                    _AyudaVenta.RemoverCampana(id, "estrato");
                }
            })
        }

    /**
    * VERIFICAR SI APLICA LA CIUDAD DE LA CAMPAÑA
    * 
    */

    Campana_VerificarCiudad() {
        let CampanasRemover = [];
        const CiudadesCampanas = this.CiudadesCampanas;
        const ciudad = this.Ciudad;
        const _ConfiguracionTipociudades = this.Configuracion_TipoCiudades;
        var Existeciudad = [];



        $.each(this.CampanasAprovadas, function (ind, val) {
            const id = val.Id;
            const _tipoCiudadActual = val.TipoCiudadesId

            if (_ConfiguracionTipociudades.nacional == _tipoCiudadActual) {
                return true;
            }



            if (CiudadesCampanas != null) {
                Existeciudad = CiudadesCampanas.filter(Result => { return Result.CampanaId == id && Result.CiudadId == ciudad });
            }
            if (Existeciudad.length == 0) {
                if (_ConfiguracionTipociudades.incluyente == _tipoCiudadActual)
                    _AyudaVenta.RemoverCampana(id, "ciudad");
            }
            if (Existeciudad.length > 0) {
                if (_ConfiguracionTipociudades.excluyente == _tipoCiudadActual)
                    _AyudaVenta.RemoverCampana(id, "ciudad");
            }



        })
    }



    RemoverCampana(id, causa) {
        this.campanasEliminadas.push({ id: id, causa: causa });

        this.CampanasAprovadas = this.CampanasAprovadas.filter(campana => {
            return campana.Id !== id
        })
        //_console(this.CampanasAprovadas);
        //_console(causa)
    }

    /**
    * DEVUELVE LA LISTA DE PROMOCIONES A APROVAR
    * 
    */

    ConverPromociones(Promocion) {
        const fila = "<li><button  class='btn bg-green'>Aplicar</button>  <span class='campanaAplica'>" + Promocion.Nombre + "</span> " + Promocion.DescripcionCorta + "</li>";
        return fila;
    }



    filterTvBa(tv, ba) {
        $(".cam_item input[type=radio]:checked").attr("checked", false)
        $(".cam_item").show();

        $(".cam_item").each((ind, item) => {
            let _item = $(item);
            const _tv = _item.data("tv");
            const _Ba = _item.data("ba");

            if (tv != 0 && tv != _tv) {
                $(item).hide();
            }
            if (ba != 0 && ba != _Ba) {
                $(item).hide();
            }
        })


    }

}


var _AyudaVenta = null;
if (isAgente == true) {
    var _AyudaVenta = new AyudaVenta();
}





/*-----------------------------------------------------------------------------------*/
/*---------| EVENTOS PARA EL MODULO DE LOS PRODUCTOS DE LA VENTA.
/*-----------------------------------------------------------------------------------*/


/*
 * Muestra los productos para el tipo de venta seleccionado.
 */

$("body").on("change", "#PRODUCTO_tipoVenta", function () {
    var elm = $(this);

    $("#PRODUCTO_paquete").html("");
    let dropdownContent = "<option selected value='0'>Seleccione</option>";
    $("#PRODUCTO_paquete").append(dropdownContent);
    // consultar las tipificaciones.
    const data = { TipoVentaId: elm.val() };
    $.post(BaseUrl + "WS_Ficha/" + 'getOfertas', data)
        .done(Result => {
            $.each(Result, function (index, Value) {
                $("#PRODUCTO_paquete").append('<option  data-Tipo="' + normalizeTrueFalse(Value.EsProductoPrincipal) + '" value="' + Value.Id + '"  >' +
                    Value.Nombre + '</option>');
            });
        })

})

/*
 * Muestra los productos para el tipo de venta seleccionado.
 */

$("body").on("change", "#PRODUCTO_paquete", function () {
    var elm = $(this);

    $("#PRODUCTO_Adicional").html("");
    let dropdownContent = "<option selected value='0'>Seleccione</option>";
    $("#PRODUCTO_Adicional").append(dropdownContent);
    // consultar las tipificaciones.
    const data = { productoPrincipal: elm.val(), productoId: G_ProductoId };
    $.post(BaseUrl + "WS_Ficha/" + 'getServicioAdicional', data)
        .done(Result => {
            $.each(Result, function (index, Value) {
                $("#PRODUCTO_Adicional").append('<option data-valor="' + Value.valor + '" data-cantidadbase="' + Value.cantidadDeVenta + '" data-valor1="' + Value.valor1 + '" data-Valor2="' + Value.valor2 + '"  data-Valor3="' + Value.valor3 + '"   data-Tipo="' + Value.productoId + '" value="' + Value.Id + '"  >' +
                    Value.Nombre + '</option>');
            });
        })
})
