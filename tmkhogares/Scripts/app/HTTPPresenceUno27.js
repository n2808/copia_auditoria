// URL of integration server
// var HttpdIntegrationURL = "https://webagent2.uno27.com"; //Se carga en el index, el cual nos entrega la url por parametro
var requestPresence;
//var contadorEnvio;
//contadorEnvio=0;
requestPresence = 0;

function QualifyContact(QCode) {

    var operation = "SetQCode&QCode=" + QCode;
    var strUrl = HttpdIntegrationURL + "/webagent/Integration.aspx?Operation=" + operation;
    //callHttpRequest(strUrl, outputResult, handler);
    enviarDatosPresence(strUrl);
    //return 0;
}

function ClearCalls() {
    var operation = "ClearCalls";
    var strUrl = HttpdIntegrationURL + "/webagent/Integration.aspx?Operation=" + operation;
    //callHttpRequest(strUrl, outputResult, handler);
    // alert(strUrl);
    enviarDatosPresence(strUrl);
    //return 0;
}

function CloseContact() {
    var operation = "CloseContact";
    var strUrl = HttpdIntegrationURL + "/webagent/Integration.aspx?Operation=" + operation;
    //callHttpRequest(strUrl, outputResult, handler);
    enviarDatosPresence(strUrl);
    //return 0;
} 

function enviarDatosPresence(url) { 
    console.log(url);
    $(document).ready(function(){
        $("#xml").attr('src', url); 
    }); 
}

function llamarClearCalls(Qcode) {
    ClearCalls();
    setTimeout(() => {
        llamarQualityContact(Qcode);
    }, 50);
}

function llamarQualityContact(Qcode) {
    QualifyContact(Qcode);
    setTimeout(() => {
        llamarCloseContact();
    }, 50);
}

function llamarCloseContact() {
    CloseContact(); 
} 