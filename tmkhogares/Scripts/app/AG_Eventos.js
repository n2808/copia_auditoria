﻿/*-----------------------------------------------------------------------------------*/
/*---------| DEFINICION DE EVENTOS DEL MODULO AGENTE.
/*-----------------------------------------------------------------------------------*/


/*
 * ACTUALIZA LAS EVIDENCIAS DEL AGENTE
 */
$("body").on("click", "#traerImagenServer", function () {
    const data = { id: $("#gtuuId").val() };
    $.get(BaseUrl + "Gestions/" + 'Evidencias', data)
        .done(Result => {
            $("#evidenciasImagenes").html(Result);
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })

})

$("body").on("change", "#QueQuiereComprar", function () {
    const venta = $(this).val();



})



/*
 * ACTUALIZAR LAS TIPIFICACIONES DE MOBILES
 */
$("body").on("change", "#QueQuiereComprar", function () {
    var ProductoId = TipoBase
    let dropdownContent = "<option selected value='0'>Seleccione</option>";

    var s_elm = $(this).val();
    if (s_elm == "Servicio Movil") {
        ProductoId = 7
    }

    $(".consecutivo1").nextAll().remove();
    $(".consecutivo1").html("");
    $(".consecutivo1").append(dropdownContent);


    const data = { Id: 0, Producto: ProductoId };
    $.post(BaseUrl + "Gestions/" + 'getTipificacion', data)
        .done(Result => {
            //updateDropdown($(".consecutivo1").first(), Result);
            $.each(Result, function (index, Value) {
                $(".consecutivo1").append('<option  data-codigo="' + Value.codigo + '" data-final="' + normalizeTrueFalse(Value.final) + '" data-efectiva="' + normalizeTrueFalse(Value.GestionEfectiva) + '"  value="'
                    + Value.Id + '">' +
                    Value.Nombre + '</option>');
            });
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })




})



/*
 * REMUEVE UN ITEM DE LA COMPRA
 */

$("body").on("click", ".removeItem", function () {
    var s_elm = $(this);
    var fila = s_elm.parents("tr");
    Carro.removeProduct(fila.data("id"));
    fila.remove();

})


/*
 * GUARDAR GESTION
 */


$("body").on("click", "#GuardarGestion", function () {

    if (Carro.ValidarVenta()) {
        Carro.GuardarVenta();
    }
    
})


$("body").on("click", "#CrearOrden", function () {
        Carro.CrearOrden();
})





/*
 * ACTUALIZA EL GUION DE LA VENTA
 */
$("body").on('click', '.direccion', function (event) {
    borrarDireccion();
    inputdireccion = $(this).data("input");
    $("#ModalDireccion").modal();
    /* Act on the event */
});




$("body").on("click", ".actualizarEstratoPlan", function () {
    const estrato = $(this).val();
    $(".estrato_select,#valorAgregado").hide();
    if (estrato != "" && estrato != 0) {
        $("#valorAgregado").show();
    }
    $("#estrato_" + estrato).show();
   
    $("#estrato_" + estrato+",#valorAgregado").find(".select2").select2();
        
})




/*
 * MUESTRA U OCULTA LOS FORMULARIOS DE LA CREACION DEL CLIENTE.
 */


$("body").on("change", "#selectClienteexiste", function () {
    const venta = $(this).val();
    venta == "1" ? $("#crearCliente_fmr").show() : $("#crearCliente_fmr").hide();

})



/*
 * MUESTRA U OCULTA LOS FORMULARIOS DE LA VENTA.
 */


$("body").on("change", "#selectEsVenta", function () {
    const venta = $(this).val();

    venta == "1" ? $("#Gestion_Venta_form").show() : $("#Gestion_Venta_form").hide();
    venta == "1" ? $(".formValidador").show() : $(".formValidador").hide();

    $("#valorAgregado").find(".select2").select2()
})


$("body").on('click', '.activeFs', function (elm) {
    var s_elm = $(this);
    $("#Gestion_Venta_form").hide();

    var status = s_elm.hasClass('up');
    $("#box_Tip").show();
    s_elm.parents("div").find(".activeFs").removeClass('btn-danger').removeClass('btn-success').addClass('btn-default');
    s_elm.parents("tr").find(".check_verificar").val(status.toString());
    if (status == true) {
        s_elm.addClass("btn-success").removeClass('btn-default');
        $("#Gestion_Venta_form").show();
        $(".formValidador").show();
    }
    else {
        s_elm.addClass("btn-danger").removeClass('btn-default');
        $(".formValidador").hide();
    }
    $(".select2").select2();
})



/*
 * ACTUALIZA EL GUION DE LA VENTA
 */
$("body").on("change", "#tipoGuion", function () {
    const id = $(this).val();
    _console(".... id Guion ...")
    _console(id);
    $(".contrato").hide();
    if (id != 0) {
        $(".contrato.con" + id).show()
    }

})


/*
 * ACTUALIZA LAS CIUDADES POR DEPARTAMENTO
 */

$("body").on('input', '#captura_departamentoId', function () {
    const value = $(this).val();
    if (!value) { return; }
    Carro.ActualizarCiudades(value);
})



/*
 * ACTUALIZA LA RAZON DE LA LLAMADA PARA EL INBOUND.
 */

$("body").on('input', '#VENTA_MotivoLlamada_Inbound', function () {
    const value = $(this).val();
    if (!value) { return; }
    Carro.ActualizarInboundMotivoLLamada(value);
})




/*
 * activa las validaciones en tiempo real para las clases seleccionado
 */


$("body").on('input', '.campoVenta,.campoVentaInbound', function () {

    $(this).parent('.form-group').find('.alert-danger').remove();
    b_validacion = true;
    primerElementoG = 1;
    $(this).each(void_validaCampo)
    if (!b_validacion) { return false }

})




/*
 * ACTUALIZA LA RAZON DE LA LLAMADA PARA EL INBOUND.
 */

$("body").on('input', '.campoVentaInbound', function () {
    $("#VENTA_NOMBRE_COMPLETO_CLIENTE").val($("#VENTA_NOMBRE__inbound").val() + " " + $("#VENTA_Apellido_inbound").val())
    $("#VENTA_NUMERO_CEDULA").val($("#VENTA_Documento_Inbound").val())
    $("#captura_telefono1").val($("#VENTA_TELEFONOCONTACTO2_Inbound").val())
    $("#txt_direccion").val($("#txt_direccion1").val())
    $("#ayuda_ciudad").val($("#ciudad_Inbound").val())
})



$("body").on('change', '#ciudad_Inbound', function () {

    $("#ayuda_ciudad").val($("#ciudad_Inbound").val())
})





$("body").on('click', '#duplicarDireccion', function (event) {

    var val = $("#txt_direccion1").val()
    $("#txt_direccion").val(val);
});





/*
 * ACTUALIZA EL GUION DE LA VENTA
 */
$("body").on('click', '#direccion', function (event) {
    $("#ModalDireccion").modal();
    /* Act on the event */
});


/*
 * ACTUALIZA EL GUION DE LA VENTA
 */
$("body").on('change', '.campana select', function (event) {
    Carro.UpdateCarrito();
});







/*
 * ACTUALIZA LA TIPFICACION DEL AGENTE.
 */

$("body").on("change", ".dro_cascade_tipificacion", function () {
    var elm = $(this);
    var final = elm.find("option:selected").data("final");
    var tipificacion = elm.find("option:selected").data("codigo");

    adminCalendar(tipificacion, final);


    $("#tipificacion").val(tipificacion);

    elm.nextAll().remove();
    if (elm.val() == 0 || final == "1")
        return
    // consultar las tipificaciones.
    const data = { Id: elm.val() };
    $.post(BaseUrl + "Gestions/" + 'getTipificacion', data)
        .done(Result => {
            updateDropdown(elm, Result);
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })

})


/*
 * Verificar Cliente
 */
$("body").on("click", "#btn_checkclient", function () {
    $(".alert-danger").remove();
    const documento = $("#documento").val()
    const elm = $(this);

    const login = $("#login").val();
    const vdn = $("#vnd").val();
    const telefono = $("#telefonoLlamada").val();
    const idPresence = $("#idPresence").val();

    if ($.trim(documento) == "") {
        elm.parent().parent().append(string_mensaje("por favor ingrese un número de documento"));
        return;
    }
    $.post(BaseUrl + "clientes/" + 'GetClient', { documento})
        .done(Result => {
            if (Result.Id == 0) {
                showForm();
            }
            else {
                window.location = "Gestion?id=" + Result.Id + "&login=" + login + "&vdn=" + vdn + "&telefono=" + telefono + "&contactoId=" + idPresence;
            }
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
            _error(data);
            _error(status);
        })
})

/*
 * Ingresar un cliente en el sistema
 */
$("body").on("click", "#btn_ingresarCliente", function () {
    $(".alert-danger").remove();
    b_validacion = true;
    primerElementoG = 1;
    $(".campoCliente").each(void_validaCampo);
    _console("saliendo de las validaciones");
    if (!b_validacion) { return false }
    
    const documento = $("#documento").val()
    const elm = $(this);

    const login = $("#login").val();
    const vdn = $("#vnd").val();
    const telefono = $("#telefonoLlamada").val();
    const idPresence = $("#idPresence").val();


    const data = {
        CUENTA_CO_ID: $("#documento").val(),
        NOMBRE_CLIENTE: $("#Nombre_cliente").val(),
        NUMERO_IDENTIFICACION: $("#documento").val(),
        TELEFONO_1: $("#telefono1").val(),
        TELEFONO_2: $("#telefono2").val(),
        TELEFONO_3: $("#telefono3").val(),
        CELULAR_1: $("#celular1").val(),
        CreatedBy: $("#AsuuId").val(),
        car_ProductoId: $("#productoId").val(),
        __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
    }

    $.post(BaseUrl + "clientes/create", data )
        .done(Result => {
            if (Result.Id != undefined && Result.Id != null) {
                window.location = "Gestion?id=" + Result.Id + "&login=" + login + "&vdn=" + vdn + "&telefono=" + telefono + "&contactoId=" + idPresence;
            }
            else {
                _console("exito");
                _console(Result);
            }
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
            _error(data);
            _error(status);
        })
})



/*===============================================|
 * SE PROGRAMA EL REGISTRO DEL AGENTE.
 ===============================================*/


$("body").on('change','#chk_programacion_manual', function () {
    var s_elm = $(this).is(':checked');
    if (s_elm === true) {
        $(".campoprogramacion").show();
        $("#fechaLlamada").attr("required")
    }
    else {
        $(".campoprogramacion").hide();
        $("#fechaLlamada").removeAttr("required")
    }
})


$("body").on("click", "#programacion_data", function () {
    var s_elm = $(this);
    $(".alert-danger,.alert-success").remove();
    b_validacion = true;
    primerElementoG = 1;
    $(".campoCliente").each(void_validaCampo);
    
    if (!b_validacion) { return false }
    if ($("#chk_programacion_manual").is(":checked")) {
        var fechaProgramada = $("#fechaLlamada").val() + " " + $("#horaLlamada").val() + ":" + $("#minutoLlamar").val();
    }
    else {
        var fechaProgramada = "";
    }
    const data = {
        fechaAgendamiento: fechaProgramada,
        telefono: $("#telefono").val(),
        Agente: $("#agente").val(),
    }
    var ruta = "http://localhost:57724/ingresaSolicitud";
    var ruta = "http://172.16.5.152:6792/ingresaSolicitud";
    

    $.post(ruta, data)
    .done(Result => {
        if (Result.codigo == 0) {

            //llamar callback
            swal({
                position: 'top-right',
                type: 'success',
                title: 'Número ' + data.telefono + ' programado correctamente',
                showConfirmButton: false,
                timer: 1500
            });
            //s_elm.after(string_mensaje("Número " + data.telefono + " programado correctamente","alert-success"))
        }
        else {
            s_elm.after(string_mensaje("Número " + data.telefono + " no se puede programar", "alert-danger"))
          
        }
    })
})


$("body").on("click", "#programacion_textolegal_recuperacion", function () {
    var s_elm = $(this);
    $(".alert-danger,.alert-success").remove();
    b_validacion = true;
    primerElementoG = 1;
    if ($("#chk_programacion_manual").is(":checked")) {
        var fechaProgramada = $("#fechaLlamada").val() + " " + $("#horaLlamada").val() + ":" + $("#minutoLlamar").val();
    }
    else {
        var fechaProgramada = "";
    }
    const data = {
        CampaignId: 1,
        telefono: s_elm.data("cliente"),
        Agente: $("#agente").val(),
        CustomData1: s_elm.data("customdata"),
    }
    var ruta = "http://localhost:57724/ingresaSolicitud";
    var ruta = "http://172.16.5.152:6792/programarRecuperacion";


    $.post(ruta, data)
        .done(Result => {
            if (Result.codigo == 0) {
                s_elm.after(string_mensaje("Número " + data.telefono + " programado correctamente", "alert-success"))
            }
            else {
                s_elm.after(string_mensaje("Número " + data.telefono + " no se puede programar", "alert-danger"))

            }
        })
})



$("body").on("blur", "#miblock", function () {
    var s_lm = $(this);
    var status = $("#labelEStatusBloc");
    const data = {
        id: $("#agente").val(),
        Mensaje: s_lm.val(),
    }




    $.post(BaseUrl + "programacion/MiBloc", data)
        .done(Result => {
            if (Result.codigo == 0) {
                //status.addClass("label-success").text("Guardado");
                status.addClass("tag teal").text("Guardado");
            }
            else {
                status.addClass("tag red").text("No se ha podido Guardar");
                //status.addClass("label-danger").text("No se ha podido Guardar");
            }
        })
})


$("body").on("focus", "#miblock", function () {
    var s_lm = $(this);
    var status = $("#labelEStatusBloc");
    status.removeClass("teal").removeClass("red").text("Por Guardar");
    //status.removeClass("label-success").removeClass("label-danger").text("Por Guardar");
})




