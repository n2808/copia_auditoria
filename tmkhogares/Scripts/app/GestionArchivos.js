﻿var imagen;

$(document).ready(function () {

    window.addEventListener("paste", processEvent);

    function processEvent(e) {


        if ($('.ui.modal.evidenciaArchivos').hasClass('active')) {
            // get the clipboard item
            var clipboardItem = e.clipboardData.items[0];
            var type = clipboardItem.type;

            // if it's an image add it to the image field
            if (type.indexOf("image") != -1) {

                // get the image content and create an img dom element
                var blob = clipboardItem.getAsFile();

                // Attach file
                imagen = blob;

                var blobUrl = window.webkitURL.createObjectURL(blob);
                var img = $("<img />");
                img.attr("src", blobUrl);

                //remover imagen evidencia actual modal
                $("#imagenEvidencia img ").remove();

                $("#imagenEvidencia").append(img);

            } else {
                alert("Formato no soportado: " + type);
            }
        }

       
    }
});

$("body").on("click", "#btnEvidencias", function () {
    $(".evidenciaArchivos").modal('show');

    $(".evidenciaArchivos").modal({
        closable: true
    });
    limpiarModalPortapapeles();
})

$("body").on("click", "#cerrarModalArchivos", function () {
    cerrarModalPortapapeles();
    LimpiarModalPortapapeles();

})

$("body").on("click", "#cerrarModalArchivoPrincipal", function () {
    cerrarModalPortapapeles();
    limpiarModalPortapapeles();
})

function cerrarModalPortapapeles() {
    $(".evidenciaArchivos").modal('hide');

    $(".evidenciaArchivos").modal({
        closable: true
    });
}
function limpiarModalPortapapeles() {
    $('.ui.fluid.selection.dropdown.tipoEvidencia').dropdown('restore defaults');  
    imagen = [];
    $("#imagenEvidencia img ").remove();
}

$("body").on("click", "#guardarEvidenciaGestion", function () {

    validarCamposModalEvidencia();

    if ($('#selectTipoEvidencia option:selected').val() != "0" && imagen != undefined && imagen.length != 0) {

        var formData = new FormData();

        formData.append('file', imagen);
        formData.append("idGestion", $("#gtuuId").val());
        formData.append("tipoEvidencia", $('#selectTipoEvidencia option:selected').val());

        $.ajax({
            type: "POST",
            url: '/Gestions/CargarEvidenciaGestion',
            contentType: false,
            processData: false,
            data: formData,
            success: function (result) {
                console.log(result);
                var _respuesta = result;

                if (_respuesta.type == "OK") {
                    swal({
                        position: 'top-right',
                        type: 'success',
                        title: _respuesta.message,
                        showConfirmButton: false,
                        timer: 1000
                    });
                    cerrarModalPortapapeles();
                    limpiarModalPortapapeles();
                } else {
                    swal({
                        position: 'top-right',
                        type: 'error',
                        title: 'Error al cargar la  evidencia',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }

            },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;
                console.log(err);
            }

        })
    }
})


function validarCamposModalEvidencia() {

    if ($('#selectTipoEvidencia option:selected').val() == "0") {     
        swal({
            position: 'top-right',
            type: 'error',
            title: 'Seleccione el tipo de evidencia',
            showConfirmButton: false,
            timer: 1000
        });
    }
    if (imagen == undefined || imagen.length == 0) {
        swal({
            position: 'top-right',
            type: 'error',
            title: 'La imagen es obligatoria',
            showConfirmButton: false,
            timer: 1000
        });
    }
}