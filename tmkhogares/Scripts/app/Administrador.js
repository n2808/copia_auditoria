﻿/*-----------------------------------------------------------------------------------*/
/*---------| CIUDADES DE CAMPAÑAS
/*-----------------------------------------------------------------------------------*/


/*
 * AGREGA UNA CIUDAD DE UN PLAN
 */
$("body").on("click", "#admin_agregarCiudad", function () {
    $(".alert").remove();
    var s_elm = $(this);
    if ($("#ciudadcampana").val() == "") {
        s_elm.parent().append(string_mensaje("Se debe seleccionar una ciudad"))
        return
    }
    var  nombreCiudad = $("#ciudadcampana option:selected").text()
    var  data = {
        CiudadId: $("#ciudadcampana").val(),
        CiudadNombre: nombreCiudad.slice(0, nombreCiudad.indexOf('(')),
        CampanaId: $("#idCampana").val(),
        __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
    };
    s_elm.attr('disabled', true)
    $.post(BaseUrl + "CiudadCampanas/" + 'Create', data)
        .done(Result => {
            if (Result.status == 200) {
                _console(Result)
                $("#table_campana tbody").append("<tr class='alert-success'><td>" + Result.message.Ciudad.State.Name + "</td><td>" + Result.message.Ciudad.Name + "</td><td><button  class='ui orange button AdminCampana_btnEliminarCiudad' data-id='" + Result.message.Id + "'><i class='ui icon trash'></i></button></td><tr>")
                s_elm.parent().append(string_mensaje("Ciudad " + Result.message.Ciudad.Name + "Agregada con exito", "alert-success"))
            }
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })
        .always(Data => {
            s_elm.attr('disabled', false)
        })

})


/*
 * ELIMINA UNA CIUDAD DE UN PLAN
 */
$("body").on("click", ".AdminCampana_btnEliminarCiudad", function () {
    
    const s_elm = $(this);

    var row = $(this).closest("tr");
    var idCiudad = row.find("#idCiudad").html();

    const data = {
        id: s_elm.data("id"),
        __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
    };
    s_elm.attr('disabled', true)
    $.post(BaseUrl + "CiudadCampanas/" + 'Delete', data)
        .done(Result => {
            if (Result.status == 200) {
                s_elm.parents("tr").removeClass("alert-success").addClass("alert-danger")
                //s_elm.remove();
                row.remove();
            }
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })
        .always(Data => {
            s_elm.attr('disabled',false)
        })
})


/*-----------------------------------------------------------------------------------*/
/*---------| PAQUETES DE LAS CAMPAÑAS.
/*-----------------------------------------------------------------------------------*/


/*
 * AGREGA UNA CIUDAD DE UN PLAN
 */
$("body").on("click", "#admin_guardarPaquete", function () {
    $(".alert").remove();
    var s_elm = $(this);
    var _tv = $("#tv_adminCampana").val() != null ?  $("#tv_adminCampana").val().join(",") : "";
    var _ba = $("#ba_adminCampana").val() != null ? $("#ba_adminCampana").val().join(",") : "";
    var _linea = $("#linea_adminCampana").val() != null ? $("#linea_adminCampana").val().join(",") : "";
    var _cod2 = "";
    var _cod1 = "";
    
    var data = {
        CampanaId: $("#idCampana").val(),
        tv : _tv, ba : _ba, Line : _linea, Cod1:_cod1 , Cod2 : _cod2,
        __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
    };
    s_elm.attr('disabled', true)

    $.post(BaseUrl + "PaquetesEspecificos/" + 'Create', data)
        .done(Result => {
            if (Result.status == 200) {
                _console(Result)
               
                s_elm.parent().append(string_mensaje("Paquete Guardado con éxito", "alert-success"))
            }
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })
        .always(Data => {
            s_elm.attr('disabled', false)
        })
})


/*
 * ELIMINA UNA CIUDAD DE UN PLAN
 */
//$("body").on("click", ".AdminCampana_btnEliminarCiudad", function () {

//    const s_elm = $(this);

//    const data = {
//        id: s_elm.data("id"),
//        __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
//    };
//    s_elm.attr('disabled', true)
//    $.post(BaseUrl + "CiudadCampanas/" + 'Delete', data)
//        .done(Result => {
//            if (Result.status == 200) {
//                s_elm.parents("tr").removeClass("alert-success").addClass("alert-danger")
//                s_elm.remove();
//            }
//        })
//        .fail((data, status) => {
//            _console("Error interno favor contactar al administrador");
//        })
//        .always(Data => {
//            s_elm.attr('disabled', false)
//        })
//})








/*-----------------------------------------------------------------------------------*/
/*---------| PLANES DE LAS CAMPAÑAS
/*-----------------------------------------------------------------------------------*/


// estados de los planes
$("body").on('click', '.AdminCampana_btn_estado_plan', void_EstadoPregunta);
$("body").on('click', '#admin_guardarPlan', Admin_GuardarPlan);


function void_EstadoPregunta() {
    console.log("adminiistrador ")
    s_elm = $(this);
    Id = s_elm.data("id");
    var i_estado = true;
    //s_elm.hasClass('btn-success') ? i_estado = false : i_estado = true;
    s_elm.hasClass('ui positive button') ? i_estado = false : i_estado = true;
    s_elm.attr('disabled', true)

    var data = {
        Estado: i_estado,
        id: Id,
        __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
    };
    _console(data)

    $.post(BaseUrl + "PlanesEspecificos/" + 'Edit', data)
    .done(Result => {
        if (Result.status == 200) {
            s_elm.toggleClass('ui positive button').toggleClass('ui red button');
        }
    })
    .fail((data, status) => {
        _console("Error interno favor contactar al administrador");
    })
    .always(Data => {
        s_elm.attr('disabled', false)
    })
}



function Admin_GuardarPlan() {
    $(".alert").remove();
    var s_elm = $(this);


    var data = {
        CampanaId: $("#idCampana").val(),
        TarifaPlenaId: $("#TarifaPlenaId").val(),
        CEstrato1: $("#adminPlanes_CEstrato1").val(),
        CEstrato2: $("#adminPlanes_CEstrato2").val(),
        CEstrato3: $("#adminPlanes_CEstrato3").val(),
        CEstrato4: $("#adminPlanes_CEstrato4").val(),
        CEstrato5: $("#adminPlanes_CEstrato5").val(),
        CEstrato6: $("#adminPlanes_CEstrato6").val(),
        VEstrato1: $("#adminPlanes_VEstrato1").val(),
        VEstrato2: $("#adminPlanes_VEstrato2").val(),
        VEstrato3: $("#adminPlanes_VEstrato3").val(),
        VEstrato4: $("#adminPlanes_VEstrato4").val(),
        VEstrato5: $("#adminPlanes_VEstrato5").val(),
        VEstrato6: $("#adminPlanes_VEstrato6").val(),
        velocidad: $("#adminPlanes_Velocidad").val(),
        __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
    };
    s_elm.attr('disabled', true)

    $.post(BaseUrl + "PlanesEspecificos/" + 'Create', data)
        .done(Result => {
            if (Result.status == 201) {
                s_elm.parent().append(string_mensaje("Paquete Guardado con éxito", "alert-success"))
                const _plan = Result.message;
                let _fila = "<tr class='alert-success'><td>" + _plan.TarifaPlena.Nombre + "</td><td>" + _plan.CEstrato1 + "</td><td>" + _plan.CEstrato2 + "</td><td>" + _plan.CEstrato3 + "</td><td>" + _plan.CEstrato4 + "</td><td>" + _plan.CEstrato5 + "</td><td>" + _plan.CEstrato6
                    + "</td><td>" + _plan.VEstrato1 + "</td><td>" + _plan.VEstrato2 + "</td><td>" + _plan.VEstrato3 + "</td><td>" + _plan.VEstrato4 + "</td><td>" + _plan.VEstrato5 + "</td><td>" + _plan.VEstrato6 + "</td><td>" + _plan.velocidad
                    + "</td><td><button  class='ui positive button AdminCampana_btn_estado_plan' data-id='" + _plan.Id + "'><i class='lock icon'></i></button></td><tr>";
                
                $("#table_planas tbody").append(_fila.replace(/null/g,""))
            }
            if (Result.status == 409) {
                s_elm.parent().append(string_mensaje("Este plan ya fue agregado", "alert-danger"))
            }
            if (Result.status == 400) {
                if (Result.message) {
                    $.each(Result.message, (ind, mess) => {
                        s_elm.parent().append(string_mensaje(mess.Message, "alert-danger"))
                    })
                }
            }


        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })
        .always(Data => {
            s_elm.attr('disabled', false)
        })
}


/*
 * configuracion del administrador de campañas.
 */


$("body").on('change', '#TipoCiudadesId', function () {

    const _TipoCiudad = $(this).val();

    if (_TipoCiudad == "" || _TipoCiudad == "6bab1ee4-d3ca-4119-ba45-92b4ae91fd49") {
        $("#AdminCampana_containerCiudades").fadeOut(400)


    }
    else {
        $("#AdminCampana_containerCiudades").fadeIn(400)
        $("#AdminCampana_containerCiudades .select2").select2();
    }

})


/*
 * configuracion duestra o oculta el tipo de paquete en el sistema
 */



$("body").on('change', '#TipoProductoId', function () {

    const _Tipo = $(this).val();
    if (_Tipo == "") {
        $("#AdminCampana_containerPaquetes").fadeOut(400)
        $("#AdminCampana_containerPlanes").fadeOut(400)

    }
    else if ( _Tipo == "cc42314b-3fd2-41d6-ad96-565020826439") {
        $("#AdminCampana_containerPaquetes").fadeIn(400)
        $("#AdminCampana_containerPaquetes .select2").select2();
        $("#AdminCampana_containerPlanes").fadeOut(400)

    }
    else {
        $("#AdminCampana_containerPaquetes").fadeOut(400)
        $("#AdminCampana_containerPlanes").fadeIn(400)
        $("#AdminCampana_containerPlanes .select2").select2();

    }

})

/*-----------------------------------------------------------------------------------*/
/*---------| DESCUENTO SECUENCIAL
/*-----------------------------------------------------------------------------------*/

$("body").on('change', '#TipoOfertaId', function () {

    const _TipoOferta = $(this).val();

    if (_TipoOferta == "" || _TipoOferta != "a4443c08-b898-4563-a2ab-4750974c0063") {
        $("#AdminCampana_containerDescuentoSecuencial").fadeOut(400)


    }
    else {
        $("#AdminCampana_containerDescuentoSecuencial").fadeIn(400)
        $("#AdminCampana_containerDescuentoSecuencial .select2").select2();
    }

})

/*
 * AGREGA UNA CIUDAD DE UN PLAN
 */
$("body").on("click", "#AgregarMesDescuentoSecuencial", function () {
    $(".alert").remove();
    var s_elm = $(this);
    if ($("#AdmininCampana_DescuentoMes").val() == "") {
        s_elm.parent().append(string_mensaje("Se debe ingresar el mes del descuento"))
        return
    }
    if ($("#AdmininCampana_DescuentoPorcentaje").val() == "" && $("#AdmininCampana_Descuentovalor").val() == "") {
        s_elm.parent().append(string_mensaje("Se debe seleccionar el % de descuento o el valor de descuento"))
        return
    }
    
    var data = {
        mes: $("#AdmininCampana_DescuentoMes").val(),
        porcentajeDescuento: $("#AdmininCampana_DescuentoPorcentaje").val(),
        valorDescuento: $("#AdmininCampana_Descuentovalor").val(),
        CampanaId: $("#idCampana").val(),
        __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
    };

    s_elm.attr('disabled', true)
    $.post(BaseUrl + "DescuentoSecuencials/" + 'Create', data)
        .done(Result => {
            if (Result.status == 200) {
                var _fila = "<tr class='alert-success'><td>" + Result.message.mes + "</td><td>" + Result.message.porcentajeDescuento + "</td><td>" + Result.message.valorDescuento + "</td><td><button  class='ui orange button AdminCampana_btnEliminarDescuentoSecuencial' data-id='" + Result.message.Id + "'><i class='ui icon trash'></i></button></td><tr>";
                $("#table_admincampanaDescuentoSecuencial tbody").append(_fila.replace(/null/g, ""))
                s_elm.parent().append(string_mensaje("Descuento Agregado con exito", "alert-success"))
            }
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })
        .always(Data => {
            s_elm.attr('disabled', false)
        })

})

/*
 * ELIMINA UNA CIUDAD DE UN PLAN
 */
$("body").on("click", ".AdminCampana_btnEliminarDescuentoSecuencial", function () {

    const s_elm = $(this);
    var row = $(this).closest("tr");
    var idDescuento = row.find("#idDescuento").html();

    const data = {
        id: s_elm.data("id"),
        __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
    };
    s_elm.attr('disabled', true)
    $.post(BaseUrl + "Descuentosecuencials/" + 'Delete', data)
        .done(Result => {
            if (Result.status == 200) {
                s_elm.parents("tr").removeClass("alert-success").addClass("alert-danger")
                //s_elm.remove();
                row.remove();
            }
        })
        .fail((data, status) => {
            _console("Error interno favor contactar al administrador");
        })
        .always(Data => {
            s_elm.attr('disabled', false)
        })
})
