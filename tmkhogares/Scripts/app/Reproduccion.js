﻿$(document).ready(function () {

    $(".escuchar").click();
});
$("body").on("click", ".escuchar", function (event) {
    $(event.target).addClass("loading");
    var tr = event.target.parentNode;
    var loginAsesor = $("#loginAsesor").val();
    if (loginAsesor == "") {
        alert("No se ha encontrado el login del asesor por favor contacte con el administrador");
        return;
    }
    $.post("/Calificaciones/ObtenerLlamadaAzar", { login: loginAsesor }, function (res1) {
        if (res1.status == 200) {
            var grabacion = JSON.parse(res1.grabacion);
            $("#IdGrabacion").val(grabacion.ID);
            $("#TelefonoGrabacion").val(grabacion.PHONE.toString());
            var idPresence = grabacion.ID;
            $.post("/Calificaciones/BusquedaGrabacion", { idPresence: idPresence }, function (res) {
                $(tr).text("");
                $(tr).append('<div class="field"><select data-id="' + idPresence + '" class="velocidad ui search dropdown w-100"><option value="1">1x</option><option value="1.5">1.5x</option><option value="2">2x</option><option value="2.5">2.5x</option></select><i data-val="<" data-id="' + idPresence + '" class="adelantar backward icon" ></i>&nbsp;&nbsp;&nbsp;<i data-val=">" data-id="' + idPresence + '" class="adelantar forward icon"></i></div >');
                $("#Grabacion").text("");
                $("#Grabacion").append(res.message);
                $("#Grabacion").parent().append("<a href='" + res.ruta + "' download='" + grabacion.PHONE + "' ><i class='download icon'></i></a>");
                $(event.target).removeClass("loading");
            });
        } else {
            $(event.target).removeClass("loading");
            console.log("El asesor no tiene llamadas de ayer ni hoy.");
            return;
        }
    });

});

$("body").on("click", ".velocidad", function (event) {

    switch (event.target.value) {
        case "1":
            var grabacion = $("#audio1")[0];
            grabacion.playbackRate = 1;
            break;
        case "1.5":
            var grabacion = $("#audio1")[0];

            grabacion.playbackRate = 1.5;
            break;
        case "2":
            var grabacion = $("#audio1")[0];

            grabacion.playbackRate = 2;
            break;
        case "2.5":
            var grabacion = $("#audio1")[0];

            grabacion.playbackRate = 2.5;
            break;
    }
});
$("body").on("click", ".adelantar", function (event) {
    var adelantar = $(event.target).data("value");
    var grabacion = $("#audio1")[0];
    switch ($(event.target).data("val")) {
        case "<":
            grabacion.currentTime = grabacion.currentTime - 10;
            break;
        case ">":
            grabacion.currentTime = grabacion.currentTime + 10;
            break;
    }
});




