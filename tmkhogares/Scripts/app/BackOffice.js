﻿
class BackGestion extends General {
    constructor() {
        super();
        this.data = {};

    }


    /** 
     * Nombre:   back_guardarLegalizacion
     * Descripcion : Guarda la legalizacion
     */
    back_guardarLegalizacion() {
        if (!confirm("¿Estas seguro de continuar?")) { return false }
        var url = this.url;
        $(".alert-danger").remove();
        this.data = {
            Id: $("#idgestionActivacion").val(),
            EstadoId: $("#EstadoId").val(),
            seguimiento_OT: $("#otCode").val(),
            seguimiento_obs: $("#obsVenta").val(),
            seguimiento_prox_fecha_inst: $("#fechaInstalacion").val(),
            __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
        };
        $.post(url.back + 'GestionLegalizacion', this.data)
            .done((Result, status, statuscode) => {
                if (statuscode.status == 201) {// error en peticion
                    $.each(Result, function (val, elm) {
                        $("#back_guardarLegalizacion").parent().append(Carro.JsonDecodeErrors(elm));
                        Carro.toBottom()
                    });
                }
                else {// peticion ok
                    window.location = BaseUrl + 'GestionActivacion/Soportadas';
                }
                
            })
            .fail((data, status) => {
                if (data.status == 400) {
       
                }
            })

    }


     /* 
     * Nombre:   back_guardarInstalacion
     * Descripcion : Guarda la Instalacion
     */
    back_guardarInstalacion() {
        if (!confirm("¿Estas seguro de continuar?")) { return false }
        var url = this.url;
            $(".alert-danger").remove();
            this.data = {
                Id: $("#idgestionActivacion").val(),
                EstadoId: $("#EstadoId").val(),
                seguimiento_obs: $("#obsVenta").val(),
                // datos para el seguimiento
                Instalacion_EstadoInicial: $("#Instalacion_EstadoInicial").val(),
                Instalacion_EstadoFinal: $("#Instalacion_EstadoFinal").val(),
                Instalacion_RazonAgendamiento: $("#Instalacion_RazonAgendamiento").val(),
                Instalacion_FechaReprogramacion: $("#fechaReprogramacion").val(),
                seguimiento_prox_fecha_inst: $("#fechaInstalacion").val(),
                __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
            };
        $.post(url.back + 'GestionarInstalacion', this.data)
            .done((Result, status, statuscode) => {
                if (statuscode.status == 201) {// error en peticion
                    $.each(Result, function (val, elm) {
                        $("#back_guardarInstalacion").parent().append(Carro.JsonDecodeErrors(elm));
                        Carro.toBottom()
                    });
                }
                else {// peticion ok
                    window.location = BaseUrl + 'GestionActivacion/PendientesInstalar';
                }
                   
                })
                .fail((data, status) => {
                    if (data.status == 400) {

                    }
                })

    }


    /* 
    * Nombre:   back_ActualizaInstalacion
    * Descripcion : Actualiza las tipificaciones de la instalacion sin cambiar el estado
    */
    back_ActualizaInstalacion() {
        if (!confirm("¿Estas seguro de continuar?")) { return false }
        var url = this.url;
        $(".alert-danger").remove();
        this.data = {
            Id: $("#idgestionActivacion").val(),
            EstadoId: $("#EstadoId").val(),
            seguimiento_obs: $("#obsVenta").val(),
            // datos para el seguimiento
            Instalacion_EstadoInicial: $("#Instalacion_EstadoInicial").val(),
            Instalacion_EstadoFinal: $("#Instalacion_EstadoFinal").val(),
            Instalacion_RazonAgendamiento: $("#Instalacion_RazonAgendamiento").val(),
            Instalacion_FechaReprogramacion: $("#fechaReprogramacion").val(),
            seguimiento_prox_fecha_inst: $("#fechaInstalacion").val(),
            __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
        };
        $.post(url.back + 'ActualizarInstalacion', this.data)
            .done((Result, status, statuscode) => {
                if (statuscode.status == 201) {// error en peticion
                    $.each(Result, function (val, elm) {
                        $("#back_guardarInstalacion").parent().append(Carro.JsonDecodeErrors(elm));
                        Carro.toBottom()
                    });
                }
                else {// peticion ok
                    window.location = BaseUrl + 'GestionActivacion/PendientesInstalar';
                }

            })
            .fail((data, status) => {
                if (data.status == 400) {

                }
            })

    }



     /* 
     * Nombre:   GuardarVentadigitada
     * Descripcion : Guarda la Digitacion
     */

    GuardarVentadigitada() {
        if (!confirm("¿Estas seguro de continuar?")) { return false }
        $(".alert-danger").remove();
        this.data = {
            Id: $("#idgestionActivacion").val(),
            EstadoId: $("#EstadoId").val(),
            seguimiento_OT: $("#otCode").val(),
            seguimiento_obs: $("#obsVenta").val(),
            TipificacionBackId: $("#tipificacionBack").val(),
            

            digitacion_NRegistros: $("#digitacion_NRegistros").val(),
            digitacion_contrato: $("#digitacion_contrato").val(),
            digitacion_cuenta: $("#digitacion_cuenta").val(),
            digitacion_franja: $("#digitacion_franja").val(),
            seguimiento_prox_fecha_inst: $("#fechaInstalacion").val(),
            __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
        };
        _console(this.data);
        var url = this.url;
        _console(this.url.back + 'Gestion');
        $.post(this.url.back + 'Gestionar', this.data)
            .done((Result, status, statuscode) => {
                if (statuscode.status == 201) {// error en petici

                        _console("error data");
                        $.each(Result, function (val, elm) {
                            _console(elm);
                            _console(Carro.JsonDecodeErrors(elm));
                            $("#back_guardarGestion").parent().append(Carro.JsonDecodeErrors(elm));
                            Carro.toBottom()
                        });
   

                }
                else {// peticion ok
                    _console("Exito al guardar la gestion.")
                    _console(BaseUrl);
                    window.location = BaseUrl + 'GestionActivacion/PendientesDigitar';
                }



            })
            .fail((data, status) => {

            })
    }
     /* 
     * Nombre:   RecuperarVenta
     * Descripcion : Recupera una venta perdida
     */

    RecuperarVenta(Id , s_elm) {
        if (!confirm("¿Estas seguro de recuperar la venta con el id "+ Id +" ?")) { return false }
        $(".alert-danger").remove();
        
        this.data = {
            Id: Id,
            __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
        };
        
        $.post(this.url.Gestion + 'Recuperar', this.data)
            .done((Result, status, statuscode) => {
                if (statuscode.status == 201) {// error en petici
                    s_elm.parent().append(string_mensaje(Result));
                }
                else {// peticion ok
                    s_elm.parent().append(string_mensaje("venta recuperada con exito", "alert-success"));
                    $("#btnCon").click()
                }
            })
            .fail((data, status) => {

            })
    }



    /* 
    * Nombre:   RecuperarVenta
    * Descripcion : Recupera una venta perdida
    */

    reiniciarAPreventa(Id, s_elm) {
        if (!confirm("¿Estas seguro de devolver esta venta a venta cantada, este paso no se puede deshacer. " + Id + " ?")) { return false }
        $(".alert-danger").remove();

        this.data = {
            Id: Id,
            __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
        };

        $.post(this.url.Gestion + 'DevolverAPreventa', this.data)
            .done((Result, status, statuscode) => {
                if (statuscode.status == 201) {// error en petici
                    s_elm.parent().append(string_mensaje(Result));
                }
                else {// peticion ok
                    s_elm.parent().append(string_mensaje("venta devuelta a preventa", "alert-success"));
                    $("#btnCon").click()
                }
            })
            .fail((data, status) => {

            })
    }



     /* 
     * Nombre:   GuardarVentaAuditada
     * Descripcion : Guarda la Auditada
     */

    GuardarVentaAuditada() {
        if (!confirm("¿Estas seguro de continuar?")) { return false }
        $(".alert-danger").remove();
        this.data = {
            Id: $("#idgestionActivacion").val(),
            EstadoId: $("#EstadoId").val(),
            seguimiento_OT: $("#otCode").val(),
            seguimiento_obs: $("#obsVenta").val(),
            TipificacionBackId: $("#tipificacionBack").val(),
            __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
        };
        
        var url = this.url;

        
        $.post(this.url.back + 'Gestionar', this.data)
            .done((Result, status, statuscode) => {
                if (statuscode.status == 201) {// error en petici

                    _console("error data");
                    $.each(Result, function (val, elm) {
                        _console(elm);
                        _console(Carro.JsonDecodeErrors(elm));
                        $("#back_guardarAuditada").parent().append(Carro.JsonDecodeErrors(elm));
                        Carro.toBottom()
                    });


                }
                else {// peticion ok
                    _console("Exito al guardar la gestion.")
                    _console(BaseUrl);
                    window.location = BaseUrl + 'GestionActivacion/PorAuditar';
                }



            })
            .fail((data, status) => {

            })
    }

}

var Back = new BackGestion();
DEV_MODE = true;
SHOW_ALERT = false;


$("body").on('click', '#back_guardarGestion', function (event) {
    _console("guardarGestion funciona");

    Back.GuardarVentadigitada();
   

});

$("body").on('click', '#back_guardarAuditada', function (event) {
    _console("guardarGestion funciona");
    Back.GuardarVentaAuditada();
});





$("body").on('click', '#back_guardarInstalacion', function (event) {
    Back.back_guardarInstalacion();
    _console("ingresando a guardar instalacion");
});


$("body").on('click', '#back_actualizarInstalacion', function (event) {
    Back.back_ActualizaInstalacion();
    _console("ingresando a Actualizar instalacion");
});



$("body").on('click', '#back_guardarLegalizacion', function (event) {
    Back.back_guardarLegalizacion();
    _console("ingresando a guardar la legalizacion");
});

$("body").on('click', '.recuperarVenta', function (event) {
    id = $(this).data('id');
    var s_elm = $(this);
    Back.RecuperarVenta(id , s_elm);
    _console("Click en recuperar venta");
});

$("body").on('click', '.reiniciarAPreventa', function (event) {
    id = $(this).data('id');
    var s_elm = $(this);
    Back.reiniciarAPreventa(id, s_elm);
    _console("Click en recuperar venta");
});



// sistema de grabaciones.
$("body").on('click', '#consultarComercializacion', function (event) {
    console.log("consultar consultarComercializacion")
    var data = {
        idPresence: $("#presence_Id").text(),
        Tipo: 1,
        IdGestion: $("#span_gestionId").text()
    };
    $.post(Back.url.Grabaciones + 'BuscarGrabacion', data)
        .done((Result, status, statuscode) => {
            if (Result.status == 200) {// error en petici
                $("#audioComercializacion").html(Result.message)
            }
            if (Result.status != 200) {// error en petici
                $("#audioComercializacion").html(Result.message)
            }


        })

});


// sistema de grabaciones.
$("body").on('click', '#consultarRecuperacion', function (event) {


});


// sistema de grabaciones.
$("body").on('click', '#consultarGrabacionLecturaContrato', function (event) {
    
    var data = {
        idPresence: $("#presence_Id").text(),
        Tipo: 2,
        IdGestion: $("#span_gestionId").text() 
    };
    $.post(Back.url.Grabaciones + 'BuscarGrabacion', data)
        .done((Result, status, statuscode) => {
            if (Result.status == 200) {// error en petici
                $("#audioContrato").html(Result.message)
            }
            if (Result.status != 200) {// error en petici
                $("#audioContrato").html(Result.message)
            }

            
        })
});



// Actualizar ventas del bo.


// sistema de grabaciones.
$("body").on('click', '#btnActualizarDatosFichaBo', function (event) {
    $(".alert-danger").remove();
    b_validacion = true;
    primerElementoG = 1;
    $('.campoVentaBack').each(void_validaCampo);
    if (b_validacion == false) { return;}
    var data = {
        Id: $("#gtuuId").val(),
        VENTA_NOMBRE_COMPLETO_CLIENTE: $("#VENTA_NOMBRE_COMPLETO_CLIENTE").val(),
        VENTA_NUMERO_CEDULA: $("#VENTA_NUMERO_CEDULA").val(),
        VENTA_CUENTA_VENTA: $("#VENTA_CUENTA_VENTA").val(),
        captura_Direccion: $("#txt_direccion").val(),
        captura_telefono1: $("#captura_telefono1").val(),
        captura_barrio: $("#captura_barrio").val(),
        captura_telefono2: $("#captura_telefono2").val()
    };
    $.post(/AyudasDetalleVenta/ + 'ActualizarFichaBack', data)
        .done((Result, status, statuscode) => {
            if (Result.status == 200) {// error en petici
               
            }
            if (Result.status != 200) {// error en petici
               
            }


        })
});

