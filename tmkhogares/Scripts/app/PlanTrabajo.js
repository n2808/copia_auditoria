﻿$(document).ready(function () {

    //Eventos
    $("#MetaMensual,#DiasMes,#DiasSemana").keyup(calcularMetas);
    $("#asignarMeta").click(asignarPlanTrabajo);

    //Funciones
    function calcularMetas(){
        const metaMensual = $("#MetaMensual").val(),
              diasMes     = $("#DiasMes").val(),
              diasSemana  = $("#DiasSemana").val();
        if(parseFloat(metaMensual) && parseFloat(diasMes) && parseFloat(diasSemana)) {
            var metaSemanal = (parseFloat(metaMensual) / (diasMes / diasSemana));
            var metaDiaria  = (parseFloat(metaSemanal) / parseFloat(diasSemana));
            $("#MetaSemanal").val(metaSemanal.toFixed(1));
            $("#MetaPromedioDia").val(metaDiaria.toFixed(1));
        } 
    }   

    function asignarPlanTrabajo() {
        const metaMensual = $("#MetaMensual").val(),
              diasMes     = $("#DiasMes").val(),
              diasSemana  = $("#DiasSemana").val();
        if (parseFloat(metaMensual) && parseFloat(diasMes) && parseFloat(diasSemana)) {
            if ((parseFloat(diasMes) >= 1 && parseFloat(diasMes) <= 31)) {
                
            }else{
                alert("Los dia del mes no son validos");
                return;
            }
            if((parseFloat(diasSemana) >= 1 && parseFloat(diasSemana) <= 7)) {

            }else{
                alert("Los dia de la semana no son validos");
                return;
            }
            var metaSemanal = (parseFloat(metaMensual) / (diasMes / diasSemana));
            var metaDiaria  = (parseFloat(metaSemanal) / parseFloat(diasSemana));
            $("#MetaSemanal").val(metaSemanal.toFixed(1));
            $("#MetaPromedioDia").val(metaDiaria.toFixed(1));
        } else {
            alert("Los numeros ingresados");
        }
    }

});