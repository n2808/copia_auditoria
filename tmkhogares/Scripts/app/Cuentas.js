﻿
class ClientesGestiones extends General {
    constructor() {
        super();
        this.data = {};

    }


    /** 
     * Nombre:   AgregarCuenta
     * Descripcion : Guarda la legalizacion
     */
    AgregarCuenta() {
        if (!confirm("¿Estas seguro de continuar?")) { return false }
        var url = this.url;
        $(".alert-danger").remove();
        this.data = {
            IdCity: $("#CUENTA_CIUDAD option:selected").val(),
            ClienteId: $("#ClienteId").val(),
            CUENTA_CODIGO_CLIENTE: $("#CUENTA_CODIGO_CLIENTE").val(),
            CUENTA_NOMBRE_CLIENTE: $("#CUENTA_NOMBRE_CLIENTE").val(),
            CUENTA_TIPO_IDENT: $("#CUENTA_TIPO_IDENT").val(),
            CUENTA_ESTRATO: $("#CUENTA_ESTRATO").val(),
            CUENTA_CIUDAD:  $('#CUENTA_CIUDAD option:selected').text(),
            CUENTA_DIRECCION: $("#CUENTA_DIRECCION").val(),
            TELEFONO_1: $("#TELEFONO_1").val(),
            REPRESENTANTE_NOMBRE: $("#REPRESENTANTE_NOMBRE").val(),
            __RequestVerificationToken: $("input[name='__RequestVerificationToken'").val()
        };
        $.post(url.Cuentas + 'create', this.data)
            .done((Result, status, statuscode) => {
                if (statuscode.status == 400) {// error en peticion
                    $.each(Result, function (val, elm) {
                        $("#back_guardarLegalizacion").parent().append(Carro.JsonDecodeErrors(elm));
                        Carro.toBottom()
                    });
                }
                else {
                    $(".form").append(string_mensaje("Cuenta Agregada con exito", "Direccion : " + $("#CUENTA_DIRECCION").val(), "positive"))
                    window.location.href = "/Clientes/Edit/" + $("#ClienteId").val();
                }

            })
            .fail((data, status) => {
                if (data.status == 400) {

                    swal({
                        position: 'top-right',
                        type: 'error',
                        title: data.responseJSON[0].Message,
                        showConfirmButton: false,
                        timer: 1800
                    });
                }
            })
    }
}

var Cuenta = new ClientesGestiones()

$("body").on('click', '#AgregarCuenta', function (event) {
    Cuenta.AgregarCuenta();
});