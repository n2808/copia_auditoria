﻿$("body").on("click", "#GuardarArchivosOrden", function (e) {
    if ($('#fileUpload')[0].files[0] == undefined) {
        swal({
            position: 'top-right',
            type: 'error',
            title: 'Por favor seleccione el archivo',
            showConfirmButton: false,
            timer: 1200
        });
        return false;
    } else {
        data = new FormData();
        data.append('file', $('#fileUpload')[0].files[0]);
        data.append("idOrden", $("#gtuuId").val());
        data.append("descripcion", $('#TipoArchivoOrden option:selected').text());

        $.ajax({
            type: "POST",
            url: '/Archivos/CargarArchivoOrden',
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                console.log(result);
                var _historialArchivoOrden = result;

                if (_historialArchivoOrden.type == "OK") {
                    ObtenerListaArchivos();
                    swal({
                        position: 'top-right',
                        type: 'success',
                        title: _historialArchivoOrden.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else {
                    swal({
                        position: 'top-right',
                        type: 'error',
                        title: _historialArchivoOrden.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }

                $('#formCargaArchivoOrden').trigger("reset");
            },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;
                console.log(err);
            }

        })
    }



});


/*file upload*/
$("input:text").click(function () {
    $(this).parent().find("input:file").click();
});

$('input:file', '.ui.action.input')
    .on('change', function (e) {
        var name = e.target.files[0].name;
        console.log(name);
        $('input:text', $(e.target).parent()).val(name);
    });

/*file upload*/
function ObtenerListaArchivos() {

    $.get("/Archivos/HistorialArchivosOrden", { id: $("#gtuuId").val() })
        .done((Result) => {
            console.log(Result);
            var _historialArchivosOrden = Result.message;
            //agregar historial archivo a la tabla

            AgregarATabla(_historialArchivosOrden, "#historialArchivoOrden")
        })
        .fail((data, status) => {
            _console("error interno ");
            _console(data);
            _console(status);

        })


}

function AgregarATabla(Item, tbl) {

    if (tbl == "#historialArchivoOrden") {


        if (Item.TipoArchivo.Name == "Excel" || Item.TipoArchivo.Name == "word") {

            var boton = "<td><a id='descargarArchivo' value='Descargar' class='ui positive button' href='/Archivos/DescargarArchivo/" + Item.Id + "'>Descargar</a>" +
                "<a id='eliminarArchivoOrden' name='eliminarArchivoOrden' class='ui orange button'>Eliminar</a></td>";
        }
        if (Item.TipoArchivo.Name == "pdf") {

           var boton = '<td><input type="button" class="ui primary button"  value="Detalle" onclick="DetallePdf(\'' + Item.Archivo + '\');" >' +
                "<a id='descargarArchivo' value='Descargar' class='ui positive button' href='/Archivos/DescargarArchivo/" + Item.Id + "'>Descargar</a>" +
               "<a id='eliminarArchivoOrden' name='eliminarArchivoOrden' class='ui orange button'>Eliminar</a></td>";
        }
        if (Item.TipoArchivo.Name == 'Imagen') {

            var boton =
                '<td><input type="button" class="ui primary button"  value="Detalle" onclick="DetalleOrden(\'' + Item.Archivo + '\');" >' +
                "<a id='descargarArchivo' value='Descargar' class='ui positive button' href='/Archivos/DescargarArchivo/" + Item.Id + "'>Descargar</a>" +
                "<a id='eliminarArchivoOrden' name='eliminarArchivoOrden' class='ui orange button'>Eliminar</a></td>";
        }
        if (Item.TipoArchivo.Name == 'Texto') {

            var boton =
                "<td><strong>" + Item.Archivo + "</strong></td>";
        }

        const ElementItem =

            "<tr>" +
            "<td id='idArchivoOrden' style='display: none'>" + Item.Id + "</td>" +
            "<td>" + Item.Nombre + "</td>" +
            "<td>" + Item.Descripcion + "</td>" +
            boton



        $(tbl).prepend(ElementItem);

    }

};

function DetalleOrden(id) {

    $("#detallearchivoOrden").show();

    $('#ImagenOrden').attr('src', 'data:image/jpeg;base64,' + id).show();

}

function DetallePdf(id) {
    $("#detallePdfOrden").show();
    $('#PdfOrden').attr('src', 'data:application/pdf;base64,' + id).show();

}

function CerrarDetallePdf() {

    $("#detallePdfOrden").hide();
}

function CerrarDetalle() {

    $("#detallearchivoOrden").hide();
}

$("body").on("click", "#tablaArchivo #eliminarArchivoOrden", function () {

    var row = $(this).closest("tr");
    var idArchivo = row.find("#idArchivoOrden").html();


    if (confirm("Confirma que desea eliminar el archivo " + "?")) {

        $.post("/Ordens/EliminarArchivoOrden", { id: idArchivo })
            .done((Result) => {
                swal({
                    position: 'top-right',
                    type: 'success',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                row.remove();
            })
            .fail((data, status) => {
                _console("error interno ");
                _console(data);
                _console(status);
                swal({
                    position: 'top-right',
                    type: 'error',
                    title: Result.message,
                    showConfirmButton: false,
                    timer: 1500
                });

            });
    }
});