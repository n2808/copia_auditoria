﻿$("#CargarUsuarios").click(function () {

    if (window.FormData == undefined)
        alert('Error: FormData is undefined');

    else {
        var fileUpload = $('#fileUsuarios').get(0);
        var files = fileUpload.files;
        var campana = $('#CampanaUsuario option:selected').val();

        if (campana == "0") {

            $('#errorCampana').text('El campo Campana es obligatorio').css('color', 'red');
        }

        if (files.length === 0) {

            $('#errorArchivo').text('Debe seleccionar un archivo').css('color', 'red');

        } else {
            var fileData = new FormData();
            var mensaje = "";

            fileData.append(files[0].name, files[0]);
            fileData.append('idCampana', campana);
            $.ajax({
                url: '/CargueUsuarios/CargarArchivoUsuarios',
                type: 'post',
                datatype: 'json',
                contentType: false,
                processData: false,
                async: true,
                data: fileData,
                success: function (response) {

                    if (response.status === 201) {

                        Swal.fire({
                            position: 'top-right',
                            icon: 'success',
                            title: "success",
                            text: "Archivo Cargado Correctamente",
                            showConfirmButton: false,
                            timer: 2000
                        });

                        $("#mostrarErrores").css("display", "none");
                        $('#CampanaUsuario').val("");
                        limpiarFileInput("fileUsuarios");


                    }
                    else if (response.status === 500) {                                            

                        Swal.fire({
                            position: 'top-right',
                            icon: 'error',
                            title: "error",
                            text: "Error al cargar el archivo, consulte con el administrador",
                            showConfirmButton: false,
                            timer: 1500
                        });

                        $('#CampanaUsuario').val("");
                        limpiarFileInput("fileUsuarios");
                    }
                    else {                     

                        Swal.fire({
                            position: 'top-right',
                            icon: 'error',
                            title: "error",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 3000
                        });

                   
                        if (response.message.includes('Error Formato')) {
                            $("#mostrarErrores").css("display", "block");
                        }

                        $('#CampanaUsuario').val("");
                        limpiarFileInput("fileUsuarios");


                    }



                }
            });
        }

    }


    $('body').on('change', '#CampanaUsuario', function () {
        $('#errorCampana').text('');
    });

    $('body').on('change', '#fileUsuarios', function () {
        $('#errorArchivo').text('');
    });

    function limpiarFileInput(idFileInput) {
        document.getElementById(idFileInput).value = "";
    }

});