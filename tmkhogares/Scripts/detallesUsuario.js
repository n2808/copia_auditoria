﻿$(document).ready(function () {
    $(".pesoSelect select").each(function (index, val) {
        var estandarId = $(val).data("estandar");
        var porcentaje = 0;
        $("div." + estandarId + " select option:selected").each(function (index, val) {
            if ($(val).data("peso") != "100,00") {
                porcentaje += parseFloat($(val).data("peso").replace(",", "."));
            }        });
        parseFloat($("span#" + estandarId).text(porcentaje));
        calcularTotal();
    });
    function calcularTotal() {
        var errorCritico = false;
        var seCuentaEnTotal = false;
        var valorItemSeCuentaEnTotal = 0;

        $("select option:selected").each(function (index, val) {

            if ($(val).data("critico") === "si" && $(val).data("peso") != "100,00") errorCritico = true;

            if ($(val).data("secuentaentotal") === "no") {
                valorItemSeCuentaEnTotal += parseFloat($(val).data("peso"));
                seCuentaEnTotal = true;
            }

        }); 

        var totalCalificacion = 0;
        $(".titulosTotales").each(function (index, val) {
            totalCalificacion += parseFloat($(val).text());
        });

        if (errorCritico) {
            $("#totalCalificacion").text(0);
        }
        else if (seCuentaEnTotal && valorItemSeCuentaEnTotal > 0) {
            totalCalificacion -= valorItemSeCuentaEnTotal;
            $("#totalCalificacion").text(Math.round(totalCalificacion));
        }
        else {
            $("#totalCalificacion").text(Math.round(totalCalificacion));
        }

    }
});

