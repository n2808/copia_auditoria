﻿$("#CargarUsuarios").click(function () {

    if (window.FormData == undefined)
        alert('Error: FormData is undefined');

    else {
        var fileUpload = $('#fileUsuarios').get(0);
        var files = fileUpload.files;
      

        if (files.length === 0) {

            $('#errorArchivo').text('Debe seleccionar un archivo').css('color', 'red');

        } else {
            var fileData = new FormData();
            var mensaje = "";

            fileData.append(files[0].name, files[0]);
         
            $.ajax({
                url: '/CargueUsuarios/EditarTeamMasivo',
                type: 'post',
                datatype: 'json',
                contentType: false,
                processData: false,
                async: true,
                data: fileData,
                success: function (response) {

                    if (response.status === 201) {

                        Swal.fire({
                            position: 'top-right',
                            icon: 'success',
                            title: "success",
                            text: "Archivo Cargado Correctamente",
                            showConfirmButton: false,
                            timer: 2000
                        });

                        $("#mostrarErrores").css("display", "none");                     
                        limpiarFileInput("fileUsuarios");


                    }
                    else if (response.status === 500) {                                            

                        Swal.fire({
                            position: 'top-right',
                            icon: 'error',
                            title: "error",
                            text: "Error al cargar el archivo, consulte con el administrador",
                            showConfirmButton: false,
                            timer: 1500
                        });

                        $('#CampanaUsuario').val("");
                        limpiarFileInput("fileUsuarios");
                    }
                    else {                     

                        Swal.fire({
                            position: 'top-right',
                            icon: 'error',
                            title: "error",
                            text: response.message,
                            showConfirmButton: false,
                            timer: 3000
                        });

                   
                        if (response.message.includes('Error Formato')) {
                            $("#mostrarErrores").css("display", "block");
                        }
                       
                        limpiarFileInput("fileUsuarios");


                    }



                }
            });
        }

    }


 

    $('body').on('change', '#fileUsuarios', function () {
        $('#errorArchivo').text('');
    });

    function limpiarFileInput(idFileInput) {
        document.getElementById(idFileInput).value = "";
    }

});