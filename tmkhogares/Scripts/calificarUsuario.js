﻿$(document).ready(function () { 


    $("#btnCalificar").click(function () {


        if (!$('#formCalificacion')[0].reportValidity()) {
            return;
        }

        var d = new Date();      
        
        var fechaEntrada = new Date($('#fechaLlamada').val());  

        if (fechaEntrada > d) {
            alert("La fecha de entrada no puede ser mayor a la actual");
            return false;
        }            
                

        var calificacionId = $('#calificacionId').val();
        var asesorId = $('#asesorId').val();
        var calidadId = $('#calidadId').val();
        var campanaId = $('#campanaId').val();
        var formularioId = $('#formularioId').val();
        var loginAsesor = $('#loginAsesor').val();
        var telefonoGrabacion = $('#telefonoGrabacion').val();
        var idGrabacion = $('#idGrabacion').val();
        var duracionTMO = $('#duracionTMO').val();
        var descripcionGeneral = $('#descripcionGeneral').val();
        var comentariosAsesor = $('#comentariosAsesor').val();
        var tipoMonitoreo = $('#tipoMonitoreo option:selected').val();
        var subCampana = $('#subCampana option:selected').val();
        var ciudad = $('#ciudad option:selected').val();
        var motivoConsulta = $('#motivoConsulta').val();
        var marcacion = $('#marcacion').val();
        var fechaLlamada = $('#fechaLlamada').val();
        var tipologia = $('#tipologia').val();
        var segmento = $('#segmento option:selected').val();
        var procede = $('#procede option:selected').val();
        var esVenta = $('#esVenta option:selected').val();
        var esVentaContado = $('#esVentaContado option:selected').val();
        var esVentaFinaciada = $('#esVentaFinaciada option:selected').val();
        var esTodoClaro = $('#esTodoClaro option:selected').val();
        var utilizaGuionBienvenida = $('#utilizaGuionBienvenida option:selected').val();
        var utilizaGuionDespedida = $('#utilizaGuionDespedida option:selected').val();
        var esVentaClaroUp = $('#esVentaClaroUp option:selected').val();
        var esRetenido = $('#esRetenido option:selected').val();
      

        var dataCalificacion = [];
        $("select.pesoSelect").each(function (index, val) {
            var estandarId = $(val).data("estandar");
            var calificacion = $(val).children("option:selected").data("seleccion");
            var tipoAtributo = $(val).children("option:selected").data("tipoid");
            var atributoId = $(val).children("option:selected").data("atributoid");
            var posicion = $(val).children("option:selected").data("posicion");
            var descripcion = $(val).parents("tr").find("textarea").val();

            dataCalificacion.push({
                "EstandarId": estandarId,
                "Atributos": {
                    "AtributoId": atributoId,
                    "Descripcion": descripcion,
                    "Calificacion": calificacion,
                    "TipoAtributo": tipoAtributo,
                    "Posicion": posicion
                }
            });
        });



        var data = {

            CalificacionId: calificacionId,
            AsesorId: asesorId,
            CalidadId: calidadId,
            CampanaId: campanaId,
            FormularioId: formularioId,
            LoginAsesor: loginAsesor,
            TelefonoGrabacion: telefonoGrabacion,
            IdGrabacion: idGrabacion,
            DuracionTMO: duracionTMO,
            DescripcionGeneral: descripcionGeneral,
            ComentariosAsesor: comentariosAsesor,
            DataCalificacion: dataCalificacion,
            TipoMonitoreo: tipoMonitoreo,
            SubCampana: subCampana,
            Ciudad: ciudad,
            MotivoConsulta: motivoConsulta,
            Segmento: segmento,
            Marcacion: marcacion,
            Procede: procede,
            EsVenta: esVenta,
            EsVentaContado: esVentaContado,
            EsVentaFinaciada: esVentaFinaciada,
            EsTodoClaro: esTodoClaro,
            UtilizaGuionBienvenida: utilizaGuionBienvenida,
            UtilizaGuionDespedida: utilizaGuionDespedida,
            EsVentaClaroUp: esVentaClaroUp,
            EsRetenido: esRetenido,
            FechaLlamada: fechaLlamada,         
            Tipologia: tipologia

        }


     
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/Calificaciones/Calificar",
            data: data,
            success: function (data) {

                if (data.status == "ok") {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: "" + data.notaFinal + "%",
                        text: "Calificación Exitosa",
                        showConfirmButton: false,
                        timer: 2500
                    });

                    setTimeout(function () {
                        limpiarCampos();
                        window.location.href = "/Home/HomeAdmin";

                    }, 2500);


                }


            },
            error: function (e) {
                console.log(e.message);
            }
        });


    })

    function limpiarCampos() {
        $("select.pesoSelect").val("");
        $('#telefonoGrabacion').val("");
        $('#idGrabacion').val("");
        $('#descripcionGeneral').val("");
        $('#comentariosAsesor').val("");
        $('#tipoMonitoreo option:selected').val("");
        $('#procede option:selected').val("");
        $('#esVenta option:selected').val("");
        $('#esVentaContado option:selected').val("");
        $('#esVentaFinaciada option:selected').val("");
        $('#esTodoClaro option:selected').val("");
        $('#utilizaGuionBienvenida option:selected').val("");
        $('#utilizaGuionDespedida option:selected').val("");
        $('#esVentaClaroUp option:selected').val("");
    }
});

$("body").on("change", "#tipoMonitoreo", function () {
    if ($("#tipoMonitoreo option:selected").val() == "PQR") {
        $(".procede").css('display', 'block');
    } else {
        $(".procede").css('display', 'none');
        $('#procede option:selected').val("NO");

    }
});


$("body").on("change", "#esVenta", function (event) {

    var venta = $('#esVenta').val();

    if (venta == "1") {
        $('#ventaContado').css('display', 'block');
    } else {
        $('#ventaContado').css('display', 'none');
        $('#tipoVenta').css('display', 'none');
        $('#esVentaContado option:selected').val("NO");
        $('#esVentaFinaciada option:selected').val("NO");
    }
});

$("body").on("change", "#esVentaContado", function (event) {

    var ventaContado = $('#esVentaContado').val();

    if (ventaContado == "SI") {
        $('#tipoVenta').css('display', 'block');
    } else {
        $('#tipoVenta').css('display', 'none');
    }
});





