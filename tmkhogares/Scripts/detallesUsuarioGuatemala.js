﻿$(document).ready(function () {

    $(".pesoSelect select").each(function (index, val) {
        var estandarId = $(val).data("estandar");
        var porcentaje = 0;
        $("div." + estandarId + " select option:selected").each(function (index, val) {
            if ($(val).data("peso") != "100,00") {
                porcentaje += parseFloat($(val).data("peso").replace(",", "."));
            }
        });
        parseFloat($("span#" + estandarId).text(porcentaje));
        calcularTotal();
    });

    function calcularTotal() {

        var errorCritico = false;
        var seCuentaEnTotal = false;
        var valorItemSeCuentaEnTotal = 0;

        $("select option:selected").each(function (index, val) {

            if ($(val).data("critico") === "si" && $(val).data("peso") != "100,00") errorCritico = true;

            if ($(val).data("secuentaentotal") === "no") {
                valorItemSeCuentaEnTotal += parseFloat($(val).data("peso"));
                seCuentaEnTotal = true;
            }

        });

        var totalCalificacion = 0;
        $(".titulosTotales").each(function (index, val) {
            totalCalificacion += parseFloat($(val).text());
        });

        if (errorCritico) {
            $("#totalCalificacion").text(0);
        }
        else if (seCuentaEnTotal && valorItemSeCuentaEnTotal > 0) {
            totalCalificacion -= valorItemSeCuentaEnTotal;
            $("#totalCalificacion").text(Math.round(totalCalificacion));
        }
        else {
            $("#totalCalificacion").text(Math.round(totalCalificacion));
        }
    }
});

$("body").on("change", ".pesoSelect select", function (event) {
    var estandarId = $(val).data("estandar");
    var porcentaje = 0;
    var totalEstandares = false;

    $("div." + estandarId + " select option:selected").each(function (index, val) {
        if ($(val).data("peso") != "100,00") {
            if ($(val).data("peso") == "0,00") {
                totalEstandares = true;
            } else {
                porcentaje += parseFloat($(val).data("peso").replace(",", "."));
            }
        }
    });
    if (totalEstandares) {
        parseFloat($("span#" + estandarId).text(0));
    } else {
        parseFloat($("span#" + estandarId).text(Math.round(porcentaje)));
    }   
});

function calcularTotal() {
    var errorCritico = false;
    $("select option:selected").each(function (index, val) {
        if ($(val).data("critico") == "si") {
            if ($(val).data("peso") != "100,00") {
                errorCritico = true;
            }
        }
    });
    var totalCalificacion = 0;
    $(".titulosTotales").each(function (index, val) {
        totalCalificacion += parseFloat($(val).text());
    });
    if (errorCritico) {
        console.log(totalCalificacion);
        if ((totalCalificacion - 50) <= 0) {
            $("#totalCalificacion").text(0);

        } else {
            $("#totalCalificacion").text((totalCalificacion));
        }

    } else {
        console.log(totalCalificacion);

        $("#totalCalificacion").text((totalCalificacion ) );

    }
}