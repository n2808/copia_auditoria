﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.Configuration
{
    public class Nivel : EntityWithIntId
    {
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public int Monitoreos { get; set; }

    }
}