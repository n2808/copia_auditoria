﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.Promedios
{
    public class PromedioAsesor : EntityWithIntId
    {
        public int CampañaAsesor { get; set; }
        public string NombreAsesor { get; set; }
        public string DocumentoAsesor { get; set; }
        public string VentasAsesor { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}