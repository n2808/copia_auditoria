﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using tmkhogares.Models.Configuration;
using tmkhogares.Models.UsuariosCampanas;

namespace Core.Models.User
{
    public class User : Entity
    {
        [Required]
        [DisplayName("Documento")]
        public string Document { get; set; }

        [Required]
        [DisplayName("Nombre Completo")]
        public string Names { get; set; } 

        [DisplayName("Correo")]
        public string Email { get; set; } = "";

        [Required]
        [DisplayName("Estado")]
        public bool Status { get; set; }

        [Required]
        [DisplayName("password")]
        public string PassWord { get; set; }
        [Required]
        public string login { get; set; }
        public List<UserRol> UserRol { get; set; }
        public List<UsuarioCampanas> UsuarioCampanas { get; set; }
        public Guid? UserId { get; set; }
        public int? NivelId { get; set; }



        [ForeignKey("UserId")]
        public User TeamLeader { get; set; }

        [ForeignKey("NivelId")]
        public Nivel Nivel { get; set; }



    }
}