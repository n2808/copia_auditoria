﻿using Core.Models.Common;
using System.ComponentModel;

namespace Core.Models.User
{
    public class Rol : Entity
	{
        [DisplayName("Perfil")]
		public string Name { get; set; }
        public bool Status { get; set; } = true;
    }
}