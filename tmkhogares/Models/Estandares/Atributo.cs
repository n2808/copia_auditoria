﻿using Core.Models.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using tmkhogares.Models.Calificacion;

namespace tmkhogares.Models.Estandares
{
    public class Atributo : Entity
    {
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public decimal Peso { get; set; }

        public Guid EstandarId { get; set; }

        public Estandar Estandares { get; set; }       
        public int Posicion { get; set; } = 0;        
        public int? AtributoModeloGanaId { get; set; } = 0;
        public bool Estado { get; set; } = true;
        public bool AplicaModeloGana { get; set; } = false;
        public Guid? TipoAtributoId { get; set; }

        [ForeignKey("TipoAtributoId")]
        public Core.Models.configuration.Configuration TipoAtributo { get; set; }

        [ForeignKey("AtributoModeloGanaId")]
        public AtributoModeloGana AtributoModeloGana { get; set; }

    }
}