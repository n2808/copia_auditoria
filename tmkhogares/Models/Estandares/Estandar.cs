﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using tmkhogares.Models.Campañas;

namespace tmkhogares.Models.Estandares
{
    public class Estandar : Entity
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public decimal Peso { get; set; }
        public  Guid FormularioId { get; set; }
        public int Posicion { get; set; } = 0;
        public Formulario Formulario { get; set; }
        public bool Estado { get; set; } = true;
        public  ICollection<Atributo> Atributos { get; set; }

    }
}