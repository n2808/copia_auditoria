﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmkhogares.Models.UsuariosCampanas
{
    public class UsuarioCampanas : Entity
    {

        public int CampanaId { get; set; }

        public Guid UserId { get; set; }

        [ForeignKey("UserId")]
        public User User{ get; set; }

        [ForeignKey("CampanaId")]

        public Campañas.Campana Campana{ get; set; }


    }
}