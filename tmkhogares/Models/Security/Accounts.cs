﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Core.Models.Security
{
	public class Accounts
	{
		public Guid? Id { get; set; }
		public string UserName { get; set; }
		public string Name { get; set; }
		public string Password { get; set; }
		public string[] Roles { get; set; }
	}
}