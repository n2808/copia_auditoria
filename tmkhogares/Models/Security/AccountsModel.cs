﻿
using System;
using System.Collections.Generic;
using System.Linq;
using tmkhogares.Models;

namespace Core.Models.Security
{
    public class AccountsModel
	{
        private Contexto db = new Contexto();

        private List<Accounts> ListAccounts = new List<Accounts>();


		public Accounts find(Guid? Id = null)
		{
			var Users = db.Usuario.Find(Id);
			string[] Roles = db.UsuariosRoles.Where(c => c.User.Id == Users.Id).Select(c => (string)c.Rol.Name).ToArray();
			Accounts UserAccount = new Accounts
			{
				UserName = Users.Names,
				Roles = Roles,
				Id = Users.Id
			};
			return UserAccount;
		}

		public Accounts Login(string UserName, string Password) {
			var Users = db.Usuario.Where(c => c.login == UserName && c.PassWord == Password && c.Status == true).FirstOrDefault();
			if (Users != null) {
				string[] Roles = db.UsuariosRoles.Where(c => c.User.Id == Users.Id).Select(c => (string)c.Rol.Name).ToArray();

                Accounts UserAccount = new Accounts
				{
					UserName = Users.Names
					,
					Roles = Roles
					,
					Id = Users.Id
					,
					Name = Users.Names
				};
				return UserAccount;
			}
			{
				return null; 
			}
			
		}
	}
} 