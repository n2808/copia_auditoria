﻿using Core.Models.Common;
using Core.Models.User;
using System.Collections.Generic;
using System.ComponentModel;

namespace tmkhogares.Models.Campañas
{
    public class Campana : EntityWithIntId
    {
       
        [DisplayName("Nombre Campaña")]
        public string Nombre { get; set; }        
        public string Descripcion { get; set; }
        public bool Estado { get; set; } = true;
        public int? PadreId { get; set; }
        public string MetaMensual { get; set; }
        public  ICollection<Formulario> Formularios { get; set; }        
        public ICollection<User> Usuarios { get; set; }

    }
}