﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using tmkhogares.Models.Configuration;
using tmkhogares.Models.Estandares;

namespace tmkhogares.Models.Campañas
{
    public class Formulario : Entity
    {
        public string Nombre { get; set; }
        public decimal NotaCritica { get; set; }
        public decimal NotaNoCritica { get; set; }
        public decimal PorcentajeGeneral { get; set; }
        public decimal PorcentajeProceso { get; set; }
        public int CampanaId { get; set; }       
        public int NivelAsesor { get; set; }
        public ICollection<Estandar> Estandares { get; set; }                                                                                                                                                       
        public Guid TipoRol { get; set; } = new Guid("C2FA35E2-1FAF-4F68-945D-9F5262484ED1");
        [ForeignKey("CampanaId")]
        public Campana Campana { get; set; }

        [ForeignKey("NivelAsesor")]
        public Nivel Nivel { get; set; }

    }
}