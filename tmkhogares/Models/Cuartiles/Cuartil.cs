﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.Cuartiles
{
    public class Cuartil : Entity
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Numero { get; set; } 
    }
}