﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.PlanTrabajoTeam
{
    public class PlanTrabajoTeam : Entity
    {
        public decimal MetaPromedioDia { get; set; }
        public int DiasSemana { get; set; }
        public decimal MetaSemanal { get; set; }
        public int DiasMes { get; set; }
        public int MetaMensual { get; set; }
        public string ObservacionesARealizar { get; set; }
        public string CompromisoTeam { get; set; }
        public string DescripcionPlanAccion { get; set; }
        public string NombreCampana { get; set; }
        public string NombreGerente { get; set; }
        public string NombreTeam { get; set; }
        public string CedulaTeam { get; set; }
        public string LoginTeam { get; set; }
        public bool Estado { get; set; }
        public string CantidadColaboradores { get; set; }
        public string Ausentismo { get; set; }
        public string Rotacion { get; set; }
        public int CampanaId { get; set; }
        public Guid TeamId { get; set; }
        public Guid GerenteId { get; set; }
        [ForeignKey("TeamId")]
        public User Team { get; set; }
        [ForeignKey("GerenteId")]
        public User Gerente { get; set; }
        public Guid? SeguimientoSemana1 { get; set; }
        [ForeignKey("SeguimientoSemana1")]
        public SeguimientoTeam Seguimiento1 { get; set; }
        public Guid? SeguimientoSemana2 { get; set; }
        [ForeignKey("SeguimientoSemana2")]
        public SeguimientoTeam Seguimiento2 { get; set; }
        public Guid? SeguimientoSemana3 { get; set; }
        [ForeignKey("SeguimientoSemana3")]
        public SeguimientoTeam Seguimiento3 { get; set; }
        public Guid? SeguimientoSemana4 { get; set; }
        [ForeignKey("SeguimientoSemana4")]
        public SeguimientoTeam Seguimiento4 { get; set; }

    }
}