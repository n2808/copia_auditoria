﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.PlanTrabajoTeam
{
    public class SeguimientoTeam : Entity
    {
        public string MetaPromedio { get; set; }
        public string PromedioInicio { get; set; }
        public string PromedioFinalizo { get; set; }
        public string CantidadVentasMeta { get; set; }
        public string CantidadVentasRealizadas { get; set; }
        public string PromedioSemana1 { get; set; }
        public string PromedioSemana2 { get; set; }
        public string PromedioSemana3 { get; set; }
        public string PromedioSemana4 { get; set; }
        public string DiasAntiguo { get; set; }
        public string PorcentajeCumplimiento { get; set; }
        public string PorcentajeAusentismo { get; set; }
        public string Evolucion { get; set; }
        public string NotaCalidad { get; set; }
        public string RetroAlimentacion { get; set; }
        public string Compromiso { get; set; }
        public bool Estado { get; set; } = false;

        public Guid TeamId { get; set; }
        [ForeignKey("TeamId")]
        public User Team { get; set; }
        public Guid PlanTrabajoTeamId { get; set; }
        [ForeignKey("PlanTrabajoTeamId")]
        public PlanTrabajoTeam PlanTrabajoTeam { get; set; }
    }
}