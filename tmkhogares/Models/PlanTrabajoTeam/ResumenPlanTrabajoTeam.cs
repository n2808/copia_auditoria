﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.PlanTrabajoTeam
{
    public class ResumenPlanTrabajoTeam : Entity
    {
        public string Promedio { get; set; }
        public int VentasRealizadas { get; set; }
        public string Cumplimiento { get; set; }
        public string Evolucion { get; set; }
        public string Ausentismo { get; set; }
        public string NotaCalidad { get; set; }
    }
}