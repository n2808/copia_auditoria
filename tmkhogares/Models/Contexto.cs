﻿namespace tmkhogares.Models
{
    using Core.Models.configuration;
    using Core.Models.User;
    using System.Data.Entity;
    using tmkhogares.Models.Calificacion;
    using tmkhogares.Models.Campañas;
    using tmkhogares.Models.Configuration;
    using tmkhogares.Models.Estandares;
    using tmkhogares.Models.UsuariosCampanas;

    public class Contexto : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'Contexto' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'tmkhogares.Models.Contexto' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'Contexto'  en el archivo de configuración de la aplicación.
        public Contexto()
            : base("name=Contexto")
        {

        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        #region GRABACIONES
        public DbSet<Grabaciones.Grabacion> Grabaciones { get; set; }
        #endregion

        #region CONFIGURATIONS
        public DbSet<Category> Categories { get; set; }
        public DbSet<Core.Models.configuration.Configuration> Configurations { get; set; }
        public DbSet<Nivel> Niveles { get; set; }

        #endregion

        #region CAMPAÑAS
        public DbSet<Campana> Campana { get; set; }

        #endregion

        #region ESTANDARES 
        public DbSet<Estandar> Estandar { get; set; }
        public DbSet<Atributo> Atributo { get; set; }
        public DbSet<ModeloGana> ModeloGana { get; set; }
        public DbSet<AtributoModeloGana> AtributoModeloGana { get; set; }
        


        #endregion

        #region USUARIOSCAMPANAS
        public DbSet<UsuarioCampanas> UsuariosCampanas { get; set; }
        #endregion

        #region USUARIOS
        public DbSet<User> Usuario { get; set; }
        public DbSet<Rol> Roles { get; set; }
        public DbSet<UserRol> UsuariosRoles { get; set; }
        #endregion

        #region CALIFICACIONES
        public System.Data.Entity.DbSet<tmkhogares.Models.Calificacion.Calificacion> Calificacions { get; set; }

        public System.Data.Entity.DbSet<tmkhogares.Models.Calificacion.CalificacionEstandarAtributo> CalificacionEstandarAtributoes { get; set; }
        #endregion

        #region CUARTIL
        public DbSet<Cuartiles.Cuartil> Cuartiles { get; set; }
        public DbSet<Cuartiles.TipoCuartil> TipoCuartiles { get; set; }

        #endregion

        #region FORMULARIOS
        public System.Data.Entity.DbSet<tmkhogares.Models.Campañas.Formulario> Formularios { get; set; }
        #endregion

        #region CAMPAÑASCUARTILES
        public DbSet<CampañasCuartiles.CampañasCuartiles> CamapanasCuartiles { get; set; }
        #endregion

        #region PLANTRABAJOASESOR
        public DbSet<PlanTrabajoAsesor.PlanTrabajoAsesor> PlanTrabajoAsesor { get; set; }
        public DbSet<PlanTrabajoAsesor.SeguimientoAsesor> SeguimientoAsesor { get; set; }
        public DbSet<PlanTrabajoAsesor.ResumenPlanTrabajoAsesor> ResumenPlanTrabajoAsesor { get; set; }

        #endregion

        #region PLANTRABAJOTEAM
        public DbSet<PlanTrabajoTeam.PlanTrabajoTeam> PlanTrabajoTeam { get; set; }
        public DbSet<PlanTrabajoTeam.SeguimientoTeam> SeguimientoTeam{ get; set; }
        public DbSet<PlanTrabajoTeam.ResumenPlanTrabajoTeam> ResumenPlanTrabajoTeam{ get; set; }

        #endregion

        #region PROMEDIOASESORES
        public DbSet<Promedios.PromedioAsesor> PromedioAsesores { get; set;}
        #endregion 


    }

}