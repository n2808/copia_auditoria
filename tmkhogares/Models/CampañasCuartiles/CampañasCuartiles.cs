﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.CampañasCuartiles
{
    public class CampañasCuartiles : Entity
    {
        public int CampanaId { get; set; }
        public Guid CuartilId { get; set; }
        public Guid TipoCuartilId { get; set; }
        public string promedio { get; set; }
        
        [ForeignKey("CampanaId")]
        public Campañas.Campana Campana { get; set; }

        [ForeignKey("CuartilId")]
        public Cuartiles.Cuartil Cuartil { get; set; }

        [ForeignKey("TipoCuartilId")]
        public Cuartiles.TipoCuartil TipoCuartiles{ get; set; }


    }
}