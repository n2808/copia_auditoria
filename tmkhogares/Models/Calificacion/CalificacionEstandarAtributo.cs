﻿using Core.Models.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using tmkhogares.Models.Estandares;

namespace tmkhogares.Models.Calificacion
{
    public class CalificacionEstandarAtributo : Entity
    {
        public Guid CalificacionId { get; set; }
        public Guid? ItemCalificadoId { get; set; }
        public Guid AtributoId { get; set; }
        public Guid EstandarId{ get; set; }
        public decimal Puntaje { get; set; }
        public string Descripcion { get; set; }
        public int PosicionAtributo { get; set; }

        [ForeignKey("EstandarId")]
        public Estandar Estandar { get; set; }

        [ForeignKey("CalificacionId")]
        public Calificacion Calificacion { get; set; }

        [ForeignKey("AtributoId")]
        public Atributo Atributo { get; set; }

        [ForeignKey("ItemCalificadoId")]
        public Core.Models.configuration.Configuration ItemCalificado { get; set; }


    }
}