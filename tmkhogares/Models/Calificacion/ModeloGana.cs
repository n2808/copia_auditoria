﻿using Core.Models.Common;

namespace tmkhogares.Models.Calificacion
{
    public class ModeloGana : EntityWithIntId
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}