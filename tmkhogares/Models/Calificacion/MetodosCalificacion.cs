﻿using Core.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tmkhogares.Models.Campañas;
using tmkhogares.Models.Configuration;
using tmkhogares.Models.Cuartiles;
using tmkhogares.Models.UsuariosCampanas;

namespace tmkhogares.Models.Calificacion
{
    public class MetodosCalificacion
    {

        public  string ObtenerCalifiacionesRestantes(string login)
        {
            Contexto db = new Contexto();
            decimal calificacionesRestantes = 0;
            User Usuario = db.Usuario.Where(u => u.login == login).FirstOrDefault();
            Nivel nivel = db.Niveles.Find(Usuario.NivelId);
            if(nivel == null)
            {
                return "El asesor no tiene un nivel definido.";

            }
            calificacionesRestantes = nivel.Monitoreos;
            DateTime Hoy = DateTime.Now;
            DateTime FechaInicio = new DateTime(Hoy.Year, Hoy.Month, 1);
            DateTime FechaFin = new DateTime(Hoy.Year, Hoy.Month, 1).AddMonths(1).AddDays(-1);
            var TotalCalificaciones = db.Calificacions.Where(c => c.CreatedAt >= FechaInicio && c.CreatedAt <= FechaFin && c.AsesorId == Usuario.Id).ToList();
            decimal numeroCalifiaciones = TotalCalificaciones.Count;
            if(numeroCalifiaciones> 0)
            {
                foreach (var calificacion in TotalCalificaciones)
                {
                    string puntaje = calificacion.Puntaje.ToString();
                    decimal puntajeDecimal = 0;
                    decimal.TryParse(puntaje, out puntajeDecimal );
                    calificacionesRestantes += puntajeDecimal;
                }
                decimal promedio = calificacionesRestantes / numeroCalifiaciones;
                string calificacionesRestantesString = String.Format("{0:0.0}", promedio);
                return calificacionesRestantesString;
            }
            else
            {
                return "No hay calificaciones en el ultimo mes";
            }
         
        }
        public  string ObtenerUltimaCalifiacion(string login)
        {
            Contexto db = new Contexto();
            User Usuario = db.Usuario.Where(u => u.login == login).FirstOrDefault();
            DateTime Hoy = DateTime.Now;
            DateTime FechaInicio = new DateTime(Hoy.Year, Hoy.Month, 1);
            DateTime FechaFin = new DateTime(Hoy.Year, Hoy.Month, 1).AddMonths(1).AddDays(-1);
            var TotalCalificaciones = db.Calificacions.Where(c => c.CreatedAt >= FechaInicio && c.CreatedAt <= FechaFin && c.AsesorId == Usuario.Id).OrderByDescending(c => c.CreatedAt).ToList();
            if(TotalCalificaciones.Count > 0)
            {
                return String.Format("{0:0.0}", TotalCalificaciones[0].Puntaje);
            }
            else
            {
                return "0";
            }

        }
        public string ObtenerCuartil(string cedula)
        {
            Contexto db = new Contexto();
            int numeroVentas = 10;
            int i = 0;
            User asesor = db.Usuario.Where(u => u.Document.Equals(cedula)).FirstOrDefault();
            if(asesor.Id == null)
            {
                return "No se encontro el asesor";
            }
            UsuarioCampanas campana = db.UsuariosCampanas.Where(uc => uc.UserId == asesor.Id).FirstOrDefault();
            List<Cuartil> cuartiles = db.Cuartiles.OrderBy(c => c.Numero).ToList();
            List<CampañasCuartiles.CampañasCuartiles> campanasCuartiles = new List<CampañasCuartiles.CampañasCuartiles>();

            foreach (var cuartil in cuartiles)
            {
                CampañasCuartiles.CampañasCuartiles campanaCuartil = db.CamapanasCuartiles.Where(cc => cc.CampanaId == campana.CampanaId && cc.CuartilId == cuartil.Id).FirstOrDefault();
                campanasCuartiles.Add(campanaCuartil);
            }

            foreach (var CCuartil in campanasCuartiles)
            {
                if(i == 0)
                {
                        
                }
                else
                {

                }
                i++;
            }

            return "Ha habido un problema por favor contacte con el administrador";
        }
    }
}