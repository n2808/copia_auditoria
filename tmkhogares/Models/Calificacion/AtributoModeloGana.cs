﻿using Core.Models.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmkhogares.Models.Calificacion
{
    public class AtributoModeloGana : EntityWithIntId
    {
        public string Nombre { get; set; }
        public int ModeloGanaId { get; set; }
        public bool Estado { get; set; }

        [ForeignKey("ModeloGanaId")]
        public ModeloGana ModeloGana { get; set; }
    }
}