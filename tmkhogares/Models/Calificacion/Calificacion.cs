﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using tmkhogares.Models.Campañas;

namespace tmkhogares.Models.Calificacion
{
    public class Calificacion : Entity
    {
        public int CampañaId { get; set; }
        public Guid FormularioId { get; set; }
        public string NombreAsesor { get; set; }
        public string NombreCalidad { get; set; }
        public Guid AsesorId { get; set; }  
        public Guid UsuarioCalidadId { get; set; }
        public Guid RolCalifica { get; set; }
        public string Descripcion { get; set; }          
        public string IdGrabacion { get; set; } = "";
        public string TelefonoGrabacion { get; set; } = "";
        public decimal? Puntaje { get; set; }
        public string ComentariosAsesor { get; set; } = "";
        public string CompromisoAsesor { get; set; } = "";
        public string EsVenta { get; set; }
        public string EsTodoClaro { get; set; }
        public string UtilizaGuionBienvenida { get; set; }
        public string UtilizaGuionDespedida { get; set; }
        public bool EsAcorde { get; set; }
        public string PorqueEsAcorde { get; set; } = "";
        public string TipoMonitoreo { get; set; }
        public string Cuartil { get; set; }
        public string procede { get; set; }
        public string EsVentaFinaciada { get; set; }
        public string EsVentaContado { get; set; }
        public string EsRetenido { get; set; }
        public string SubCampana { get; set; }
        public string Segmento { get; set; }
        public string Ciudad { get; set; }
        public string MotivoConsulta { get; set; }
        public string Marcacion { get; set; }
        public int? DuracionTMO { get; set; } = 0;
        public string EsVentaClaroUp { get; set; }
        public decimal ModeloGana_G { get; set; }
        public decimal ModeloGana_A1 { get; set; }
        public decimal ModeloGana_N { get; set; }
        public decimal ModeloGana_A2 { get; set; }
        public DateTime? FechaLlamada { get; set; }
        public string Tipologia { get; set; }

        [ForeignKey("AsesorId")]
        public User Usuario { get; set; }

        [ForeignKey("CampañaId")]
        public Campana Campana { get; set; }

        [ForeignKey("UsuarioCalidadId")]
        public User Calidad { get; set; }
    }
   
}