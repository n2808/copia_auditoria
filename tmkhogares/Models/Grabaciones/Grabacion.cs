﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.Grabaciones
{
    public class Grabacion
    {
        public decimal ID { get; set; }

        public DateTime RDATE { get; set; }

        public decimal DURATION { get; set; }

        public decimal TOTALDURATION { get; set; }

        public decimal? SERVICEID { get; set; }

        public decimal GROUPID { get; set; }

        public decimal STATION { get; set; }

        public decimal LOGIN { get; set; }

        public decimal CALLTYPE { get; set; }

        public string PHONE { get; set; }

        public decimal? CONTACTID { get; set; }
    }
}