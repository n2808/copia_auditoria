﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.Rutas
{
    public class Rutas
    {
        public Guid Id { get; set; }
        public string Ruta { get; set; }
        public string Descripcion { get; set; } = "";
    }
}