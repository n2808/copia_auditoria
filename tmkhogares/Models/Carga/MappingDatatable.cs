﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace tmkhogares.Models.Carga.MappingDatatable
{
    public static class MappingDatatable
    {
        public static DataTable ToDataTable(this ExcelPackage package)
        {
            ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
            DataTable table = new DataTable();
            table.Columns.Add("NombreAsesor");
            table.Columns.Add("DocumentoAsesor");
            table.Columns.Add("VentasAsesor");
            table.Columns.Add("FechaCreacion");
            for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
            {
                var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                var newRow = table.NewRow();
                foreach (var cell in row)
                {
                    if (!string.IsNullOrEmpty(cell.Text))
                    {

                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                }

                table.Rows.Add(newRow);
            

            }
            return table;
        }
    }
}
