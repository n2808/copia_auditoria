﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.Carga
{
    public class Carga : EntityWithIntId
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int NumeroRegistros { get; set; }
        public bool Estado { get;set; }

    }
}