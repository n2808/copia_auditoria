﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.ProcesoDisciplinario
{
    public class ProcesoDisicplinario : Entity
    {
        public string Nombre { get; set; }
        public int Medida1 { get; set; }
        public int Medida2 { get; set; }
        public int Medida3 { get; set; }
        public int Medida4 { get; set; }
        [ForeignKey("Medida1")]
        public Medida medida1 { get; set; }
        [ForeignKey("Medida2")]
        public Medida medida2 { get; set; }
        [ForeignKey("Medida3")]
        public Medida medida3 { get; set; }
        [ForeignKey("Medida4")]
        public Medida medida4 { get; set; }

    }
}