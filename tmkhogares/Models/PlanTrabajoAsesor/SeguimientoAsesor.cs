﻿using Core.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.PlanTrabajoAsesor
{
    public class SeguimientoAsesor : Entity
    {
        public string MetaPromedio                  { get; set; }
        public string PromedioInicio                { get; set; }
        public string PromedioFinalizo              { get; set; }
        public string CantidadVentasMeta            { get; set; }
        public string CantidadVentasRealizadas      { get; set; }
        public string PorcentajeCumplimiento        { get; set; }
        public string PorcentajeAusentismo          { get; set; }
        public string Evolucion                     { get; set; }
        public string NotaCalidad                   { get; set; }
        public string ComentariosSeguimientoTeam    { get; set; }
        public string ComentariosSeguimientoAsesor  { get; set; }
        public string SeguimientoDia1               { get; set; } = "";
        public string SeguimientoDia2               { get; set; } = "";
        public string SeguimientoDia3               { get; set; } = "";
        public string SeguimientoDia4               { get; set; } = "";
        public string SeguimientoDia5               { get; set; } = "";
        public string SeguimientoDia6               { get; set; } = "";
        public string SeguimientoDia7               { get; set; } = "";
        public bool   Estado                        { get; set; } = false;
        public Guid   PlanTrabajoAsesorId           { get; set; }
        [ForeignKey("PlanTrabajoAsesorId")]         
        public PlanTrabajoAsesor PlanTrabajoAsesor  { get; set; }

    }
}