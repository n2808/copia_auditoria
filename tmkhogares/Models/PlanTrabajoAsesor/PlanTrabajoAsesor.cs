﻿using Core.Models.Common;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace tmkhogares.Models.PlanTrabajoAsesor
{
    public class PlanTrabajoAsesor : Entity
    {
        public decimal MetaPromedioDia          { get; set; }
        public int DiasSemana                   { get; set; }
        public decimal MetaSemanal              { get; set; }
        public int DiasMes                      { get; set; }
        public int MetaMensual                  { get; set; }
        public string OportunidadesMejora       { get; set; }
        public string CompromisoAsesor          { get; set; }
        public string DescripcionPlanAccion     { get; set; }
        public string NombreCampana             { get; set; }
        public string NombreTeamLeader          { get; set; }
        public string NombreAsesor              { get; set; }
        public string CedulaAsesor              { get; set; }
        public string LoginAsesor               { get; set; }
        public bool   Estado                    { get; set; }
        public int CampanaId                    { get; set; }
        public Guid AsesorId                    { get; set; }
        public Guid TeamLeaderId                { get; set; }
        [ForeignKey("AsesorId")]
        public User Asesor                      { get; set; }
        [ForeignKey("TeamLeaderId")]
        public User TeamLeader                  { get; set; }
        public Guid? SeguimientoSemana1         { get; set; }
        [ForeignKey("SeguimientoSemana1")]
        public SeguimientoAsesor Seguimiento1   { get; set; }
        public Guid? SeguimientoSemana2         { get; set; }
        [ForeignKey("SeguimientoSemana2")]
        public SeguimientoAsesor Seguimiento2   { get; set; }
        public Guid? SeguimientoSemana3         { get; set; }
        [ForeignKey("SeguimientoSemana3")]
        public SeguimientoAsesor Seguimiento3   { get; set; }
        public Guid? SeguimientoSemana4         { get; set; }
        [ForeignKey("SeguimientoSemana4")]
        public SeguimientoAsesor Seguimiento4   { get; set; }
    }
}