namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregancamposparaestablecerelmodeloGana : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PesoAtributoes", "AtributoId", "dbo.Atributoes");
            DropIndex("dbo.PesoAtributoes", new[] { "AtributoId" });
            CreateTable(
                "dbo.ModeloGanas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Atributoes", "ModeloGanaId", c => c.Int());
            AddColumn("dbo.Atributoes", "AplicaModeloGana", c => c.Boolean(nullable: false));
            CreateIndex("dbo.Atributoes", "ModeloGanaId");
            AddForeignKey("dbo.Atributoes", "ModeloGanaId", "dbo.ModeloGanas", "Id");
            DropTable("dbo.PesoAtributoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PesoAtributoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AtributoId = c.Guid(nullable: false),
                        Peso = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Nombre = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Atributoes", "ModeloGanaId", "dbo.ModeloGanas");
            DropIndex("dbo.Atributoes", new[] { "ModeloGanaId" });
            DropColumn("dbo.Atributoes", "AplicaModeloGana");
            DropColumn("dbo.Atributoes", "ModeloGanaId");
            DropTable("dbo.ModeloGanas");
            CreateIndex("dbo.PesoAtributoes", "AtributoId");
            AddForeignKey("dbo.PesoAtributoes", "AtributoId", "dbo.Atributoes", "Id", cascadeDelete: true);
        }
    }
}
