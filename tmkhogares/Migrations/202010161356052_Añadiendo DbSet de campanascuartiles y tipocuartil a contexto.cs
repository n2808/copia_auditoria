namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoDbSetdecampanascuartilesytipocuartilacontexto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CampañasCuartiles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CampanaId = c.Int(nullable: false),
                        CuartilId = c.Guid(nullable: false),
                        TipoCuartilId = c.Guid(nullable: false),
                        promedio = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campanas", t => t.CampanaId, cascadeDelete: true)
                .ForeignKey("dbo.Cuartils", t => t.CuartilId, cascadeDelete: true)
                .ForeignKey("dbo.TipoCuartils", t => t.TipoCuartilId, cascadeDelete: true)
                .Index(t => t.CampanaId)
                .Index(t => t.CuartilId)
                .Index(t => t.TipoCuartilId);
            
            CreateTable(
                "dbo.TipoCuartils",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CampañasCuartiles", "TipoCuartilId", "dbo.TipoCuartils");
            DropForeignKey("dbo.CampañasCuartiles", "CuartilId", "dbo.Cuartils");
            DropForeignKey("dbo.CampañasCuartiles", "CampanaId", "dbo.Campanas");
            DropIndex("dbo.CampañasCuartiles", new[] { "TipoCuartilId" });
            DropIndex("dbo.CampañasCuartiles", new[] { "CuartilId" });
            DropIndex("dbo.CampañasCuartiles", new[] { "CampanaId" });
            DropTable("dbo.TipoCuartils");
            DropTable("dbo.CampañasCuartiles");
        }
    }
}
