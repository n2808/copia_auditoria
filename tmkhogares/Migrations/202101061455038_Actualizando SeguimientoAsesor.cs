namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActualizandoSeguimientoAsesor : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.SeguimientoTeams", name: "AsesorId", newName: "TeamId");
            RenameIndex(table: "dbo.SeguimientoTeams", name: "IX_AsesorId", newName: "IX_TeamId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.SeguimientoTeams", name: "IX_TeamId", newName: "IX_AsesorId");
            RenameColumn(table: "dbo.SeguimientoTeams", name: "TeamId", newName: "AsesorId");
        }
    }
}
