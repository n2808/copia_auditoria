namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoCuartilaCalificacion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "Cuartil", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "Cuartil");
        }
    }
}
