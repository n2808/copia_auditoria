namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActualizandoContexto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanTrabajoAsesors", "MetaPromedioDia", c => c.String());
            AddColumn("dbo.PlanTrabajoAsesors", "DiasSemana", c => c.String());
            AddColumn("dbo.PlanTrabajoAsesors", "MetaSemanal", c => c.String());
            AddColumn("dbo.PlanTrabajoAsesors", "DiasMes", c => c.String());
            AddColumn("dbo.PlanTrabajoAsesors", "MetaMensual", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlanTrabajoAsesors", "MetaMensual");
            DropColumn("dbo.PlanTrabajoAsesors", "DiasMes");
            DropColumn("dbo.PlanTrabajoAsesors", "MetaSemanal");
            DropColumn("dbo.PlanTrabajoAsesors", "DiasSemana");
            DropColumn("dbo.PlanTrabajoAsesors", "MetaPromedioDia");
        }
    }
}
