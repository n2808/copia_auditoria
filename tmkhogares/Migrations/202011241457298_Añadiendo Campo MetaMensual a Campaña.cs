namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoCampoMetaMensualaCampaña : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campanas", "MetaMensual", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Campanas", "MetaMensual");
        }
    }
}
