namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoclaseGrabacion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Grabacions",
                c => new
                    {
                        ID = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RDATE = c.DateTime(nullable: false),
                        DURATION = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TOTALDURATION = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SERVICEID = c.Decimal(precision: 18, scale: 2),
                        GROUPID = c.Decimal(nullable: false, precision: 18, scale: 2),
                        STATION = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LOGIN = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CALLTYPE = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PHONE = c.String(),
                        CONTACTID = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Grabacions");
        }
    }
}
