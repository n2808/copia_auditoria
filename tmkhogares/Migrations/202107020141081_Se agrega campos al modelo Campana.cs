namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacamposalmodeloCampana : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Campanas", "Estado", c => c.Boolean(nullable: false));
            AddColumn("dbo.Campanas", "PadreId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Campanas", "PadreId");
            DropColumn("dbo.Campanas", "Estado");
        }
    }
}
