namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoComentariosAsesoraCalificacion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "ComentariosAsesor", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "ComentariosAsesor");
        }
    }
}
