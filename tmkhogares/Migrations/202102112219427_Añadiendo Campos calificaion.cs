namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoCamposcalificaion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "EsVentaFinaciada", c => c.String());
            AddColumn("dbo.Calificacions", "EsVentaContado", c => c.String());
            AddColumn("dbo.Calificacions", "EsVentaClaroUp", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "EsVentaClaroUp");
            DropColumn("dbo.Calificacions", "EsVentaContado");
            DropColumn("dbo.Calificacions", "EsVentaFinaciada");
        }
    }
}
