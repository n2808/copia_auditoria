namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cambiostablaclientes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "IdCity", c => c.Guid(nullable: true));
            CreateIndex("dbo.Clientes", "IdCity");
            AddForeignKey("dbo.Clientes", "IdCity", "dbo.Cities", "Id", cascadeDelete: true);
            DropColumn("dbo.Clientes", "RAZON_SOCIAL");
            DropColumn("dbo.Clientes", "DEPARTAMENTO");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clientes", "DEPARTAMENTO", c => c.String());
            AddColumn("dbo.Clientes", "RAZON_SOCIAL", c => c.String());
            DropForeignKey("dbo.Clientes", "IdCity", "dbo.Cities");
            DropIndex("dbo.Clientes", new[] { "IdCity" });
            DropColumn("dbo.Clientes", "IdCity");
        }
    }
}
