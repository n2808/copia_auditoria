namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixingPlanTrabajoGerenteTeamLeader : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanTrabajoTeams", "ObservacionesARealizar", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana1", c => c.Guid());
            AddColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana2", c => c.Guid());
            AddColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana3", c => c.Guid());
            AddColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana4", c => c.Guid());
            AddColumn("dbo.SeguimientoTeams", "RetroAlimentacion", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "Compromiso", c => c.String());
            CreateIndex("dbo.PlanTrabajoTeams", "SeguimientoSemana1");
            CreateIndex("dbo.PlanTrabajoTeams", "SeguimientoSemana2");
            CreateIndex("dbo.PlanTrabajoTeams", "SeguimientoSemana3");
            CreateIndex("dbo.PlanTrabajoTeams", "SeguimientoSemana4");
            AddForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana1", "dbo.SeguimientoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana2", "dbo.SeguimientoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana3", "dbo.SeguimientoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana4", "dbo.SeguimientoTeams", "Id");
            DropColumn("dbo.PlanTrabajoTeams", "OportunidadesMejora");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PlanTrabajoTeams", "OportunidadesMejora", c => c.String());
            DropForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana4", "dbo.SeguimientoTeams");
            DropForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana3", "dbo.SeguimientoTeams");
            DropForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana2", "dbo.SeguimientoTeams");
            DropForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana1", "dbo.SeguimientoTeams");
            DropIndex("dbo.PlanTrabajoTeams", new[] { "SeguimientoSemana4" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "SeguimientoSemana3" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "SeguimientoSemana2" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "SeguimientoSemana1" });
            DropColumn("dbo.SeguimientoTeams", "Compromiso");
            DropColumn("dbo.SeguimientoTeams", "RetroAlimentacion");
            DropColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana4");
            DropColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana3");
            DropColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana2");
            DropColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana1");
            DropColumn("dbo.PlanTrabajoTeams", "ObservacionesARealizar");
        }
    }
}
