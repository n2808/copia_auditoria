namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregandocamposalmodelocalificiónparaelmodeloGANA : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "ModeloGana_G", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Calificacions", "ModeloGana_A1", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Calificacions", "ModeloGana_N", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Calificacions", "ModeloGana_A2", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "ModeloGana_A2");
            DropColumn("dbo.Calificacions", "ModeloGana_N");
            DropColumn("dbo.Calificacions", "ModeloGana_A1");
            DropColumn("dbo.Calificacions", "ModeloGana_G");
        }
    }
}
