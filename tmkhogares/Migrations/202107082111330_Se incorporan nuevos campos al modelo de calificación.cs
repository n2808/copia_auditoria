namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seincorporannuevoscamposalmodelodecalificación : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "EsTodoClaro", c => c.String());
            AddColumn("dbo.Calificacions", "UtilizaGuionBienvenida", c => c.String());
            AddColumn("dbo.Calificacions", "UtilizaGuionDespedida", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "UtilizaGuionDespedida");
            DropColumn("dbo.Calificacions", "UtilizaGuionBienvenida");
            DropColumn("dbo.Calificacions", "EsTodoClaro");
        }
    }
}
