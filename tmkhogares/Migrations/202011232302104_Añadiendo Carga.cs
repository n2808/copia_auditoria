namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoCarga : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PromedioAsesors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CampañaAsesor = c.Int(nullable: false),
                        NombreAsesor = c.String(),
                        DocumentoAsesor = c.String(),
                        VentasAsesor = c.String(),
                        FechaCreacion = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PromedioAsesors");
        }
    }
}
