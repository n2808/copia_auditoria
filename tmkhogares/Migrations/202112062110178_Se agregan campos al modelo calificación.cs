namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seagregancamposalmodelocalificación : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "SubCampana", c => c.String());
            AddColumn("dbo.Calificacions", "Segmento", c => c.String());
            AddColumn("dbo.Calificacions", "Ciudad", c => c.String());
            AddColumn("dbo.Calificacions", "MotivoConsulta", c => c.String());
            AddColumn("dbo.Calificacions", "Marcación", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "Marcación");
            DropColumn("dbo.Calificacions", "MotivoConsulta");
            DropColumn("dbo.Calificacions", "Ciudad");
            DropColumn("dbo.Calificacions", "Segmento");
            DropColumn("dbo.Calificacions", "SubCampana");
        }
    }
}
