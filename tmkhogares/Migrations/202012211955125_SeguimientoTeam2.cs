namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeguimientoTeam2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SeguimientoTeams", "PlanTrabajoTeam_Id", "dbo.PlanTrabajoTeams");
            DropIndex("dbo.SeguimientoTeams", new[] { "PlanTrabajoTeam_Id" });
            RenameColumn(table: "dbo.SeguimientoTeams", name: "PlanTrabajoTeam_Id", newName: "PlanTrabajoTeamId");
            AlterColumn("dbo.SeguimientoTeams", "PlanTrabajoTeamId", c => c.Guid(nullable: false));
            CreateIndex("dbo.SeguimientoTeams", "PlanTrabajoTeamId");
            AddForeignKey("dbo.SeguimientoTeams", "PlanTrabajoTeamId", "dbo.PlanTrabajoTeams", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SeguimientoTeams", "PlanTrabajoTeamId", "dbo.PlanTrabajoTeams");
            DropIndex("dbo.SeguimientoTeams", new[] { "PlanTrabajoTeamId" });
            AlterColumn("dbo.SeguimientoTeams", "PlanTrabajoTeamId", c => c.Guid());
            RenameColumn(table: "dbo.SeguimientoTeams", name: "PlanTrabajoTeamId", newName: "PlanTrabajoTeam_Id");
            CreateIndex("dbo.SeguimientoTeams", "PlanTrabajoTeam_Id");
            AddForeignKey("dbo.SeguimientoTeams", "PlanTrabajoTeam_Id", "dbo.PlanTrabajoTeams", "Id");
        }
    }
}
