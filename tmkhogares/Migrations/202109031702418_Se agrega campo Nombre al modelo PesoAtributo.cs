namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacampoNombrealmodeloPesoAtributo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PesoAtributoes", "Nombre", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PesoAtributoes", "Nombre");
        }
    }
}
