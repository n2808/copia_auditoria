namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoCantidadVentasMetayCantidadVentasRealizadasaSeguimientoTeam : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SeguimientoTeams", "CantidadVentasMeta", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "CantidadVentasRealizadas", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SeguimientoTeams", "CantidadVentasRealizadas");
            DropColumn("dbo.SeguimientoTeams", "CantidadVentasMeta");
        }
    }
}
