namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoCompromisoAsesor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "CompromisoAsesor", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "CompromisoAsesor");
        }
    }
}
