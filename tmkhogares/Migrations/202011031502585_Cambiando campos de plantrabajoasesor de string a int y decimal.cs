namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambiandocamposdeplantrabajoasesordestringaintydecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PlanTrabajoAsesors", "MetaPromedioDia", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.PlanTrabajoAsesors", "DiasSemana", c => c.Int(nullable: false));
            AlterColumn("dbo.PlanTrabajoAsesors", "MetaSemanal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.PlanTrabajoAsesors", "DiasMes", c => c.Int(nullable: false));
            AlterColumn("dbo.PlanTrabajoAsesors", "MetaMensual", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PlanTrabajoAsesors", "MetaMensual", c => c.String());
            AlterColumn("dbo.PlanTrabajoAsesors", "DiasMes", c => c.String());
            AlterColumn("dbo.PlanTrabajoAsesors", "MetaSemanal", c => c.String());
            AlterColumn("dbo.PlanTrabajoAsesors", "DiasSemana", c => c.String());
            AlterColumn("dbo.PlanTrabajoAsesors", "MetaPromedioDia", c => c.String());
        }
    }
}
