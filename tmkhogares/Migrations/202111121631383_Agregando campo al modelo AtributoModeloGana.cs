namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgregandocampoalmodeloAtributoModeloGana : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AtributoModeloGanas", "Estado", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AtributoModeloGanas", "Estado");
        }
    }
}
