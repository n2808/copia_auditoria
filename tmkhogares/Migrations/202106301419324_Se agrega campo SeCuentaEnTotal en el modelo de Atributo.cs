namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacampoSeCuentaEnTotalenelmodelodeAtributo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Atributoes", "SeCuentaEnTotal", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Atributoes", "SeCuentaEnTotal");
        }
    }
}
