namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoPosicionaAtributo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Atributoes", "Posicion", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Atributoes", "Posicion");
        }
    }
}
