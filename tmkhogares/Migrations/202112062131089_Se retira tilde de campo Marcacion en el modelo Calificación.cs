namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeretiratildedecampoMarcacionenelmodeloCalificación : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "Marcacion", c => c.String());
            DropColumn("dbo.Calificacions", "Marcación");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Calificacions", "Marcación", c => c.String());
            DropColumn("dbo.Calificacions", "Marcacion");
        }
    }
}
