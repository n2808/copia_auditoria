namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacampoTipoAtributoIdalmodeloAtributo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Atributoes", "TipoAtributoId", c => c.Guid());
            CreateIndex("dbo.Atributoes", "TipoAtributoId");
            AddForeignKey("dbo.Atributoes", "TipoAtributoId", "dbo.Configurations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Atributoes", "TipoAtributoId", "dbo.Configurations");
            DropIndex("dbo.Atributoes", new[] { "TipoAtributoId" });
            DropColumn("dbo.Atributoes", "TipoAtributoId");
        }
    }
}
