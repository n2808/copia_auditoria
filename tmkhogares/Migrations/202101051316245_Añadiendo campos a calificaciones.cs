namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Añadiendocamposacalificaciones : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "TipoMonitoreo", c => c.String());
            AddColumn("dbo.Calificacions", "procede", c => c.String());
            AlterColumn("dbo.Calificacions", "EsVenta", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Calificacions", "EsVenta", c => c.Boolean(nullable: false));
            DropColumn("dbo.Calificacions", "procede");
            DropColumn("dbo.Calificacions", "TipoMonitoreo");
        }
    }
}
