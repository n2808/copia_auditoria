namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregancamposEsRetenidoyDuraciónTMOalmodeloCalificación : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "EsRetenido", c => c.String());
            AddColumn("dbo.Calificacions", "DuracionTMO", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "DuracionTMO");
            DropColumn("dbo.Calificacions", "EsRetenido");
        }
    }
}
