namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambiandomodeloplantrabajoasesor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanTrabajoAsesors", "SeguimientoSemana1", c => c.Guid());
            AddColumn("dbo.PlanTrabajoAsesors", "SeguimientoSemana2", c => c.Guid());
            AddColumn("dbo.PlanTrabajoAsesors", "SeguimientoSemana3", c => c.Guid());
            AddColumn("dbo.PlanTrabajoAsesors", "SeguimientoSemana4", c => c.Guid());
            AddColumn("dbo.SeguimientoAsesors", "Estado", c => c.Boolean(nullable: false));
            CreateIndex("dbo.PlanTrabajoAsesors", "SeguimientoSemana1");
            CreateIndex("dbo.PlanTrabajoAsesors", "SeguimientoSemana2");
            CreateIndex("dbo.PlanTrabajoAsesors", "SeguimientoSemana3");
            CreateIndex("dbo.PlanTrabajoAsesors", "SeguimientoSemana4");
            AddForeignKey("dbo.PlanTrabajoAsesors", "SeguimientoSemana1", "dbo.SeguimientoAsesors", "Id");
            AddForeignKey("dbo.PlanTrabajoAsesors", "SeguimientoSemana2", "dbo.SeguimientoAsesors", "Id");
            AddForeignKey("dbo.PlanTrabajoAsesors", "SeguimientoSemana3", "dbo.SeguimientoAsesors", "Id");
            AddForeignKey("dbo.PlanTrabajoAsesors", "SeguimientoSemana4", "dbo.SeguimientoAsesors", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanTrabajoAsesors", "SeguimientoSemana4", "dbo.SeguimientoAsesors");
            DropForeignKey("dbo.PlanTrabajoAsesors", "SeguimientoSemana3", "dbo.SeguimientoAsesors");
            DropForeignKey("dbo.PlanTrabajoAsesors", "SeguimientoSemana2", "dbo.SeguimientoAsesors");
            DropForeignKey("dbo.PlanTrabajoAsesors", "SeguimientoSemana1", "dbo.SeguimientoAsesors");
            DropIndex("dbo.PlanTrabajoAsesors", new[] { "SeguimientoSemana4" });
            DropIndex("dbo.PlanTrabajoAsesors", new[] { "SeguimientoSemana3" });
            DropIndex("dbo.PlanTrabajoAsesors", new[] { "SeguimientoSemana2" });
            DropIndex("dbo.PlanTrabajoAsesors", new[] { "SeguimientoSemana1" });
            DropColumn("dbo.SeguimientoAsesors", "Estado");
            DropColumn("dbo.PlanTrabajoAsesors", "SeguimientoSemana4");
            DropColumn("dbo.PlanTrabajoAsesors", "SeguimientoSemana3");
            DropColumn("dbo.PlanTrabajoAsesors", "SeguimientoSemana2");
            DropColumn("dbo.PlanTrabajoAsesors", "SeguimientoSemana1");
        }
    }
}
