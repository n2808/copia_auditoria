namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeeliminancamposdelmodeloAtributo : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Atributoes", "EsPrincipal");
            DropColumn("dbo.Atributoes", "SeCuentaEnTotal");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Atributoes", "SeCuentaEnTotal", c => c.Boolean(nullable: false));
            AddColumn("dbo.Atributoes", "EsPrincipal", c => c.Boolean(nullable: false));
        }
    }
}
