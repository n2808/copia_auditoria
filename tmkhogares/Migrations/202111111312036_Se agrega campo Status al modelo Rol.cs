namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacampoStatusalmodeloRol : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rols", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rols", "Status");
        }
    }
}
