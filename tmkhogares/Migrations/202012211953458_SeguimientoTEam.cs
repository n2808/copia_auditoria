namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeguimientoTEam : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanTrabajoTeams", "MetaPromedioDia", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PlanTrabajoTeams", "DiasSemana", c => c.Int(nullable: false));
            AddColumn("dbo.PlanTrabajoTeams", "MetaSemanal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PlanTrabajoTeams", "DiasMes", c => c.Int(nullable: false));
            AddColumn("dbo.PlanTrabajoTeams", "MetaMensual", c => c.Int(nullable: false));
            AddColumn("dbo.PlanTrabajoTeams", "OportunidadesMejora", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "CompromisoTeam", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "DescripcionPlanAccion", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "NombreCampana", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "NombreGerente", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "NombreTeam", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "CedulaTeam", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "LoginTeam", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "Estado", c => c.Boolean(nullable: false));
            AddColumn("dbo.PlanTrabajoTeams", "CantidadColaboradores", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "Ausentismo", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "Rotacion", c => c.String());
            AddColumn("dbo.PlanTrabajoTeams", "TeamId", c => c.Guid(nullable: false));
            AddColumn("dbo.PlanTrabajoTeams", "GerenteId", c => c.Guid(nullable: false));
            AddColumn("dbo.ResumenPlanTrabajoTeams", "Promedio", c => c.String());
            AddColumn("dbo.ResumenPlanTrabajoTeams", "VentasRealizadas", c => c.Int(nullable: false));
            AddColumn("dbo.ResumenPlanTrabajoTeams", "Cumplimiento", c => c.String());
            AddColumn("dbo.ResumenPlanTrabajoTeams", "Evolucion", c => c.String());
            AddColumn("dbo.ResumenPlanTrabajoTeams", "Ausentismo", c => c.String());
            AddColumn("dbo.ResumenPlanTrabajoTeams", "NotaCalidad", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "MetaPromedio", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "PromedioSemana1", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "PromedioSemana2", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "PromedioSemana3", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "PromedioSemana4", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "DiasAntiguo", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "PorcentajeCumplimiento", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "PorcentajeAusentismo", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "Evolucion", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "NotaCalidad", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "Estado", c => c.Boolean(nullable: false));
            AddColumn("dbo.SeguimientoTeams", "AsesorId", c => c.Guid(nullable: false));
            AddColumn("dbo.SeguimientoTeams", "PlanTrabajoTeam_Id", c => c.Guid());
            CreateIndex("dbo.PlanTrabajoTeams", "TeamId");
            CreateIndex("dbo.PlanTrabajoTeams", "GerenteId");
            CreateIndex("dbo.SeguimientoTeams", "AsesorId");
            CreateIndex("dbo.SeguimientoTeams", "PlanTrabajoTeam_Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "GerenteId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SeguimientoTeams", "AsesorId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.SeguimientoTeams", "PlanTrabajoTeam_Id", "dbo.PlanTrabajoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "TeamId", "dbo.Users", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanTrabajoTeams", "TeamId", "dbo.Users");
            DropForeignKey("dbo.SeguimientoTeams", "PlanTrabajoTeam_Id", "dbo.PlanTrabajoTeams");
            DropForeignKey("dbo.SeguimientoTeams", "AsesorId", "dbo.Users");
            DropForeignKey("dbo.PlanTrabajoTeams", "GerenteId", "dbo.Users");
            DropIndex("dbo.SeguimientoTeams", new[] { "PlanTrabajoTeam_Id" });
            DropIndex("dbo.SeguimientoTeams", new[] { "AsesorId" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "GerenteId" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "TeamId" });
            DropColumn("dbo.SeguimientoTeams", "PlanTrabajoTeam_Id");
            DropColumn("dbo.SeguimientoTeams", "AsesorId");
            DropColumn("dbo.SeguimientoTeams", "Estado");
            DropColumn("dbo.SeguimientoTeams", "NotaCalidad");
            DropColumn("dbo.SeguimientoTeams", "Evolucion");
            DropColumn("dbo.SeguimientoTeams", "PorcentajeAusentismo");
            DropColumn("dbo.SeguimientoTeams", "PorcentajeCumplimiento");
            DropColumn("dbo.SeguimientoTeams", "DiasAntiguo");
            DropColumn("dbo.SeguimientoTeams", "PromedioSemana4");
            DropColumn("dbo.SeguimientoTeams", "PromedioSemana3");
            DropColumn("dbo.SeguimientoTeams", "PromedioSemana2");
            DropColumn("dbo.SeguimientoTeams", "PromedioSemana1");
            DropColumn("dbo.SeguimientoTeams", "MetaPromedio");
            DropColumn("dbo.ResumenPlanTrabajoTeams", "NotaCalidad");
            DropColumn("dbo.ResumenPlanTrabajoTeams", "Ausentismo");
            DropColumn("dbo.ResumenPlanTrabajoTeams", "Evolucion");
            DropColumn("dbo.ResumenPlanTrabajoTeams", "Cumplimiento");
            DropColumn("dbo.ResumenPlanTrabajoTeams", "VentasRealizadas");
            DropColumn("dbo.ResumenPlanTrabajoTeams", "Promedio");
            DropColumn("dbo.PlanTrabajoTeams", "GerenteId");
            DropColumn("dbo.PlanTrabajoTeams", "TeamId");
            DropColumn("dbo.PlanTrabajoTeams", "Rotacion");
            DropColumn("dbo.PlanTrabajoTeams", "Ausentismo");
            DropColumn("dbo.PlanTrabajoTeams", "CantidadColaboradores");
            DropColumn("dbo.PlanTrabajoTeams", "Estado");
            DropColumn("dbo.PlanTrabajoTeams", "LoginTeam");
            DropColumn("dbo.PlanTrabajoTeams", "CedulaTeam");
            DropColumn("dbo.PlanTrabajoTeams", "NombreTeam");
            DropColumn("dbo.PlanTrabajoTeams", "NombreGerente");
            DropColumn("dbo.PlanTrabajoTeams", "NombreCampana");
            DropColumn("dbo.PlanTrabajoTeams", "DescripcionPlanAccion");
            DropColumn("dbo.PlanTrabajoTeams", "CompromisoTeam");
            DropColumn("dbo.PlanTrabajoTeams", "OportunidadesMejora");
            DropColumn("dbo.PlanTrabajoTeams", "MetaMensual");
            DropColumn("dbo.PlanTrabajoTeams", "DiasMes");
            DropColumn("dbo.PlanTrabajoTeams", "MetaSemanal");
            DropColumn("dbo.PlanTrabajoTeams", "DiasSemana");
            DropColumn("dbo.PlanTrabajoTeams", "MetaPromedioDia");
        }
    }
}
