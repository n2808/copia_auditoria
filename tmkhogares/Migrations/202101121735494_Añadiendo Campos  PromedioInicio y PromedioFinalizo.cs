namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoCamposPromedioInicioyPromedioFinalizo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SeguimientoTeams", "PromedioInicio", c => c.String());
            AddColumn("dbo.SeguimientoTeams", "PromedioFinalizo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SeguimientoTeams", "PromedioFinalizo");
            DropColumn("dbo.SeguimientoTeams", "PromedioInicio");
        }
    }
}
