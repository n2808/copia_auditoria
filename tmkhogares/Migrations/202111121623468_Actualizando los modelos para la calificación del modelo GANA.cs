namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActualizandolosmodelosparalacalificacióndelmodeloGANA : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Atributoes", "ModeloGanaId", "dbo.ModeloGanas");
            DropIndex("dbo.Atributoes", new[] { "ModeloGanaId" });
            CreateTable(
                "dbo.AtributoModeloGanas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        ModeloGanaId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ModeloGanas", t => t.ModeloGanaId, cascadeDelete: false)
                .Index(t => t.ModeloGanaId);
            
            AddColumn("dbo.Atributoes", "AtributoModeloGanaId", c => c.Int());
            CreateIndex("dbo.Atributoes", "AtributoModeloGanaId");
            AddForeignKey("dbo.Atributoes", "AtributoModeloGanaId", "dbo.AtributoModeloGanas", "Id");
            DropColumn("dbo.Atributoes", "ModeloGanaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Atributoes", "ModeloGanaId", c => c.Int());
            DropForeignKey("dbo.Atributoes", "AtributoModeloGanaId", "dbo.AtributoModeloGanas");
            DropForeignKey("dbo.AtributoModeloGanas", "ModeloGanaId", "dbo.ModeloGanas");
            DropIndex("dbo.AtributoModeloGanas", new[] { "ModeloGanaId" });
            DropIndex("dbo.Atributoes", new[] { "AtributoModeloGanaId" });
            DropColumn("dbo.Atributoes", "AtributoModeloGanaId");
            DropTable("dbo.AtributoModeloGanas");
            CreateIndex("dbo.Atributoes", "ModeloGanaId");
            AddForeignKey("dbo.Atributoes", "ModeloGanaId", "dbo.ModeloGanas", "Id");
        }
    }
}
