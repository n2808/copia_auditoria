namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoRelacionesdeseguimientosenplantrabajoteamleader : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana1");
            DropColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana2");
            DropColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana3");
            DropColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana4");
            RenameColumn(table: "dbo.PlanTrabajoTeams", name: "Seguimiento1_Id", newName: "SeguimientoSemana1");
            RenameColumn(table: "dbo.PlanTrabajoTeams", name: "Seguimiento2_Id", newName: "SeguimientoSemana2");
            RenameColumn(table: "dbo.PlanTrabajoTeams", name: "Seguimiento3_Id", newName: "SeguimientoSemana3");
            RenameColumn(table: "dbo.PlanTrabajoTeams", name: "Seguimiento4_Id", newName: "SeguimientoSemana4");
            RenameIndex(table: "dbo.PlanTrabajoTeams", name: "IX_Seguimiento1_Id", newName: "IX_SeguimientoSemana1");
            RenameIndex(table: "dbo.PlanTrabajoTeams", name: "IX_Seguimiento2_Id", newName: "IX_SeguimientoSemana2");
            RenameIndex(table: "dbo.PlanTrabajoTeams", name: "IX_Seguimiento3_Id", newName: "IX_SeguimientoSemana3");
            RenameIndex(table: "dbo.PlanTrabajoTeams", name: "IX_Seguimiento4_Id", newName: "IX_SeguimientoSemana4");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.PlanTrabajoTeams", name: "IX_SeguimientoSemana4", newName: "IX_Seguimiento4_Id");
            RenameIndex(table: "dbo.PlanTrabajoTeams", name: "IX_SeguimientoSemana3", newName: "IX_Seguimiento3_Id");
            RenameIndex(table: "dbo.PlanTrabajoTeams", name: "IX_SeguimientoSemana2", newName: "IX_Seguimiento2_Id");
            RenameIndex(table: "dbo.PlanTrabajoTeams", name: "IX_SeguimientoSemana1", newName: "IX_Seguimiento1_Id");
            RenameColumn(table: "dbo.PlanTrabajoTeams", name: "SeguimientoSemana4", newName: "Seguimiento4_Id");
            RenameColumn(table: "dbo.PlanTrabajoTeams", name: "SeguimientoSemana3", newName: "Seguimiento3_Id");
            RenameColumn(table: "dbo.PlanTrabajoTeams", name: "SeguimientoSemana2", newName: "Seguimiento2_Id");
            RenameColumn(table: "dbo.PlanTrabajoTeams", name: "SeguimientoSemana1", newName: "Seguimiento1_Id");
            AddColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana4", c => c.Guid());
            AddColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana3", c => c.Guid());
            AddColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana2", c => c.Guid());
            AddColumn("dbo.PlanTrabajoTeams", "SeguimientoSemana1", c => c.Guid());
        }
    }
}
