namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoCampoRolCalificaacalificacion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "RolCalifica", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "RolCalifica");
        }
    }
}
