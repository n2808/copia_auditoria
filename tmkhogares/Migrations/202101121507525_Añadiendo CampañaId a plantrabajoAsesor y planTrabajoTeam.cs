namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoCampañaIdaplantrabajoAsesoryplanTrabajoTeam : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanTrabajoAsesors", "CampanaId", c => c.Int(nullable: false));
            AddColumn("dbo.PlanTrabajoTeams", "CampanaId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlanTrabajoTeams", "CampanaId");
            DropColumn("dbo.PlanTrabajoAsesors", "CampanaId");
        }
    }
}
