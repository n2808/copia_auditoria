namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActualizandoContext : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cuartils", "Numero", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cuartils", "Numero", c => c.String());
        }
    }
}
