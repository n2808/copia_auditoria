namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ActualizandoModelo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Formularios", "NotaCritica", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Formularios", "NotaNoCritica", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Formularios", "NotaNoCritica");
            DropColumn("dbo.Formularios", "NotaCritica");
        }
    }
}
