namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Añadiendoestadoaplantrabajoasesor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlanTrabajoAsesors", "Estado", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlanTrabajoAsesors", "Estado");
        }
    }
}
