namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoUsuarioCampanas : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Users", name: "campanaId", newName: "Campana_Id");
            RenameIndex(table: "dbo.Users", name: "IX_campanaId", newName: "IX_Campana_Id");
            CreateTable(
                "dbo.UsuarioCampanas",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CampanaId = c.Int(nullable: false),
                        UserId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campanas", t => t.CampanaId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.CampanaId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsuarioCampanas", "UserId", "dbo.Users");
            DropForeignKey("dbo.UsuarioCampanas", "CampanaId", "dbo.Campanas");
            DropIndex("dbo.UsuarioCampanas", new[] { "UserId" });
            DropIndex("dbo.UsuarioCampanas", new[] { "CampanaId" });
            DropTable("dbo.UsuarioCampanas");
            RenameIndex(table: "dbo.Users", name: "IX_Campana_Id", newName: "IX_campanaId");
            RenameColumn(table: "dbo.Users", name: "Campana_Id", newName: "campanaId");
        }
    }
}
