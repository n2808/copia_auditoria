namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregacampoItemCalificadoIdalmodeloCalificacionEstandarAtributo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId", c => c.Guid());
            CreateIndex("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId");
            AddForeignKey("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId", "dbo.Configurations", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId", "dbo.Configurations");
            DropIndex("dbo.CalificacionEstandarAtributoes", new[] { "ItemCalificadoId" });
            DropColumn("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId");
        }
    }
}
