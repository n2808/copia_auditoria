namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoEstadosaEstandaryAtributo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Atributoes", "Estado", c => c.Boolean(nullable: false));
            AddColumn("dbo.Estandars", "Estado", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Estandars", "Estado");
            DropColumn("dbo.Atributoes", "Estado");
        }
    }
}
