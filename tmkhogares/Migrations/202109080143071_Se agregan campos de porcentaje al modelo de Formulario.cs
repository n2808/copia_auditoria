namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregancamposdeporcentajealmodelodeFormulario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Formularios", "PorcentajeGeneral", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Formularios", "PorcentajeProceso", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Formularios", "PorcentajeProceso");
            DropColumn("dbo.Formularios", "PorcentajeGeneral");
        }
    }
}
