namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendoTipoRolaFormulario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Formularios", "TipoRol", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Formularios", "TipoRol");
        }
    }
}
