namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregaelcampoposiciónalmodelodeEstandar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Estandars", "Posicion", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Estandars", "Posicion");
        }
    }
}
