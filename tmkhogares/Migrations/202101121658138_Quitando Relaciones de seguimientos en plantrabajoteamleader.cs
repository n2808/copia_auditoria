namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class QuitandoRelacionesdeseguimientosenplantrabajoteamleader : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana1", "dbo.SeguimientoTeams");
            DropForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana2", "dbo.SeguimientoTeams");
            DropForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana3", "dbo.SeguimientoTeams");
            DropForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana4", "dbo.SeguimientoTeams");
            DropIndex("dbo.PlanTrabajoTeams", new[] { "SeguimientoSemana1" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "SeguimientoSemana2" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "SeguimientoSemana3" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "SeguimientoSemana4" });
            AddColumn("dbo.PlanTrabajoTeams", "Seguimiento1_Id", c => c.Guid());
            AddColumn("dbo.PlanTrabajoTeams", "Seguimiento2_Id", c => c.Guid());
            AddColumn("dbo.PlanTrabajoTeams", "Seguimiento3_Id", c => c.Guid());
            AddColumn("dbo.PlanTrabajoTeams", "Seguimiento4_Id", c => c.Guid());
            CreateIndex("dbo.PlanTrabajoTeams", "Seguimiento1_Id");
            CreateIndex("dbo.PlanTrabajoTeams", "Seguimiento2_Id");
            CreateIndex("dbo.PlanTrabajoTeams", "Seguimiento3_Id");
            CreateIndex("dbo.PlanTrabajoTeams", "Seguimiento4_Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "Seguimiento1_Id", "dbo.SeguimientoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "Seguimiento2_Id", "dbo.SeguimientoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "Seguimiento3_Id", "dbo.SeguimientoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "Seguimiento4_Id", "dbo.SeguimientoTeams", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanTrabajoTeams", "Seguimiento4_Id", "dbo.SeguimientoTeams");
            DropForeignKey("dbo.PlanTrabajoTeams", "Seguimiento3_Id", "dbo.SeguimientoTeams");
            DropForeignKey("dbo.PlanTrabajoTeams", "Seguimiento2_Id", "dbo.SeguimientoTeams");
            DropForeignKey("dbo.PlanTrabajoTeams", "Seguimiento1_Id", "dbo.SeguimientoTeams");
            DropIndex("dbo.PlanTrabajoTeams", new[] { "Seguimiento4_Id" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "Seguimiento3_Id" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "Seguimiento2_Id" });
            DropIndex("dbo.PlanTrabajoTeams", new[] { "Seguimiento1_Id" });
            DropColumn("dbo.PlanTrabajoTeams", "Seguimiento4_Id");
            DropColumn("dbo.PlanTrabajoTeams", "Seguimiento3_Id");
            DropColumn("dbo.PlanTrabajoTeams", "Seguimiento2_Id");
            DropColumn("dbo.PlanTrabajoTeams", "Seguimiento1_Id");
            CreateIndex("dbo.PlanTrabajoTeams", "SeguimientoSemana4");
            CreateIndex("dbo.PlanTrabajoTeams", "SeguimientoSemana3");
            CreateIndex("dbo.PlanTrabajoTeams", "SeguimientoSemana2");
            CreateIndex("dbo.PlanTrabajoTeams", "SeguimientoSemana1");
            AddForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana4", "dbo.SeguimientoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana3", "dbo.SeguimientoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana2", "dbo.SeguimientoTeams", "Id");
            AddForeignKey("dbo.PlanTrabajoTeams", "SeguimientoSemana1", "dbo.SeguimientoTeams", "Id");
        }
    }
}
