namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambios : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId", "dbo.Configurations");
            DropIndex("dbo.CalificacionEstandarAtributoes", new[] { "ItemCalificadoId" });
            AlterColumn("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId", c => c.Guid());
            CreateIndex("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId");
            AddForeignKey("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId", "dbo.Configurations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId", "dbo.Configurations");
            DropIndex("dbo.CalificacionEstandarAtributoes", new[] { "ItemCalificadoId" });
            AlterColumn("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId", c => c.Guid(nullable: false));
            CreateIndex("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId");
            AddForeignKey("dbo.CalificacionEstandarAtributoes", "ItemCalificadoId", "dbo.Configurations", "Id", cascadeDelete: true);
        }
    }
}
