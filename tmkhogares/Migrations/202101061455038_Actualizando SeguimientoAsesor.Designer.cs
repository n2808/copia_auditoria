// <auto-generated />
namespace tmkhogares.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class ActualizandoSeguimientoAsesor : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ActualizandoSeguimientoAsesor));
        
        string IMigrationMetadata.Id
        {
            get { return "202101061455038_Actualizando SeguimientoAsesor"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
