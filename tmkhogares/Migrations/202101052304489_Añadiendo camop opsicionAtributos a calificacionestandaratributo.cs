namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AñadiendocamopopsicionAtributosacalificacionestandaratributo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CalificacionEstandarAtributoes", "PosicionAtributo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CalificacionEstandarAtributoes", "PosicionAtributo");
        }
    }
}
