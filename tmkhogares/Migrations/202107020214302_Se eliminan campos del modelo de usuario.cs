namespace tmkhogares.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Seeliminancamposdelmodelodeusuario : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "LastName");
            DropColumn("dbo.Users", "Phone1");
            DropColumn("dbo.Users", "Phone2");
            DropColumn("dbo.Users", "Phone3");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Phone3", c => c.String());
            AddColumn("dbo.Users", "Phone2", c => c.String());
            AddColumn("dbo.Users", "Phone1", c => c.String());
            AddColumn("dbo.Users", "LastName", c => c.String());
        }
    }
}
