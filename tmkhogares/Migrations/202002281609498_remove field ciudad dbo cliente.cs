namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removefieldciudaddbocliente : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Clientes", "CIUDAD");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clientes", "CIUDAD", c => c.String());
        }
    }
}
