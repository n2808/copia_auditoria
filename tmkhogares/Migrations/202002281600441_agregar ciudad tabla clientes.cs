namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agregarciudadtablaclientes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clientes", "CIUDAD", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clientes", "CIUDAD");
        }
    }
}
