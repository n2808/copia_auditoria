namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SubiendoCambios : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "EsVenta", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "EsVenta");
        }
    }
}
