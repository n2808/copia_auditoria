namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeagregancamposFechaLlamadayTipologiaalmodeloCalificación : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "FechaLlamada", c => c.DateTime());
            AddColumn("dbo.Calificacions", "Tipologia", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "Tipologia");
            DropColumn("dbo.Calificacions", "FechaLlamada");
        }
    }
}
