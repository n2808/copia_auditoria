namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Cambiandostringaguidenclaseplantrabajoasesorcamposasesoridtemaleaderid : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PlanTrabajoAsesors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OportunidadesMejora = c.String(),
                        CompromisoAsesor = c.String(),
                        DescripcionPlanAccion = c.String(),
                        NombreCampana = c.String(),
                        NombreTeamLeader = c.String(),
                        NombreAsesor = c.String(),
                        CedulaAsesor = c.String(),
                        LoginAsesor = c.String(),
                        AsesorId = c.Guid(nullable: false),
                        TeamLeaderId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.AsesorId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.TeamLeaderId, cascadeDelete: false)
                .Index(t => t.AsesorId)
                .Index(t => t.TeamLeaderId);
            
            CreateTable(
                "dbo.PlanTrabajoTeams",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ResumenPlanTrabajoAsesors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Promedio = c.String(),
                        VentasRealizadas = c.Int(nullable: false),
                        Cumplimiento = c.String(),
                        Evolucion = c.String(),
                        Ausentismo = c.String(),
                        NotaCalidad = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ResumenPlanTrabajoTeams",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SeguimientoAsesors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MetaPromedio = c.String(),
                        PromedioInicio = c.String(),
                        PromedioFinalizo = c.String(),
                        CantidadVentasMeta = c.String(),
                        CantidadVentasRealizadas = c.String(),
                        PorcentajeCumplimiento = c.String(),
                        PorcentajeAusentismo = c.String(),
                        Evolucion = c.String(),
                        NotaCalidad = c.String(),
                        ComentariosSeguimientoTeam = c.String(),
                        ComentariosSeguimientoAsesor = c.String(),
                        SeguimientoDia1 = c.String(),
                        SeguimientoDia2 = c.String(),
                        SeguimientoDia3 = c.String(),
                        SeguimientoDia4 = c.String(),
                        SeguimientoDia5 = c.String(),
                        SeguimientoDia6 = c.String(),
                        SeguimientoDia7 = c.String(),
                        PlanTrabajoAsesorId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PlanTrabajoAsesors", t => t.PlanTrabajoAsesorId, cascadeDelete: true)
                .Index(t => t.PlanTrabajoAsesorId);
            
            CreateTable(
                "dbo.SeguimientoTeams",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SeguimientoAsesors", "PlanTrabajoAsesorId", "dbo.PlanTrabajoAsesors");
            DropForeignKey("dbo.PlanTrabajoAsesors", "TeamLeaderId", "dbo.Users");
            DropForeignKey("dbo.PlanTrabajoAsesors", "AsesorId", "dbo.Users");
            DropIndex("dbo.SeguimientoAsesors", new[] { "PlanTrabajoAsesorId" });
            DropIndex("dbo.PlanTrabajoAsesors", new[] { "TeamLeaderId" });
            DropIndex("dbo.PlanTrabajoAsesors", new[] { "AsesorId" });
            DropTable("dbo.SeguimientoTeams");
            DropTable("dbo.SeguimientoAsesors");
            DropTable("dbo.ResumenPlanTrabajoTeams");
            DropTable("dbo.ResumenPlanTrabajoAsesors");
            DropTable("dbo.PlanTrabajoTeams");
            DropTable("dbo.PlanTrabajoAsesors");
        }
    }
}
