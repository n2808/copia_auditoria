namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigracionInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Atributoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                        Peso = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EstandarId = c.Guid(nullable: false),
                        EsPrincipal = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Estandars", t => t.EstandarId, cascadeDelete: true)
                .Index(t => t.EstandarId);
            
            CreateTable(
                "dbo.Estandars",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                        Peso = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FormularioId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Formularios", t => t.FormularioId, cascadeDelete: true)
                .Index(t => t.FormularioId);
            
            CreateTable(
                "dbo.Formularios",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Nombre = c.String(),
                        CampanaId = c.Int(nullable: false),
                        NivelAsesor = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campanas", t => t.CampanaId, cascadeDelete: true)
                .ForeignKey("dbo.Nivels", t => t.NivelAsesor, cascadeDelete: true)
                .Index(t => t.CampanaId)
                .Index(t => t.NivelAsesor);
            
            CreateTable(
                "dbo.Campanas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Document = c.String(nullable: false),
                        Names = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Phone1 = c.String(),
                        Phone2 = c.String(),
                        Phone3 = c.String(),
                        Email = c.String(),
                        Status = c.Boolean(nullable: false),
                        PassWord = c.String(nullable: false),
                        login = c.String(nullable: false),
                        campanaId = c.Int(),
                        UserId = c.Guid(),
                        NivelId = c.Int(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Campanas", t => t.campanaId)
                .ForeignKey("dbo.Nivels", t => t.NivelId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.campanaId)
                .Index(t => t.UserId)
                .Index(t => t.NivelId);
            
            CreateTable(
                "dbo.Nivels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Descripcion = c.String(),
                        Monitoreos = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserRols",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RolId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rols", t => t.RolId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RolId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PesoAtributoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AtributoId = c.Guid(nullable: false),
                        Peso = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Atributoes", t => t.AtributoId, cascadeDelete: true)
                .Index(t => t.AtributoId);
            
            CreateTable(
                "dbo.CalificacionEstandarAtributoes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CalificacionId = c.Guid(nullable: false),
                        AtributoId = c.Guid(nullable: false),
                        EstandarId = c.Guid(nullable: false),
                        Puntaje = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Descripcion = c.String(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Atributoes", t => t.AtributoId, cascadeDelete: false)
                .ForeignKey("dbo.Calificacions", t => t.CalificacionId, cascadeDelete: false)
                .ForeignKey("dbo.Estandars", t => t.EstandarId, cascadeDelete: false)
                .Index(t => t.CalificacionId)
                .Index(t => t.AtributoId)
                .Index(t => t.EstandarId);
            
            CreateTable(
                "dbo.Calificacions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CampañaId = c.Int(nullable: false),
                        FormularioId = c.Guid(nullable: false),
                        NombreAsesor = c.String(),
                        NombreCalidad = c.String(),
                        AsesorId = c.Guid(nullable: false),
                        UsuarioCalidadId = c.Guid(nullable: false),
                        Descripcion = c.String(),
                        IdGrabacion = c.String(),
                        TelefonoGrabacion = c.String(),
                        Puntaje = c.Decimal(precision: 18, scale: 2),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UsuarioCalidadId, cascadeDelete: false)
                .ForeignKey("dbo.Campanas", t => t.CampañaId, cascadeDelete: false)
                .ForeignKey("dbo.Users", t => t.AsesorId, cascadeDelete: false)
                .Index(t => t.CampañaId)
                .Index(t => t.AsesorId)
                .Index(t => t.UsuarioCalidadId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Status = c.Boolean(nullable: false),
                        Description = c.String(),
                        CanChanged = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Configurations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        OrderId = c.Int(nullable: false),
                        Description = c.String(),
                        Status = c.Boolean(nullable: false),
                        CategoriesId = c.Guid(nullable: false),
                        GuidId = c.Guid(),
                        IntId = c.Int(),
                        CreatedAt = c.DateTime(),
                        UpdatedAt = c.DateTime(),
                        DeletedAt = c.DateTime(),
                        CreatedBy = c.Guid(),
                        UpdatedBy = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoriesId, cascadeDelete: true)
                .Index(t => t.CategoriesId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Configurations", "CategoriesId", "dbo.Categories");
            DropForeignKey("dbo.CalificacionEstandarAtributoes", "EstandarId", "dbo.Estandars");
            DropForeignKey("dbo.CalificacionEstandarAtributoes", "CalificacionId", "dbo.Calificacions");
            DropForeignKey("dbo.Calificacions", "AsesorId", "dbo.Users");
            DropForeignKey("dbo.Calificacions", "CampañaId", "dbo.Campanas");
            DropForeignKey("dbo.Calificacions", "UsuarioCalidadId", "dbo.Users");
            DropForeignKey("dbo.CalificacionEstandarAtributoes", "AtributoId", "dbo.Atributoes");
            DropForeignKey("dbo.PesoAtributoes", "AtributoId", "dbo.Atributoes");
            DropForeignKey("dbo.Formularios", "NivelAsesor", "dbo.Nivels");
            DropForeignKey("dbo.Estandars", "FormularioId", "dbo.Formularios");
            DropForeignKey("dbo.UserRols", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRols", "RolId", "dbo.Rols");
            DropForeignKey("dbo.Users", "UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "NivelId", "dbo.Nivels");
            DropForeignKey("dbo.Users", "campanaId", "dbo.Campanas");
            DropForeignKey("dbo.Formularios", "CampanaId", "dbo.Campanas");
            DropForeignKey("dbo.Atributoes", "EstandarId", "dbo.Estandars");
            DropIndex("dbo.Configurations", new[] { "CategoriesId" });
            DropIndex("dbo.Calificacions", new[] { "UsuarioCalidadId" });
            DropIndex("dbo.Calificacions", new[] { "AsesorId" });
            DropIndex("dbo.Calificacions", new[] { "CampañaId" });
            DropIndex("dbo.CalificacionEstandarAtributoes", new[] { "EstandarId" });
            DropIndex("dbo.CalificacionEstandarAtributoes", new[] { "AtributoId" });
            DropIndex("dbo.CalificacionEstandarAtributoes", new[] { "CalificacionId" });
            DropIndex("dbo.PesoAtributoes", new[] { "AtributoId" });
            DropIndex("dbo.UserRols", new[] { "UserId" });
            DropIndex("dbo.UserRols", new[] { "RolId" });
            DropIndex("dbo.Users", new[] { "NivelId" });
            DropIndex("dbo.Users", new[] { "UserId" });
            DropIndex("dbo.Users", new[] { "campanaId" });
            DropIndex("dbo.Formularios", new[] { "NivelAsesor" });
            DropIndex("dbo.Formularios", new[] { "CampanaId" });
            DropIndex("dbo.Estandars", new[] { "FormularioId" });
            DropIndex("dbo.Atributoes", new[] { "EstandarId" });
            DropTable("dbo.Configurations");
            DropTable("dbo.Categories");
            DropTable("dbo.Calificacions");
            DropTable("dbo.CalificacionEstandarAtributoes");
            DropTable("dbo.PesoAtributoes");
            DropTable("dbo.Rols");
            DropTable("dbo.UserRols");
            DropTable("dbo.Nivels");
            DropTable("dbo.Users");
            DropTable("dbo.Campanas");
            DropTable("dbo.Formularios");
            DropTable("dbo.Estandars");
            DropTable("dbo.Atributoes");
        }
    }
}
