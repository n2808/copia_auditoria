namespace tmkhogares.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatingDatabase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calificacions", "EsAcorde", c => c.Boolean(nullable: false));
            AddColumn("dbo.Calificacions", "PorqueEsAcorde", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calificacions", "PorqueEsAcorde");
            DropColumn("dbo.Calificacions", "EsAcorde");
        }
    }
}
