﻿using Core.Models.Security;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using tmkhogares.Models;

namespace tmkhogares.Servicios
{
    public class ServicioRol
    {
        private readonly Contexto db = new Contexto();
        private readonly ServicioCampanas servicioCampanas = new ServicioCampanas();
        public ServicioRol() { }

        /// <summary>
        /// Devuelve el listado de roles, teniendo en cuenta el usuario en session.
        /// </summary>
        /// <returns></returns>
        public List<Core.Models.User.Rol> RetornarRoles()
        {
            if (SessionPersister.HasRol("Gerente"))
            {
                return db.Roles.Where(r => r.Id == tmkhogares.ViewModel.Roles.Asesor || r.Id == tmkhogares.ViewModel.Roles.Reporting || r.Id == tmkhogares.ViewModel.Roles.TeamLeader).ToList();
            }
            else if (SessionPersister.HasRol("TeamLeader"))
            {
                return db.Roles.Where(r => r.Id == tmkhogares.ViewModel.Roles.Asesor).ToList();

            }
            else if (SessionPersister.HasRol("Administrador"))
            {
                return db.Roles.ToList();
            }
            else
            {
                return db.Roles.Where(r => r.Id == tmkhogares.ViewModel.Roles.Asesor).ToList();
            }
        }

        /// <summary>
        /// Método que se encarga de asignarle un rol a un usuario.
        /// </summary>
        /// <param name="rolId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool AsignarRolUsuario(Guid rolId, Guid userId)
        {
            var usuarioRol = new UserRol()
            {
                Id = new Guid(),
                RolId = rolId,
                UserId = userId
            };

            db.UsuariosRoles.Add(usuarioRol);
            db.SaveChanges();
            return true;
        }

        /// <summary>
        /// Método para permitir asignar campañas, teniendo en cuenta el rol de administrador o de calidad.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool ValidarRolPorUsuario(Guid? userId)
        {
            var campanas = servicioCampanas.RetornarCampanasPorUsuarioId((Guid)userId);
            var rol = db.UsuariosRoles.Where(x => x.UserId == userId).FirstOrDefault();

            if (rol == null)
            {
                return false;
            }

            if (rol.RolId == new Guid(tmkhogares.Constantes.Rol.ASESOR) && campanas.Count() == 0)
            {
                return true;
            }
            else
            {
                if (campanas.Any(x => x.CampanaId == Constantes.Campanas.TERCER_ANILLO_HOGAR) || campanas.Any(x => x.CampanaId == Constantes.Campanas.TERCER_ANILLO_MOVIL))
                {
                    return true;
                }
                else
                {
                    if (rol.RolId == new Guid(tmkhogares.Constantes.Rol.ADMINISTRADOR) || rol.RolId == new Guid(tmkhogares.Constantes.Rol.CALIDAD) || campanas.Any(x => x.CampanaId == 0))
                    {

                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
            }

         

        }
    }
}