﻿using Core.Models.Security;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using tmkhogares.Constantes;
using tmkhogares.Models;
using tmkhogares.Models.Calificacion;
using tmkhogares.Models.Campañas;
using tmkhogares.ViewModel.Calificacion;
using Calificacion = tmkhogares.Constantes.Calificacion;

namespace tmkhogares.Servicios
{
    public class ServicioCalificacion
    {
        private readonly Contexto db = new Contexto();
        private readonly ServicioAtributos _atributo = new ServicioAtributos();
        public ServicioCalificacion() { }

        public decimal Calificar(CalificacionVM model)
        {

            IQueryable<Formulario> form = db.Formularios;

            // Variable utilizada para indicarle al sistema si cumple con los atributos de estado crítico.
            bool esCritico = false;
            decimal notaFinal = 0;

            // Variables utilizadas para calcular el número de atributos que no aplican en el proceso de calificación.
            int totalAtributosGeneral_NA = 0;
            int totalAtributosProceso_NA = 0;

            foreach (var item in model.DataCalificacion)
            {
                if (item.Atributos.TipoAtributo == new Guid(TipoAtributo.PROCESO)) // Atributo de tipo proceso.
                {
                    if (item.Atributos.Calificacion == new Guid(Calificacion.NoAplica))
                    {
                        totalAtributosProceso_NA++;
                    }
                }

                if (item.Atributos.TipoAtributo == new Guid(TipoAtributo.GENERAL)) // Atributo de tipo proceso.
                {
                    if (item.Atributos.Calificacion == new Guid(Calificacion.NoAplica))
                    {
                        totalAtributosGeneral_NA++;
                    }
                }
            }


            var porcentajeGeneral = form.Where(x => x.Id == model.FormularioId).FirstOrDefault().PorcentajeGeneral;
            var porcentajeProceso = form.Where(x => x.Id == model.FormularioId).FirstOrDefault().PorcentajeProceso;



            var totalAtributosGenerales = db.Atributo.Where(x => x.TipoAtributoId == new Guid(TipoAtributo.GENERAL)
                                                    && x.Estandares.FormularioId == model.FormularioId
                                                    && x.Estado).Count() - totalAtributosGeneral_NA;

            var totalAtributosProceso = db.Atributo.Where(x => x.TipoAtributoId == new Guid(TipoAtributo.PROCESO)
                                                    && x.Estandares.FormularioId == model.FormularioId
                                                    && x.Estado).Count() - totalAtributosProceso_NA;

            int tag = totalAtributosGenerales; // Valor constante que representará el total de atributos generales.
            int tap = totalAtributosProceso; // Valor constante que representará el total de atributos de proceso.

            User UsuarioCalidad = db.Usuario.Find(model.CalidadId);
            User UsuarioAsesor = db.Usuario.Find(model.AsesorId);

            var calificacion = new tmkhogares.Models.Calificacion.Calificacion
            {

                AsesorId = model.AsesorId,
                CampañaId = model.CampanaId,
                ComentariosAsesor = model.ComentariosAsesor,
                Descripcion = model.DescripcionGeneral,
                EsTodoClaro = model.EsTodoClaro,
                EsVenta = model.EsVenta,
                EsVentaClaroUp = model.EsVentaClaroUp,
                EsVentaContado = model.EsVentaContado,
                EsVentaFinaciada = model.EsVentaFinaciada,
                FormularioId = model.FormularioId,
                Id = Guid.NewGuid(),
                IdGrabacion = model.IdGrabacion,
                NombreAsesor = UsuarioAsesor.Names,
                NombreCalidad = UsuarioCalidad.Names,
                Puntaje = Decimal.Parse("0"),
                RolCalifica = Guid.Parse(SessionPersister.Id.ToString()),
                TelefonoGrabacion = model.TelefonoGrabacion,
                TipoMonitoreo = model.TipoMonitoreo,
                SubCampana = model.SubCampana,
                Ciudad = model.Ciudad,
                MotivoConsulta = model.MotivoConsulta,
                Segmento = model.Segmento,
                Marcacion = model.Marcacion,
                UsuarioCalidadId = UsuarioCalidad.Id,
                UtilizaGuionBienvenida = model.UtilizaGuionBienvenida,
                UtilizaGuionDespedida = model.UtilizaGuionDespedida,
                EsRetenido = model.EsRetenido,
                DuracionTMO = model.DuracionTMO,
                procede = model.Procede,
                FechaLlamada = model.FechaLlamada,
                Tipologia = model.Tipologia

            };

            db.Calificacions.Add(calificacion);
            db.SaveChanges();

           
            // Valores que serán asignados al modelo GANA.
            decimal G   = _atributo.ProrrateoModeloGana(model.DataCalificacion, Letra.LETRA_G);
            decimal A_1 = _atributo.ProrrateoModeloGana(model.DataCalificacion, Letra.LETRA_A1);
            decimal N   = _atributo.ProrrateoModeloGana(model.DataCalificacion, Letra.LETRA_N);
            decimal A_2 = _atributo.ProrrateoModeloGana(model.DataCalificacion, Letra.LETRA_A2);




            foreach (var item in model.DataCalificacion)
            {
                CalificacionEstandarAtributo cea = new CalificacionEstandarAtributo();

                if (item.Atributos.TipoAtributo == new Guid(TipoAtributo.PROCESO)) // Atributo de tipo proceso.
                {
                    if (item.Atributos.Calificacion == new Guid(Calificacion.NoAplica))
                    {
                        notaFinal += Decimal.Parse("0");
                        

                        // Se crea el detalle de la calificación.
                        cea.AtributoId = item.Atributos.AtributoId;
                        cea.PosicionAtributo = item.Atributos.Posicion;
                        cea.Descripcion = item.Atributos.Descripcion;
                        cea.CalificacionId = calificacion.Id;
                        cea.EstandarId = item.EstandarId;
                        cea.Id = Guid.NewGuid();
                        cea.ItemCalificadoId = item.Atributos.Calificacion;
                        db.CalificacionEstandarAtributoes.Add(cea);
                        db.SaveChanges();

                    }
                    else if (item.Atributos.Calificacion == new Guid(Calificacion.NoCumple))
                    {
                        notaFinal += Decimal.Parse("0");

                        // Se crea el detalle de la calificación.
                        cea.AtributoId = item.Atributos.AtributoId;
                        cea.PosicionAtributo = item.Atributos.Posicion;
                        cea.Descripcion = item.Atributos.Descripcion;
                        cea.CalificacionId = calificacion.Id;
                        cea.EstandarId = item.EstandarId;
                        cea.Id = Guid.NewGuid();
                        cea.ItemCalificadoId = item.Atributos.Calificacion;
                        db.CalificacionEstandarAtributoes.Add(cea);
                        db.SaveChanges();
                    }
                    else
                    {

                        notaFinal += porcentajeProceso / totalAtributosProceso;

                        // Se crea el detalle de la calificación.
                        cea.AtributoId = item.Atributos.AtributoId;
                        cea.PosicionAtributo = item.Atributos.Posicion;
                        cea.Descripcion = item.Atributos.Descripcion;
                        cea.CalificacionId = calificacion.Id;
                        cea.EstandarId = item.EstandarId;
                        cea.Id = Guid.NewGuid();
                        cea.ItemCalificadoId = item.Atributos.Calificacion;
                        db.CalificacionEstandarAtributoes.Add(cea);
                        db.SaveChanges();
                    }
                }
                else if (item.Atributos.TipoAtributo == new Guid(TipoAtributo.GENERAL)) // Atributo de tipo general.
                {
                    if (item.Atributos.Calificacion == new Guid(Calificacion.NoAplica))
                    {

                        notaFinal += Decimal.Parse("0");                   


                        // Se crea el detalle de la calificación.
                        cea.AtributoId = item.Atributos.AtributoId;
                        cea.PosicionAtributo = item.Atributos.Posicion;
                        cea.Descripcion = item.Atributos.Descripcion;
                        cea.CalificacionId = calificacion.Id;
                        cea.EstandarId = item.EstandarId;
                        cea.Id = Guid.NewGuid();
                        cea.ItemCalificadoId = item.Atributos.Calificacion;
                        db.CalificacionEstandarAtributoes.Add(cea);
                        db.SaveChanges();

                    }
                    else if (item.Atributos.Calificacion == new Guid(Calificacion.NoCumple))
                    {
                        notaFinal += Decimal.Parse("0");

                        // Se crea el detalle de la calificación.
                        cea.AtributoId = item.Atributos.AtributoId;
                        cea.PosicionAtributo = item.Atributos.Posicion;
                        cea.Descripcion = item.Atributos.Descripcion;
                        cea.CalificacionId = calificacion.Id;
                        cea.EstandarId = item.EstandarId;
                        cea.Id = Guid.NewGuid();
                        cea.ItemCalificadoId = item.Atributos.Calificacion;
                        db.CalificacionEstandarAtributoes.Add(cea);
                        db.SaveChanges();
                    }
                    else
                    {

                        notaFinal += porcentajeGeneral / totalAtributosGenerales;

                        // Se crea el detalle de la calificación.
                        cea.AtributoId = item.Atributos.AtributoId;
                        cea.PosicionAtributo = item.Atributos.Posicion;
                        cea.Descripcion = item.Atributos.Descripcion;
                        cea.CalificacionId = calificacion.Id;
                        cea.EstandarId = item.EstandarId;
                        cea.Id = Guid.NewGuid();
                        cea.ItemCalificadoId = item.Atributos.Calificacion;
                        db.CalificacionEstandarAtributoes.Add(cea);
                        db.SaveChanges();
                    }
                }
                else if (item.Atributos.TipoAtributo == new Guid(TipoAtributo.GENERICO)) // Atributo de tipo proceso.
                {

                    // Se crea el detalle de la calificación.
                    cea.AtributoId = item.Atributos.AtributoId;
                    cea.PosicionAtributo = item.Atributos.Posicion;
                    cea.Descripcion = item.Atributos.Descripcion;
                    cea.CalificacionId = calificacion.Id;
                    cea.EstandarId = item.EstandarId;
                    cea.Id = Guid.NewGuid();
                    cea.ItemCalificadoId = item.Atributos.Calificacion;
                    db.CalificacionEstandarAtributoes.Add(cea);
                    db.SaveChanges();

                }
                else
                {
                    // Atributo de tipo crítico.

                    if (item.Atributos.Calificacion == new Guid(Calificacion.NoAplica))
                    {  
                        // Se crea el detalle de la calificación.
                        cea.AtributoId = item.Atributos.AtributoId;
                        cea.PosicionAtributo = item.Atributos.Posicion;
                        cea.Descripcion = item.Atributos.Descripcion;
                        cea.CalificacionId = calificacion.Id;
                        cea.EstandarId = item.EstandarId;
                        cea.Id = Guid.NewGuid();
                        cea.ItemCalificadoId = item.Atributos.Calificacion;
                        db.CalificacionEstandarAtributoes.Add(cea);
                        db.SaveChanges();

                    }
                    else if (item.Atributos.Calificacion == new Guid(Calificacion.NoCumple))
                    {
                        esCritico = true;

                        // Se crea el detalle de la calificación.
                        cea.AtributoId = item.Atributos.AtributoId;
                        cea.PosicionAtributo = item.Atributos.Posicion;
                        cea.Descripcion = item.Atributos.Descripcion;
                        cea.CalificacionId = calificacion.Id;
                        cea.EstandarId = item.EstandarId;
                        cea.Id = Guid.NewGuid();
                        cea.ItemCalificadoId = item.Atributos.Calificacion;
                        db.CalificacionEstandarAtributoes.Add(cea);
                        db.SaveChanges();
                    }
                    else
                    {

                        // Se crea el detalle de la calificación.
                        cea.AtributoId = item.Atributos.AtributoId;
                        cea.PosicionAtributo = item.Atributos.Posicion;
                        cea.Descripcion = item.Atributos.Descripcion;
                        cea.CalificacionId = calificacion.Id;
                        cea.EstandarId = item.EstandarId;
                        cea.Id = Guid.NewGuid();
                        cea.ItemCalificadoId = item.Atributos.Calificacion;
                        db.CalificacionEstandarAtributoes.Add(cea);
                        db.SaveChanges();
                    }
                }

                if (esCritico)
                {
                    notaFinal = (notaFinal * 0);
                }


                // Creamos el quartil para cada calificación.
                if (notaFinal <= 75)
                {
                    calificacion.Cuartil = "Q4";
                }
                else if (notaFinal > 75 && notaFinal <= 85)
                {
                    calificacion.Cuartil = "Q3";
                }
                else if (notaFinal > 85 && notaFinal <= 94)
                {
                    calificacion.Cuartil = "Q2";
                }
                else if (notaFinal > 94 && notaFinal <= 100)
                {
                    calificacion.Cuartil = "Q1";
                }

               

                calificacion.Puntaje = notaFinal;
                db.Entry(calificacion).State = EntityState.Modified;
                db.SaveChanges();


            }


            ActualizarModeloGana(calificacion.Id, G, A_1, N, A_2);

            return notaFinal;
        }


        private void ActualizarModeloGana(Guid id, decimal G, decimal A_1, decimal N, decimal A_2)
        {
            var calificacion = db.Calificacions.Where(x => x.Id == id).FirstOrDefault();

            calificacion.ModeloGana_G  = G;
            calificacion.ModeloGana_A1 = A_1;
            calificacion.ModeloGana_N  = N;
            calificacion.ModeloGana_A2 = A_2;
            db.Entry(calificacion).State = EntityState.Modified;
            db.SaveChanges();

        }

        public void Delete(Guid? id)
        {
          
            using (var context = new Contexto())
            {
                // CREATING A BEGIN TRANSACTION AND COMMIT.
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        //BEGIN OF THE QUERY
                        // Consultamos la lista de atributos por calificación.
                        List<CalificacionEstandarAtributo> calificacionEstandarAtributos = db.CalificacionEstandarAtributoes.Where(x => x.CalificacionId == id).ToList();

                        //Ahora eliminamos toda la lista.
                        db.CalificacionEstandarAtributoes.RemoveRange(calificacionEstandarAtributos);
                        db.SaveChanges();

                        tmkhogares.Models.Calificacion.Calificacion calificacion = db.Calificacions.Find(id);
                        db.Calificacions.Remove(calificacion);
                        db.SaveChanges();

                      
                        //COMMIT TRANSACTION
                        dbContextTransaction.Commit();

                        

                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);

                    }
                }
            }
        }
    }
}