﻿using System;
using tmkhogares.Models;
using tmkhogares.Models.UsuariosCampanas;

namespace tmkhogares.Servicios
{
    public class ServicioUsuarioCampana
    {
        private readonly Contexto db = new Contexto();
        public ServicioUsuarioCampana() { }
        public bool AsignarCampanaUsuario(int campanaId, Guid userId)
        {
            var usuarioCampana = new UsuarioCampanas()
            {
                Id        = new Guid(),
                CampanaId = campanaId,
                UserId    = userId
            };

            db.UsuariosCampanas.Add(usuarioCampana);
            db.SaveChanges();
            return true;
        }
    }
}