﻿using System;
using System.Collections.Generic;
using System.Linq;
using tmkhogares.Models;
using tmkhogares.Models.Campañas;
using tmkhogares.Models.UsuariosCampanas;

namespace tmkhogares.Servicios
{
    public class ServicioCampanas
    {
        private readonly Contexto db = new Contexto();
        public ServicioCampanas() { }

        /// <summary>
        /// Devuelve el listado de campañas que se encuentren activas.
        /// </summary>
        /// <returns></returns>
        public List<Campana> RetornarListaCampanas()
        {
            return db.Campana.OrderBy(x => x.Nombre).Where(x => x.Estado).ToList();
        }

        /// <summary>
        /// Devuelve el listado de campañas que se le han asignado al usuario que está en session.
        /// </summary>
        /// <returns></returns>
        public List<Campana> RetornarListaCampanasPorUsuarioSession(Guid usuarioSession)
        {
            var campanas = new List<Campana>();
            var campanasId = db.UsuariosCampanas.Where(x => x.UserId == usuarioSession).ToList();
            foreach (var item in campanasId)
            {
                campanas.Add(db.Campana.Where(x => x.Id == item.CampanaId).FirstOrDefault());
            }

            return campanas;
        }

        public Campana RetornarCampanaPorId(int campana)
        {
            return db.Campana.Where(x => x.Id == campana && x.Estado).FirstOrDefault();

        }


        public int RetornarCampanaPorUsuarioId(Guid id)
        {
            return db.UsuariosCampanas.Where(x => x.UserId == id).Select(x => x.CampanaId).FirstOrDefault();
        }

        public List<UsuarioCampanas> RetornarCampanasPorUsuarioId(Guid id)
        {
            return db.UsuariosCampanas.Where(x => x.UserId == id).ToList();
        }

        public (bool, string) ValidarAsignacionCampanasPorUsuario(Guid? userId, int? campanaId)
        {
            var campanas = RetornarCampanasPorUsuarioId((Guid)userId);

            if (campanas.Any(x => x.CampanaId == campanaId))
            {
                return (false, "La campaña ya fue asignada para este usuario");
            }
            else
            {
                if (!ValidarCampanasTercerAnillo(campanas, campanaId))
                {
                    return (false, "No es posible asignar más campañas al usuario. Consulte con el administrador");

                }
                else
                {
                    return (true, string.Empty);

                }
            }

        }

        public bool ValidarCampanasTercerAnillo(List<UsuarioCampanas> campanas, int? campanaId)
        {
            var retorno = false;

            if (campanas.Any(x => x.CampanaId == Constantes.Campanas.TERCER_ANILLO_HOGAR) && campanas.Any(x => x.CampanaId == Constantes.Campanas.TERCER_ANILLO_MOVIL))
            {
                retorno = false;
            }
            else
            {
                if (campanas.Count() == 1)
                {
                    if ((campanas.Any(x => x.CampanaId == Constantes.Campanas.TERCER_ANILLO_HOGAR) && campanaId != Constantes.Campanas.TERCER_ANILLO_MOVIL)
                        || (campanas.Any(x => x.CampanaId == Constantes.Campanas.TERCER_ANILLO_MOVIL) && campanaId != Constantes.Campanas.TERCER_ANILLO_HOGAR)
                        )
                    {
                        retorno = false;

                    }
                    else
                    {
                        retorno = true;

                    }
                }
                else
                {

                    retorno = true;

                }

            }

            return retorno;
        }


    }
}