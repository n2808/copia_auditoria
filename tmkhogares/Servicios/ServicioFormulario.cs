﻿using System.Linq;
using tmkhogares.Models;
using tmkhogares.Models.Campañas;

namespace tmkhogares.Servicios
{
    public class ServicioFormulario
    {
        private readonly Contexto db = new Contexto();
        public ServicioFormulario() { }

        /// <summary>
        /// Retorna el formulario teniendo en cuenta el padreId de la campaña.
        /// </summary>
        /// <param name="campana"></param>
        /// <returns></returns>
        public Formulario RetornarFormularioPorCampana(int campana)
        {
            var idCampana = db.Campana.Where(x => x.Id == campana).Select(x => x.PadreId).FirstOrDefault();
            return db.Formularios.Where(x => x.CampanaId == idCampana).FirstOrDefault();
        }     
    }
}