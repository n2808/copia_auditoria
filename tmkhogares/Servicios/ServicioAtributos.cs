﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using tmkhogares.Constantes;
using tmkhogares.Models;
using tmkhogares.Models.Estandares;
using tmkhogares.ViewModel.Calificacion;

namespace tmkhogares.Servicios
{
    public class ServicioAtributos
    {
        private readonly Contexto db = new Contexto();
        public ServicioAtributos() { }

        /// <summary>
        /// Edita las posiciones de los atributos.
        /// </summary>
        /// <param name="atributos"></param>
        public void EditarAtributos(List<Atributo> atributos)
        {
            foreach (var item in atributos)
            {
                item.Posicion += 1;
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
            }
        }



        public decimal ProrrateoModeloGana(List<DataCalificacionVM> dcvm, int letra)
        {
            List<Atributo> atributos = new List<Atributo>();
            List<Atributo> atributosNuevos = new List<Atributo>();
            decimal valorRetorno = 0M;
            decimal valorDistribuido = 0M;
            decimal valorprorrateo = 0M;
            var valorTotal = 100M;
            var cont = 0;
            var totalNoAplican = 0;
            var totalCumplen = 0;
            var totalNoCumplen = 0;
            var estado1 = false;
            var estado2 = false;


            foreach (var item in dcvm)
            {
                var atributo = db.Atributo.Include(x => x.AtributoModeloGana)
                                          .Include(x => x.AtributoModeloGana.ModeloGana)
                                          .Include(x => x.Estandares).Where(x => x.Id == item.Atributos.AtributoId)
                                          .FirstOrDefault();

                if (atributo.AplicaModeloGana && atributo.AtributoModeloGana.ModeloGana.Id == letra) atributos.Add(atributo);


            }


            // Validamos si existen atributos para la realización del prorrateo.
            if (atributos.Count() > 0)
            {

                var atributosAgrupados = atributos.GroupBy(x => x.AtributoModeloGanaId).ToList();

                // Validamos si alguno de los atributos pertenecen a un grupo.
                foreach (var item in atributosAgrupados)
                {
                    if (item.Count() > 1)
                    {
                        estado1 = true;
                    }
                    else
                    {
                        estado2 = true;
                    }
                }
                               
                valorDistribuido = ObtenerValorDistribuido(atributosAgrupados, dcvm, valorTotal, atributos);

                totalNoAplican = ObtenerTotalNoAplican(atributosAgrupados, dcvm);
                totalCumplen = ObtenerTotalCumplen(atributosAgrupados, dcvm);
                totalNoCumplen = ObtenerTotalNoCumplen(atributosAgrupados, dcvm);

                if (totalCumplen > 0 || totalNoCumplen > 0)
                {
                    valorprorrateo = valorTotal / (totalCumplen + totalNoCumplen);
                }


              

                foreach (var i in atributosAgrupados)
                {
                    if (i.Count() > 1)
                    {

                        foreach (var j in i.AsEnumerable().ToList())
                        {
                            foreach (var k in dcvm)
                            {

                                if (k.Atributos.AtributoId == j.Id && k.Atributos.Calificacion == new Guid(Calificacion.NoAplica))
                                {
                                    cont++;
                                }
                            }
                        }

                        foreach (var j in i.AsEnumerable().ToList())
                        {
                            foreach (var k in dcvm)
                            {
                                if (k.Atributos.AtributoId == j.Id && (k.Atributos.Calificacion == new Guid(Calificacion.Cumple)
                                    || k.Atributos.Calificacion == new Guid(Calificacion.Cinco)
                                    || k.Atributos.Calificacion == new Guid(Calificacion.Cuatro)))
                                {
                                    valorRetorno += valorDistribuido / (i.Count() - cont);
                                }
                            }
                        }

                    }
                    else
                    {

                        foreach (var j in i.AsEnumerable().ToList())
                        {
                            foreach (var k in dcvm)
                            {
                                if (k.Atributos.AtributoId == j.Id && (k.Atributos.Calificacion == new Guid(Calificacion.Cumple)
                                    || k.Atributos.Calificacion == new Guid(Calificacion.Cinco)
                                    || k.Atributos.Calificacion == new Guid(Calificacion.Cuatro)))
                                {
                                    if (estado1 && estado2)
                                    {
                                        valorRetorno += valorDistribuido;
                                    }
                                    else
                                    {
                                        valorRetorno += valorprorrateo;
                                    }
                                }


                            }
                        }

                    }
                }

                // Si todos los atributos no aplican el valor del retorno es 0;
                if (atributos.Count() == (cont + totalNoAplican)) valorRetorno = 0M;


                return valorRetorno;
            }
            else
            {
                return 0M;

            }


        }


        private int ObtenerTotalNoAplican(List<IGrouping<int?, Atributo>> atributosAgrupados, List<DataCalificacionVM> dcvm)
        {
            var totalNoAplican = 0;

            foreach (var i in atributosAgrupados)
            {
                if (i.Count() == 1)
                {
                    foreach (var j in i.AsEnumerable().ToList())
                    {
                        foreach (var k in dcvm)
                        {
                            if (k.Atributos.AtributoId == j.Id && k.Atributos.Calificacion == new Guid(Calificacion.NoAplica))
                            {
                                totalNoAplican++;
                            }
                        }
                    }

                }

            }
            return totalNoAplican;
        }

        private int ObtenerTotalCumplen(List<IGrouping<int?, Atributo>> atributosAgrupados, List<DataCalificacionVM> dcvm)
        {
            var totalCumplen = 0;

            foreach (var i in atributosAgrupados)
            {
                if (i.Count() == 1)
                {
                    foreach (var j in i.AsEnumerable().ToList())
                    {
                        foreach (var k in dcvm)
                        {
                            if (k.Atributos.AtributoId == j.Id && (k.Atributos.Calificacion == new Guid(Calificacion.Cumple)
                                || k.Atributos.Calificacion == new Guid(Calificacion.Cinco)
                                || k.Atributos.Calificacion == new Guid(Calificacion.Cuatro)))
                            {
                                totalCumplen++;
                            }
                        }
                    }

                }

            }
            return totalCumplen;
        }

        private int ObtenerTotalNoCumplen(List<IGrouping<int?, Atributo>> atributosAgrupados, List<DataCalificacionVM> dcvm)
        {
            var totalNoCumplen = 0;

            foreach (var i in atributosAgrupados)
            {
                if (i.Count() == 1)
                {
                    foreach (var j in i.AsEnumerable().ToList())
                    {
                        foreach (var k in dcvm)
                        {
                            if (k.Atributos.AtributoId == j.Id && (k.Atributos.Calificacion == new Guid(Calificacion.NoCumple)
                                || k.Atributos.Calificacion == new Guid(Calificacion.Tres)
                                || k.Atributos.Calificacion == new Guid(Calificacion.Dos)
                                || k.Atributos.Calificacion == new Guid(Calificacion.Uno)))
                            {
                                totalNoCumplen++;
                            }
                        }
                    }

                }

            }
            return totalNoCumplen;
        }

        private decimal ObtenerValorDistribuido(List<IGrouping<int?, Atributo>> atributosAgrupados, List<DataCalificacionVM> dcvm, decimal valorTotal, List<Atributo> atributos)
        {
            var itemsIndividuales = false;
            var contGrupos = 0;
            var count = atributos.GroupBy(x => x.AtributoModeloGanaId).Count();

            foreach (var i in atributosAgrupados)
            {
                if (i.Count() == 1)
                {

                    itemsIndividuales = true;
                }
                else
                {
                    contGrupos++;
                }

            }

            if (itemsIndividuales)
            {
                return valorTotal / (contGrupos + 1);

            }
            else
            {
                return valorTotal;
            }
        }


    }

}






