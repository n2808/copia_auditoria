﻿using System.Web;
using System.Web.Optimization;

namespace tmkhogares
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Scripts/jquery-3.1.1.min.js",
                "~/Content/select2/select2.min.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/jquery-ui-1.12.1.min.js",
                "~/Scripts/jquery-slimscroll/jquery.slimscroll.min.js",
                "~/Scripts/fastclick/lib/fastclick.js",
                "~/Content/dist/semantic.min.js",
                "~/Scripts/datatables.net/js/jquery.dataTables.js",
                "~/Scripts/app/globales.js",
                "~/Scripts/moment.min.js",
                "~/Scripts/fullcalendar/fullcalendar.min.js",
                "~/Scripts/fullcalendar/fullcalendar-locale.min.js",
                "~/Scripts/sweetAlert/sweetAlert.min.js",
                "~/Scripts/sweetalert2.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery.validate*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/themes/base/jquery-ui.min.css",
                      "~/Content/select2/select2.min.css",
                      "~/Content/semantic/dist/semantic.min.cs",
                      "~/Content/Site.css",
                      "~/Content/fullcalendar.min.css",
                      "~/Content/sweetalert/sweetalert.min.css"
            ));
        }
    }
}

