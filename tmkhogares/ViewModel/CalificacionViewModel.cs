﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.ViewModel
{
    public class CalificacionViewModel
    {
        public class _CalificarEstandar
        {
            public Guid Id { get; set; }
            public string Nombre { get; set; }
            public List<_Atributo> atributos { get; set; }
        }
        public class _Atributo
        {
            public Guid Id { get; set; }
            public string Nombre { get; set; }
            public double Peso { get; set; }
        }
    }
}