﻿namespace tmkhogares.ViewModel
{
    public class ResumenFeedBackVM
    {
        public int? PendientesFeedBack { get; set; }
        public int? NoPendientesFeedBack { get; set; }
    }
}