﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.ViewModel
{
    public class ErroresFormatoVM
    {
        public string Identificacion { get; set; }
        public string TipoError { get; set; }
    }
}