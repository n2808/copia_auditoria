﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using tmkhogares.Models;

namespace tmkhogares.ViewModel
{
    public class CustoErrors
    {
        public string Name { get; set; }
        public string Message { get; set; }
        public CustomErrorType Type { get; set; }
    }

    public enum CustomErrorType
    {
        Simple = 0,
        Notfound = 404,
        badRequest = 500
    }

    public class Roles
    {
        public static Guid Asesor        { get; set; } = new Guid("4450491C-32E6-41AE-B301-CB22DFC8BBD9");
        public static Guid Reporting     { get; set; } = new Guid("EC52E1C8-D717-4762-B40E-7BEC049EDC45");
        public static Guid Calidad       { get; set; } = new Guid("C2FA35E2-1FAF-4F68-945D-9F5262484ED1");
        public static Guid TeamLeader    { get; set; } = new Guid("B0A06B9B-FB6E-4195-BC1C-AD84AD9A194D");
        public static Guid Gerente       { get; set; } = new Guid("0A2155D6-3E87-485E-8233-2DB2EA0DEDBE");
        public static Guid Administrador { get; set; } = new Guid("13FA2EA3-C034-4B0A-8F66-F2AB10DA8944");

    }

    public class Configurations
    {
        public static Guid TipoRolFormulario { get; set; } = new Guid("840824b0-3658-4ac2-9e7a-8a5b24e2cc7e");
    }
   public class _Formularios
    {
        public static Guid ModeloGana { get; set; } = new Guid("E57EC1E1-3E5F-4962-975F-4F966F6EA48D");
    }
    public class Errors
    {
        //TeamLeader Dont Exist
        public static string TLDE { get; set; } = "TLDE";
        //Campaign Dont Exist
        public static string CDE { get; set; } = "CDE";
    }

    public class Metodos
    {
        private static Contexto db = new Contexto();

        public static string ObtenerTipoMonitoreo(string FormularioId = "")
        {
            Guid Formulario = new Guid();
            Guid.TryParse(FormularioId, out Formulario);
            if (FormularioId == "")
            {
                return "Error Formulario";
            }

            var Matriz = db.Formularios.Where(f => f.Id == Formulario).FirstOrDefault();
            if (Matriz == null)
            {
                return "Error Matriz";
            }
            var Rol = db.Roles.Where(r => r.Id == Matriz.TipoRol).FirstOrDefault();
            if(Rol == null)
            {
                return "Error Rol";
            }
            else
            {
                return Rol.Name;
            }

            
        }
    }
}