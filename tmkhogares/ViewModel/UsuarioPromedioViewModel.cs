﻿using Core.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.ViewModel
{
    public class UsuarioPromedioViewModel
    {
        public User Usuario { get;set; }
        public string Promedio { get; set; }
        
    }
}