﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.ViewModel
{
    public class AuditadosVM
    {
        public Guid? Id { get; set; }
        public string Documento { get; set; }
        public string Nombre { get; set; }
        public string TeamLeader { get; set; }
        public Guid? CalificacionId{ get; set; }
        public Guid? FormularioId{ get; set; }
        public Guid? AuditorId{ get; set; }
        public int? CampanaId{ get; set; }
        public DateTime? FechaMonitoreo{ get; set; }
     
    }
}