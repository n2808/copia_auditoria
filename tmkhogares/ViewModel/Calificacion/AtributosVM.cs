﻿using System;

namespace tmkhogares.ViewModel.Calificacion
{
    public class AtributosVM
    {
        public Guid AtributoId { get; set; }
        public string Descripcion { get; set; }
        public Guid Calificacion { get; set; }
        public Guid TipoAtributo { get; set; }
        public int Posicion { get; set; }
    }
}

