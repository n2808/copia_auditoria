﻿using System;
using System.Collections.Generic;

namespace tmkhogares.ViewModel.Calificacion
{
    public class CalificacionVM
    {
        public Guid? CalificacionId { get; set; }
        public Guid AsesorId { get; set; }
        public Guid CalidadId { get; set; }
        public int CampanaId { get; set; }
        public Guid FormularioId { get; set; }
        public string LoginAsesor { get; set; }
        public string ComentariosAsesor { get; set; }
        public string DescripcionGeneral { get; set; }
        public string TipoMonitoreo { get; set; }
        public string Procede { get; set; }
        public string EsVenta { get; set; }
        public string EsVentaContado { get; set; }
        public string EsVentaFinaciada { get; set; }
        public string EsTodoClaro { get; set; }
        public string UtilizaGuionBienvenida { get; set; }
        public string UtilizaGuionDespedida { get; set; }
        public string EsRetenido { get; set; }
        public int? DuracionTMO { get; set; }
        public string EsVentaClaroUp { get; set; }
        public string TelefonoGrabacion { get; set; }
        public string IdGrabacion { get; set; }
        public string SubCampana { get; set; }
        public string Segmento { get; set; }
        public string Ciudad { get; set; }
        public string MotivoConsulta { get; set; }
        public string Marcacion { get; set; }
        public DateTime? FechaLlamada { get; set; }  
        public string Tipologia { get; set; }
        public List<DataCalificacionVM> DataCalificacion { get; set; }


    }
}

