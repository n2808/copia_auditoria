﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmkhogares.ViewModel
{
    public class CargueViewModel
    {
        public List<string[]> AsesoresNoCreados { get; set; }
        public List<string[]> AsesoresNoPertenecen { get; set; }
        public List<string[]> AsesoresYaActualizados { get; set; }
    }
}