﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.Cuartiles;

namespace tmkhogares.Controllers
{
    public class TipoCuartilesController : Controller
    {
        private Contexto db = new Contexto();

        // GET: TipoCuartiles
        public ActionResult Index()
        {
            return View(db.TipoCuartiles.ToList());
        }

        // GET: TipoCuartiles/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCuartil tipoCuartil = db.TipoCuartiles.Find(id);
            if (tipoCuartil == null)
            {
                return HttpNotFound();
            }
            return View(tipoCuartil);
        }

        // GET: TipoCuartiles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoCuartiles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] TipoCuartil tipoCuartil)
        {
            if (ModelState.IsValid)
            {
                tipoCuartil.Id = Guid.NewGuid();
                db.TipoCuartiles.Add(tipoCuartil);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoCuartil);
        }

        // GET: TipoCuartiles/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCuartil tipoCuartil = db.TipoCuartiles.Find(id);
            if (tipoCuartil == null)
            {
                return HttpNotFound();
            }
            return View(tipoCuartil);
        }

        // POST: TipoCuartiles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] TipoCuartil tipoCuartil)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoCuartil).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoCuartil);
        }

        // GET: TipoCuartiles/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoCuartil tipoCuartil = db.TipoCuartiles.Find(id);
            if (tipoCuartil == null)
            {
                return HttpNotFound();
            }
            return View(tipoCuartil);
        }

        // POST: TipoCuartiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            TipoCuartil tipoCuartil = db.TipoCuartiles.Find(id);
            db.TipoCuartiles.Remove(tipoCuartil);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
