﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.PlanTrabajoAsesor;

namespace tmkhogares.Controllers
{
    public class ResumenPlanTrabajoAsesoresController : Controller
    {
        private Contexto db = new Contexto();

        // GET: ResumenPlanTrabajoAsesores
        public ActionResult Index()
        {
            return View(db.ResumenPlanTrabajoAsesor.ToList());
        }

        // GET: ResumenPlanTrabajoAsesores/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResumenPlanTrabajoAsesor resumenPlanTrabajoAsesor = db.ResumenPlanTrabajoAsesor.Find(id);
            if (resumenPlanTrabajoAsesor == null)
            {
                return HttpNotFound();
            }
            return View(resumenPlanTrabajoAsesor);
        }

        // GET: ResumenPlanTrabajoAsesores/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ResumenPlanTrabajoAsesores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Promedio,VentasRealizadas,Cumplimiento,Evolucion,Ausentismo,NotaCalidad,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] ResumenPlanTrabajoAsesor resumenPlanTrabajoAsesor)
        {
            if (ModelState.IsValid)
            {
                resumenPlanTrabajoAsesor.Id = Guid.NewGuid();
                db.ResumenPlanTrabajoAsesor.Add(resumenPlanTrabajoAsesor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(resumenPlanTrabajoAsesor);
        }

        // GET: ResumenPlanTrabajoAsesores/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResumenPlanTrabajoAsesor resumenPlanTrabajoAsesor = db.ResumenPlanTrabajoAsesor.Find(id);
            if (resumenPlanTrabajoAsesor == null)
            {
                return HttpNotFound();
            }
            return View(resumenPlanTrabajoAsesor);
        }

        // POST: ResumenPlanTrabajoAsesores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Promedio,VentasRealizadas,Cumplimiento,Evolucion,Ausentismo,NotaCalidad,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] ResumenPlanTrabajoAsesor resumenPlanTrabajoAsesor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(resumenPlanTrabajoAsesor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(resumenPlanTrabajoAsesor);
        }

        // GET: ResumenPlanTrabajoAsesores/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ResumenPlanTrabajoAsesor resumenPlanTrabajoAsesor = db.ResumenPlanTrabajoAsesor.Find(id);
            if (resumenPlanTrabajoAsesor == null)
            {
                return HttpNotFound();
            }
            return View(resumenPlanTrabajoAsesor);
        }

        // POST: ResumenPlanTrabajoAsesores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ResumenPlanTrabajoAsesor resumenPlanTrabajoAsesor = db.ResumenPlanTrabajoAsesor.Find(id);
            db.ResumenPlanTrabajoAsesor.Remove(resumenPlanTrabajoAsesor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
