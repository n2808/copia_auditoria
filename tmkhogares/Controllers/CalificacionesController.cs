﻿using Core.Models.Security;
using Core.Models.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using tmkhogares.Constantes;
using tmkhogares.Models;
using tmkhogares.Models.Calificacion;
using tmkhogares.Models.Campañas;
using tmkhogares.Models.Estandares;
using tmkhogares.Models.Grabaciones;
using tmkhogares.Models.PlanTrabajoAsesor;
using tmkhogares.Models.PlanTrabajoTeam;
using tmkhogares.Models.Rutas;
using tmkhogares.Models.UsuariosCampanas;
using tmkhogares.Servicios;
using tmkhogares.ViewModel;
using tmkhogares.ViewModel.Calificacion;
using Calificacion = tmkhogares.Models.Calificacion.Calificacion;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Calidad,TeamLeader,Gerente,Asesor,Administrador")]

    public class CalificacionesController : Controller
    {

        private Contexto db = new Contexto();
        private ServicioCampanas campanas = new ServicioCampanas();
        private ServicioFormulario formulario = new ServicioFormulario();
        private ServicioUsuarios _usuarios = new ServicioUsuarios();
        private ServicioCalificacion _calificacion = new ServicioCalificacion();
        private string ConexionPresence = @"Data Source=172.16.5.21\PRESENCE; Initial Catalog=SQLPR1; Integrated Security=false;User ID=sa; Password=Pr3s3nc3";
        private string ConexionRutas = @"Data Source=172.16.5.153;Initial Catalog=DBClasificacionGRabaciones;User ID=usrDBClasificacionGRabaciones;Password=usrDBClasificacionGRabaciones123;MultipleActiveResultSets=True;";
        private string nombreArchivo = "", nombreArchivoMp3 = "", rutaMP3 = "", path = "", CarpetaTemporal = "", nombreGrabacion = "", rutaDestino = "", RutaRecording, _idPresence = "";
        private bool Retorno = false;
        private int? _Tipo = null, _IdGestion = null;
        private decimal? ContactId = 0;

        public JsonResult BusquedaGrabacion(string idPresence = "", int Tipo = 1, int? IdGestion = null)
        {

            ContactId = null;
            _Tipo = Tipo;
            _idPresence = idPresence;
            _IdGestion = IdGestion;
            //1 => recuperacion
            //2 => Agente
            //3 => recuperacion

            if (idPresence == null)
            {
                return Json(new { status = 404, message = "no found" });
            }

            nombreArchivoMp3 = IdGestion + ".mp3";
            rutaMP3 = @"\\172.16.5.132\Claro_Legalizaciones\tempConversion\" + nombreArchivoMp3;

            if (Tipo == 1)
            {
                ContactId = BuscarEnPresence(idPresence);
                nombreArchivo = ContactId.ToString() + ".gsm";
            }

            if (Tipo == 2) // Agente
            {

                if (copiarArchivoGsm(rutaMP3, nombreArchivoMp3) == 1)
                {
                    string audio = "<audio id='audio2' src='/audioLectura/" + nombreArchivoMp3 + "' autoplay  controls></audio>";
                    string ruta = "/audioLectura/temp_" + nombreArchivoMp3 + ".mp3";
                    return Json(new { status = 200, message = audio, ruta = ruta });
                }



            }
            var i = 0;
            List<Rutas> ListaRutas = new List<Rutas>();
            using (SqlConnection conexionRutas = new SqlConnection(ConexionRutas))
            {
                string ProcedimientoRutas = "SP_TraerRutas";

                SqlCommand oCmd = new SqlCommand(ProcedimientoRutas, conexionRutas);
                conexionRutas.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        Rutas Ruta = new Rutas();
                        Guid Id = Guid.Parse(oReader["Id"].ToString());
                        Ruta.Id = Id;
                        Ruta.Ruta = oReader["Ruta"].ToString();
                        Ruta.Descripcion = oReader["Descripcion"].ToString();
                        ListaRutas.Add(Ruta);
                    }

                    conexionRutas.Close();
                }
            }



            foreach (Rutas R in ListaRutas.ToList())
            {
                string route = @R.Ruta + nombreArchivo;
                if (copiarArchivoGsm(route, nombreArchivo) == 1)
                {
                    string audio = "<audio id='audio1' src='/audioLectura/temp_" + ContactId + ".mp3' autoplay  controls></audio>";
                    string ruta = "/audioLectura/temp_" + ContactId + ".mp3";
                    return Json(new { status = 200, message = audio, ruta = ruta });
                }

                i++;
            }


            return Json(new { status = 404, message = "audio no encontrado" });

        }
        public decimal BuscarEnPresence(string Id = "", bool Comercializacion = true)
        {
            string Procedimiento = "SP_BuscarGrabacionesClasificacionIdPresence";
            SqlConnection connection = new SqlConnection(ConexionPresence);
            connection.Open();
            SqlCommand comando = new SqlCommand(Procedimiento, connection);
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@ID", Id);
            decimal contactId = (decimal)comando.ExecuteScalar();
            connection.Close();
            return contactId;
        }

        //COPIA EL ARCHIVO GSM A LA CARPETA INDICADA.
        public int copiarArchivoGsm(string ruta, string NombreArchivo)
        {

            if (System.IO.File.Exists(ruta))
            {
                //System.IO.File.Copy(Evidencia_Evidente, path, true);
                if (_Tipo == 2)
                {
                    var path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(NombreArchivo));
                    System.IO.File.Copy(ruta, path, true);
                    return 1;


                }
                else
                {

                    NombreArchivo = Path.GetFileNameWithoutExtension(NombreArchivo);
                    //convertir en Gsm
                    path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(nombreArchivo));
                    nombreGrabacion = "temp_" + NombreArchivo;
                    CarpetaTemporal = "GrabacionesApp";
                    rutaDestino = @"\\172.16.5.132\Claro_Legalizaciones\tempConversion\" + nombreGrabacion + ".gsm";
                    System.IO.File.Copy(ruta, rutaDestino, true);

                    using (var client = new HttpClient())
                    {
                        var uri = new Uri("http://172.16.5.35/recording/convertirArchivo.php");

                        var parametros = new
                        Dictionary<string, string>
                        {
                            { "idGestion" , nombreGrabacion },
                            { "carpeta" , CarpetaTemporal },
                            { "nomgrabacion" , nombreGrabacion }
                        };
                        var encodedContent = new FormUrlEncodedContent(parametros);
                        var response = client.PostAsync(uri.ToString(), encodedContent).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            // codigo exitoso.
                            var customerJsonString = response.Content.ReadAsStringAsync().Result;
                            rutaDestino = @"\\172.16.5.132\Claro_Legalizaciones\tempConversion\" + nombreGrabacion + ".mp3";
                            path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(nombreGrabacion + ".mp3"));
                            System.IO.File.Copy(rutaDestino, path, true);
                            System.IO.File.Delete(rutaDestino);
                            return 1;

                        }

                    }



                }
            }
            // \.fin the file exit
            else
            {
                Retorno = false;

                if (_Tipo == 2)
                {

                    // definir id de presence a buscar
                    if (_idPresence != "" && _idPresence != null)
                    {
                        ContactId = BuscarEnPresence(_idPresence, false);
                        nombreArchivo = ContactId + ".gsm";

                        if (ContactId != null && ContactId != 0)
                        {
                            CarpetaTemporal = "Fija";
                            nombreGrabacion = _IdGestion.ToString();


                            rutaDestino = @"\\172.16.5.132\Claro_Legalizaciones\tempConversion\" + CarpetaTemporal + "\\" + nombreGrabacion + ".gsm";
                            RutaRecording = @"\\172.16.5.27\Recording\rec\" + nombreArchivo;

                            if (System.IO.File.Exists(RutaRecording))
                            {

                                var path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(nombreArchivo));
                                System.IO.File.Copy(RutaRecording, rutaDestino, true);

                                // CODIGO PARA GUARDAR EL ID 
                                using (var client = new HttpClient())
                                {
                                    var uri = new Uri("http://172.16.5.35/recording/convertirArchivo.php");

                                    var parametros = new
                                    {
                                        idGestion = nombreGrabacion,
                                        carpeta = CarpetaTemporal,
                                        nomgrabacion = nombreGrabacion
                                    };
                                    var response = client.PostAsJsonAsync(uri.ToString(), parametros).Result;


                                    if (response.IsSuccessStatusCode)
                                    {

                                        // codigo exitoso.
                                        path = Path.Combine(Server.MapPath("~/audioLectura/"), Path.GetFileName(nombreGrabacion + ".mp3"));
                                        System.IO.File.Copy(nombreArchivo, path, true);
                                        System.IO.File.Delete(rutaDestino);
                                        return 1;

                                    }

                                }

                            }


                        }


                    }
                }
            }

            return 2;
        }

        // GET: Calificaciones
        public ActionResult Index(Guid Id, string TipoFormulario = "", string TipoMonitoreo = "")
        {
            ViewBag.TipoRol = db.Roles.Where(r => r.Id == Roles.Calidad || r.Id == Roles.TeamLeader).ToList();
            TempData["MensajeEliminar"] = TempData["MensajeEliminar"];

            if (SessionPersister.HasRol("Calidad"))
            {
                Guid calidadId = SessionPersister.Id.Value;
                User Asesor = db.Usuario.Where(u => u.Id == Id).FirstOrDefault();
                User UsuarioCalidad = db.Usuario.Where(u => u.Id == calidadId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == Id && u.User.Status == true).ToList();
                List<string> Campanas = new List<string>();
                List<Calificacion> CalificacionesUsuario = new List<Calificacion>();

                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var calificacions = db.Calificacions.Where(c => c.CampañaId == UC.CampanaId && c.AsesorId == Id).Include(c => c.Calidad).Include(c => c.Usuario).ToList();
                    CalificacionesUsuario.AddRange(calificacions);

                }

                ViewBag.CampañaId = Campanas.ToList();

                try
                {
                    int primerCampanaId = int.Parse(Campanas[0]);
                    var Formulario = db.Formularios.Where(f => f.CampanaId == primerCampanaId).FirstOrDefault();
                    if (Formulario == null)
                    {
                        TempData["MensajeError"] = $"No exite un formulario para la campaña { campanas.RetornarCampanaPorId(primerCampanaId).Nombre }, consulte con el administrador del sistema";
                        return RedirectToAction("HomeAdmin", "Home");
                    }
                    var Estandares = db.Estandar.Where(e => e.FormularioId == Formulario.Id).Include(e => e.Atributos).ToList();

                    ViewBag.primerCampanaId = primerCampanaId;
                    ViewBag.FormularioId = db.Formularios.Where(f => f.CampanaId == primerCampanaId).Select(x => x.Id).FirstOrDefault();
                    ViewBag.UsuarioId = Id;
                }
                catch (Exception ex)
                {
                    return HttpNotFound(ex.Message);
                }

                return View(CalificacionesUsuario.OrderByDescending(c => c.CreatedAt).ToList());
            }
            else
            {
                return HttpNotFound();
            }
        }
        public ActionResult IndexAsesor(Guid Id)
        {
            List<Calificacion> CalificacionesUsuario = new List<Calificacion>().ToList();

            if (SessionPersister.HasRol("Asesor"))
            {

                if (Id == null) return View(CalificacionesUsuario);

                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == Id).ToList();
                if (UsuariosCampanas.Count() <= 0)
                {
                    TempData["MensajeError"] = "El asesor no posee una campaña asignada, consulte con el administrador";
                    return RedirectToAction("HomeAdmin", "Home");
                }

                List<string> Campanas = new List<string>();
                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                }

                var calificacions = db.Calificacions.Where(c => c.AsesorId == Id && c.CompromisoAsesor == "").Include(c => c.Calidad).Include(c => c.Usuario).ToList();
                CalificacionesUsuario.AddRange(calificacions);
                ViewBag.CampañaId = Campanas.ToList();

                try
                {
                    int primerCampanaId = int.Parse(Campanas[0]);
                    ViewBag.primerCampanaId = primerCampanaId;
                    ViewBag.UsuarioId = Id;
                    Guid FormularioId = formulario.RetornarFormularioPorCampana(primerCampanaId).Id;

                    if (FormularioId == null)
                    {
                        TempData["MensajeError"] = "No existe un formulario para la campaña a la que se encuentra asociada el asesor. Consulte con el administrador";
                        return RedirectToAction("HomeAdmin", "Home");
                    }

                    var Estandares = db.Estandar.Where(e => e.FormularioId == FormularioId).Include(e => e.Atributos).ToList();

                    var modelo = CalificacionesUsuario.OrderByDescending(c => c.CreatedAt).ToList();

                    return View(modelo);

                }
                catch (Exception)
                {
                    return HttpNotFound();
                }

            }
            else
            {
                return HttpNotFound();
            }
        }

        // GET: Calificaciones/Details/5
        public ActionResult Details(Guid? id, Guid FormularioId, int? feedback)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Calificacion calificacion = db.Calificacions.Find(id);

            if (calificacion == null) return HttpNotFound();


            var Estandares                  = db.Estandar.Where(e => e.FormularioId == FormularioId && e.Estado == true).Include(e => e.Atributos).OrderBy(e => e.Posicion).ToList();
            var asesor                      = db.Usuario.Find(calificacion.AsesorId);
            var atributosCalificacion       = db.CalificacionEstandarAtributoes.Include(x => x.ItemCalificado).Where(cea => cea.CalificacionId == calificacion.Id).ToList();
            ViewBag.AtributosCalificacion   = atributosCalificacion;
            ViewBag.CalificacionId          = id;
            ViewBag.AsesorId                = calificacion.AsesorId;
            ViewBag.ComentariosAsesor       = calificacion.ComentariosAsesor;
            ViewBag.DescripcionCalificacion = calificacion.Descripcion;
            ViewBag.CompromisoAsesor        = calificacion.CompromisoAsesor;
            ViewBag.IdLlamada               = calificacion.IdGrabacion;
            ViewBag.TelefonoLlamada         = calificacion.TelefonoGrabacion;
            ViewBag.FechaCalificacion       = calificacion.CreatedAt;
            ViewBag.CampanaId               = calificacion.CampañaId;
            ViewBag.DuracionTMO             = calificacion.DuracionTMO;
            ViewBag.Segmento                = calificacion.Segmento;
            ViewBag.puntaje                 = calificacion.Puntaje;
            ViewBag.TipoMonitoreo           = calificacion.TipoMonitoreo;
            ViewBag.Procede                 = calificacion.procede;
            ViewBag.EsVenta                 = calificacion.EsVenta;
            ViewBag.EsVentaContado          = calificacion.EsVentaContado;
            ViewBag.EsVentaFinaciada        = calificacion.EsVentaFinaciada;
            ViewBag.EsVenta                 = calificacion.EsVenta;
            ViewBag.EsTodoClaro             = calificacion.EsTodoClaro;
            ViewBag.UtilizaGuionBienvenida  = calificacion.UtilizaGuionBienvenida;
            ViewBag.UtilizaGuionDespedida   = calificacion.UtilizaGuionDespedida;
            ViewBag.EsVentaClaroUp          = calificacion.EsVentaClaroUp;
            ViewBag.EsRetenido              = calificacion.EsRetenido;
            ViewBag.Ciudad                  = calificacion.Ciudad;
            ViewBag.Marcacion               = calificacion.Marcacion;
            ViewBag.SubCampana              = calificacion.SubCampana;
            ViewBag.MotivoConsulta          = calificacion.MotivoConsulta;
            ViewBag.DisableProcede          = calificacion.TipoMonitoreo == "PQR" ? "block" : "none";
            ViewBag.DisableVenta            = calificacion.EsVenta == "1" ? "block" : "none";
            ViewBag.DisableVentaContado     = calificacion.EsVentaContado == "SI" ? "block" : "none";
            ViewBag.Feedback                = feedback;
            ViewBag.NombreAsesor            = asesor.Names;

            var incumplimiento = "No";
            foreach (var atributoCalificacion in atributosCalificacion)
            {
                Atributo atributo = db.Atributo.Find(atributoCalificacion.AtributoId);
                if (atributo.TipoAtributoId == new Guid(TipoAtributo.CRITICO))
                {
                    if (atributoCalificacion.ItemCalificadoId == new Guid(tmkhogares.Constantes.Calificacion.NoCumple)) incumplimiento = "Si";

                }
            }

            ViewBag.Incumplimiento = incumplimiento;
            return View(Estandares);   

        }

        public ActionResult DetailsAuditados(Guid? id, Guid FormularioId, Guid? auditorId)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Calificacion calificacion = db.Calificacions.Find(id);

            if (calificacion == null) return HttpNotFound();


            var Estandares = db.Estandar.Where(e => e.FormularioId == FormularioId && e.Estado == true).Include(e => e.Atributos).OrderBy(e => e.Posicion).ToList();

            var atributosCalificacion = db.CalificacionEstandarAtributoes.Include(x => x.ItemCalificado).Where(cea => cea.CalificacionId == calificacion.Id).ToList();
            ViewBag.AtributosCalificacion = atributosCalificacion;
            ViewBag.CalificacionId = id;
            ViewBag.AsesorId = calificacion.AsesorId;
            ViewBag.ComentariosAsesor = calificacion.ComentariosAsesor;
            ViewBag.DescripcionCalificacion = calificacion.Descripcion;
            ViewBag.CompromisoAsesor = calificacion.CompromisoAsesor;
            ViewBag.IdLlamada = calificacion.IdGrabacion;
            ViewBag.TelefonoLlamada = calificacion.TelefonoGrabacion;
            ViewBag.FechaCalificacion = calificacion.CreatedAt;
            ViewBag.CampanaId = calificacion.CampañaId;
            ViewBag.puntaje = calificacion.Puntaje;
            ViewBag.AuditorId = auditorId;

            var incumplimiento = "No";
            foreach (var atributoCalificacion in atributosCalificacion)
            {
                Atributo atributo = db.Atributo.Find(atributoCalificacion.AtributoId);
                if (atributo.TipoAtributoId == new Guid(TipoAtributo.CRITICO))
                {
                    if (atributoCalificacion.ItemCalificadoId == new Guid(tmkhogares.Constantes.Calificacion.NoCumple)) incumplimiento = "Si";

                }
            }

            ViewBag.Incumplimiento = incumplimiento;
            return View(Estandares);
        }

        // GET: Calificaciones/Create
        public ActionResult Create(Guid Id, int primerCampanaId, Guid? FormularioId)
        {
            List<Estandar> Estandares = new List<Estandar>();


            if (SessionPersister.HasRol("Calidad"))
            {
                Formulario formulario = new Formulario();
                User UsuarioCalidad = db.Usuario.Find(SessionPersister.Id);
                var asesor = db.Usuario.Find(Id);
                var campañaAsesor = db.UsuariosCampanas.Where(uc => uc.UserId == asesor.Id).FirstOrDefault();

                ViewBag.UsuarioCalidadId = new SelectList(db.Usuario, "Id", "Document");
                ViewBag.AsesorId = new SelectList(db.Usuario, "Id", "Document");
                ViewBag.Campañas = new SelectList(db.Campana, "Id", "Nombre");
                ViewBag.NombreAsesor = asesor.Names;
                ViewBag.LoginAsesor = asesor.login;

                if (FormularioId != null)
                {
                    formulario = db.Formularios.Where(f => f.Id == FormularioId).FirstOrDefault();
                    if (formulario == null) return HttpNotFound();

                }
                else
                {
                    formulario = db.Formularios.Where(f => f.CampanaId == campañaAsesor.CampanaId && f.TipoRol == Roles.Calidad).FirstOrDefault();
                    if (formulario == null) return HttpNotFound();

                }

                Estandares = db.Estandar.Where(e => e.FormularioId == formulario.Id && e.Estado && e.Atributos.Any(a => a.Estado == true)).Include(e => e.Atributos).OrderBy(e => e.Posicion).ToList();
                ViewBag.IdCalidad = SessionPersister.Id;
                ViewBag.IdAsesor = Id;
                ViewBag.FormularioId = formulario.Id;
                ViewBag.CampanaId = primerCampanaId;
                return View(Estandares);
            }
            else
            {
                return HttpNotFound();
            }

        }


        [HttpPost]
        public JsonResult ObtenerLlamadaAzar(string Login, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.Login = Login;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            DateTime FechaInicio = DateTime.Now;
            string FechaTest = FechaInicio.AddDays(-1).ToString("ddd", CultureInfo.CreateSpecificCulture("es-CO"));
            if (FechaInicio.AddDays(-1).ToString("ddd", CultureInfo.CreateSpecificCulture("es-CO")) == "dom.")
            {
                FechaInicio = DateTime.Now.AddDays(-2);
            }
            else
            {
                FechaInicio = DateTime.Now.AddDays(-1);
            }
            DateTime FechaFin = DateTime.Now.AddDays(1);

            string consulta = " ";
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["pagination"]);
            int pageNumber = (page ?? 1);
            consulta += " and RDATE Between '" + FechaInicio.ToString("yyyyMMdd") + "' AND '" + FechaFin.ToString("yyyyMMdd") + "' and TOTALDURATION > 60 ";

            if (Login != "")
            {
                consulta += " AND LOGIN IN(" + Login + ")";
            }

            //consulta = "and RDATE Between '20210327' AND '20210330' and TOTALDURATION > 60  AND LOGIN IN(12087)"; // Esto es una prueba
            db.Database.CommandTimeout = 60;
            try
            {
                var grab = db.Database.SqlQuery<Grabacion>("SP_TraerGrabaciones @QUERY = {0}", consulta).ToList();
                var totalGrabaciones = grab.Count;
                if (totalGrabaciones == 0)
                {
                    return Json(new { status = 404, message = "No hemos encontrado grabaciones de ayer ni hoy del asesor" });
                }
                Random r = new Random();
                int numeroAleatorio = r.Next(0, (totalGrabaciones - 1)); //for ints
                Grabacion grabacionSeleccionada = grab[numeroAleatorio];
                var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string grabacion = jsonSerializer.Serialize(grabacionSeleccionada);
                return Json(new { status = 200, message = "grabacion encontrada", grabacion = grabacion });

                //return View(grab.OrderByDescending(g => g.RDATE).ToPagedList(pageNumber, pageSize));

            }
            catch (Exception e)
            {
                ModelState.AddModelError("Model", "Error cargando las grabaciones intentelo nuevamente");
                //return View(db.Grabaciones.ToPagedList(pageNumber, pageSize));
                return Json(new { status = 404, message = "grabacion no encontrada" });
            }
        }
        [HttpPost]
        public JsonResult EditarCompromiso(Guid? id, string CompromisoAsesor = "")
        {
            if (id == null)
            {
                return Json(new { status = false });
            }
            Calificacion calificacion = db.Calificacions.Find(id);
            if (calificacion == null)
            {
                return Json(new { status = false });
            }

            try
            {
                calificacion.CompromisoAsesor = CompromisoAsesor;
                db.Entry(calificacion).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { status = true, asesorId = calificacion.AsesorId });

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        // GET: Calificaciones/Edit/5
        public ActionResult Edit(Guid? id, Guid FormularioId, int? feedback)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Calificacion calificacion = db.Calificacions.Find(id);

            if (calificacion == null) return HttpNotFound();


            var Estandares = db.Estandar.Where(e => e.FormularioId == FormularioId && e.Estado == true).Include(e => e.Atributos).OrderBy(e => e.Posicion).ToList();
            var asesor = db.Usuario.Find(calificacion.AsesorId);
            var atributosCalificacion = db.CalificacionEstandarAtributoes.Include(x => x.ItemCalificado).Where(cea => cea.CalificacionId == calificacion.Id).ToList();
            ViewBag.AtributosCalificacion = atributosCalificacion;
            ViewBag.CalificacionId = id;
            ViewBag.IdAsesor = calificacion.AsesorId;
            ViewBag.IdCalidad = SessionPersister.Id;
            ViewBag.FormularioId = calificacion.FormularioId;           
            ViewBag.LoginAsesor = asesor.login;           
            ViewBag.ComentariosAsesor = calificacion.ComentariosAsesor;
            ViewBag.DescripcionCalificacion = calificacion.Descripcion;            
            ViewBag.IdLlamada = calificacion.IdGrabacion;
            ViewBag.TelefonoLlamada = calificacion.TelefonoGrabacion;
            ViewBag.FechaCalificacion = calificacion.CreatedAt;
            ViewBag.CampanaId = calificacion.CampañaId;
            ViewBag.DuracionTMO = calificacion.DuracionTMO;
            ViewBag.Segmento = string.IsNullOrEmpty(calificacion.Segmento) ? "" : calificacion.Segmento;
            ViewBag.puntaje = calificacion.Puntaje;
            ViewBag.TipoMonitoreo = string.IsNullOrEmpty(calificacion.TipoMonitoreo) ? "" : calificacion.TipoMonitoreo;
            ViewBag.Procede = string.IsNullOrEmpty(calificacion.procede) ? "" : calificacion.procede;           
            ViewBag.EsVenta = string.IsNullOrEmpty(calificacion.EsVenta) ? "0" : calificacion.EsVenta;
            ViewBag.EsVentaContado = string.IsNullOrEmpty(calificacion.EsVentaContado) ? "NO" : calificacion.EsVentaContado;
            ViewBag.EsVentaFinaciada = string.IsNullOrEmpty(calificacion.EsVentaFinaciada) ? "NO" : calificacion.EsVentaFinaciada; 
            ViewBag.EsTodoClaro = string.IsNullOrEmpty(calificacion.EsTodoClaro) ? "NO" : calificacion.EsTodoClaro;
            ViewBag.UtilizaGuionBienvenida = string.IsNullOrEmpty(calificacion.UtilizaGuionBienvenida) ? "NO" : calificacion.UtilizaGuionBienvenida;
            ViewBag.UtilizaGuionDespedida = string.IsNullOrEmpty(calificacion.UtilizaGuionDespedida) ? "NO" : calificacion.UtilizaGuionDespedida;
            ViewBag.EsVentaClaroUp = string.IsNullOrEmpty(calificacion.EsVentaClaroUp) ? "NO" : calificacion.EsVentaClaroUp;
            ViewBag.EsRetenido = string.IsNullOrEmpty(calificacion.EsRetenido) ? "NO" : calificacion.EsRetenido;
            ViewBag.Ciudad = string.IsNullOrEmpty(calificacion.Ciudad) ? "" : calificacion.Ciudad;
            ViewBag.Marcacion = string.IsNullOrEmpty(calificacion.Marcacion) ? "" : calificacion.Marcacion;
            ViewBag.SubCampana = string.IsNullOrEmpty(calificacion.SubCampana) ? "" : calificacion.SubCampana;
            ViewBag.MotivoConsulta = string.IsNullOrEmpty(calificacion.MotivoConsulta) ? "" : calificacion.MotivoConsulta;
            ViewBag.DisplayProcede = calificacion.TipoMonitoreo == "PQR" ? "block" : "none";
            ViewBag.DisplayVenta = calificacion.EsVenta == "1" ? "block" : "none";
            ViewBag.DisplayVentaContado = calificacion.EsVentaContado == "SI" ? "block" : "none";
            ViewBag.Feedback = feedback;
            ViewBag.NombreAsesor = asesor.Names;
        
            return View(Estandares);
        }


        public ActionResult Delete(Guid? id, int feedback)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            using (var context = new Contexto())
            {
                // CREATING A BEGIN TRANSACTION AND COMMIT.
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        //BEGIN OF THE QUERY
                        // Consultamos la lista de atributos por calificación.
                        List<CalificacionEstandarAtributo> calificacionEstandarAtributos = db.CalificacionEstandarAtributoes.Where(x => x.CalificacionId == id).ToList();

                        //Ahora eliminamos toda la lista.
                        db.CalificacionEstandarAtributoes.RemoveRange(calificacionEstandarAtributos);
                        db.SaveChanges();

                        Calificacion calificacion = db.Calificacions.Find(id);
                        db.Calificacions.Remove(calificacion);
                        db.SaveChanges();

                        TempData["MensajeEliminar"] = "Calificación eliminada con éxito.";
                        //COMMIT TRANSACTION
                        dbContextTransaction.Commit();

                        return RedirectToAction("MonitoreosFeedback", "Calificaciones", new { feedback });

                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        throw new Exception(ex.Message);

                    }
                }
            }
        }
        public ActionResult CalificarUsuario(string Documento = "", string Campana = "")
        {

            if (SessionPersister.HasRol("Calidad"))
            {
                List<User> usuarios = new List<User>();
                Guid usuarioSession = (Guid)SessionPersister.Id;
                var campanasUsuario = db.UsuariosCampanas.Where(x => x.UserId.Equals(usuarioSession)).Select(x => x.CampanaId).ToList();

                if (campanasUsuario.Count <= 0)
                {
                    TempData["Mensaje"] = "Se le debe asignar una Campaña al usuario para continuar con el proceso";
                    return RedirectToAction("Index", "UsuarioCampanas", new { id = usuarioSession });
                }

                ViewBag.Campana = new SelectList(campanas.RetornarListaCampanasPorUsuarioSession(usuarioSession).OrderBy(x => x.Nombre), "Id", "Nombre");


                if (!string.IsNullOrEmpty(Campana))
                {
                    List<int> campanas = new List<int>();
                    campanas.Add(int.Parse(Campana));
                    var CampañaId = campanas[0];

                    if (!campanasUsuario.Contains(CampañaId))
                    {
                        TempData["Mensaje"] = "La campaña seleccionada no ha sido asignada al usuario de calidad.";
                        return RedirectToAction("Index", "UsuarioCampanas", new { id = usuarioSession });
                    }

                    usuarios = _usuarios.RetornarUsuariosPorCampana(campanas);

                    ViewBag.CampañaId = CampañaId;
                    ViewBag.Documento = Documento;
                    ViewBag.FormularioId = formulario.RetornarFormularioPorCampana(CampañaId).Id;
                    ViewBag.NombreCampana = db.Campana.Where(x => x.Id == CampañaId).Select(x => x.Nombre.ToUpper()).FirstOrDefault();


                    if (!string.IsNullOrEmpty(Documento))
                    {
                        return View(usuarios.Where(u => u.Document.Contains(Documento)).ToList());
                    }
                    else
                    {
                        return View(usuarios.ToList());
                    }
                }
                else
                {
                    ViewBag.CampanaId = null;
                    return View(usuarios.ToList());
                }

            }
            else
            {
                return HttpNotFound();
            }
        }
        public ActionResult CalificarUsuarioGerente(string Documento = "")
        {
            if (SessionPersister.HasRol("Gerente"))
            {
                Guid calidadId = SessionPersister.Id.Value;
                User UsuarioCalidad = db.Usuario.Where(u => u.Id == calidadId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == calidadId).ToList();
                List<string> Campanas = new List<string>();
                List<User> usuarios = new List<User>().ToList();

                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var UsuariosDeCampana = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == new Guid("4450491C-32E6-41AE-B301-CB22DFC8BBD9")) && u.UsuarioCampanas.Any(c => c.CampanaId == UC.CampanaId)).Include(u => u.Nivel).ToList(); ;
                    usuarios.AddRange(UsuariosDeCampana);
                }
                ViewBag.CampañaId = db.UsuariosCampanas.Where(u => u.UserId == calidadId).FirstOrDefault().CampanaId;
                ViewBag.Documento = Documento;
                if (Documento != "")
                {
                    return View(usuarios.Where(u => u.Document.Contains(Documento)).ToList());
                }
                else
                {
                    return View(usuarios.ToList());
                }
            }
            else
            {
                return HttpNotFound();
            }
        }
        public ActionResult MonitoreosFeedback(int feedback, string Documento = "")
        {
            if (SessionPersister.HasRol("Calidad"))
            {
                Guid calidadId = SessionPersister.Id.Value;
                User UsuarioCalidad = db.Usuario.Where(u => u.Id == calidadId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == calidadId).ToList();
                List<string> Campanas = new List<string>();
                List<Calificacion> monitoreos = new List<Calificacion>().ToList();
                IQueryable<Calificacion> monitoreosDeCampana;

                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                }

                if (feedback > 0)
                {
                    monitoreosDeCampana = db.Calificacions.Where(c => c.UsuarioCalidadId == calidadId && !string.IsNullOrEmpty(c.CompromisoAsesor));

                }
                else
                {

                    monitoreosDeCampana = db.Calificacions.Where(c => (c.UsuarioCalidadId == calidadId) && (c.CompromisoAsesor == "" || c.CompromisoAsesor == null));
                }
                monitoreos.AddRange(monitoreosDeCampana);
                ViewBag.CampañaId = Campanas.ToList();
                ViewBag.Documento = Documento;
                ViewBag.Feedback = feedback;

                return View(monitoreos.OrderByDescending(mon => mon.CreatedAt).ToList());
            }
            else
            {
                return HttpNotFound();
            }
        }    
        public ActionResult PlanTrabajoFeedback(string Documento = "")
        {
            if (SessionPersister.HasRol("TeamLeader"))
            {
                Guid calidadId = SessionPersister.Id.Value;
                User UsuarioCalidad = db.Usuario.Where(u => u.Id == calidadId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == calidadId).ToList();
                List<string> Campanas = new List<string>();
                List<PlanTrabajoAsesor> planestrabajo = new List<PlanTrabajoAsesor>().ToList();

                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var plantrabajoDeCampana = db.PlanTrabajoAsesor.Where(pta => pta.CampanaId == UC.CampanaId && (pta.CompromisoAsesor == null || pta.CompromisoAsesor == ""));
                    planestrabajo.AddRange(plantrabajoDeCampana);
                }
                ViewBag.CampañaId = Campanas.ToList();
                ViewBag.Documento = Documento;
                //if (Documento != "")
                //{
                //    return View(monitoreos.Where(u => u.Document.Contains(Documento)).ToList());
                //}
                //else
                //{
                //}
                return View(planestrabajo.ToList());
            }
            else
            {
                return HttpNotFound();
            }
        }
        public ActionResult PlanTrabajoTeamFeedback(string Documento = "")
        {
            if (SessionPersister.HasRol("Gerente"))
            {
                Guid calidadId = SessionPersister.Id.Value;
                User UsuarioCalidad = db.Usuario.Where(u => u.Id == calidadId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == calidadId).ToList();
                List<string> Campanas = new List<string>();
                List<PlanTrabajoTeam> planestrabajo = new List<PlanTrabajoTeam>().ToList();

                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var plantrabajoDeCampana = db.PlanTrabajoTeam.Where(pta => pta.CampanaId == UC.CampanaId && (pta.CompromisoTeam == null || pta.CompromisoTeam == ""));
                    planestrabajo.AddRange(plantrabajoDeCampana);
                }
                ViewBag.CampañaId = Campanas.ToList();
                ViewBag.Documento = Documento;
                //if (Documento != "")
                //{
                //    return View(monitoreos.Where(u => u.Document.Contains(Documento)).ToList());
                //}
                //else
                //{
                //}
                return View(planestrabajo.ToList());
            }
            else
            {
                return HttpNotFound();
            }
        }
        public ActionResult MonitoreosFeedbackAsesor(Guid Id)
        {
            if (SessionPersister.HasRol("Calidad"))
            {
                Guid calidadId = SessionPersister.Id.Value;
                User Asesor = db.Usuario.Where(u => u.Id == Id).FirstOrDefault();
                User UsuarioCalidad = db.Usuario.Where(u => u.Id == calidadId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == Id).ToList();
                List<string> Campanas = new List<string>();
                List<Calificacion> CalificacionesUsuario = new List<Calificacion>().ToList();
                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var calificacions = db.Calificacions.Where(c => c.CampañaId == UC.CampanaId && c.AsesorId == Id && (c.CompromisoAsesor == null || c.CompromisoAsesor == "")).Include(c => c.Calidad).Include(c => c.Usuario).ToList();
                    CalificacionesUsuario.AddRange(calificacions);

                }
                ViewBag.CampañaId = Campanas.ToList();
                try
                {
                    int primerCampanaId = int.Parse(Campanas[0]);
                    ViewBag.primerCampanaId = primerCampanaId;
                    ViewBag.FormularioId = db.Formularios.Where(f => f.CampanaId == primerCampanaId).FirstOrDefault().Id;
                    ViewBag.UsuarioId = Id;
                    Guid FormularioId = Guid.Parse(db.Formularios.Where(f => f.CampanaId == primerCampanaId).FirstOrDefault().Id.ToString());
                    var Estandares = db.Estandar.Where(e => e.FormularioId == FormularioId).Include(e => e.Atributos).ToList();

                }
                catch (Exception e)
                {
                    return HttpNotFound();
                }

                return View(CalificacionesUsuario.OrderByDescending(c => c.CreatedAt).ToList());
            }
            else if (SessionPersister.HasRol("TeamLeader"))
            {
                Guid teamId = SessionPersister.Id.Value;
                User Asesor = db.Usuario.Where(u => u.Id == Id).FirstOrDefault();
                User UsuarioCalidad = db.Usuario.Where(u => u.Id == teamId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == Id).ToList();
                List<string> Campanas = new List<string>();
                List<Calificacion> CalificacionesUsuario = new List<Calificacion>().ToList();
                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var calificacions = db.Calificacions.Where(c => c.CampañaId == UC.CampanaId && c.AsesorId == Id && c.FormularioId == new Guid("A2687862-0931-4996-8074-4EA122A1846B") && (c.CompromisoAsesor == null || c.CompromisoAsesor == "")).Include(c => c.Calidad).Include(c => c.Usuario).ToList();
                    CalificacionesUsuario.AddRange(calificacions);

                }

                ViewBag.CampañaId = Campanas.ToList();
                try
                {
                    int primerCampanaId = int.Parse(Campanas[0]);
                    ViewBag.primerCampanaId = primerCampanaId;
                    ViewBag.FormularioId = db.Formularios.Where(f => f.CampanaId == primerCampanaId).FirstOrDefault().Id;
                    ViewBag.UsuarioId = Id;
                    Guid FormularioId = Guid.Parse(db.Formularios.Where(f => f.CampanaId == primerCampanaId).FirstOrDefault().Id.ToString());
                    var Estandares = db.Estandar.Where(e => e.FormularioId == FormularioId).Include(e => e.Atributos).ToList();

                }
                catch (Exception e)
                {
                    return HttpNotFound();
                }

                return View(CalificacionesUsuario.OrderByDescending(c => c.CreatedAt).ToList());
            }
            else
            {
                return HttpNotFound();
            }
        }
        public string EstandarNotaCero(string CalificacionId, string EstandarId)
        {
            Guid calificacionId = Guid.Parse(CalificacionId);
            Guid estandarId = Guid.Parse(EstandarId);
            List<CalificacionEstandarAtributo> CEAS = db.CalificacionEstandarAtributoes.Where(cea => cea.EstandarId == estandarId && cea.CalificacionId == calificacionId).ToList();
            foreach (CalificacionEstandarAtributo CEA in CEAS)
            {

            }

            return "";

        }
        public ActionResult CalificarUsuarioTeam(string Documento = "")
        {
            if (SessionPersister.HasRol("TeamLeader"))
            {
                Guid teamId = SessionPersister.Id.Value;
                User UsuarioTeam = db.Usuario.Where(u => u.Id == teamId).FirstOrDefault();
                var usuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == teamId).ToList();

                if (usuariosCampanas.Count() <= 0)
                {
                    TempData["MensajeError"] = $"{ UsuarioTeam.Names} no se encuentra asociado(a) a una campaña, debe ser asignada por el administrador";
                    return RedirectToAction("HomeAdmin", "Home");
                }
                var CampanaId = usuariosCampanas[0].CampanaId;
                ViewBag.NombreCampana = db.Campana.Where(x => x.Id == CampanaId).Select(x => x.Nombre.ToUpper()).FirstOrDefault();
                List<string> Campanas = new List<string>();
                List<User> usuarios = new List<User>().ToList();

                foreach (UsuarioCampanas UC in usuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var usuariosDeCampana = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == new Guid("4450491C-32E6-41AE-B301-CB22DFC8BBD9"))
                                            && u.UsuarioCampanas.Any(c => c.CampanaId == UC.CampanaId))
                                            .Include(u => u.UsuarioCampanas)
                                            .Include(x => x.TeamLeader)
                                            .Include(u => u.Nivel).ToList();

                    usuarios.AddRange(usuariosDeCampana);
                }
                ViewBag.CampañaId = Campanas.ToList();
                ViewBag.Documento = Documento;
                if (Documento != "")
                {
                    return View(usuarios.Where(u => u.Document.Contains(Documento)).ToList());
                }
                else
                {
                    return View(usuarios.ToList());
                }
            }
            else
            {
                return HttpNotFound();
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        [CustomAuthorize(Roles = "Calidad")]
        public JsonResult Calificar(CalificacionVM model)
        {
            if (model.CalificacionId != null) _calificacion.Delete(model.CalificacionId);

            var nota = decimal.Round(_calificacion.Calificar(model), 2, MidpointRounding.AwayFromZero);

            return Json(new { status = "ok", notaFinal = nota, message = "Calificación exitosa" });
        }


    }
}
