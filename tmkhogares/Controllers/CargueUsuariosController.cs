﻿using Core.Models.User;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tmkhogares.BusinessLogic;
using tmkhogares.helper;
using tmkhogares.Models;
using tmkhogares.Models.Campañas;

namespace tmkhogares.Controllers
{
    public class CargueUsuariosController : Controller
    {
        private Contexto db = new Contexto();
        CargueUsuariosAplicativosBS cargueUsuariosAplicativosBS = new CargueUsuariosAplicativosBS();

        // GET: CargueUsuarios
        public ActionResult Index()
        {          
            List<Campana> campanas = db.Campana.OrderBy(x => x.Nombre).ToList();
            return View(campanas);
        }

        public ActionResult EditarTeamsMasivo()
        {
            List<Campana> campanas = db.Campana.OrderBy(x => x.Nombre).ToList();
            return View(campanas);
        }


        public JsonResult CargarArchivoUsuarios(int? idCampana)
        {
            if (Request.Files.Count > 0)
            {
                if (idCampana <= 0)
                {
                    return Json(new { status = 504, message = "Debe ingresar una Campaña" });
                }


                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    EliminarArchivosResources();

                    string path = Path.Combine(Server.MapPath("~/Resources/CargueAppUsuario"),
                                  Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    var dt = cargueUsuariosAplicativosBS.InsertarUsuariosAplicativos(path, idCampana);

                    return Json(new { status = 201, message = "El archivo se ha cargado correctamente!" });

                }

                catch (Exception ex)
                {
                    return Json(new { status = 504, message = ex.Message });
                    
                }
            }

            return Json("Por favor seleccione el archivo");
        }

        public JsonResult EditarTeamMasivo()
        {
            if (Request.Files.Count > 0)
            {          

                try
                {
                    HttpFileCollectionBase files = Request.Files;

                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;

                    EliminarArchivosResources();

                    string path = Path.Combine(Server.MapPath("~/Resources/CargueAppUsuario"),
                                  Path.GetFileName(file.FileName));
                    file.SaveAs(path);

                    var dt = cargueUsuariosAplicativosBS.EditarTeamLeader(path);

                    return Json(new { status = 201, message = "El archivo se ha cargado correctamente!" });

                }

                catch (Exception ex)
                {
                    return Json(new { status = 504, message = ex.Message });

                }
            }

            return Json("Por favor seleccione el archivo");
        }


        public void EliminarArchivosResources()
        {
            try
            {
                var pathResources = Path.Combine(Server.MapPath("~/Resources/CargueAppUsuario").ToString());

                DirectoryInfo di = new DirectoryInfo(pathResources);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();

                }
            }
            catch (Exception ex)
            {

                throw new Exception($"error al eliminar los archivos");
            }
        }
              
    }
}