﻿using Core.Models.configuration;
using Core.Models.Security;
using DocumentFormat.OpenXml.InkML;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using tmkhogares.Constantes;
using tmkhogares.Models;
using tmkhogares.Models.Campañas;
using tmkhogares.Models.Estandares;
using tmkhogares.Servicios;

namespace tmkhogares.Controllers
{


    public class AtributoesController : Controller
    {
        private Contexto db = new Contexto();
        private ServicioAtributos _atributos = new ServicioAtributos();


        [CustomAuthorize(Roles = "Administrador")]
        // GET: Atributoes
        public ActionResult Index(Guid Id, Guid FormularioId)
        {
            if (Id != null)
            {
                ViewBag.FormularioId = FormularioId;
                List<Atributo> Atributos = db.Atributo.Include(x => x.TipoAtributo).OrderBy(a => a.Posicion).Where(a => a.EstandarId == Id && a.Estado).ToList();
                Formulario formulario = db.Formularios.Find(FormularioId);
                Estandar estandar = db.Estandar.Find(Id);
                ViewBag.CampanaNombre = formulario.Nombre.ToUpper();
                ViewBag.EstandarNombre = estandar.Nombre.ToUpper();
                ViewBag.EstandarId = estandar.Id;

                return View(Atributos);
            }
            else
            {
                return HttpNotFound();
            }
        }

        [CustomAuthorize(Roles = "Administrador")]
        // GET: Atributoes/Create
        public ActionResult Create(Guid Id, Guid FormularioId)
        {
            ViewBag.TipoAtributoId = new SelectList(db.Configurations.OrderBy(x => x.OrderId).Where(x => x.CategoriesId == new Guid("05503CAD-3EE1-4F2C-9EB9-67A923DEE24C") && x.Status), "Id", "Name");
            ViewBag.ModeloGanaId = new SelectList(db.ModeloGana.OrderBy(x => x.Id).ToList(), "Id", "Nombre");
            ViewBag.AtributoModeloGanaId = new SelectList(db.AtributoModeloGana.Where(x => x.Estado).OrderBy(x => x.Id).ToList(), "Id", "Nombre");
            ViewBag.EstandarId = Id;
            ViewBag.FormularioId = FormularioId;
            TempData["Error"] = TempData["Error"];
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Administrador")]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,EstandarId,TipoAtributoId,AplicaModeloGana,AtributoModeloGanaId")] Atributo atributo)
        {

            Estandar estandar = db.Estandar.Where(x => x.Id == atributo.EstandarId).FirstOrDefault();

            if (ModelState.IsValid)
            {
                IQueryable<Configuration> porcentajeAtributo = db.Configurations;
                List<Atributo> atributosEditar = new List<Atributo>();
                var valorGana = decimal.Parse("0");

                if (string.IsNullOrEmpty(atributo.Nombre) || string.IsNullOrEmpty(atributo.Descripcion))
                {
                    TempData["Error"] = "Los campos son requeridos";
                    return RedirectToAction("Create", "Atributoes", new { Id = atributo.EstandarId, FormularioId = estandar.FormularioId });

                }

             
                var posicionNuevoAtributo = 0;
                var posicion = 0;
                var totalAtributosPorEstandar = db.Atributo.Where(x => x.EstandarId == estandar.Id).ToList();
                var totalAtributosPorFormulario = db.Atributo.Where(x => x.Estandares.FormularioId == estandar.FormularioId).ToList();

                if (totalAtributosPorEstandar.Count() > 0)
                {
                    posicionNuevoAtributo = db.Atributo.Where(x => x.EstandarId == estandar.Id).Select(x => x.Posicion).Max();
                }
                else
                {
                    if (totalAtributosPorFormulario.Count() > 0)
                    {
                        posicionNuevoAtributo = totalAtributosPorFormulario.OrderByDescending(x => x.Posicion).FirstOrDefault().Posicion;
                    }
                }

                if (totalAtributosPorFormulario.Count() > 0)
                {
                    atributosEditar = db.Atributo.OrderBy(x => x.Posicion).Where(x => x.Estandares.FormularioId == estandar.FormularioId && x.Posicion > posicionNuevoAtributo).ToList();
                }


                if (atributosEditar.Count() > 0) _atributos.EditarAtributos(atributosEditar);

                posicion = posicionNuevoAtributo + 1;
                string tipoAtributo = string.Empty;

                // Estableciendo los pesos según el tipo de atributo.
                if (atributo.TipoAtributoId == new Guid(TipoAtributo.CRITICO)) tipoAtributo = TipoAtributo.CRITICO;

                if (atributo.TipoAtributoId == new Guid(TipoAtributo.GENERICO)) tipoAtributo = TipoAtributo.GENERICO;

                if (atributo.TipoAtributoId == new Guid(TipoAtributo.PROCESO)) tipoAtributo = TipoAtributo.PROCESO;

                if (atributo.TipoAtributoId == new Guid(TipoAtributo.GENERAL)) tipoAtributo = TipoAtributo.GENERAL;


                ViewBag.EstandarId = estandar.Id;
                ViewBag.FormularioId = estandar.FormularioId;

                try
                {
                    atributo.Id = Guid.NewGuid();
                    atributo.AtributoModeloGanaId = atributo.AplicaModeloGana == false ? null : atributo.AtributoModeloGanaId;
                    atributo.Posicion = (int)posicion;
                    atributo.EstandarId = atributo.EstandarId;
                    db.Atributo.Add(atributo);
                    db.SaveChanges(); 
                    return RedirectToAction("Index", "Atributoes", new { Id = atributo.EstandarId, FormularioId = estandar.FormularioId });
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);

                }

            }

            TempData["Error"] = "El atributo no ha sido creado, revise los datos de entrada";

            return RedirectToAction("Create", new { Id = atributo.EstandarId, FormularioId = estandar.FormularioId });
        }



        // GET: Atributoes/Edit/5
        [CustomAuthorize(Roles = "Administrador")]
        public ActionResult Desactivar(Guid? id, Guid EstandarId, Guid FormularioId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Atributo atributo = db.Atributo.Find(id);

            if (atributo == null)
            {
                return HttpNotFound();
            }

            ViewBag.EstandarId = EstandarId;
            ViewBag.FormularioId = FormularioId;

            var existe = db.CalificacionEstandarAtributoes.Where(x => x.AtributoId == atributo.Id).Count();

            try
            {
                if (existe > 0)
                {
                    atributo.Posicion = 0;
                    atributo.Estado = false;
                    db.Entry(atributo).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    db.Atributo.Remove(atributo);
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }


            return RedirectToAction("Index", "Atributoes", new { Id = EstandarId, FormularioId = FormularioId });
        }



    }
}
