﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using Core.Models.configuration;
using Core.Models.Security;

namespace ClaroGrandesSuperficies.Controllers
{
	public class ConfigurationsController : Controller
	{

        private Contexto db = new Contexto();


        //[CustomAuthorize(Roles ="superadmin")]
        // GET: Configurations
        [CustomAuthorize]
        public ActionResult Index(Guid? Category , string Name)
		{
            Category ActualCategory =  db.Categories.Find(Category);

            if(ActualCategory == null || ActualCategory.CanChanged == false)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "la category no existe.");
            }

			ViewBag.Name = ActualCategory.Name;
			ViewBag.FName =  Name;
            ViewBag.ColumName = ActualCategory.Name;
            ViewBag.Category = Category;
			var configurations = db.Configurations.Include(c => c.Category).Where(C => C.CategoriesId == Category);
            if(Name != null && Name.Trim() != "")
            {
                configurations = configurations.Where(c => c.Name.Contains(Name));
            }

            return View(configurations.ToList());
		}

        // GET: Configurations/Details/5
        
        public ActionResult Details(Guid? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Configuration configurations = db.Configurations.Include(c => c.Category).Where(c => c.Id == id).FirstOrDefault();
			if (configurations == null)
			{
				return HttpNotFound();
			}

            Category ActualCategory = db.Categories.Find(configurations.CategoriesId);
            ViewBag.Name = ActualCategory.Name;
            ViewBag.Guid = ActualCategory.Name;

            return View(configurations);
		}

        // GET: Configurations/Create
        [CustomAuthorize(Roles = "Administrador")]
        public ActionResult Create(Guid? Category)
		{
            Category ActualCategory = db.Categories.Find(Category);
            if (ActualCategory == null || ActualCategory.CanChanged == false)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "La Categoria no existe.");
            }

            ViewBag.Category = Category;
            ViewBag.Name = ActualCategory.Name;
			ViewBag.Guid = ActualCategory.Name;
            ViewBag.Configtype = ActualCategory.Name;
            ViewBag.ColumName = ActualCategory.Name;
            return View();
		}

		// POST: Configurations/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Administrador")]
        public ActionResult Create([Bind(Include = "Id,Name,Status,CategoriesId,Description,GuidId")] Configuration configurations)
		{
			if (ModelState.IsValid)
			{
				
				db.Configurations.Add(configurations);
				db.SaveChanges();
				return RedirectToAction("Index" , new { Category = configurations.CategoriesId} );
			}
            ViewBag.GuidId = new SelectList(db.Configurations, "Id", "Name", configurations.GuidId);
            ViewBag.CategoriesId = new SelectList(db.Categories, "Id", "Name", configurations.CategoriesId);
			return View(configurations);
		}

        // GET: Configurations/Edit/5
        [CustomAuthorize(Roles = "Administrador")]
        public ActionResult Edit(Guid? id)
		{


			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Configuration configurations = db.Configurations.Find(id);


            if (configurations == null)
			{
				return HttpNotFound();
			}
            Category ActualCategory = db.Categories.Find(configurations.CategoriesId);
            ViewBag.Name = ActualCategory.Name;
            ViewBag.Guid = ActualCategory.Name;
            return View(configurations);
		}

		// POST: Configurations/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "Administrador")]
        public ActionResult Edit([Bind(Include = "Id,Name,Status,Description,GuidId")] Configuration configurations)
		{
			if (ModelState.IsValid)
			{

				db.Entry(configurations).State = EntityState.Modified;
				db.Entry(configurations).Property(c => c.CategoriesId).IsModified = false;
				db.SaveChanges();

                Contexto db2 = new Contexto();
                var CategoryId = db2.Configurations.Find(configurations.Id).CategoriesId;


                return RedirectToAction("Index", new { Category = CategoryId});
			}
            Category ActualCategory = db.Categories.Find(configurations.CategoriesId);
            ViewBag.Name = ActualCategory.Name;
            ViewBag.Guid = ActualCategory.Name;
            return View(configurations);
		}

		// GET: Configurations/Delete/5
		public ActionResult Delete(Guid? id, int Configtype = 0)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
            Configuration configurations = db.Configurations.Include(c => c.Category).Where(c => c.Id == id).FirstOrDefault();
            if (configurations == null)
			{
				return HttpNotFound();
			}

			return View(configurations);
		}

		// POST: Configurations/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(Guid id, int Configtype = 0)
		{
                Configuration configurations = db.Configurations.Find(id);
                db.Configurations.Remove(configurations);
                db.SaveChanges();
			return RedirectToAction("Index", new { Category = configurations.CategoriesId });
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
