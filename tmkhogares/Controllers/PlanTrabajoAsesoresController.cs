﻿using Core.Models.Security;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.PlanTrabajoAsesor;
using tmkhogares.Models.UsuariosCampanas;

namespace tmkhogares.Controllers
{
    public class PlanTrabajoAsesoresController : Controller
    {
        private Contexto db = new Contexto();


        public ActionResult ListaAsesores(string Documento = "")
        {
            if (SessionPersister.HasRol("TeamLeader"))
            {
                List<User> usuarios = new List<User>();
                List<string> Campanas = new List<string>();

                Guid calidadId = SessionPersister.Id.Value;               
                Guid? TeamId = db.Usuario.Where(x => x.Id == calidadId).Select(x => x.Id).FirstOrDefault();               
                var CampanaId = db.UsuariosCampanas.Where(u => u.UserId == calidadId && u.User.Status == true).Select(x => x.CampanaId).FirstOrDefault();
                ViewBag.NombreCampana = db.Campana.Where(x => x.Id == CampanaId).Select(x => x.Nombre.ToUpper()).FirstOrDefault();

                usuarios = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == new Guid("4450491C-32E6-41AE-B301-CB22DFC8BBD9"))
                                            && u.UserId == TeamId
                                            && u.UsuarioCampanas.Any(c => c.CampanaId == CampanaId))
                                            .Include(u => u.UsuarioCampanas)
                                            .Include(x => x.TeamLeader)
                                            .Include(u => u.Nivel).ToList();               

                ViewBag.CampañaId = Campanas.ToList();
                ViewBag.Documento = Documento;

                if (Documento != "")
                {
                    return View(usuarios.Where(u => u.Document.Contains(Documento)).ToList());
                }
                else
                {
                    return View(usuarios.ToList());
                }
            }
            else
            {
                return HttpNotFound();
            }

        }
        public ActionResult ListaAsesoresGerente(string Documento = "")
        {
            if (SessionPersister.HasRol("Gerente"))
            {
                Guid calidadId = SessionPersister.Id.Value;
                User UsuarioCalidad = db.Usuario.Where(u => u.Id == calidadId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == calidadId && u.User.Status == true).ToList();
                List<string> Campanas = new List<string>();
                List<User> usuarios = new List<User>().ToList();

                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var UsuariosDeCampana = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == new Guid("4450491C-32E6-41AE-B301-CB22DFC8BBD9")) && u.UsuarioCampanas.Any(c => c.CampanaId == UC.CampanaId)).Include(u => u.Nivel).ToList(); ;
                    usuarios.AddRange(UsuariosDeCampana);
                }
                ViewBag.CampañaId = Campanas.ToList();
                ViewBag.Documento = Documento;
                if (Documento != "")
                {
                    return View(usuarios.Where(u => u.Document.Contains(Documento)).ToList());
                }
                else
                {
                    return View(usuarios.ToList());
                }
            }
            else
            {
                return HttpNotFound();
            }

        }
        // GET: PlanTrabajoAsesores
        public ActionResult Index(Guid id)
        {
            ViewBag.AsesorId = id;
            var planTrabajoAsesor = db.PlanTrabajoAsesor.Include(p => p.Asesor).Include(p => p.TeamLeader).Where(p => p.AsesorId == id);
            return View(planTrabajoAsesor.ToList());
        }
        public ActionResult IndexAsesor(Guid id)
        {
            ViewBag.AsesorId = id;
            var planTrabajoAsesor = db.PlanTrabajoAsesor.Where(pta => pta.AsesorId == id && pta.CompromisoAsesor == null)
                                                        .Include(p => p.Asesor)
                                                        .Include(p => p.TeamLeader)
                                                        .Where(p => p.AsesorId == id).ToList();
            return View(planTrabajoAsesor);
        }

        // GET: PlanTrabajoAsesores/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlanTrabajoAsesor planTrabajoAsesor = db.PlanTrabajoAsesor.Find(id);
            if (planTrabajoAsesor == null)
            {
                return HttpNotFound();
            }
            return View(planTrabajoAsesor);
        }

        // GET: PlanTrabajoAsesores/Create
        public ActionResult Create(Guid AsesorId)
        {
            if(AsesorId == null)
            {
                return HttpNotFound();
            }
            ViewBag.AsesorId = AsesorId;
            ViewBag.TeamLeaderId = SessionPersister.Id;
            return View();
        }

        // POST: PlanTrabajoAsesores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MetaPromedioDia,DiasSemana,MetaSemanal,DiasMes,MetaMensual,OportunidadesMejora,CompromisoAsesor,DescripcionPlanAccion,NombreCampana,NombreTeamLeader,NombreAsesor,CedulaAsesor,LoginAsesor,AsesorId,TeamLeaderId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] PlanTrabajoAsesor planTrabajoAsesor)
        { 
            if(ModelState.IsValid)
            {
                var PlanesTrabajoAsesor = db.PlanTrabajoAsesor.Where(pta => pta.AsesorId == planTrabajoAsesor.AsesorId && pta.Estado == false).ToList();
                if(PlanesTrabajoAsesor.Count > 0)
                {
                    ModelState.AddModelError("OportunidadesMejora", "Error el asesor ya tiene un plan de trabajo activo");
                    //return View(db.Grabaciones.ToPagedList(pageNumber, pageSize));
                    return View();
                }
                else
                {
                    User Asesor = db.Usuario.Find(planTrabajoAsesor.AsesorId);
                    User TeamLeader = db.Usuario.Find(SessionPersister.Id);
                    var campanaId = db.UsuariosCampanas.Where(u => u.UserId == Asesor.Id).FirstOrDefault().CampanaId;
                    decimal metaSemanal = (decimal.Parse(planTrabajoAsesor.MetaMensual.ToString()) / (decimal.Parse(planTrabajoAsesor.DiasMes.ToString()) / decimal.Parse(planTrabajoAsesor.DiasSemana.ToString())));
                    decimal metaDiaria = (decimal.Parse(planTrabajoAsesor.MetaMensual.ToString()) / decimal.Parse(planTrabajoAsesor.DiasSemana.ToString()));
                    if (metaSemanal == null || metaDiaria == null)
                    {
                        return HttpNotFound();
                    }
                    if (Asesor == null)
                    {
                        return HttpNotFound();
                    }
                    if (TeamLeader == null)
                    {
                        return HttpNotFound();
                    }
                    planTrabajoAsesor.Id = Guid.NewGuid();
                    planTrabajoAsesor.AsesorId = Asesor.Id;
                    planTrabajoAsesor.NombreAsesor      = Asesor.Names;
                    planTrabajoAsesor.CedulaAsesor      = Asesor.Document;
                    planTrabajoAsesor.LoginAsesor       = Asesor.login;
                    planTrabajoAsesor.TeamLeaderId      = TeamLeader.Id;
                    planTrabajoAsesor.NombreTeamLeader  = TeamLeader.Names;
                    planTrabajoAsesor.MetaSemanal       = metaSemanal;
                    planTrabajoAsesor.MetaPromedioDia   = metaDiaria;
                    planTrabajoAsesor.Estado            = false;
                    planTrabajoAsesor.CampanaId = campanaId;
                    db.PlanTrabajoAsesor.Add(planTrabajoAsesor);
                    db.SaveChanges();
                    return RedirectToAction("Index",new { id = Asesor.Id});
                }
            }

            ViewBag.AsesorId = new SelectList(db.Usuario, "Id", "Document", planTrabajoAsesor.AsesorId);
            ViewBag.TeamLeaderId = new SelectList(db.Usuario, "Id", "Document", planTrabajoAsesor.TeamLeaderId);
            return View(planTrabajoAsesor);
        }

        // GET: PlanTrabajoAsesores/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlanTrabajoAsesor planTrabajoAsesor = db.PlanTrabajoAsesor.Find(id);
            if (planTrabajoAsesor == null)
            {
                return HttpNotFound();
            }
            ViewBag.AsesorId = new SelectList(db.Usuario, "Id", "Document", planTrabajoAsesor.AsesorId);
            ViewBag.TeamLeaderId = new SelectList(db.Usuario, "Id", "Document", planTrabajoAsesor.TeamLeaderId);
            return View(planTrabajoAsesor);
        }

        // POST: PlanTrabajoAsesores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MetaPromedioDia,DiasSemana,MetaSemanal,DiasMes,MetaMensual,OportunidadesMejora,CompromisoAsesor,DescripcionPlanAccion,NombreCampana,NombreTeamLeader,NombreAsesor,CedulaAsesor,LoginAsesor,AsesorId,TeamLeaderId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] PlanTrabajoAsesor planTrabajoAsesor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(planTrabajoAsesor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AsesorId = new SelectList(db.Usuario, "Id", "Document", planTrabajoAsesor.AsesorId);
            ViewBag.TeamLeaderId = new SelectList(db.Usuario, "Id", "Document", planTrabajoAsesor.TeamLeaderId);
            return View(planTrabajoAsesor);
        }

        // GET: PlanTrabajoAsesores/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlanTrabajoAsesor planTrabajoAsesor = db.PlanTrabajoAsesor.Find(id);
            if (planTrabajoAsesor == null)
            {
                return HttpNotFound();
            }
            return View(planTrabajoAsesor);
        }

        // POST: PlanTrabajoAsesores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            PlanTrabajoAsesor planTrabajoAsesor = db.PlanTrabajoAsesor.Find(id);
            db.PlanTrabajoAsesor.Remove(planTrabajoAsesor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
