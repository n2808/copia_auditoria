﻿using Core.Models.Security;
using Core.Models.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.PlanTrabajoTeam;
using tmkhogares.Models.UsuariosCampanas;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Gerente")]

    public class PlanTrabajoTeamsController : Controller
    {
        private Contexto db = new Contexto();
        
        public ActionResult ListarTeams(string Documento = "")
        {
            if (SessionPersister.HasRol("Gerente"))
            {
                Guid GerenteId = SessionPersister.Id.Value;
                User UsuarioGerente = db.Usuario.Where(u => u.Id == GerenteId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == GerenteId && u.User.Status == true).ToList();
                List<string> Campanas = new List<string>();
                List<User> usuarios = new List<User>().ToList();

                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var UsuariosDeCampana = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == new Guid("b0a06b9b-fb6e-4195-bc1c-ad84ad9a194d")) && u.UsuarioCampanas.Any(c => c.CampanaId == UC.CampanaId)).Include(u => u.Nivel).ToList(); ;
                    usuarios.AddRange(UsuariosDeCampana);
                }
                ViewBag.CampañaId = Campanas.ToList();
                ViewBag.Documento = Documento;
                if (Documento != "")
                {
                    return View(usuarios.Where(u => u.Document.Contains(Documento)).ToList());
                }
                else
                {
                    return View(usuarios.ToList());
                }
            }
            else
            {
                return HttpNotFound();
            }

        }
        // GET: PlanTrabajoTeams
        public ActionResult Index(Guid id)
        {
            ViewBag.TeamId = id;
            var planTrabajoTeam = db.PlanTrabajoTeam.Include(p => p.Gerente).Include(p => p.Seguimiento1).Include(p => p.Seguimiento2).Include(p => p.Seguimiento3).Include(p => p.Seguimiento4).Include(p => p.Team);
            return View(planTrabajoTeam.ToList());
        }

        // GET: PlanTrabajoTeams/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlanTrabajoTeam planTrabajoTeam = db.PlanTrabajoTeam.Find(id);
            if (planTrabajoTeam == null)
            {
                return HttpNotFound();
            }
            return View(planTrabajoTeam);
        }

        // GET: PlanTrabajoTeams/Create
        public ActionResult Create(Guid TeamId)
        {
            ViewBag.SeguimientoSemana1 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio");
            ViewBag.SeguimientoSemana2 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio");
            ViewBag.SeguimientoSemana3 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio");
            ViewBag.SeguimientoSemana4 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio");
            ViewBag.TeamId = TeamId;
            ViewBag.GerenteId = SessionPersister.Id.Value;
            return View();
        }

        // POST: PlanTrabajoTeams/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MetaPromedioDia,DiasSemana,MetaSemanal,DiasMes,MetaMensual,ObservacionesARealizar,CompromisoTeam,DescripcionPlanAccion,NombreCampana,NombreGerente,NombreTeam,CedulaTeam,LoginTeam,Estado,CantidadColaboradores,Ausentismo,Rotacion,TeamId,GerenteId,SeguimientoSemana1,SeguimientoSemana2,SeguimientoSemana3,SeguimientoSemana4,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] PlanTrabajoTeam planTrabajoTeam)
        {
            if (ModelState.IsValid)
            {
                //planTrabajoTeam.Id = Guid.NewGuid();
                //db.PlanTrabajoTeam.Add(planTrabajoTeam);
                //db.SaveChanges();
                //return RedirectToAction("Index");
                var PlanesTrabajoTeam = db.PlanTrabajoTeam.Where(pta => pta.TeamId == planTrabajoTeam.TeamId && pta.Estado == false).ToList();
                if (PlanesTrabajoTeam.Count > 0)
                {
                    ModelState.AddModelError("OportunidadesMejora", "Error el team ya tiene un plan de trabajo activo");
                    //return View(db.Grabaciones.ToPagedList(pageNumber, pageSize));
                    return View();
                }
                else
                {
                    User Team = db.Usuario.Find(planTrabajoTeam.TeamId);
                    User Gerente= db.Usuario.Find(SessionPersister.Id);
                    var campanaId = db.UsuariosCampanas.Where(u => u.UserId == Team.Id).FirstOrDefault().CampanaId;
                    var cantidadColaboradores = db.Usuario.Where(u => u.UserId == Team.Id).ToList().Count;
                    decimal metaSemanal = (decimal.Parse(planTrabajoTeam.MetaMensual.ToString()) / (decimal.Parse(planTrabajoTeam.DiasMes.ToString()) / decimal.Parse(planTrabajoTeam.DiasSemana.ToString())));
                    decimal metaDiaria = (decimal.Parse(planTrabajoTeam.MetaMensual.ToString()) / decimal.Parse(planTrabajoTeam.DiasSemana.ToString()));
                    if (metaSemanal == null || metaDiaria == null)
                    {
                        return HttpNotFound();
                    }

                    if (Team == null)
                    {
                        return HttpNotFound();
                    }
                    if (Gerente == null)
                    {
                        return HttpNotFound();
                    }
                    
                    planTrabajoTeam.Id = Guid.NewGuid();
                    planTrabajoTeam.TeamId = Team.Id;
                    planTrabajoTeam.NombreTeam = Team.Names;
                    planTrabajoTeam.CedulaTeam = Team.Document;
                    planTrabajoTeam.LoginTeam = Team.login;
                    planTrabajoTeam.GerenteId = Team.Id;
                    planTrabajoTeam.NombreGerente = Gerente.Names;
                    planTrabajoTeam.MetaSemanal = metaSemanal;
                    planTrabajoTeam.MetaPromedioDia = metaDiaria;
                    planTrabajoTeam.Estado = false;
                    planTrabajoTeam.CampanaId = campanaId;
                    planTrabajoTeam.CantidadColaboradores = cantidadColaboradores.ToString();

                    db.PlanTrabajoTeam.Add(planTrabajoTeam);
                    db.SaveChanges();
                    return RedirectToAction("Index",new { id = Team.Id});
                }
            }

            ViewBag.GerenteId = new SelectList(db.Usuario, "Id", "Document", planTrabajoTeam.GerenteId);
            ViewBag.SeguimientoSemana1 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana1);
            ViewBag.SeguimientoSemana2 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana2);
            ViewBag.SeguimientoSemana3 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana3);
            ViewBag.SeguimientoSemana4 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana4);
            ViewBag.TeamId = new SelectList(db.Usuario, "Id", "Document", planTrabajoTeam.TeamId);
            return View(planTrabajoTeam);
        }

        // GET: PlanTrabajoTeams/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlanTrabajoTeam planTrabajoTeam = db.PlanTrabajoTeam.Find(id);
            if (planTrabajoTeam == null)
            {
                return HttpNotFound();
            }
            ViewBag.GerenteId = new SelectList(db.Usuario, "Id", "Document", planTrabajoTeam.GerenteId);
            ViewBag.SeguimientoSemana1 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana1);
            ViewBag.SeguimientoSemana2 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana2);
            ViewBag.SeguimientoSemana3 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana3);
            ViewBag.SeguimientoSemana4 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana4);
            ViewBag.TeamId = new SelectList(db.Usuario, "Id", "Document", planTrabajoTeam.TeamId);
            return View(planTrabajoTeam);
        }

        // POST: PlanTrabajoTeams/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,MetaPromedioDia,DiasSemana,MetaSemanal,DiasMes,MetaMensual,ObservacionesARealizar,CompromisoTeam,DescripcionPlanAccion,NombreCampana,NombreGerente,NombreTeam,CedulaTeam,LoginTeam,Estado,CantidadColaboradores,Ausentismo,Rotacion,TeamId,GerenteId,SeguimientoSemana1,SeguimientoSemana2,SeguimientoSemana3,SeguimientoSemana4,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] PlanTrabajoTeam planTrabajoTeam)
        {
            if (ModelState.IsValid)
            {
                db.Entry(planTrabajoTeam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GerenteId = new SelectList(db.Usuario, "Id", "Document", planTrabajoTeam.GerenteId);
            ViewBag.SeguimientoSemana1 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana1);
            ViewBag.SeguimientoSemana2 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana2);
            ViewBag.SeguimientoSemana3 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana3);
            ViewBag.SeguimientoSemana4 = new SelectList(db.SeguimientoTeam, "Id", "MetaPromedio", planTrabajoTeam.SeguimientoSemana4);
            ViewBag.TeamId = new SelectList(db.Usuario, "Id", "Document", planTrabajoTeam.TeamId);
            return View(planTrabajoTeam);
        }

        // GET: PlanTrabajoTeams/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PlanTrabajoTeam planTrabajoTeam = db.PlanTrabajoTeam.Find(id);
            if (planTrabajoTeam == null)
            {
                return HttpNotFound();
            }
            return View(planTrabajoTeam);
        }

        // POST: PlanTrabajoTeams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            PlanTrabajoTeam planTrabajoTeam = db.PlanTrabajoTeam.Find(id);
            db.PlanTrabajoTeam.Remove(planTrabajoTeam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
