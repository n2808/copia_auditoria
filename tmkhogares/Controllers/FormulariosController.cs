﻿using Core.Models.Security;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.Campañas;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class FormulariosController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Formularios
        public ActionResult Index(int Id)
        {
            var formularios      = db.Formularios.Where(f => f.CampanaId == Id).Include(f => f.Campana);
            ViewBag.CampanaId    = Id;
            ViewBag.FormularioId = new SelectList(db.Campana.OrderBy(x => x.Nombre).Where(x => x.PadreId == x.Id && x.Estado).ToList(), "Id", "Nombre");
            TempData["Mensaje"]  = TempData["Mensaje"];
            return View(formularios.ToList());
        }

        // GET: Formularios/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Formulario formulario = db.Formularios.Find(id);
            if (formulario == null)
            {
                return HttpNotFound();
            }
            return View(formulario);
        }

        // GET: Formularios/Create
        public ActionResult Create(int Id)
        {
            ViewBag.CampId = Id;
            ViewBag.CampanaId = new SelectList(db.Campana, "Id", "Nombre");
            this.ViewData["NivelAsesor"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre");
            return View();
        }

    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Nombre,CampanaId,NivelAsesor,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy,TipoRol,PorcentajeProceso,PorcentajeGeneral")] Formulario formulario)
        {

            formulario.Id = Guid.NewGuid();
            if (ModelState.IsValid)
            {
           
                db.Formularios.Add(formulario);
                db.SaveChanges();
                return RedirectToAction("Index", "Formularios", new { Id = formulario.CampanaId });
            }

            ViewBag.CampanaId = new SelectList(db.Campana, "Id", "Nombre", formulario.CampanaId);
            ViewBag.CampId = formulario.CampanaId;         

            this.ViewData["NivelAsesor"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre");
            return View(formulario);
        }

        // GET: Formularios/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Formulario formulario = db.Formularios.Find(id);
            if (formulario == null)
            {
                return HttpNotFound();
            }
            ViewBag.CampanaId = new SelectList(db.Campana, "Id", "Nombre", formulario.CampanaId);
            return View(formulario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,CampanaId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy,,PorcentajeProceso,PorcentajeGeneral")] Formulario formulario)
        {
            if (ModelState.IsValid)
            {
                var _formulario = db.Formularios.Where(x => x.Id == formulario.Id).FirstOrDefault();
                _formulario.PorcentajeProceso = formulario.PorcentajeProceso;
                _formulario.PorcentajeGeneral = formulario.PorcentajeGeneral;
                _formulario.Nombre = formulario.Nombre;
                _formulario.CampanaId = formulario.CampanaId;

                db.Entry(_formulario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Formularios", new { Id = formulario.CampanaId });
            }
            ViewBag.CampanaId = new SelectList(db.Campana, "Id", "Nombre", formulario.CampanaId);
            return View(formulario);
        }

        // GET: Formularios/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            IQueryable<Formulario> formularioQuery = db.Formularios.Where(x => x.Id == id);

            // Traemos el nombre de la campaña a la que pertenece el formulario.
            ViewBag.Campana = formularioQuery.Select(x => x.Campana.Nombre).FirstOrDefault();

            Formulario formulario = formularioQuery.FirstOrDefault();

            if (formulario == null)
            {
                return HttpNotFound();
            }
            return View(formulario);
        }

        // POST: Formularios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Formulario formulario = db.Formularios.Find(id);

            int totalEstandares = db.Estandar.Where(x => x.FormularioId == formulario.Id).Count();

            if (totalEstandares > 0)
            {
                TempData["Mensaje"] = "Lo sentimos, no se puede eliminar este formulario porque depende de algún estandar";
                return RedirectToAction("Index", "Formularios", new { Id = formulario.CampanaId });
            }
            else
            {
                try
                {
                    db.Formularios.Remove(formulario);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Formularios", new { Id = formulario.CampanaId });
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
              
            }           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
