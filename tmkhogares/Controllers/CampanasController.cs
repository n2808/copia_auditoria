﻿using Core.Models.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using tmkhogares.Constantes;
using tmkhogares.Models;
using tmkhogares.Models.Campañas;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]
    public class CampanasController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Campanas
        public ActionResult Index()
        {
            TempData["Mensaje"] = TempData["Mensaje"];
            List<Campana> campanas = db.Campana.OrderBy(x => x.Nombre).Where(x => x.PadreId == x.Id && x.Estado).ToList();
            return View(campanas);
        }              

        // GET: Campanas/Create
        public ActionResult Create()
        {
            return View();
        }
    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Campana campana)
        {
            if (ModelState.IsValid)
            {

                using (var context = new Contexto())
                {
                    // CREATING A BEGIN TRANSACTION AND COMMIT.
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            campana.Id      = new int();
                            campana.PadreId = 0;
                            db.Campana.Add(campana);
                            db.SaveChanges();

                            var campanaEditar             = db.Campana.Find(campana.Id);
                            campanaEditar.PadreId         = campana.Id;
                            db.Entry(campanaEditar).State = EntityState.Modified;
                            db.SaveChanges();

                            //COMMIT TRANSACTION
                            dbContextTransaction.Commit();
                            
                            return RedirectToAction("Index");


                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            throw new Exception(ex.Message);

                        }
                    }
                }

               
            }

            return View(campana);
        }

        public ActionResult AsignarCampanaPadre(int? id, int? padreId)
        {
            if (id <= 0 || padreId <= 0) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            Campana campana         = db.Campana.Find(id);

            if (campana == null) return HttpNotFound();

            campana.PadreId         = padreId;
            db.Entry(campana).State = EntityState.Modified;
            db.SaveChanges();
            TempData["Mensaje"] = "El formulario ha sido asignado con éxito";
            return RedirectToAction("HomeAdmin", "Home");
        }

        // GET: Campanas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campana campana = db.Campana.Find(id);
            if (campana == null)
            {
                return HttpNotFound();
            }
            return View(campana);
        }

     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,MetaMensual,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Campana campana)
        {
            if (ModelState.IsValid)
            {
                
                db.Entry(campana).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(campana);
        }

       

        // GET: Campanas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campana campana = db.Campana.Find(id);
            ViewBag.CampanaNombre = campana.Nombre;
            if (campana == null)
            {
                return HttpNotFound();
            }
            return View(campana);
        }

        // POST: Campanas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Campana campana = db.Campana.Find(id);
       
            if (campana == null)
            {
                return HttpNotFound();
            }

            int totalFormularios = db.Formularios.Where(x => x.CampanaId == campana.Id).Count();

            if (totalFormularios > 0)
            {
                TempData["Mensaje"] = "Lo sentimos, no se puede eliminar esta Campaña porque depende de un formulario";
                return RedirectToAction("Index");
            }
            else
            {
                db.Campana.Remove(campana);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
