﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.UsuariosCampanas;
using tmkhogares.Servicios;

namespace tmkhogares.Controllers
{
    public class UsuarioCampanasController : Controller
    {
        private Contexto db                = new Contexto();
        private ServicioCampanas _campanas = new ServicioCampanas();
        private ServicioRol _rol           = new ServicioRol();

    
        public ActionResult Index(Guid id)
        {
            TempData["Mensaje"]  = TempData["Mensaje"];
            ViewBag.UserId       = id;
            var usuariosCampanas = db.UsuariosCampanas.Where(uc => uc.UserId == id).Include(u => u.Campana).Include(u => u.User).ToList();
            return View(usuariosCampanas);
        }   

        public ActionResult Create(Guid id)
        {

            // Seleccionamos todas las campañas donde no sea una campaña padre.           
            ViewBag.CampanaId   = new SelectList(_campanas.RetornarListaCampanas(), "Id", "Nombre");
            ViewBag.UserId      = id;
            TempData["Mensaje"] = TempData["Mensaje"];
            return View();
        }

     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CampanaId,UserId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] UsuarioCampanas usuarioCampanas)
        {
            if (ModelState.IsValid)
            {
                usuarioCampanas.Id = Guid.NewGuid();

                // Si el Usuario es Asesor no se le permite asignarle más Campañas.
                if (!_rol.ValidarRolPorUsuario(usuarioCampanas.UserId))
                {                  
                    TempData["Mensaje"] = "Error, El Asesor no puede poseer más de una campaña, o no se le ha asignado un rol";
                    return RedirectToAction("Create", new { id = usuarioCampanas.UserId });
                }

                // Si el Usuario es Asesor no se le permite asignarle más Campañas.
                var validarCampanas = _campanas.ValidarAsignacionCampanasPorUsuario(usuarioCampanas.UserId, usuarioCampanas.CampanaId);
                if (!validarCampanas.Item1)
                {
                    TempData["Mensaje"] = validarCampanas.Item2;
                    return RedirectToAction("Create", new { id = usuarioCampanas.UserId });
                }

                db.UsuariosCampanas.Add(usuarioCampanas);
                db.SaveChanges();
                TempData["Mensaje"] = "La campaña se agregó correctamente";
                return RedirectToAction("HomeAdmin", "Home");
            }
         
            ViewBag.CampanaId = new SelectList(_campanas.RetornarListaCampanas(), "Id", "Nombre", usuarioCampanas.CampanaId);
            ViewBag.UserId    = new SelectList(db.Usuario, "Id", "Document", usuarioCampanas.UserId);
            return View(usuarioCampanas);
        }  
        
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UsuarioCampanas usuarioCampanas = db.UsuariosCampanas.Find(id);
            if (usuarioCampanas == null)
            {
                return HttpNotFound();
            }
            return View(usuarioCampanas);
        }

       
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            UsuarioCampanas usuarioCampanas = db.UsuariosCampanas.Find(id);
            db.UsuariosCampanas.Remove(usuarioCampanas);
            db.SaveChanges();
            return RedirectToAction("Index",new { id = usuarioCampanas.UserId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
