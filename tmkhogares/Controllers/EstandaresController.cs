﻿using Core.Models.Security;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.Campañas;
using tmkhogares.Models.Estandares;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador")]

    public class EstandaresController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Estandares

        public ActionResult Index(Guid Id)
        {
            if (Id != Guid.Empty)
            {
                ViewBag.FormularioId = Id;
                List<Estandar> Estandares = db.Estandar.Where(ce => ce.FormularioId == Id && ce.Estado).OrderBy(ce => ce.Posicion).ToList();
                Formulario formulario = db.Formularios.Find(Id);
                ViewBag.FormularioNombre = formulario.Nombre;
                TempData["Mensaje"] = TempData["Mensaje"];
                TempData["MensajeError"] = TempData["MensajeError"];
                return View(Estandares);
            }
            else
            {
                return HttpNotFound();
            }


        }

        // GET: Estandares/Create
        public ActionResult Create(Guid Id)
        {
            ViewBag.FormularioId = Id;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Nombre,Descripcion,FormularioId")] Estandar estandar)
        {
            if (ModelState.IsValid)
            {
                var estandarBd      = db.Estandar.OrderByDescending(x => x.Posicion).Where(x => x.FormularioId == estandar.FormularioId).FirstOrDefault();
                estandar.Posicion   = estandarBd == null ? 1 : estandarBd.Posicion + 1;
                estandar.Id         = Guid.NewGuid();
                estandar.Nombre     = estandar.Nombre.ToUpper();
                db.Estandar.Add(estandar);
                db.SaveChanges();
                return RedirectToAction("Index", "Estandares", new { Id = estandar.FormularioId });
            }
            return View(estandar);
        }

        // GET: Estandares/Edit/5
        public ActionResult Edit(Guid? id, Guid FormularioId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estandar estandar = db.Estandar.Find(id);
            if (estandar == null)
            {
                return HttpNotFound();
            }

            ViewBag.FormularioId = FormularioId;

            TempData["Error"] = TempData["Error"];
            return View(estandar);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,Posicion,FormularioId,Estado,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Estandar estandar, string peso)
        {
            if (ModelState.IsValid)
            {
                if (peso.Contains("."))
                {
                    peso = peso.Replace(".", ",");
                }


                if (string.IsNullOrEmpty(peso))
                {
                    TempData["Error"] = "El valor del peso no puede estar vacío";
                    return RedirectToAction("Edit", "Estandares", new { id = estandar.Id, FormularioId = estandar.FormularioId });
                }

                // Asignamos el nuevo peso al estandar para su validación.
                estandar.Peso = decimal.Parse(peso);

                // Validación del total del peso de los estándares.
                if (ValidarPesoEstandar(estandar, estandar.Peso))
                {
                    TempData["Error"] = "El peso de los Estandares no debe superar el 100%";
                    return RedirectToAction("Edit", "Estandares", new { id = estandar.Id, FormularioId = estandar.FormularioId });
                }

                ViewBag.AtributoId = estandar.FormularioId;

                try
                {
                    db.Entry(estandar).State = EntityState.Modified;
                    db.SaveChanges();
                    ViewBag.FormularioId = estandar.FormularioId;
                    TempData["Mensaje"] = "El estandar se editó correctamente";
                    return RedirectToAction("Index", "Estandares", new { Id = estandar.FormularioId });

                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }

            }
            return View(estandar);
        }

        /// <summary>
        /// Método que se encarga de validar el peso de los estándares según el formulario.
        /// </summary>
        /// <param name="estandar"></param>
        /// <param name="peso"></param>
        /// <returns></returns>
        public bool ValidarPesoEstandar(Estandar estandar, decimal peso)
        {
            if (!estandar.Estado)
            {
                // Si el estado es inactivo no es necesario validar el estandar.
                return false;
            }
            else
            {
                List<decimal> dataEstandar = new List<decimal>();

                decimal totalPesosEstandares = 0;

                dataEstandar = (db.Estandar.Where(x => x.Estado && x.FormularioId == estandar.FormularioId && x.Id != estandar.Id))
                                   .Select(a => a.Peso)
                                   .ToList();

                if (dataEstandar.Count > 0)
                {
                    totalPesosEstandares = dataEstandar.Sum() + peso;
                }
                else
                {
                    totalPesosEstandares = peso;
                }

                if (totalPesosEstandares > 100)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    
      
        // GET: Estandares/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Estandar estandar = db.Estandar.Find(id);
            if (estandar == null)
            {
                return HttpNotFound();
            }
            return View(estandar);
        }

        // POST: Estandares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Estandar estandar = db.Estandar.Find(id);

            if (estandar == null)
            {
                return HttpNotFound();
            }

            List<Atributo> totalAtributos = db.Atributo.Where(x => x.EstandarId == estandar.Id).ToList();

            if (totalAtributos.Count > 0)
            {
                foreach (var item in totalAtributos)
                {
                    item.Estado = false;
                    db.Entry(item).State = EntityState.Modified;
                    db.SaveChanges();
                }

                estandar.Estado = false;
                db.Entry(estandar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Estandares", new { Id = estandar.FormularioId });
            }
            else
            {
                try
                {
                    db.Estandar.Remove(estandar);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Estandares", new { Id = estandar.FormularioId });
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
