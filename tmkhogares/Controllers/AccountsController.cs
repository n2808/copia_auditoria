﻿using Core.Models.Security;
using System.Web.Mvc;


namespace ClaroGrandesSuperficies.Controllers
{
    public class AccountsController : Controller
    {
        // GET: Accounts
        public ActionResult Index()
        {
            //SessionPersister.Id = new Guid("F4DA2ADE-CA21-4428-B8A7-FD8A366E07C5");
            //SessionPersister.UserName = "auto generado pruebas";

            if (SessionPersister.Id != null)
                return RedirectToAction("HomeAdmin","Home",null);

            return View();
        }

        [HttpPost]
        public ActionResult Login(AccountsViewModel avm)
        {

            AccountsModel am = new AccountsModel();
            Accounts ResultUser = am.Login(avm.Accounts.UserName, avm.Accounts.Password);
            if (string.IsNullOrEmpty(avm.Accounts.UserName) || string.IsNullOrEmpty(avm.Accounts.Password) || ResultUser == null)
            {
                ViewBag.Error = "Usuario o contraseña incorrectos";
                return View("index");
            }
            // get user data for the session.
            SessionPersister.UserName = avm.Accounts.UserName;
            SessionPersister.Id = ResultUser.Id;
            SessionPersister.Name = ResultUser.Name;
            
           

            return RedirectToAction("index");

        }

        public ActionResult Logout()
        {
            SessionPersister.UserName = string.Empty;
            SessionPersister.Id = null;
            return View("index");
        }


        public ActionResult AccessDenied()
        {
            return View();
        }
    }
}