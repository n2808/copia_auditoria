﻿using Core.Models.Security;
using Core.Models.User;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Constantes;
using tmkhogares.Models;
using tmkhogares.Models.Campañas;
using tmkhogares.Models.Carga.MappingDatatable;
using tmkhogares.Models.Promedios;
using tmkhogares.Models.UsuariosCampanas;
using tmkhogares.Servicios;
using tmkhogares.ViewModel;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador,Gerente,TeamLeader,Calidad")]
    public class UsersController : Controller
    {

        private Contexto db = new Contexto();
        private ServicioUsuarios _usuarios = new ServicioUsuarios();
        private ServicioCampanas _campanas = new ServicioCampanas();
        private ServicioUsuarioCampana _usuarioCampana = new ServicioUsuarioCampana();
        private ServicioRol _roles = new ServicioRol();

        /*------------------------------------------------------*
        * ADMINISTRACION DE USUARIOS
        *------------------------------------------------------*/

        // GET: Users
        public ActionResult Index(string documento)
        {
            List<User> usuarios = new List<User>();
            Guid usuarioSession = (Guid)SessionPersister.Id;
            var campanasUsuario = db.UsuariosCampanas.Where(x => x.UserId.Equals(usuarioSession)).Select(x => x.CampanaId).ToList();

            if (campanasUsuario.Count <= 0)
            {
                TempData["Mensaje"] = "Se le debe asignar una Campaña al usuario para continuar con el proceso";
                return RedirectToAction("Index", "UsuarioCampanas", new { id = usuarioSession });
            }

            TempData["Mensaje"] = TempData["Mensaje"];

            if (string.IsNullOrEmpty(documento))
            {
                usuarios = _usuarios.RetornarUsuariosPorCampana(campanasUsuario);
            }
            else
            {
                usuarios = _usuarios.RetornarUsuarioPorDocumento(documento);
            }

            return View(usuarios);
        }

        public ActionResult IndexListadoAuditores(int? CampanaId)
        {

            List<User> auditores = new List<User>();

            ViewBag.CampanaId = new SelectList(_campanas.RetornarListaCampanas(), "Id", "Nombre");

            if (CampanaId > 0)
            {
                auditores             = _usuarios.ObtenerListadoAutitores((int) CampanaId);
                ViewBag.NombreCampana = _campanas.RetornarCampanaPorId((int) CampanaId).Nombre.ToUpper();
                ViewBag.IdCampana     = CampanaId;

            }

            return View(auditores);
        }


        public ActionResult IndexListadoAuditados(Guid? usuarioId, int? estado)
        {

            List<AuditadosVM> auditados = new List<AuditadosVM>();
            if (usuarioId != null)
            {
                var item                          = _usuarios.IndexListadoAuditados((Guid)usuarioId, estado);
                auditados                         = item.Item1;
                ViewBag.TotalPendientesFeedBack   = item.Item2.PendientesFeedBack;              
                ViewBag.TotalNoPendientesFeedBack = item.Item2.NoPendientesFeedBack;              
                ViewBag.NombreAuditor             = _usuarios.RetornarUsuarioPorId((Guid) usuarioId).Names.ToUpper();                
                ViewBag.UsuarioId                 = usuarioId;                

            }

            return View(auditados);
        }

        public ActionResult IndexGerente(string Documento = "")
        {
            if (SessionPersister.HasRol("Gerente"))
            {
                Guid GerenteId = SessionPersister.Id.Value;
                User UsuarioGerente = db.Usuario.Where(u => u.Id == GerenteId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == GerenteId).ToList();
                List<string> Campanas = new List<string>();
                List<User> usuarios = new List<User>().ToList();

                foreach (UsuarioCampanas UC in UsuariosCampanas)
                {
                    Campanas.Add(UC.CampanaId.ToString());
                    var UsuariosDeCampana = db.Usuario.Where(u => u.UsuarioCampanas.Any(c => c.CampanaId == UC.CampanaId)).Include(u => u.Nivel).Include(x => x.TeamLeader).ToList(); ;
                    usuarios.AddRange(UsuariosDeCampana);
                }
                ViewBag.CampañaId = Campanas.ToList();
                ViewBag.Documento = Documento;
                if (Documento != "")
                {
                    return View(usuarios.Where(u => u.Document.Contains(Documento)).ToList());
                }
                else
                {
                    return View(usuarios.ToList());
                }
            }
            else
            {
                return HttpNotFound();
            }
        }

        public ActionResult IndexCalidad(string Documento = "", string Campana = "")
        {
            if (SessionPersister.HasRol("Calidad") || SessionPersister.HasRol("Administrador"))
            {
                List<User> usuarios = new List<User>().ToList();

                if (!string.IsNullOrEmpty(Campana))
                {
                    TempData["Mensaje"] = TempData["Mensaje"];
                    var CampanaId = int.Parse(Campana);
                    ViewBag.NombreCampana = db.Campana.Where(x => x.Id == CampanaId).Select(x => x.Nombre.ToUpper()).FirstOrDefault();
                    Guid GerenteId = SessionPersister.Id.Value;
                    User UsuarioGerente = db.Usuario.Where(u => u.Id == GerenteId).FirstOrDefault();
                    usuarios = db.Usuario.OrderBy(u => u.Names).Where(u => u.UsuarioCampanas.Any(c => c.CampanaId == CampanaId))
                                                          .Include(u => u.Nivel)
                                                          .Include(u => u.TeamLeader)
                                                          .ToList();

                    ViewBag.CampañaId = CampanaId;
                    ViewBag.Documento = Documento;

                    if (Documento != "")
                    {
                        usuarios = usuarios.Where(u => u.Document.Contains(Documento)).ToList();
                        return View(usuarios);
                    }
                    else
                    {
                        return View(usuarios.ToList());
                    }
                }
                else
                {
                    ViewBag.CampanaId = null;
                    return View(usuarios.ToList());
                }

            }
            else
            {
                return HttpNotFound();
            }
        }
        public ActionResult IndexTeamLeader(string Documento = "")
        {
            if (SessionPersister.HasRol("TeamLeader"))
            {
                Guid TeamId = SessionPersister.Id.Value;
                User UsuarioTeam = db.Usuario.Where(u => u.Id == TeamId).FirstOrDefault();
                var usuarios = db.Usuario.Where(u => u.UserId == UsuarioTeam.Id).ToList();
                ViewBag.Documento = Documento;
                if (Documento != "")
                {
                    return View(usuarios.Where(u => u.Document.Contains(Documento)).ToList());
                }
                else
                {
                    return View(usuarios.ToList());
                }
            }
            else
            {
                return HttpNotFound();
            }
        }


        // GET: Users/Create
        public ActionResult Create()
        {

            var Usuarios = new List<User>();
            Guid usuarioSesion = (Guid)SessionPersister.Id;
            Guid rolId = new Guid(tmkhogares.Constantes.Rol.TEAM_LEADER);
            var TeamLeaders = db.UsuariosRoles.Where(ur => ur.RolId == rolId).ToList();

            foreach (var TeamLeader in TeamLeaders)
            {
                User TeamL = db.Usuario.Find(TeamLeader.UserId);
                Usuarios.Add(TeamL);
            }


            this.ViewData["UserId"] = new SelectList(Usuarios.ToList(), "Id", "Names");
            this.ViewData["NivelId"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre");
            this.ViewData["RolId"] = new SelectList(_roles.RetornarRoles(), "Id", "Name");
            this.ViewData["CampanaId"] = new SelectList(_campanas.RetornarListaCampanas(), "Id", "Nombre");
            TempData["MensajeError"] = TempData["MensajeError"];

            return View();

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Document,Names,UserId,NivelId,Status,PassWord,login,CreatedAt,CreatedBy")] User user, int? CampanaId, Guid? RolId)
        {
            if (CampanaId == null)
            {
                TempData["MensajeError"] = "El campo campaña es requerido";
                return RedirectToAction("Create");
            }

            if (RolId == null)
            {
                TempData["MensajeError"] = "El campo Rol es requerido";
                return RedirectToAction("Create");
            }


            if (ModelState.IsValid)
            {

                using (var context = new Contexto())
                {
                    // CREATING A BEGIN TRANSACTION AND COMMIT.
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            // Validamos si el usuario a ingresar ya existe.
                            if (_usuarios.UsuarioExiste(user.Document.Trim()))
                            {
                                TempData["MensajeError"] = "El usuario que está ingresando ya existe";
                                return RedirectToAction("Create");
                            }
                            else
                            {
                                user.Id = Guid.NewGuid();
                                user.CreatedBy = SessionPersister.Id;
                                db.Usuario.Add(user);
                                db.SaveChanges();

                                // Asignamos el rol y la campaña al usuario.
                                _roles.AsignarRolUsuario((Guid)RolId, user.Id);
                                _usuarioCampana.AsignarCampanaUsuario((int)CampanaId, user.Id);

                                //COMMIT TRANSACTION
                                dbContextTransaction.Commit();

                                return RedirectToAction("Index");
                            }

                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            throw new Exception(ex.Message);

                        }
                    }
                }

            }

            return View(user);
        }


        public ActionResult CreateCalidad()
        {
            if (SessionPersister.HasRol("Calidad"))
            {

                Guid GerenteId = SessionPersister.Id.Value;
                User UsuarioGerente = db.Usuario.Where(u => u.Id == GerenteId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == GerenteId).ToList();
                List<Campana> Campañas = new List<Campana>();
                List<User> TeamLeaders = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == new Guid(Constantes.Rol.TEAM_LEADER))).ToList();
                Campañas = db.Campana.OrderBy(x => x.Nombre).Where(x => x.Id > 1).ToList();
                this.ViewData["UserId"] = new SelectList(TeamLeaders, "Id", "Names");
                this.ViewData["CampanaId"] = new SelectList(Campañas, "Id", "Nombre");
                this.ViewData["NivelId"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre");

                return View();
            }
            else
            {
                return HttpNotFound();
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCalidad([Bind(Include = "Document,Names,LastName,UserId,CampanaId,NivelId,Status,PassWord,login,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] User user, int CampanaId)
        {
            user.Id = Guid.NewGuid();

            if (ModelState.IsValid)
            {
                // Validamos si el usuario a ingresar ya existe.
                if (_usuarios.UsuarioExiste(user.Document.Trim()))
                {
                    TempData["MensajeError"] = "El usuario que está ingresando ya existe";
                    return RedirectToAction("CreateCalidad");
                }
                else
                {
                    using (var context = new Contexto())
                    {
                        using (var dbContextTransaction = context.Database.BeginTransaction())
                        {
                            try
                            {
                                db.Usuario.Add(user);
                                db.SaveChanges();

                                var Campana = db.Campana.Find(CampanaId);
                                if (Campana == null)
                                {
                                    return HttpNotFound();
                                }
                                UsuarioCampanas uc = new UsuarioCampanas();
                                uc.UserId = user.Id;
                                uc.CampanaId = Campana.Id;
                                db.UsuariosCampanas.Add(uc);
                                db.SaveChanges();
                                dbContextTransaction.Commit();
                                TempData["Mensaje"] = "El Usuario ha sido creado con éxito.";
                                return RedirectToAction("IndexCalidad");
                            }
                            catch (Exception ex)
                            {

                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }

                }

            }

            Guid GerenteId = SessionPersister.Id.Value;
            User UsuarioGerente = db.Usuario.Where(u => u.Id == GerenteId).FirstOrDefault();
            var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == GerenteId).ToList();
            List<Campana> Campañas = new List<Campana>();
            List<User> TeamLeaders = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == new Guid(Constantes.Rol.TEAM_LEADER))).ToList();
            Campañas = db.Campana.OrderBy(x => x.Nombre).Where(x => x.Id > 1).ToList();
            this.ViewData["UserId"] = new SelectList(TeamLeaders, "Id", "Names");
            this.ViewData["CampanaId"] = new SelectList(Campañas, "Id", "Nombre");
            this.ViewData["NivelId"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre");

            return View(user);
        }
        public ActionResult CreateTeamLeader()
        {
            if (SessionPersister.HasRol("TeamLeader"))
            {

                Guid TeamId = SessionPersister.Id.Value;
                User UsuarioTeam = db.Usuario.Where(u => u.Id == TeamId).FirstOrDefault();
                var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == TeamId).FirstOrDefault();

                Guid TeamLeaderId = Guid.Parse("B0A06B9B-FB6E-4195-BC1C-AD84AD9A194D");
                Guid Niveles = Guid.Parse("0F1B989F-738F-4C33-81E2-C35F5E4BC8A0");

                ViewBag.UserId = TeamId;
                this.ViewData["NivelId"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre");

                return View();
            }
            else
            {
                return HttpNotFound();
            }

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTeamLeader([Bind(Include = "Document,Names,LastName,UserId,CampanaId,NivelId,Status,PassWord,login,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] User user)
        {
            User Team = db.Usuario.Find(SessionPersister.Id.Value);
            int CampanaId = db.UsuariosCampanas.Where(uc => uc.UserId == Team.Id).FirstOrDefault().CampanaId;
            if (CampanaId <= 0)
            {
                return HttpNotFound();
            }

            if (Team == null)
            {
                return HttpNotFound();
            }
            user.Id = Guid.NewGuid();

            if (ModelState.IsValid)
            {
                user.UserId = Team.Id;
                db.Usuario.Add(user);
                db.SaveChanges();
                var Campana = db.Campana.Find(CampanaId);
                if (Campana == null)
                {
                    return HttpNotFound();
                }
                UsuarioCampanas uc = new UsuarioCampanas();
                uc.UserId = user.Id;
                uc.CampanaId = Campana.Id;
                db.UsuariosCampanas.Add(uc);
                db.SaveChanges();
                return RedirectToAction("IndexTeamLeader");
            }
            Guid GerenteId = SessionPersister.Id.Value;
            User UsuarioGerente = db.Usuario.Where(u => u.Id == GerenteId).FirstOrDefault();
            var UsuariosCampanas = db.UsuariosCampanas.Where(u => u.UserId == GerenteId).ToList();
            Guid TeamLeaderId = Guid.Parse("B0A06B9B-FB6E-4195-BC1C-AD84AD9A194D");
            Guid Niveles = Guid.Parse("0F1B989F-738F-4C33-81E2-C35F5E4BC8A0");
            List<Campana> Campañas = new List<Campana>();
            List<User> TeamLeaders = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == tmkhogares.ViewModel.Roles.TeamLeader)).ToList();
            foreach (var usuariocampana in UsuariosCampanas)
            {
                Campana campaña = db.Campana.Find(usuariocampana.CampanaId);
                if (campaña != null)
                {
                    Campañas.Add(campaña);
                }
            }
            this.ViewData["UserId"] = new SelectList(TeamLeaders, "Id", "Names");
            this.ViewData["CampanaId"] = new SelectList(Campañas, "Id", "Nombre");
            this.ViewData["NivelId"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre");
            return View(user);
        }
        // GET: Users/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var usuarioCampana = db.UsuariosCampanas.Where(x => x.UserId == user.Id).FirstOrDefault();

            if (usuarioCampana != null)
            {
                var campana = db.Campana.Where(x => x.Id == usuarioCampana.CampanaId).FirstOrDefault();
                ViewBag.CampanaId = new SelectList(db.Campana.OrderBy(x => x.Nombre), "Id", "Nombre", campana?.Id, campana.Nombre);

            }
            else
            {
                ViewBag.CampanaId = new SelectList(db.Campana.OrderBy(x => x.Nombre), "Id", "Nombre");

            }

            var nivel = db.Niveles.Where(x => x.Id == user.NivelId).FirstOrDefault();

            if (nivel == null)
            {
                this.ViewData["NivelId"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre");

            }
            else
            {
                this.ViewData["NivelId"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre", nivel?.Id, nivel.Nombre);

            }

            Roles Roles = new Roles();

            var teamLeader = db.Usuario.Where(x => x.UserId == user.UserId).FirstOrDefault();

            if (teamLeader == null)
            {
                ViewBag.UserId = new SelectList(db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == Roles.TeamLeader)).ToList(), "Id", "Names");
            }
            else
            {
                ViewBag.UserId = new SelectList(db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == Roles.TeamLeader)).ToList(), "Id", "Names", teamLeader?.Id, teamLeader.Names);
            }


            //ViewBag.TeamLeaders = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == Roles.TeamLeader)).ToList();

            return View(user);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Document,Names,LastName,Phone1,Phone2,Phone3,Email,Status,PassWord,UserId,CampanaId,NivelId,login,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] User user, int? CampanaId)
        {
            if (ModelState.IsValid)
            {

                using (var context = new Contexto())
                {
                    // CREATING A BEGIN TRANSACTION AND COMMIT.
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //BEGIN OF THE QUERY
                            db.Entry(user).State = EntityState.Modified;
                            db.SaveChanges();

                            // Proceso para editar el modelo UsuarioCampanas.
                            UsuarioCampanas usuarioCampanas = db.UsuariosCampanas.Where(x => x.UserId == user.Id).FirstOrDefault();
                            if (usuarioCampanas == null)
                            {
                                TempData["MensajeError"] = "El usuario que intenta editar no se encuentra asociado a ninguna Campaña, por favor asígnela por el administrador";
                                return RedirectToAction("Index");
                            }

                            // Proceso para que el campo Campaña no sea vacío.
                            if (CampanaId == null)
                            {
                                TempData["MensajeError"] = "El campo Campaña es obligatorio.";
                                return RedirectToAction("Edit", new { id = user.Id });
                            }

                            usuarioCampanas.CampanaId = (int)CampanaId;
                            db.Entry(usuarioCampanas).State = EntityState.Modified;
                            db.SaveChanges();


                            //COMMIT TRANSACTION
                            dbContextTransaction.Commit();

                            TempData["Mensaje"] = "Usuario editado con éxito";

                            if (SessionPersister.HasRol("Gerente"))
                            {

                                return RedirectToAction("IndexGerente");
                            }
                            else
                            {
                                return RedirectToAction("Index");
                            }


                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            throw new Exception(ex.Message);

                        }
                    }
                }

            }

            var usuarioCampana = db.UsuariosCampanas.Where(x => x.UserId == user.Id).FirstOrDefault();

            if (usuarioCampana != null)
            {
                var campana = db.Campana.Where(x => x.Id == usuarioCampana.CampanaId).FirstOrDefault();
                ViewBag.CampanaId = new SelectList(db.Campana.OrderBy(x => x.Nombre), "Id", "Nombre", campana?.Id, campana.Nombre);

            }
            else
            {
                ViewBag.CampanaId = new SelectList(db.Campana.OrderBy(x => x.Nombre), "Id", "Nombre");

            }

            this.ViewData["NivelId"] = new SelectList(db.Niveles.ToList(), "Id", "Nombre");
            Roles Roles = new Roles();
            this.ViewData["UserId"] = new SelectList(db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == Roles.TeamLeader)).ToList(), "Id", "Nombre");
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Usuario.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            User user = db.Usuario.Find(id);
            db.Usuario.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /*--------------------------*
        * ADMINISTRACION DE ROLES  *
        *-------------------------*/

        public ActionResult Roles(Guid Id)
        {
            User Usuario = db.Usuario.Find(Id);
            if (Id == null || Usuario == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Name = Usuario.Names;
            ViewBag.Id = Id;
            ViewBag.UsersRol = db.UsuariosRoles.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            if (SessionPersister.HasRol("Gerente"))
            {
                ViewBag.RolId = new SelectList(db.Roles.Where(r => r.Id == tmkhogares.ViewModel.Roles.Asesor || r.Id == tmkhogares.ViewModel.Roles.Reporting || r.Id == tmkhogares.ViewModel.Roles.TeamLeader), "Id", "Name");
            }
            else if (SessionPersister.HasRol("TeamLeader"))
            {
                ViewBag.RolId = new SelectList(db.Roles.Where(r => r.Id == tmkhogares.ViewModel.Roles.Asesor), "Id", "Name");

            }
            else if (SessionPersister.HasRol("Administrador"))
            {
                ViewBag.RolId = new SelectList(db.Roles.OrderBy(x => x.Name).Where(x => x.Status), "Id", "Name");
            }
            else if (SessionPersister.HasRol("Calidad"))
            {
                ViewBag.RolId = new SelectList(db.Roles.Where(r => r.Id == tmkhogares.ViewModel.Roles.Asesor), "Id", "Name");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Roles([Bind(Include = "Id,RolId,UserId")] UserRol userRol, string Name)
        {
            if (ModelState.IsValid)
            {

                var Exist = db.UsuariosRoles
                    .Where(
                    c => c.UserId == userRol.UserId &&
                    c.RolId == userRol.RolId
                ).FirstOrDefault();

                var rol = Constantes.Rol.TEAM_LEADER.ToString();

                var rolTeam = db.UsuariosRoles.Where(c => c.UserId == userRol.UserId && c.RolId == new Guid(Constantes.Rol.TEAM_LEADER)).Count();

                if (Exist != null)
                {
                    ModelState.AddModelError("RolId", "Este permiso ya fue asignado.");
                }
                else if (rolTeam > 0)
                {
                    ModelState.AddModelError("RolId", "El Usuario ya posee un Rol de TeamLeader, no se le puede asignar otro Rol.");
                }
                else
                {
                    userRol.Id = Guid.NewGuid();
                    db.UsuariosRoles.Add(userRol);
                    db.SaveChanges();
                    return RedirectToAction("Roles", new { Id = userRol.UserId });
                }


            }
            var Id = userRol.UserId;
            ViewBag.Name = Name;
            ViewBag.Id = Id;
            ViewBag.UsersRol = db.UsuariosRoles.Include(u => u.Rol).Include(u => u.User).Where(p => p.UserId == Id);
            ViewBag.RolId = new SelectList(db.Roles, "Id", "Name");
            return View("Roles");
        }

        // POST: UserOfPointOfCares/Delete/5
        [HttpPost, ActionName("DeleteRol")]
        [ValidateAntiForgeryToken]
        public ActionResult RolesDeleteConfirmed(Guid id)
        {
            UserRol UsusarioRol = db.UsuariosRoles.Find(id);
            var UserId = UsusarioRol.UserId;
            db.UsuariosRoles.Remove(UsusarioRol);
            db.SaveChanges();
            return RedirectToAction("Roles", new { Id = UserId });
        }

        [HttpGet]
        public ActionResult CargueVentas()
        {
            var TeamId = SessionPersister.Id;
            if (TeamId == null)
            {
                return HttpNotFound();
            }
            ViewBag.Usuarios = db.Usuario.Where(u => u.UserId == TeamId).ToList();
            return View();
        }
        [HttpPost]
        public JsonResult CargarVentas()
        {
            var TeamId = SessionPersister.Id;
            User TeamLeader = db.Usuario.Find(TeamId);
            if (TeamLeader == null)
            {
                return Json(new { status = 400, message = Errors.TLDE });
            }
            if (TeamId == null)
            {
                //TLDE : TEAM LEADER DONT EXIST
                return Json(new { status = 400, message = Errors.TLDE });
            }
            var campañaTeamLeader = db.UsuariosCampanas.Where(uc => uc.UserId == TeamId).Include(c => c.Campana).FirstOrDefault();
            if (campañaTeamLeader == null)
            {
                return Json(new { status = 400, message = Errors.CDE });
            }


            ViewBag.Usuarios = db.Usuario.Where(u => u.UserId == TeamId).ToList();
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase fileUpload = HttpContext.Request.Files[0];
                var excel = new ExcelPackage(fileUpload.InputStream);
                var dt = excel.ToDataTable();
                CargueViewModel carga = InsertWebContenido(dt, campañaTeamLeader.CampanaId);
                return Json(new
                {
                    asesoresNoCreados = carga.AsesoresNoCreados,
                    asesoresNoPertenecen = carga.AsesoresNoPertenecen,
                    asesoresYaActualizados = carga.AsesoresYaActualizados,
                    status = 200
                });
            }
            else
            {
                return Json(new { status = 400, message = "Error debe seleccionar un archivo" });
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public JsonResult EnviarCorreo()
        {
            var correo = new _Correo()
            {
                ServerName = "mail.uno27.com",
                port = "25",
                senderEmailId = "Operacion@uno27.com",
                senderPassword = "Operacion@uno27.com"
            };

            var UsuariosGerentes = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == new Guid("0A2155D6-3E87-485E-8233-2DB2EA0DEDBE"))).Include(u => u.Nivel).ToList(); ;

            foreach (var gerente in UsuariosGerentes)
            {
                List<User> usuariosCuartilBajo = new List<User>();
                var campanasGerente = db.UsuariosCampanas.Where(u => u.UserId == gerente.Id).ToList();
                foreach (var campanaGerente in campanasGerente)
                {

                    var UsuariosDeCampana = db.Usuario.Where(u => u.UserRol.Any(r => r.RolId == new Guid("4450491C-32E6-41AE-B301-CB22DFC8BBD9")) && u.UsuarioCampanas.Any(c => c.CampanaId == campanaGerente.CampanaId)).Include(u => u.Nivel).ToList();
                    foreach (var usuario in UsuariosDeCampana)
                    {
                        var ultimaNota = ObtenerUltimaCalifiacion(usuario.login);
                        if (decimal.Parse(ultimaNota) <= 86)
                        {
                            usuariosCuartilBajo.Add(usuario);
                        }
                    }
                }


                var bodyMessage = "";

                var mensaje = "Señor GERENTE<br />Se relacionan los asesores que una vez efectuados los monitores por parte del area de Calidad quedaron en cuartiles 3 y 4.<br /> Se solicita su gestion inmediata con el fin de mejorar dicha calificacion.<br /><br /> <table border='1'><tr><td>Documento</td><td>Nombre</td><td>Campaña</td><td>Ultima Nota</td></tr>";

                foreach (var user in usuariosCuartilBajo)
                {
                    var campanasUsuario = db.UsuariosCampanas.Where(u => u.UserId == user.Id).FirstOrDefault();
                    var nombreCampana = "";

                    if (campanasUsuario != null)
                    {
                        var campaña = db.Campana.Where(c => c.Id == campanasUsuario.CampanaId).FirstOrDefault();
                        if (campaña != null)
                        {
                            nombreCampana = campaña.Nombre;
                        }
                    }

                    mensaje += "<tr>";
                    mensaje += "<td>" + user.Document + "</td>";
                    mensaje += "<td>" + user.Names + "</td>";
                    mensaje += "<td>" + nombreCampana + "</td>";
                    mensaje += "<td>" + ObtenerUltimaCalifiacion(user.login) + "</td>";
                    mensaje += "</tr>";
                }
                mensaje += "</table>";

                bodyMessage = "<!DOCTYPE html> <html lang='en'> <head> <meta charset='UTF-8'> <title>ALERTA</title> </head> <body><p>" + mensaje + "</p><body></html>";
                Email.EnviarCorreoElectronico(gerente.Email, bodyMessage, "ALERTA", correo);


            }






            return Json(new { status = 201 });
        }
        public string ObtenerUltimaCalifiacion(string login)
        {
            Contexto db = new Contexto();
            User Usuario = db.Usuario.Where(u => u.login == login).FirstOrDefault();
            DateTime Hoy = DateTime.Now;
            DateTime FechaInicio = new DateTime(Hoy.Year, Hoy.Month, 1);
            DateTime FechaFin = new DateTime(Hoy.Year, Hoy.Month, 1).AddMonths(1).AddDays(-1);
            var TotalCalificaciones = db.Calificacions.Where(c => c.CreatedAt >= FechaInicio && c.CreatedAt <= FechaFin && c.AsesorId == Usuario.Id).OrderByDescending(c => c.CreatedAt).ToList();
            if (TotalCalificaciones.Count > 0)
            {
                return String.Format("{0:0.0}", TotalCalificaciones[0].Puntaje);
            }
            else
            {
                return "0";
            }

        }
        public CargueViewModel InsertWebContenido(DataTable dtWebContenido, int campaña)
        {


            var datosActuales = dtWebContenido;
            List<string[]> asesoresNoCreados = new List<string[]>();
            List<string[]> asesoresNoPertenecen = new List<string[]>();
            List<string[]> asesoresYaActualizados = new List<string[]>();

            foreach (DataRow row in dtWebContenido.Rows)
            {
                if (campaña > 0)
                {
                    var nombreAsesor = row.Field<string>(0);
                    var documentoAsesor = row.Field<string>(1);
                    var ventasAsesor = row.Field<string>(2);
                    var Fecha = row.Field<string>(3);
                    DateTime fechaCreacion = DateTime.Parse(Fecha);
                    var TeamId = SessionPersister.Id;
                    var usuario = db.Usuario.Where(u => u.Document == documentoAsesor).FirstOrDefault();
                    if (usuario == null)
                    {
                        string[] user = { documentoAsesor, nombreAsesor };
                        asesoresNoCreados.Add(user);
                    }
                    else if (usuario.UserId != TeamId && usuario != null)
                    {
                        string[] user = { documentoAsesor, nombreAsesor };
                        asesoresNoPertenecen.Add(user);
                    }
                    else if (usuario != null && usuario.UserId == TeamId)
                    {
                        var promedioAsesor = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == documentoAsesor).OrderByDescending(c => c.CreatedAt).FirstOrDefault();

                        if (promedioAsesor == null)
                        {
                            PromedioAsesor promAsesor = new PromedioAsesor();
                            promAsesor.CampañaAsesor = campaña;
                            promAsesor.DocumentoAsesor = documentoAsesor;
                            promAsesor.FechaCreacion = fechaCreacion;
                            promAsesor.NombreAsesor = nombreAsesor;
                            promAsesor.VentasAsesor = ventasAsesor;
                            db.PromedioAsesores.Add(promAsesor);
                            db.SaveChanges();
                        }
                        else
                        {
                            if ((promedioAsesor.CreatedAt >= DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")) && promedioAsesor.CreatedAt <= DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"))) || (promedioAsesor.UpdatedAt >= DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd")) && promedioAsesor.UpdatedAt <= DateTime.Parse(DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"))))
                            {
                                string[] user = { documentoAsesor, nombreAsesor };
                                asesoresYaActualizados.Add(user);
                                continue;
                            }
                            else
                            {
                                PromedioAsesor promAsesor = new PromedioAsesor();
                                promAsesor.CampañaAsesor = campaña;
                                promAsesor.DocumentoAsesor = documentoAsesor;
                                promAsesor.FechaCreacion = fechaCreacion;
                                promAsesor.NombreAsesor = nombreAsesor;
                                promAsesor.VentasAsesor = ventasAsesor;
                                db.PromedioAsesores.Add(promAsesor);
                                db.SaveChanges();
                            }
                        }
                    }

                }

            }
            CargueViewModel cargueViewModel = new CargueViewModel();
            cargueViewModel.AsesoresNoCreados = asesoresNoCreados;
            cargueViewModel.AsesoresNoPertenecen = asesoresNoPertenecen;
            cargueViewModel.AsesoresYaActualizados = asesoresYaActualizados;
            return cargueViewModel;


            //using (var conn = new SqlConnection("Server=172.16.5.153;Database=BDAuditoria;User ID=usrBDAuditoria;Password=usrBDAuditoria123"))
            //{
            //    //var bulkCopy = new SqlBulkCopy(conn);
            //    //bulkCopy.DestinationTableName = destinationTable;
            //    //conn.Open();
            //    //var schema = conn.GetSchema("Columns", new[] { null, null, destinationTable, null });
            //    //foreach (DataColumn sourceColumn in dtWebContenido.Columns)
            //    //{
            //    //    foreach (DataRow row in schema.Rows)
            //    //    {
            //    //        if (string.Equals(sourceColumn.ColumnName, (string)row["COLUMN_NAME"], StringComparison.OrdinalIgnoreCase))
            //    //        {

            //    //            bulkCopy.ColumnMappings.Add(sourceColumn.ColumnName, (string)row["COLUMN_NAME"]);
            //    //            break;
            //    //        }
            //    //    }
            //    //}

            //    //bulkCopy.WriteToServer(dtWebContenido);
            //    //conn.Close();

            //}
        }
    }
}
