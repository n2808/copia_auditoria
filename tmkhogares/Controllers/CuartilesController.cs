﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.Cuartiles;

namespace tmkhogares.Controllers
{
    public class CuartilesController : Controller
    {
        private Contexto db = new Contexto();

        // GET: Cuartiles
        public ActionResult Index()
        {
            return View(db.Cuartiles.ToList());
        }

        // GET: Cuartiles/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cuartil cuartil = db.Cuartiles.Find(id);
            if (cuartil == null)
            {
                return HttpNotFound();
            }
            return View(cuartil);
        }

        // GET: Cuartiles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cuartiles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,Numero,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Cuartil cuartil)
        {
            if (ModelState.IsValid)
            {
                cuartil.Id = Guid.NewGuid();
                db.Cuartiles.Add(cuartil);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cuartil);
        }

        // GET: Cuartiles/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cuartil cuartil = db.Cuartiles.Find(id);
            if (cuartil == null)
            {
                return HttpNotFound();
            }
            return View(cuartil);
        }

        // POST: Cuartiles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,Numero,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] Cuartil cuartil)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cuartil).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cuartil);
        }

        // GET: Cuartiles/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cuartil cuartil = db.Cuartiles.Find(id);
            if (cuartil == null)
            {
                return HttpNotFound();
            }
            return View(cuartil);
        }

        // POST: Cuartiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Cuartil cuartil = db.Cuartiles.Find(id);
            db.Cuartiles.Remove(cuartil);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
