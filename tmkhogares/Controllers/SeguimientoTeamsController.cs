﻿using Core.Models.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.PlanTrabajoTeam;
using tmkhogares.ViewModel;

namespace tmkhogares.Controllers
{
    public class SeguimientoTeamsController : Controller
    {
        private Contexto db = new Contexto();

        // GET: SeguimientoTeams
        public ActionResult Index(Guid PlanTrabajoTeamId, string Error = "")
        {
            //var seguimientoTeam = db.SeguimientoTeam.Include(s => s.Asesor).Include(s => s.PlanTrabajoTeam);
            //return View(seguimientoTeam.ToList());
            var PlanTrabajo = db.PlanTrabajoTeam.Find(PlanTrabajoTeamId);
            var Team = db.Usuario.Find(PlanTrabajo.TeamId);
            var UsuarioCampañas = db.UsuariosCampanas.Where(uc => uc.UserId == Team.Id).FirstOrDefault();
            var Campaña = db.Campana.Find(UsuarioCampañas.CampanaId);
            var FechaHoy = DateTime.Now;
            var fechaInicioMes = new DateTime(FechaHoy.Year, FechaHoy.Month, 1);
            var fechaFinMes = new DateTime(FechaHoy.Year, FechaHoy.Month, 1).AddMonths(1);
            var promedioAsesor = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Team.Document && (pa.FechaCreacion >= fechaInicioMes && pa.FechaCreacion >= fechaFinMes)).ToList();
            var promedioVentas = promedioAsesor.Sum(pa => int.Parse(pa.VentasAsesor));
            ViewBag.TeamId = Team.Id;
            ViewBag.MetaMensual = Campaña.MetaMensual;
            ViewBag.PromedioAsesor = promedioVentas / 30;
            if (PlanTrabajo == null)
            {
                return HttpNotFound();
            }
            if (PlanTrabajo.SeguimientoSemana1 != null)
            {
                var SeguimientoAsesor1 = db.SeguimientoTeam.Find(PlanTrabajo.SeguimientoSemana1);

                if (SeguimientoAsesor1 != null)
                {
                    DateTime fechaInicioPlanTrabajo = DateTime.Parse(PlanTrabajo.CreatedAt.ToString());
                    DateTime fechaFinSemana1 = fechaInicioPlanTrabajo.AddDays(7.5);
                    var promediosAsesorSemana1 = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Team.Document && (pa.FechaCreacion >= fechaInicioPlanTrabajo && pa.FechaCreacion <= fechaFinSemana1)).ToList();
                    var promedioVentasSemana1 = promediosAsesorSemana1.Sum(pa => int.Parse(pa.VentasAsesor));
                    if (promedioVentasSemana1 > 0)
                    {
                        ViewBag.PromedioVentasSemana1 = promedioVentasSemana1;
                    }
                    else
                    {
                        ViewBag.PromedioVentasSemana1 = 0;
                    }
                    ViewBag.seguimientoAsesor1 = SeguimientoAsesor1;
                    List <UsuarioPromedioViewModel> usuariosPromedios1 = new List<UsuarioPromedioViewModel>();
                    var usuariosTeam1 = db.Usuario.Where(u => u.UserId == Team.Id).ToList();
                    foreach (var usuario in usuariosTeam1)
                    {
                        var promAsesor = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == usuario.Document && (pa.FechaCreacion >= fechaInicioPlanTrabajo && pa.FechaCreacion <= fechaFinSemana1)).ToList();
                        var promVentas = promedioAsesor.Sum(pa => int.Parse(pa.VentasAsesor));
                        User asesor = usuario;
                        UsuarioPromedioViewModel userPromedio = new UsuarioPromedioViewModel();
                        userPromedio.Usuario = asesor;
                        userPromedio.Promedio = promVentas.ToString();
                        usuariosPromedios1.Add(userPromedio);
                    }
                    ViewBag.ListaAsesoresSemana1 = usuariosPromedios1;
                }
            }
            if (PlanTrabajo.SeguimientoSemana2 != null)
            {
                var SeguimientoAsesor2 = db.SeguimientoTeam.Find(PlanTrabajo.SeguimientoSemana2);
                if (SeguimientoAsesor2 != null)
                {
                    DateTime fechaInicioPlanTrabajo = DateTime.Parse(PlanTrabajo.CreatedAt.ToString());
                    DateTime fechaFinSemana1 = fechaInicioPlanTrabajo.AddDays(7.5);
                    DateTime fechaFinSemana2 = fechaInicioPlanTrabajo.AddDays(15);

                    var promediosAsesorSemana2 = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Team.Document && (pa.FechaCreacion >= fechaFinSemana1 && pa.FechaCreacion <= fechaFinSemana2)).ToList();
                    var promedioVentasSemana2 = promediosAsesorSemana2.Sum(pa => int.Parse(pa.VentasAsesor));
                    if (promedioVentasSemana2 > 0)
                    {
                        ViewBag.PromedioVentasSemana2 = promedioVentasSemana2;
                    }
                    else
                    {
                        ViewBag.PromedioVentasSemana2 = 0;

                    }
                    ViewBag.seguimientoAsesor2 = SeguimientoAsesor2;
                    List<UsuarioPromedioViewModel> usuariosPromedios2 = new List<UsuarioPromedioViewModel>();
                    var usuariosTeam2 = db.Usuario.Where(u => u.UserId == Team.Id).ToList();
                    foreach (var usuario in usuariosTeam2)
                    {
                        var promAsesor = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == usuario.Document && (pa.FechaCreacion >= fechaFinSemana1 && pa.FechaCreacion <= fechaFinSemana2)).ToList();
                        var promVentas = promedioAsesor.Sum(pa => int.Parse(pa.VentasAsesor));
                        User asesor = usuario;
                        UsuarioPromedioViewModel userPromedio = new UsuarioPromedioViewModel();
                        userPromedio.Usuario = asesor;
                        userPromedio.Promedio = promVentas.ToString();
                        usuariosPromedios2.Add(userPromedio);

                    }
                    ViewBag.ListaAsesoresSemana2 = usuariosPromedios2;
                }
            }
            if (PlanTrabajo.SeguimientoSemana3 != null)
            {
                var SeguimientoAsesor3 = db.SeguimientoTeam.Find(PlanTrabajo.SeguimientoSemana3);
                if (SeguimientoAsesor3 != null)
                {
                    DateTime fechaInicioPlanTrabajo = DateTime.Parse(PlanTrabajo.CreatedAt.ToString());
                    DateTime fechaFinSemana2 = fechaInicioPlanTrabajo.AddDays(15);
                    DateTime fechaFinSemana3 = fechaInicioPlanTrabajo.AddDays(22.5);
                    var promediosAsesorSemana3 = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Team.Document && (pa.FechaCreacion >= fechaFinSemana2 && pa.FechaCreacion <= fechaFinSemana3)).ToList();
                    var promedioVentasSemana3 = promediosAsesorSemana3.Sum(pa => int.Parse(pa.VentasAsesor));
                    if (promedioVentasSemana3 > 0)
                    {
                        ViewBag.PromedioVentasSemana3 = promedioVentasSemana3;
                    }
                    else
                    {
                        ViewBag.PromedioVentasSemana3 = 0;

                    }
                    ViewBag.seguimientoAsesor3 = SeguimientoAsesor3;
                    List<UsuarioPromedioViewModel> usuariosPromedios3 = new List<UsuarioPromedioViewModel>();
                    var usuariosTeam3 = db.Usuario.Where(u => u.UserId == Team.Id).ToList();
                    foreach (var usuario in usuariosTeam3)
                    {
                        var promAsesor = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == usuario.Document && (pa.FechaCreacion >= fechaFinSemana2 && pa.FechaCreacion <= fechaFinSemana3)).ToList();
                        var promVentas = promedioAsesor.Sum(pa => int.Parse(pa.VentasAsesor));
                        User asesor = usuario;
                        UsuarioPromedioViewModel userPromedio = new UsuarioPromedioViewModel();
                        userPromedio.Usuario = asesor;
                        userPromedio.Promedio = promVentas.ToString();
                        usuariosPromedios3.Add(userPromedio);

                    }
                    ViewBag.ListaAsesoresSemana3 = usuariosPromedios3;
                }
            }
            if (PlanTrabajo.SeguimientoSemana4 != null)
            {
                var SeguimientoAsesor4 = db.SeguimientoTeam.Find(PlanTrabajo.SeguimientoSemana4);
                if (SeguimientoAsesor4 != null)
                {
                    DateTime fechaInicioPlanTrabajo = DateTime.Parse(PlanTrabajo.CreatedAt.ToString());
                    DateTime fechaFinSemana3 = fechaInicioPlanTrabajo.AddDays(22.5);
                    DateTime fechaFinSemana4 = fechaInicioPlanTrabajo.AddDays(30);
                    var promediosAsesorSemana4 = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Team.Document && (pa.FechaCreacion >= fechaFinSemana3 && pa.FechaCreacion <= fechaFinSemana4)).ToList();
                    var promedioVentasSemana4 = promediosAsesorSemana4.Sum(pa => int.Parse(pa.VentasAsesor));
                    if (promedioVentasSemana4 > 0)
                    {
                        ViewBag.PromedioVentasSemana4 = promedioVentasSemana4;
                    }
                    else
                    {
                        ViewBag.PromedioVentasSemana4 = promedioVentasSemana4;

                    }
                    ViewBag.seguimientoAsesor4 = SeguimientoAsesor4;
                    List<UsuarioPromedioViewModel> usuariosPromedios4 = new List<UsuarioPromedioViewModel>();
                    var usuariosTeam4 = db.Usuario.Where(u => u.UserId == Team.Id).ToList();
                    foreach (var usuario in usuariosTeam4)
                    {
                        var promAsesor = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == usuario.Document && (pa.FechaCreacion >= fechaFinSemana3 && pa.FechaCreacion <= fechaFinSemana4)).ToList();
                        var promVentas = promedioAsesor.Sum(pa => int.Parse(pa.VentasAsesor));
                        User asesor = usuario;
                        UsuarioPromedioViewModel userPromedio = new UsuarioPromedioViewModel();
                        userPromedio.Usuario = asesor;
                        userPromedio.Promedio = promVentas.ToString();
                        usuariosPromedios4.Add(userPromedio);

                    }
                    ViewBag.ListaAsesoresSemana4 = usuariosPromedios4;
                }
            }
            if (Error != "")
            {
                ViewBag.Error = Error;
            }
            ViewBag.AsesoresTeam = db.Usuario.Where(u => u.UserId == Team.Id).ToList();
            ViewBag.PlanTrabajo = PlanTrabajo;
            return View();
        }
        [HttpPost]
        public ActionResult CrearSeguimiento(Guid PlanTrabajoTeamId)
        {
            var PlanTeam = db.PlanTrabajoTeam.Find(PlanTrabajoTeamId);
            if (PlanTeam == null)
            {
                return HttpNotFound();
            }
            var seguimientosTeam = db.SeguimientoTeam.Where(sa => sa.PlanTrabajoTeamId == PlanTeam.Id && sa.Estado == false).ToList();
            if (seguimientosTeam.Count > 0)
            {
                //return View("index",new { PlanTrabajoAsesorId = PlanTrabajoAsesorId });
                return RedirectToAction("index", new { PlanTrabajoAsesorId = PlanTeam.Id, Error = "Actualmente hay un seguimiento activo por favor finalicelo" });
            }
            SeguimientoTeam seguimientoTeam = new SeguimientoTeam();
            seguimientoTeam.Id = Guid.NewGuid();
            seguimientoTeam.PlanTrabajoTeamId = PlanTeam.Id;
            seguimientoTeam.Estado = false;
            seguimientoTeam.TeamId = PlanTeam.TeamId;
            db.SeguimientoTeam.Add(seguimientoTeam);
            db.SaveChanges();
            if (PlanTeam.SeguimientoSemana1 == null)
            {
                PlanTeam.SeguimientoSemana1 = seguimientoTeam.Id;

            }
            else if (PlanTeam.SeguimientoSemana2 == null)
            {
                PlanTeam.SeguimientoSemana2 = seguimientoTeam.Id;

            }
            else if (PlanTeam.SeguimientoSemana3 == null)
            {
                PlanTeam.SeguimientoSemana3 = seguimientoTeam.Id;

            }
            else if (PlanTeam.SeguimientoSemana4 == null)
            {
                PlanTeam.SeguimientoSemana4 = seguimientoTeam.Id;
            }
            db.Entry(PlanTeam).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", new { PlanTrabajoTeamId = seguimientoTeam.PlanTrabajoTeamId });
        }

        // GET: SeguimientoTeams/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeguimientoTeam seguimientoTeam = db.SeguimientoTeam.Find(id);
            if (seguimientoTeam == null)
            {
                return HttpNotFound();
            }
            return View(seguimientoTeam);
        }

        // GET: SeguimientoTeams/Create
        public ActionResult Create()
        {
            ViewBag.AsesorId = new SelectList(db.Usuario, "Id", "Document");
            ViewBag.PlanTrabajoTeamId = new SelectList(db.PlanTrabajoTeam, "Id", "ObservacionesARealizar");
            return View();
        }

        // POST: SeguimientoTeams/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,MetaPromedio,PromedioSemana1,PromedioSemana2,PromedioSemana3,PromedioSemana4,DiasAntiguo,PorcentajeCumplimiento,PorcentajeAusentismo,CantidadVentasMeta,CantidadVentasRealizadas,Evolucion,NotaCalidad,RetroAlimentacion,Compromiso,Estado,AsesorId,PlanTrabajoTeamId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] SeguimientoTeam seguimientoTeam)
        {
            if (ModelState.IsValid)
            {
                seguimientoTeam.Id = Guid.NewGuid();
                db.SeguimientoTeam.Add(seguimientoTeam);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TeamId = new SelectList(db.Usuario, "Id", "Document", seguimientoTeam.TeamId);
            ViewBag.PlanTrabajoTeamId = new SelectList(db.PlanTrabajoTeam, "Id", "ObservacionesARealizar", seguimientoTeam.PlanTrabajoTeamId);
            return View(seguimientoTeam);
        }

        // GET: SeguimientoTeams/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeguimientoTeam seguimientoTeam = db.SeguimientoTeam.Find(id);
            if (seguimientoTeam == null)
            {
                return HttpNotFound();
            }
            ViewBag.TeamId = new SelectList(db.Usuario, "Id", "Document", seguimientoTeam.TeamId);
            ViewBag.PlanTrabajoTeamId = new SelectList(db.PlanTrabajoTeam, "Id", "ObservacionesARealizar", seguimientoTeam.PlanTrabajoTeamId);
            return View(seguimientoTeam);
        }

        // POST: SeguimientoTeams/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,PromedioInicio,PromedioFinalizo,MetaPromedio,PromedioSemana1,PromedioSemana2,PromedioSemana3,PromedioSemana4,DiasAntiguo,PorcentajeCumplimiento,PorcentajeAusentismo,Evolucion,NotaCalidad,RetroAlimentacion,Compromiso,Estado,TeamId,PlanTrabajoTeamId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] SeguimientoTeam seguimientoTeam)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    seguimientoTeam.Estado = true;
                    db.Entry(seguimientoTeam).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index",new { PlanTrabajoTeamId = seguimientoTeam.PlanTrabajoTeamId });
                }
                catch (Exception e)
                {

                    throw;
                }
                
            }
            ViewBag.TeamId = new SelectList(db.Usuario, "Id", "Document", seguimientoTeam.TeamId);
            ViewBag.PlanTrabajoTeamId = new SelectList(db.PlanTrabajoTeam, "Id", "ObservacionesARealizar", seguimientoTeam.PlanTrabajoTeamId);
            return View(seguimientoTeam);
        }

        // GET: SeguimientoTeams/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeguimientoTeam seguimientoTeam = db.SeguimientoTeam.Find(id);
            if (seguimientoTeam == null)
            {
                return HttpNotFound();
            }
            return View(seguimientoTeam);
        }

        // POST: SeguimientoTeams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            SeguimientoTeam seguimientoTeam = db.SeguimientoTeam.Find(id);
            db.SeguimientoTeam.Remove(seguimientoTeam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
