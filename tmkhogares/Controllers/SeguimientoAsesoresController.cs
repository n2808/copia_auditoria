﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.PlanTrabajoAsesor;

namespace tmkhogares.Controllers
{
    public class SeguimientoAsesoresController : Controller
    {
        private Contexto db = new Contexto();

        // GET: SeguimientoAsesores
        public ActionResult Index(Guid PlanTrabajoAsesorId,string Error = "")
        {
            
            var PlanTrabajo         = db.PlanTrabajoAsesor.Find(PlanTrabajoAsesorId);
            var Asesor              = db.Usuario.Find(PlanTrabajo.AsesorId);
            var UsuarioCampañas     = db.UsuariosCampanas.Where(uc => uc.UserId == Asesor.Id).FirstOrDefault();
            var Campaña             = db.Campana.Find(UsuarioCampañas.CampanaId);
            var FechaHoy            = DateTime.Now;
            var fechaInicioMes      = new DateTime(FechaHoy.Year,FechaHoy.Month,1);
            var fechaFinMes         = new DateTime(FechaHoy.Year, FechaHoy.Month, 1).AddMonths(1);
            var promedioAsesor      = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Asesor.Document && (pa.FechaCreacion >= fechaInicioMes && pa.FechaCreacion >= fechaFinMes)).ToList();
            var promedioVentas      = promedioAsesor.Sum(pa => int.Parse(pa.VentasAsesor));
            ViewBag.MetaMensual     = Campaña.MetaMensual;
            ViewBag.PromedioAsesor  = promedioVentas / 30;
            if(PlanTrabajo == null)
            {
                return HttpNotFound();
            }
            if (PlanTrabajo.SeguimientoSemana1 != null) {
                var SeguimientoAsesor1 = db.SeguimientoAsesor.Find(PlanTrabajo.SeguimientoSemana1);

                if(SeguimientoAsesor1 != null)
                {
                    DateTime fechaInicioPlanTrabajo = DateTime.Parse(PlanTrabajo.CreatedAt.ToString());
                    DateTime fechaFinSemana1 = fechaInicioPlanTrabajo.AddDays(7.5);
                    var promediosAsesorSemana1 = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Asesor.Document && (pa.FechaCreacion >= fechaInicioPlanTrabajo && pa.FechaCreacion >= fechaFinSemana1)).ToList();
                    var promedioVentasSemana1 = promediosAsesorSemana1.Sum(pa => int.Parse(pa.VentasAsesor));
                    if (promedioVentasSemana1 > 0)
                    {
                        ViewBag.PromedioVentasSemana1 = promedioVentasSemana1;
                    }
                    else
                    {
                        ViewBag.PromedioVentasSemana1 = 0;
                    }
                    ViewBag.seguimientoAsesor1 = SeguimientoAsesor1;
                }
            }
            if (PlanTrabajo.SeguimientoSemana2 != null)
            {
                var SeguimientoAsesor2 = db.SeguimientoAsesor.Find(PlanTrabajo.SeguimientoSemana2);
                if (SeguimientoAsesor2 != null)
                {
                    DateTime fechaInicioPlanTrabajo = DateTime.Parse(PlanTrabajo.CreatedAt.ToString());
                    DateTime fechaFinSemana1 = fechaInicioPlanTrabajo.AddDays(7.5);
                    DateTime fechaFinSemana2 = fechaInicioPlanTrabajo.AddDays(15);

                    var promediosAsesorSemana2 = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Asesor.Document && (pa.FechaCreacion >= fechaFinSemana1 && pa.FechaCreacion >= fechaFinSemana2)).ToList();
                    var promedioVentasSemana2 = promediosAsesorSemana2.Sum(pa => int.Parse(pa.VentasAsesor));
                    if(promedioVentasSemana2 > 0)
                    {
                        ViewBag.PromedioVentasSemana2 = promedioVentasSemana2;
                    }
                    else
                    {
                        ViewBag.PromedioVentasSemana2 = 0;

                    }
                    ViewBag.seguimientoAsesor2 = SeguimientoAsesor2;
                }
            }
            if (PlanTrabajo.SeguimientoSemana3 != null)
            {
                var SeguimientoAsesor3 = db.SeguimientoAsesor.Find(PlanTrabajo.SeguimientoSemana3);
                if (SeguimientoAsesor3 != null)
                {
                    DateTime fechaInicioPlanTrabajo = DateTime.Parse(PlanTrabajo.CreatedAt.ToString());
                    DateTime fechaFinSemana2 = fechaInicioPlanTrabajo.AddDays(15);
                    DateTime fechaFinSemana3 = fechaInicioPlanTrabajo.AddDays(22.5);
                    var promediosAsesorSemana3 = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Asesor.Document && (pa.FechaCreacion >= fechaFinSemana2 && pa.FechaCreacion >= fechaFinSemana3)).ToList();
                    var promedioVentasSemana3 = promediosAsesorSemana3.Sum(pa => int.Parse(pa.VentasAsesor));
                    if (promedioVentasSemana3 > 0)
                    {
                        ViewBag.PromedioVentasSemana3 = promedioVentasSemana3;
                    }
                    else {
                        ViewBag.PromedioVentasSemana3 = 0;

                    }
                    ViewBag.seguimientoAsesor3 = SeguimientoAsesor3;
                }
            }
            if (PlanTrabajo.SeguimientoSemana4 != null)
            {
                var SeguimientoAsesor4 = db.SeguimientoAsesor.Find(PlanTrabajo.SeguimientoSemana4);
                if (SeguimientoAsesor4 != null)
                {
                    DateTime fechaInicioPlanTrabajo = DateTime.Parse(PlanTrabajo.CreatedAt.ToString());
                    DateTime fechaFinSemana3 = fechaInicioPlanTrabajo.AddDays(22.5);
                    DateTime fechaFinSemana4 = fechaInicioPlanTrabajo.AddDays(30);
                    var promediosAsesorSemana4 = db.PromedioAsesores.Where(pa => pa.DocumentoAsesor == Asesor.Document && (pa.FechaCreacion >= fechaFinSemana3 && pa.FechaCreacion >= fechaFinSemana4)).ToList();
                    var promedioVentasSemana4 = promediosAsesorSemana4.Sum(pa => int.Parse(pa.VentasAsesor));
                    if (promedioVentasSemana4 > 0)
                    {
                        ViewBag.PromedioVentasSemana4 = promedioVentasSemana4;
                    }
                    else
                    {
                        ViewBag.PromedioVentasSemana4 = promedioVentasSemana4;

                    }
                    ViewBag.seguimientoAsesor4 = SeguimientoAsesor4;
                }
            }
            if(Error != "")
            {
                ViewBag.Error = Error;
            }
            ViewBag.PlanTrabajo = PlanTrabajo;
            return View();
        }

        // GET: SeguimientoAsesores/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeguimientoAsesor seguimientoAsesor = db.SeguimientoAsesor.Find(id);
            if (seguimientoAsesor == null)
            {
                return HttpNotFound();
            }
            return View(seguimientoAsesor);
        }

        // GET: SeguimientoAsesores/Create
        public ActionResult Create(Guid PlanTrabajoAsesorId)
        {
            var PlanTrabajo = db.PlanTrabajoAsesor.Find(PlanTrabajoAsesorId);
            if(PlanTrabajo == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlanTrabajoId = PlanTrabajo.Id;
            ViewBag.PlanTrabajoAsesorId = new SelectList(db.PlanTrabajoAsesor, "Id", "MetaPromedioDia");
            return View();
        }

        // POST: SeguimientoAsesores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult CrearSeguimiento(Guid PlanTrabajoAsesorId)
        {
            var PlanAsesor = db.PlanTrabajoAsesor.Find(PlanTrabajoAsesorId);
            if (PlanAsesor == null)
            {
                return HttpNotFound();
            }
            var seguimientosAsesor = db.SeguimientoAsesor.Where(sa => sa.PlanTrabajoAsesorId == PlanAsesor.Id && sa.Estado == false).ToList();
            if (seguimientosAsesor.Count > 0)
            {
                //return View("index",new { PlanTrabajoAsesorId = PlanTrabajoAsesorId });
                return RedirectToAction("index", new { PlanTrabajoAsesorId = PlanAsesor.Id ,Error = "Actualmente hay un seguimiento activo por favor finalicelo" });
            }
            SeguimientoAsesor seguimientoAsesor = new SeguimientoAsesor();
            seguimientoAsesor.Id = Guid.NewGuid();
            seguimientoAsesor.PlanTrabajoAsesorId = PlanAsesor.Id;
            seguimientoAsesor.Estado = false;
            db.SeguimientoAsesor.Add(seguimientoAsesor);
            db.SaveChanges();
            if (PlanAsesor.SeguimientoSemana1 == null)
            {
                PlanAsesor.SeguimientoSemana1 = seguimientoAsesor.Id;

            }
            else if (PlanAsesor.SeguimientoSemana2 == null)
            {
                PlanAsesor.SeguimientoSemana2 = seguimientoAsesor.Id;

            }
            else if (PlanAsesor.SeguimientoSemana3 == null)
            {
                PlanAsesor.SeguimientoSemana3 = seguimientoAsesor.Id;

            }
            else if (PlanAsesor.SeguimientoSemana4 == null)
            {
                PlanAsesor.SeguimientoSemana4 = seguimientoAsesor.Id;
            }
            db.Entry(PlanAsesor).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index",new { PlanTrabajoAsesorId=seguimientoAsesor.PlanTrabajoAsesorId });
        }

        // GET: SeguimientoAsesores/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeguimientoAsesor seguimientoAsesor = db.SeguimientoAsesor.Find(id);
            if (seguimientoAsesor == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlanTrabajoAsesorId = new SelectList(db.PlanTrabajoAsesor, "Id", "MetaPromedioDia", seguimientoAsesor.PlanTrabajoAsesorId);
            return View(seguimientoAsesor);
        }

        // POST: SeguimientoAsesores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,MetaPromedio,PromedioInicio,PromedioFinalizo,CantidadVentasMeta,CantidadVentasRealizadas,PorcentajeCumplimiento,PorcentajeAusentismo,Evolucion,NotaCalidad,ComentariosSeguimientoTeam,ComentariosSeguimientoAsesor,SeguimientoDia1,SeguimientoDia2,SeguimientoDia3,SeguimientoDia4,SeguimientoDia5,SeguimientoDia6,SeguimientoDia7,PlanTrabajoAsesorId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] SeguimientoAsesor seguimientoAsesor)
        {
            if (ModelState.IsValid)
            {
                seguimientoAsesor.Estado = true;
                db.Entry(seguimientoAsesor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index",new { PlanTrabajoAsesorId = seguimientoAsesor.PlanTrabajoAsesorId});
            }
            return RedirectToAction("Index");
        }

        public ActionResult ActualizarSeguimiento([Bind(Include = "Id,MetaPromedio,PromedioInicio,PromedioFinalizo,CantidadVentasMeta,CantidadVentasRealizadas,PorcentajeCumplimiento,PorcentajeAusentismo,Evolucion,NotaCalidad,ComentariosSeguimientoTeam,ComentariosSeguimientoAsesor,SeguimientoDia1,SeguimientoDia2,SeguimientoDia3,SeguimientoDia4,SeguimientoDia5,SeguimientoDia6,SeguimientoDia7,PlanTrabajoAsesorId,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] SeguimientoAsesor seguimientoAsesor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seguimientoAsesor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { PlanTrabajoAsesorId = seguimientoAsesor.PlanTrabajoAsesorId });
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult ActualizarCompromisos(Guid Id,string CompromisoG = "",string Compromiso1 = "", string Compromiso2 = "", string Compromiso3 = "", string Compromiso4 = "")
        {

            var PlanTrabajo = db.PlanTrabajoAsesor.Find(Id);
            if(PlanTrabajo == null)
            {
                return HttpNotFound();
            }else if(PlanTrabajo != null && CompromisoG != ""){
                PlanTrabajo.CompromisoAsesor = CompromisoG;
                db.Entry(PlanTrabajo).State = EntityState.Modified;
                db.SaveChanges();
            }
            
            var seguimiento1 = db.SeguimientoAsesor.Find(PlanTrabajo.SeguimientoSemana1);
            if(seguimiento1 != null && Compromiso1 != "")
            {
                seguimiento1.ComentariosSeguimientoAsesor = Compromiso1;
                db.Entry(seguimiento1).State = EntityState.Modified;
                db.SaveChanges();
            }
            var seguimiento2 = db.SeguimientoAsesor.Find(PlanTrabajo.SeguimientoSemana2);
            if (seguimiento2 != null && Compromiso2 != "")
            {
                seguimiento2.ComentariosSeguimientoAsesor = Compromiso2;
                db.Entry(seguimiento2).State = EntityState.Modified;
                db.SaveChanges();
            }
            var seguimiento3 = db.SeguimientoAsesor.Find(PlanTrabajo.SeguimientoSemana3);
            if (seguimiento3 != null && Compromiso3 != "")
            {
                seguimiento3.ComentariosSeguimientoAsesor = Compromiso3;
                db.Entry(seguimiento3).State = EntityState.Modified;
                db.SaveChanges();
            }
            var seguimiento4 = db.SeguimientoAsesor.Find(PlanTrabajo.SeguimientoSemana4);
            if (seguimiento4 != null && Compromiso4 != "")
            {
                seguimiento4.ComentariosSeguimientoAsesor = Compromiso4;
                db.Entry(seguimiento4).State = EntityState.Modified;
                db.SaveChanges();
            }

            
            return RedirectToAction("Index", new { PlanTrabajoAsesorId = PlanTrabajo.Id});
        }

        // GET: SeguimientoAsesores/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeguimientoAsesor seguimientoAsesor = db.SeguimientoAsesor.Find(id);
            if (seguimientoAsesor == null)
            {
                return HttpNotFound();
            }
            return View(seguimientoAsesor);
        }

        // POST: SeguimientoAsesores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            SeguimientoAsesor seguimientoAsesor = db.SeguimientoAsesor.Find(id);
            db.SeguimientoAsesor.Remove(seguimientoAsesor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
