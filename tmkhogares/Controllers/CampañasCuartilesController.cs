﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.CampañasCuartiles;

namespace tmkhogares.Controllers
{
    public class CampañasCuartilesController : Controller
    {
        private Contexto db = new Contexto();

        // GET: CampañasCuartiles
        public ActionResult Index()
        {
            var camapanasCuartiles = db.CamapanasCuartiles.Include(c => c.Campana).Include(c => c.Cuartil).Include(c => c.TipoCuartiles);
            return View(camapanasCuartiles.ToList());
        }

        // GET: CampañasCuartiles/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampañasCuartiles campañasCuartiles = db.CamapanasCuartiles.Find(id);
            if (campañasCuartiles == null)
            {
                return HttpNotFound();
            }
            return View(campañasCuartiles);
        }

        // GET: CampañasCuartiles/Create
        public ActionResult Create()
        {
            ViewBag.CampanaId = new SelectList(db.Campana, "Id", "Nombre");
            ViewBag.CuartilId = new SelectList(db.Cuartiles, "Id", "Nombre");
            ViewBag.TipoCuartilId = new SelectList(db.TipoCuartiles, "Id", "Nombre");
            return View();
        }

        // POST: CampañasCuartiles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CampanaId,CuartilId,TipoCuartilId,promedio,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] CampañasCuartiles campañasCuartiles)
        {
            if (ModelState.IsValid)
            {
                campañasCuartiles.Id = Guid.NewGuid();
                db.CamapanasCuartiles.Add(campañasCuartiles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CampanaId = new SelectList(db.Campana, "Id", "Nombre", campañasCuartiles.CampanaId);
            ViewBag.CuartilId = new SelectList(db.Cuartiles, "Id", "Nombre", campañasCuartiles.CuartilId);
            ViewBag.TipoCuartilId = new SelectList(db.TipoCuartiles, "Id", "Nombre", campañasCuartiles.TipoCuartilId);
            return View(campañasCuartiles);
        }

        // GET: CampañasCuartiles/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampañasCuartiles campañasCuartiles = db.CamapanasCuartiles.Find(id);
            if (campañasCuartiles == null)
            {
                return HttpNotFound();
            }
            ViewBag.CampanaId = new SelectList(db.Campana, "Id", "Nombre", campañasCuartiles.CampanaId);
            ViewBag.CuartilId = new SelectList(db.Cuartiles, "Id", "Nombre", campañasCuartiles.CuartilId);
            ViewBag.TipoCuartilId = new SelectList(db.TipoCuartiles, "Id", "Nombre", campañasCuartiles.TipoCuartilId);
            return View(campañasCuartiles);
        }

        // POST: CampañasCuartiles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CampanaId,CuartilId,TipoCuartilId,promedio,CreatedAt,UpdatedAt,DeletedAt,CreatedBy,UpdatedBy")] CampañasCuartiles campañasCuartiles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(campañasCuartiles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CampanaId = new SelectList(db.Campana, "Id", "Nombre", campañasCuartiles.CampanaId);
            ViewBag.CuartilId = new SelectList(db.Cuartiles, "Id", "Nombre", campañasCuartiles.CuartilId);
            ViewBag.TipoCuartilId = new SelectList(db.TipoCuartiles, "Id", "Nombre", campañasCuartiles.TipoCuartilId);
            return View(campañasCuartiles);
        }

        // GET: CampañasCuartiles/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CampañasCuartiles campañasCuartiles = db.CamapanasCuartiles.Find(id);
            if (campañasCuartiles == null)
            {
                return HttpNotFound();
            }
            return View(campañasCuartiles);
        }

        // POST: CampañasCuartiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            CampañasCuartiles campañasCuartiles = db.CamapanasCuartiles.Find(id);
            db.CamapanasCuartiles.Remove(campañasCuartiles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
