﻿using Core.Models.Security;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using tmkhogares.Models;
using tmkhogares.Models.Campañas;

namespace tmkhogares.Controllers
{
    [CustomAuthorize(Roles = "Administrador,Calidad,Asesor,TeamLeader,Gerente,Reporting")]

    public class HomeController : Controller
    {
        private Contexto db = new Contexto();
        //private EstadosGestion estadosGestion = new EstadosGestion();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Mantenimiento()
        {
            return View();
        }

        public ActionResult HomeAdmin()
        {

            var usuario = db.Usuario.Find(SessionPersister.Id.Value);
            var verTodos = 0;
            var campanaUsuarios = db.UsuariosCampanas.Where(uc => uc.UserId == usuario.Id).ToList();
            TempData["Mensaje"] = TempData["Mensaje"];

            if (campanaUsuarios.Count > 0)
            {

                if (SessionPersister.HasRol("Reporting") || SessionPersister.HasRol("Calidad") || SessionPersister.HasRol("Administrador"))
                {
                    foreach (var item in campanaUsuarios)
                    {
                        if (   item.CampanaId == Constantes.Campanas.CELULA_DE_VENTAS
                            || item.CampanaId == Constantes.Campanas.CLARO_COLOMBIA_TMK
                            || item.CampanaId == Constantes.Campanas.CLARO_TYT
                            || item.CampanaId == Constantes.Campanas.CLARO_COLOMBIA_HOGARES_DIGITAL
                            || item.CampanaId == Constantes.Campanas.CLARO_COLOMBIA_HOGARES_INBOUND
                            || item.CampanaId == Constantes.Campanas.CLARO_COLOMBIA_HOGARES_OUTBOUND
                            || item.CampanaId == Constantes.Campanas.CLARO_COLOMBIA_MIGRACIONES
                            || item.CampanaId == Constantes.Campanas.CLARO_COLOMBIA_PERSONAS_INBOUND
                            || item.CampanaId == Constantes.Campanas.CLARO_COLOMBIA_PERSONAS_OUTBOUND
                            || item.CampanaId == Constantes.Campanas.CLARO_COLOMBIA_PORTA_POS
                            || item.CampanaId == Constantes.Campanas.CLARO_DIGITAL_PERSONAS
                            )
                        {
                            verTodos = 1;
                        }

                        if ( item.CampanaId == Constantes.Campanas.SEGUNDO_ANILLO
                          || item.CampanaId == Constantes.Campanas.TERCER_ANILLO_HOGAR
                          || item.CampanaId == Constantes.Campanas.TERCER_ANILLO_MOVIL
                          || item.CampanaId == Constantes.Campanas.TRAFICO_INBOUND
                          )
                        {
                            verTodos = 2;
                        }
                    }
                }
            }

            ViewBag.Vertodos = verTodos;

            return View();
        }

        public ActionResult ReportesRetencion()
        {
            return View();
        }

        private void DLog(string msg = "")
        {
            Debug.WriteLine("-------------------------------------------------------\n");
            Debug.WriteLine(msg + "\n");
            Debug.WriteLine("-------------------------------------------------------\n");
        }
    }
}