﻿//using OfficeOpenXml;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using tmkhogares.BusinessLogic.validaciones;
using tmkhogares.Constantes;
using tmkhogares.Models;
using tmkhogares.ViewModel;

namespace tmkhogares.BusinessLogic
{
    public class CargueUsuariosAplicativosBS
    {

        private Contexto db = new Contexto();

        public bool InsertarUsuariosAplicativos(string path, int? idCampana)
        {

            try
            {
                LimpiarTablaTemporalBS.TruncarTablaTemporal("TemporalCargueUsuarios");

                DataTable dtUsuarios = CargarExcel(path);

                BulkAsignacionAplicativosUsuario(dtUsuarios, idCampana);

                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public bool EditarTeamLeader(string path)
        {

            try
            {
                LimpiarTablaTemporalBS.TruncarTablaTemporal("TemporalEditarTeamLeaderMasivo");

                DataTable dtUsuarios = CargarExcel(path);

                BulkEditarTeamLeaderUsuario(dtUsuarios);

                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public DataTable CargarExcel(string cadenaConexion)
        {

            try
            {
                var strconn = ("Provider=Microsoft.ACE.OLEDB.12.0;" +
                ("Data Source=" + (cadenaConexion + ";Extended Properties=\"Excel 12.0;HDR=YES\"")));

                DataTable dataTable = new DataTable();


                OleDbConnection mconn = new OleDbConnection(strconn);
                mconn.Open();
                DataTable dtSchema = mconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                if ((null != dtSchema) && (dtSchema.Rows.Count > 0))
                {
                    string firstSheetName1 = dtSchema.Rows[0]["TABLE_NAME"].ToString();
                    string firstSheetName = "Cargue$";
                    new OleDbDataAdapter("SELECT * FROM [" + firstSheetName.Trim() + "]", mconn).Fill(dataTable);
                }
                mconn.Close();

                return dataTable;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }


        public void BulkAsignacionAplicativosUsuario(DataTable dtUsuarios, int? idCampana)
        {


            List<ErroresFormatoVM> errores = new List<ErroresFormatoVM>();

            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(db.Database.Connection.ConnectionString))
            {
                bulkcopy.DestinationTableName = "TemporalCargueUsuarios";
                bulkcopy.ColumnMappings.Add("Documento_Usuario", "Documento_Usuario");
                bulkcopy.ColumnMappings.Add("Nombre_Usuario", "Nombre_Usuario");
                bulkcopy.ColumnMappings.Add("Documento_TeamLeader", "Documento_TeamLeader");

                try
                {
                    bulkcopy.WriteToServer(dtUsuarios);
                    errores = ValidarCamposVaciosBS.ValidarCamposVaciosCargueUsuarios("P_ValidarCamposVaciosCargueUsuarios");


                    if (errores.Count > 0)
                    {
                        ExportExcel(errores);
                        throw new Exception($"{ Configuraciones.MENSAJE_ERROR_FORMATO_ASIGNACION }");

                    }
                    else
                    {
                        InsertarAplicativosUsuario(idCampana);
                    }


                }
                catch (Exception ex)
                {
                    if (ex.HResult == -2146233079)
                    {
                        throw new Exception($"{ Configuraciones.MENSAJE_ERROR_COLUMNAS }, { ex.Message }");

                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }

                }
            }
        }

        public void BulkEditarTeamLeaderUsuario(DataTable dtUsuarios)
        {


            List<ErroresFormatoVM> errores = new List<ErroresFormatoVM>();

            using (SqlBulkCopy bulkcopy = new SqlBulkCopy(db.Database.Connection.ConnectionString))
            {
                bulkcopy.DestinationTableName = "TemporalEditarTeamLeaderMasivo";
                bulkcopy.ColumnMappings.Add("Documento_Asesor", "Documento_Asesor");
                bulkcopy.ColumnMappings.Add("Documento_TeamLeader", "Documento_TeamLeader");

                try
                {
                    bulkcopy.WriteToServer(dtUsuarios);
                    errores = ValidarCamposVaciosBS.ValidarCamposVaciosCargueUsuarios("P_ValidarCamposVaciosEditarTeamLeaderMasivo");


                    if (errores.Count > 0)
                    {
                        ExportExcel(errores);
                        throw new Exception($"{ Configuraciones.MENSAJE_ERROR_FORMATO_ASIGNACION }");

                    }
                    else
                    {
                        EditarTeamLeaderUsuario();
                    }


                }
                catch (Exception ex)
                {
                    if (ex.HResult == -2146233079)
                    {
                        throw new Exception($"{ Configuraciones.MENSAJE_ERROR_COLUMNAS }, { ex.Message }");

                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }

                }
            }
        }
        public void InsertarAplicativosUsuario(int? idCampana)
        {
            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("SP_CrearActualizarUsuarios", connection);
                comandoInsert.Parameters.Add("@CAMPANA", SqlDbType.Int);
                comandoInsert.Parameters["@CAMPANA"].Value = idCampana;

                comandoInsert.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                //comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

        public void EditarTeamLeaderUsuario()
        {
            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("SP_EditarTeamLeaderMasivo", connection);               

                comandoInsert.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                //comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

        public void ExportExcel(List<ErroresFormatoVM> erroresFormatoCargue)
        {

            var archivoErroresCargue = ValidarArchivoExistenteCarpeta();

            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

            ExcelPackage pck = new ExcelPackage(archivoErroresCargue);
            try
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ErroresCargue" + DateTime.Now);

                ws.Cells["A1"].Value = "Id";
                ws.Cells["B1"].Value = "TipoError";

                int loop = 1;

                foreach (var item in erroresFormatoCargue)
                {
                    loop++;

                    ws.Cells["A" + loop].Value = item.Identificacion;
                    ws.Cells["B" + loop].Value = item.TipoError;

                }
                ws.Cells["A:AZ"].AutoFitColumns();
                pck.Save();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public FileInfo ValidarArchivoExistenteCarpeta()
        {
            FileInfo newFile = new FileInfo(HttpContext.Current.Server.MapPath("~/ErroresFormato/ErroresFormatoCargueUsuarios.xlsx"));

            if (newFile.Exists)
            {
                newFile.Delete();
            }
            return newFile;
        }


    }
}