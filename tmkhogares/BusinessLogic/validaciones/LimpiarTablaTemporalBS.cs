﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using tmkhogares.Models;

namespace tmkhogares.BusinessLogic.validaciones
{
    public static class LimpiarTablaTemporalBS
    {

        private static Contexto db = new Contexto();

        public static void TruncarTablaTemporal(string Tabla)
        {

            try
            {
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comandoInsert = new SqlCommand("TRUNCATE TABLE " + Tabla, connection);

                comandoInsert.CommandType = CommandType.Text;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comandoInsert.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comandoInsert.ExecuteReader();

                connection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

    }
}