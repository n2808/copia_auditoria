﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using tmkhogares.Models;
using tmkhogares.ViewModel;

namespace tmkhogares.BusinessLogic.validaciones
{
    public static class ValidarCamposVaciosBS
    {

        private static Contexto db = new Contexto();


        public static List<ErroresFormatoVM> ValidarCamposVaciosCargueUsuarios(string StoreProcedure)
        {
            try
            {

                List<ErroresFormatoVM> errores = new List<ErroresFormatoVM>();

                int count = 0;
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comando = new SqlCommand(StoreProcedure, connection);

                comando.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comando.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comando.ExecuteReader();

                while (reader.Read())
                {
                    //count = int.Parse(reader[0].ToString());
                    errores.Add(new ErroresFormatoVM { Identificacion = reader[0].ToString(), TipoError = reader[1].ToString() });
                }

                connection.Close();


                return errores;
                //return count;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

        public static int ValidarCamposVacios(string StoreProcedure)
        {
            try
            {

                List<ErroresFormatoVM> errores = new List<ErroresFormatoVM>();

                int count = 0;
                SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

                SqlCommand comando = new SqlCommand(StoreProcedure, connection);

                comando.CommandType = CommandType.StoredProcedure;

                // Add the input parameter and set its properties.
                SqlParameter parameter = new SqlParameter();

                comando.Parameters.Clear();

                connection.Open();
                SqlDataReader reader = comando.ExecuteReader();

                while (reader.Read())
                {
                    count = int.Parse(reader[0].ToString());

                }

                connection.Close();

                return count;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
            }
        }

    }
}